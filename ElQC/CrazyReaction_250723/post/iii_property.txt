-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -946.9792455169
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 1
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1067
     Surface Area:         795.3863294389
     Dielectric Energy:     -0.0075518384
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 17
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 17
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.0772     6.0000    -0.0772     3.9416     3.9416    -0.0000
  1   0     6.1860     6.0000    -0.1860     4.0104     4.0104     0.0000
  2   0     6.1417     6.0000    -0.1417     3.9761     3.9761     0.0000
  3   0     5.7297     6.0000     0.2703     3.7293     3.7293     0.0000
  4   0     6.1747     6.0000    -0.1747     4.0002     4.0002     0.0000
  5   0     6.0986     6.0000    -0.0986     3.9615     3.9615     0.0000
  6   0     0.8724     1.0000     0.1276     1.0080     1.0080    -0.0000
  7   0     5.4264     6.0000     0.5736     4.2000     4.2000    -0.0000
  8   0     0.8745     1.0000     0.1255     0.9962     0.9962    -0.0000
  9   0     0.8751     1.0000     0.1249     0.9971     0.9971     0.0000
 10   0     0.8895     1.0000     0.1105     1.0196     1.0196     0.0000
 11   0     0.8694     1.0000     0.1306     0.9952     0.9952     0.0000
 12   0     7.3677     7.0000    -0.3677     3.2066     3.2066    -0.0000
 13   0     8.4217     8.0000    -0.4217     2.1145     2.1145    -0.0000
 14   0     8.4914     8.0000    -0.4914     1.9300     1.9300    -0.0000
 15   0    15.0247    16.0000     0.9753     3.6812     3.6812     0.0000
 16   0     8.4795     8.0000    -0.4795     1.9349     1.9349    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.476435
                0             6               2            6                1.449282
                0             6               8            1                0.975582
                1             6               3            6                1.426386
                1             6               6            1                0.971461
                2             6               5            6                1.445085
                2             6               9            1                0.977362
                3             6               4            6                1.392320
                3             6              12            7                0.928318
                4             6               5            6                1.485583
                4             6              10            1                0.977565
                5             6              11            1                0.969153
                7             6              12            7                2.164533
                7             6              13            8                1.963525
               12             7              13            8                0.117203
               14             8              15           16                1.825933
               15            16              16            8                1.837917
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 1
   prop. index: 1
        Total number of Electrons                                                     94
        Number of correlated Electrons                                                62
        Number of alpha correlated Electrons                                          31
        Number of beta correlated Electrons                                           31
        Reference Energy:                                                -946.9792455170
        Total Correlation Energy:                                          -2.2926678155
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1182786971
        Total MDCI Energy:                                               -949.2719133325
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 1
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       17
     number of electrons:                   94
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             464
     number of aux C basis functions:       1109
     number of aux J basis functions:       5138
     number of aux JK basis functions:      5138
     number of aux CABS basis functions:    0
     Total Energy                           -949.271913
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 1
   prop. index: 1
       Filename                          : iii.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        1.1174578089
        Electronic Contribution:
                  0    
      0       9.801646
      1       9.804767
      2      -3.612194
        Nuclear Contribution:
                  0    
      0     -10.128129
      1      -9.682427
      2       3.879997
        Total Dipole moment:
                  0    
      0      -0.326483
      1       0.122340
      2       0.267804
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     1 
    Coordinates:
               0 C      0.669504798765   -1.415214852088    0.048939733976
               1 C      1.403293934984   -0.815692947197   -0.968740824783
               2 C      0.666479699676   -0.871708284401    1.330939966701
               3 C      2.138472101656    0.338295450627   -0.695969266831
               4 C      2.142861103348    0.892194805890    0.588150990513
               5 C      1.404763497163    0.281007738484    1.593852977390
               6 H      1.413697302309   -1.230070222676   -1.971796381776
               7 C      3.578345136500    1.868551940173   -1.930725207102
               8 H      0.097220696717   -2.313331008018   -0.164256547390
               9 H      0.092490410253   -1.343404042225    2.122683910477
              10 H      2.720486163350    1.790318601850    0.788412923672
              11 H      1.408652043422    0.711028160336    2.591327576170
              12 N      2.867801093569    0.917930473825   -1.740389854773
              13 O      4.282304807587    2.754062311389   -2.255236836892
              14 O      4.211807930689    3.973656445063    0.829646022666
              15 S      5.563113684020    3.932188402336    0.245581567023
              16 O      6.334205593992    2.695086026630    0.440481253957
