-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -947.0179730219
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 1
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1067
     Surface Area:         795.3863294389
     Dielectric Energy:     -0.0074307835
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 17
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 16
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     5.9868     6.0000     0.0132     3.8410     3.8410     0.0000
  1   0     6.0125     6.0000    -0.0125     3.8231     3.8231    -0.0000
  2   0     6.0409     6.0000    -0.0409     3.8773     3.8773     0.0000
  3   0     5.8300     6.0000     0.1700     3.7090     3.7090    -0.0000
  4   0     5.9933     6.0000     0.0067     3.8072     3.8072    -0.0000
  5   0     6.0023     6.0000    -0.0023     3.8355     3.8355    -0.0000
  6   0     0.9802     1.0000     0.0198     1.0074     1.0074     0.0000
  7   0     5.5472     6.0000     0.4528     4.2331     4.2331     0.0000
  8   0     0.9808     1.0000     0.0192     0.9942     0.9942     0.0000
  9   0     0.9823     1.0000     0.0177     0.9965     0.9965     0.0000
 10   0     0.9985     1.0000     0.0015     1.0233     1.0233     0.0000
 11   0     0.9760     1.0000     0.0240     0.9933     0.9933    -0.0000
 12   0     7.2982     7.0000    -0.2982     3.1541     3.1541    -0.0000
 13   0     8.3711     8.0000    -0.3711     2.1424     2.1424     0.0000
 14   0     8.4741     8.0000    -0.4741     1.9962     1.9962     0.0000
 15   0    15.0660    16.0000     0.9340     3.8457     3.8457    -0.0000
 16   0     8.4598     8.0000    -0.4598     2.0093     2.0093     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.389888
                0             6               2            6                1.392456
                0             6               8            1                0.998255
                1             6               3            6                1.372515
                1             6               6            1                0.994037
                2             6               5            6                1.385005
                2             6               9            1                0.998883
                3             6               4            6                1.325345
                3             6              12            7                0.954608
                4             6               5            6                1.391753
                4             6              10            1                1.007799
                5             6              11            1                0.997897
                7             6              12            7                2.156105
                7             6              13            8                2.010007
               14             8              15           16                1.902852
               15            16              16            8                1.923099
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 1
   prop. index: 1
        Total number of Electrons                                                     94
        Number of correlated Electrons                                                62
        Number of alpha correlated Electrons                                          31
        Number of beta correlated Electrons                                           31
        Reference Energy:                                                -947.0179730323
        Total Correlation Energy:                                          -2.4305712192
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1264734734
        Total MDCI Energy:                                               -949.4485442515
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 1
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       17
     number of electrons:                   94
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             863
     number of aux C basis functions:       1998
     number of aux J basis functions:       8822
     number of aux JK basis functions:      8822
     number of aux CABS basis functions:    0
     Total Energy                           -949.448544
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 1
   prop. index: 1
       Filename                          : ii.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        1.1218178216
        Electronic Contribution:
                  0    
      0       9.791348
      1       9.792032
      2      -3.616647
        Nuclear Contribution:
                  0    
      0     -10.128129
      1      -9.682427
      2       3.879997
        Total Dipole moment:
                  0    
      0      -0.336781
      1       0.109605
      2       0.263351
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     1 
    Coordinates:
               0 C      0.669504798765   -1.415214852088    0.048939733976
               1 C      1.403293934984   -0.815692947197   -0.968740824783
               2 C      0.666479699676   -0.871708284401    1.330939966701
               3 C      2.138472101656    0.338295450627   -0.695969266831
               4 C      2.142861103348    0.892194805890    0.588150990513
               5 C      1.404763497163    0.281007738484    1.593852977390
               6 H      1.413697302309   -1.230070222676   -1.971796381776
               7 C      3.578345136500    1.868551940173   -1.930725207102
               8 H      0.097220696717   -2.313331008018   -0.164256547390
               9 H      0.092490410253   -1.343404042225    2.122683910477
              10 H      2.720486163350    1.790318601850    0.788412923672
              11 H      1.408652043422    0.711028160336    2.591327576170
              12 N      2.867801093569    0.917930473825   -1.740389854773
              13 O      4.282304807587    2.754062311389   -2.255236836892
              14 O      4.211807930689    3.973656445063    0.829646022666
              15 S      5.563113684020    3.932188402336    0.245581567023
              16 O      6.334205593992    2.695086026630    0.440481253957
