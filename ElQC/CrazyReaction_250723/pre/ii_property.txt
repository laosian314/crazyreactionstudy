-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -946.9064924583
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 1
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1067
     Surface Area:         770.2256958174
     Dielectric Energy:     -0.0097472455
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 17
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 17
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.0073     6.0000    -0.0073     3.8556     3.8556    -0.0000
  1   0     6.0160     6.0000    -0.0160     3.8313     3.8313    -0.0000
  2   0     5.9862     6.0000     0.0138     3.8695     3.8695    -0.0000
  3   0     5.8601     6.0000     0.1399     3.7089     3.7089    -0.0000
  4   0     5.9901     6.0000     0.0099     3.7941     3.7941     0.0000
  5   0     6.0152     6.0000    -0.0152     3.8424     3.8424    -0.0000
  6   0     0.9778     1.0000     0.0222     1.0000     1.0000    -0.0000
  7   0     5.8898     6.0000     0.1102     3.6537     3.6537    -0.0000
  8   0     0.9754     1.0000     0.0246     0.9975     0.9975    -0.0000
  9   0     0.9720     1.0000     0.0280     0.9945     0.9945    -0.0000
 10   0     0.9684     1.0000     0.0316     1.0057     1.0057     0.0000
 11   0     0.9710     1.0000     0.0290     0.9963     0.9963    -0.0000
 12   0     6.8381     7.0000     0.1619     3.8271     3.8271     0.0000
 13   0     8.4837     8.0000    -0.4837     1.6541     1.6541    -0.0000
 14   0     8.5105     8.0000    -0.5105     1.9565     1.9565     0.0000
 15   0    15.0583    16.0000     0.9417     3.8611     3.8611     0.0000
 16   0     8.4800     8.0000    -0.4800     1.9840     1.9840     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.402961
                0             6               2            6                1.379187
                0             6               8            1                0.998137
                1             6               3            6                1.326747
                1             6               6            1                0.997692
                2             6               5            6                1.378980
                2             6               9            1                0.996441
                3             6               4            6                1.312228
                3             6               7            6                1.034087
                4             6               5            6                1.394087
                4             6              10            1                0.997951
                5             6              11            1                0.997999
                7             6              12            7                2.432899
                7             6              13            8                0.184970
               12             7              13            8                1.359170
               14             8              15           16                1.872076
               15            16              16            8                1.906023
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 1
   prop. index: 1
        Total number of Electrons                                                     94
        Number of correlated Electrons                                                62
        Number of alpha correlated Electrons                                          31
        Number of beta correlated Electrons                                           31
        Reference Energy:                                                -946.9064924508
        Total Correlation Energy:                                          -2.4499487022
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1296507845
        Total MDCI Energy:                                               -949.3564411530
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 1
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       17
     number of electrons:                   94
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             863
     number of aux C basis functions:       1998
     number of aux J basis functions:       8822
     number of aux JK basis functions:      8822
     number of aux CABS basis functions:    0
     Total Energy                           -949.356441
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 1
   prop. index: 1
       Filename                          : ii.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        6.3545142449
        Electronic Contribution:
                  0    
      0      10.336041
      1       8.420112
      2       0.817659
        Nuclear Contribution:
                  0    
      0     -12.357920
      1      -9.765427
      2      -0.224217
        Total Dipole moment:
                  0    
      0      -2.021878
      1      -1.345315
      2       0.593442
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     1 
    Coordinates:
               0 C     -0.088055453945    0.068068884386   -0.213306208747
               1 C      0.699727070840    1.202382792647   -0.347377865779
               2 C      0.433092607805   -1.086779258414    0.367451325249
               3 C      2.028705526402    1.177035326332    0.106333633944
               4 C      2.556215459620    0.013988548638    0.692398932620
               5 C      1.751902704105   -1.109533379650    0.817820136705
               6 H      0.302188327983    2.105681768476   -0.798499106754
               7 C      2.850331572034    2.323203283910   -0.021942412809
               8 H     -1.115377507958    0.086006762427   -0.564450383849
               9 H     -0.189779105615   -1.970461205928    0.469107423296
              10 H      3.584246428022    0.010463533047    1.039470076012
              11 H      2.158150051279   -2.008986132165    1.270536843522
              12 N      3.527095576708    3.258622603300   -0.136622167403
              13 O      4.284870366258    4.208756984134   -0.222258758998
              14 O      5.598067531069    2.005339406671    1.463789406842
              15 S      6.438625432518    3.083806747590    0.899527211511
              16 O      7.188593412876    2.748903334599   -0.324578085361
