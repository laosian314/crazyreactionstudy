-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -946.8675216755
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 1
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1067
     Surface Area:         770.2256958174
     Dielectric Energy:     -0.0098594077
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 17
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 17
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.1433     6.0000    -0.1433     3.9712     3.9712    -0.0000
  1   0     6.1580     6.0000    -0.1580     4.0024     4.0024     0.0000
  2   0     6.0694     6.0000    -0.0694     3.9542     3.9542    -0.0000
  3   0     5.9156     6.0000     0.0844     3.6251     3.6251    -0.0000
  4   0     6.1516     6.0000    -0.1516     3.9879     3.9879    -0.0000
  5   0     6.1352     6.0000    -0.1352     3.9627     3.9627    -0.0000
  6   0     0.8629     1.0000     0.1371     1.0027     1.0027     0.0000
  7   0     5.8041     6.0000     0.1959     3.7485     3.7485    -0.0000
  8   0     0.8685     1.0000     0.1315     0.9988     0.9988    -0.0000
  9   0     0.8663     1.0000     0.1337     0.9944     0.9944    -0.0000
 10   0     0.8607     1.0000     0.1393     1.0174     1.0174    -0.0000
 11   0     0.8664     1.0000     0.1336     0.9983     0.9983    -0.0000
 12   0     6.6595     7.0000     0.3405     3.8740     3.8740     0.0000
 13   0     8.5984     8.0000    -0.5984     1.5220     1.5220    -0.0000
 14   0     8.5197     8.0000    -0.5197     1.8917     1.8917    -0.0000
 15   0    15.0192    16.0000     0.9808     3.6858     3.6858     0.0000
 16   0     8.5013     8.0000    -0.5013     1.9044     1.9044    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.504487
                0             6               2            6                1.438841
                0             6               8            1                0.973774
                1             6               3            6                1.382373
                1             6               6            1                0.969444
                2             6               5            6                1.438600
                2             6               9            1                0.973191
                3             6               4            6                1.392975
                3             6               7            6                0.914590
                4             6               5            6                1.497245
                4             6              10            1                0.968673
                5             6              11            1                0.972996
                7             6              12            7                2.654742
                7             6              13            8                0.238873
               12             7              13            8                1.179595
               14             8              15           16                1.794379
               15            16              16            8                1.816380
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 1
   prop. index: 1
        Total number of Electrons                                                     94
        Number of correlated Electrons                                                62
        Number of alpha correlated Electrons                                          31
        Number of beta correlated Electrons                                           31
        Reference Energy:                                                -946.8675216868
        Total Correlation Energy:                                          -2.3116980520
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1214410033
        Total MDCI Energy:                                               -949.1792197388
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 1
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       17
     number of electrons:                   94
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             464
     number of aux C basis functions:       1109
     number of aux J basis functions:       5138
     number of aux JK basis functions:      5138
     number of aux CABS basis functions:    0
     Total Energy                           -949.179220
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 1
   prop. index: 1
       Filename                          : iii.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        6.3100398879
        Electronic Contribution:
                  0    
      0      10.357043
      1       8.425820
      2       0.828209
        Nuclear Contribution:
                  0    
      0     -12.357920
      1      -9.765427
      2      -0.224217
        Total Dipole moment:
                  0    
      0      -2.000876
      1      -1.339606
      2       0.603992
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     1 
    Coordinates:
               0 C     -0.088055453945    0.068068884386   -0.213306208747
               1 C      0.699727070840    1.202382792647   -0.347377865779
               2 C      0.433092607805   -1.086779258414    0.367451325249
               3 C      2.028705526402    1.177035326332    0.106333633944
               4 C      2.556215459620    0.013988548638    0.692398932620
               5 C      1.751902704105   -1.109533379650    0.817820136705
               6 H      0.302188327983    2.105681768476   -0.798499106754
               7 C      2.850331572034    2.323203283910   -0.021942412809
               8 H     -1.115377507958    0.086006762427   -0.564450383849
               9 H     -0.189779105615   -1.970461205928    0.469107423296
              10 H      3.584246428022    0.010463533047    1.039470076012
              11 H      2.158150051279   -2.008986132165    1.270536843522
              12 N      3.527095576708    3.258622603300   -0.136622167403
              13 O      4.284870366258    4.208756984134   -0.222258758998
              14 O      5.598067531069    2.005339406671    1.463789406842
              15 S      6.438625432518    3.083806747590    0.899527211511
              16 O      7.188593412876    2.748903334599   -0.324578085361
