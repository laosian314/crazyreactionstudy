-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -397.6752509969
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 1
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                843
     Surface Area:         579.1005067033
     Dielectric Energy:     -0.0085175130
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 14
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 15
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.0062     6.0000    -0.0062     3.8528     3.8528    -0.0000
  1   0     6.0113     6.0000    -0.0113     3.8211     3.8211    -0.0000
  2   0     5.9888     6.0000     0.0112     3.8646     3.8646    -0.0000
  3   0     5.8611     6.0000     0.1389     3.7388     3.7388    -0.0000
  4   0     6.0116     6.0000    -0.0116     3.8208     3.8208    -0.0000
  5   0     6.0064     6.0000    -0.0064     3.8531     3.8531    -0.0000
  6   0     0.9770     1.0000     0.0230     0.9981     0.9981     0.0000
  7   0     5.9392     6.0000     0.0608     3.6555     3.6555     0.0000
  8   0     0.9757     1.0000     0.0243     0.9976     0.9976    -0.0000
  9   0     0.9730     1.0000     0.0270     0.9955     0.9955    -0.0000
 10   0     0.9763     1.0000     0.0237     0.9979     0.9979    -0.0000
 11   0     0.9756     1.0000     0.0244     0.9975     0.9975    -0.0000
 12   0     6.8261     7.0000     0.1739     3.8990     3.8990    -0.0000
 13   0     8.4716     8.0000    -0.4716     1.6842     1.6842    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.400296
                0             6               2            6                1.379173
                0             6               8            1                0.997967
                1             6               3            6                1.323197
                1             6               6            1                0.998356
                2             6               5            6                1.379142
                2             6               9            1                0.996846
                3             6               4            6                1.323328
                3             6               7            6                1.040383
                4             6               5            6                1.400533
                4             6              10            1                0.998038
                5             6              11            1                0.997929
                7             6              12            7                2.425760
                7             6              13            8                0.211784
               12             7              13            8                1.432187
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 1
   prop. index: 1
        Total number of Electrons                                                     62
        Number of correlated Electrons                                                44
        Number of alpha correlated Electrons                                          22
        Number of beta correlated Electrons                                           22
        Reference Energy:                                                -397.6752510024
        Total Correlation Energy:                                          -1.7048397346
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.0925467717
        Total MDCI Energy:                                               -399.3800907371
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 1
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       14
     number of electrons:                   62
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             672
     number of aux C basis functions:       1549
     number of aux J basis functions:       6760
     number of aux JK basis functions:      6760
     number of aux CABS basis functions:    0
     Total Energy                           -399.380091
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 1
   prop. index: 1
       Filename                          : i.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        7.0960098612
        Electronic Contribution:
                  0    
      0       3.857143
      1       4.782604
      2       0.275681
        Nuclear Contribution:
                  0    
      0      -5.607997
      1      -6.953518
      2      -0.399747
        Total Dipole moment:
                  0    
      0      -1.750854
      1      -2.170914
      2      -0.124067
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    14 
    Geometry Index:     1 
    Coordinates:
               0 C     -0.068393608799   -0.062884915609    0.354178782328
               1 C      0.794489374671    1.022222801032    0.415874495799
               2 C      0.429061025183   -1.363651485835    0.408976045237
               3 C      2.178524134986    0.806596906432    0.534356806379
               4 C      2.678111759430   -0.506218797850    0.589554512339
               5 C      1.800912788756   -1.579717900186    0.526497859066
               6 H      0.413054901805    2.037315668563    0.373590898683
               7 C      3.067024197556    1.908633041952    0.597661339508
               8 H     -1.136856762408    0.108458656288    0.262566420042
               9 H     -0.251782988322   -2.208178889194    0.360199699752
              10 H      3.747262272802   -0.667995706737    0.681112210845
              11 H      2.191314049528   -2.592190574077    0.569521833120
              12 N      3.797374492328    2.812935305671    0.649184527325
              13 O      4.556203362484    3.752974889550    0.702725569579
