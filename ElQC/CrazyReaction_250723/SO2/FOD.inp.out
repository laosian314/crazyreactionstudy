
                                 *****************
                                 * O   R   C   A *
                                 *****************

                                            #,                                       
                                            ###                                      
                                            ####                                     
                                            #####                                    
                                            ######                                   
                                           ########,                                 
                                     ,,################,,,,,                         
                               ,,#################################,,                 
                          ,,##########################################,,             
                       ,#########################################, ''#####,          
                    ,#############################################,,   '####,        
                  ,##################################################,,,,####,       
                ,###########''''           ''''###############################       
              ,#####''   ,,,,##########,,,,          '''####'''          '####       
            ,##' ,,,,###########################,,,                        '##       
           ' ,,###''''                  '''############,,,                           
         ,,##''                                '''############,,,,        ,,,,,,###''
      ,#''                                            '''#######################'''  
     '                                                          ''''####''''         
             ,#######,   #######,   ,#######,      ##                                
            ,#'     '#,  ##    ##  ,#'     '#,    #''#        ######   ,####,        
            ##       ##  ##   ,#'  ##            #'  '#       #        #'  '#        
            ##       ##  #######   ##           ,######,      #####,   #    #        
            '#,     ,#'  ##    ##  '#,     ,#' ,#      #,         ##   #,  ,#        
             '#######'   ##     ##  '#######'  #'      '#     #####' # '####'        



                  #######################################################
                  #                        -***-                        #
                  #          Department of theory and spectroscopy      #
                  #    Directorship and core code : Frank Neese         #
                  #        Max Planck Institute fuer Kohlenforschung    #
                  #                Kaiser Wilhelm Platz 1               #
                  #                 D-45470 Muelheim/Ruhr               #
                  #                      Germany                        #
                  #                                                     #
                  #                  All rights reserved                #
                  #                        -***-                        #
                  #######################################################


                         Program Version 5.0.4 -  RELEASE  -


 With contributions from (in alphabetic order):
   Daniel Aravena         : Magnetic Suceptibility
   Michael Atanasov       : Ab Initio Ligand Field Theory (pilot matlab implementation)
   Alexander A. Auer      : GIAO ZORA, VPT2 properties, NMR spectrum
   Ute Becker             : Parallelization
   Giovanni Bistoni       : ED, misc. LED, open-shell LED, HFLD
   Martin Brehm           : Molecular dynamics
   Dmytro Bykov           : SCF Hessian
   Vijay G. Chilkuri      : MRCI spin determinant printing, contributions to CSF-ICE
   Dipayan Datta          : RHF DLPNO-CCSD density
   Achintya Kumar Dutta   : EOM-CC, STEOM-CC
   Dmitry Ganyushin       : Spin-Orbit,Spin-Spin,Magnetic field MRCI
   Miquel Garcia          : C-PCM and meta-GGA Hessian, CC/C-PCM, Gaussian charge scheme
   Yang Guo               : DLPNO-NEVPT2, F12-NEVPT2, CIM, IAO-localization
   Andreas Hansen         : Spin unrestricted coupled pair/coupled cluster methods
   Benjamin Helmich-Paris : MC-RPA, TRAH-SCF, COSX integrals
   Lee Huntington         : MR-EOM, pCC
   Robert Izsak           : Overlap fitted RIJCOSX, COSX-SCS-MP3, EOM
   Marcus Kettner         : VPT2
   Christian Kollmar      : KDIIS, OOCD, Brueckner-CCSD(T), CCSD density, CASPT2, CASPT2-K
   Simone Kossmann        : Meta GGA functionals, TD-DFT gradient, OOMP2, MP2 Hessian
   Martin Krupicka        : Initial AUTO-CI
   Lucas Lang             : DCDCAS
   Marvin Lechner         : AUTO-CI (C++ implementation), FIC-MRCC
   Dagmar Lenk            : GEPOL surface, SMD
   Dimitrios Liakos       : Extrapolation schemes; Compound Job, initial MDCI parallelization
   Dimitrios Manganas     : Further ROCIS development; embedding schemes
   Dimitrios Pantazis     : SARC Basis sets
   Anastasios Papadopoulos: AUTO-CI, single reference methods and gradients
   Taras Petrenko         : DFT Hessian,TD-DFT gradient, ASA, ECA, R-Raman, ABS, FL, XAS/XES, NRVS
   Peter Pinski           : DLPNO-MP2, DLPNO-MP2 Gradient
   Christoph Reimann      : Effective Core Potentials
   Marius Retegan         : Local ZFS, SOC
   Christoph Riplinger    : Optimizer, TS searches, QM/MM, DLPNO-CCSD(T), (RO)-DLPNO pert. Triples
   Tobias Risthaus        : Range-separated hybrids, TD-DFT gradient, RPA, STAB
   Michael Roemelt        : Original ROCIS implementation
   Masaaki Saitow         : Open-shell DLPNO-CCSD energy and density
   Barbara Sandhoefer     : DKH picture change effects
   Avijit Sen             : IP-ROCIS
   Kantharuban Sivalingam : CASSCF convergence, NEVPT2, FIC-MRCI
   Bernardo de Souza      : ESD, SOC TD-DFT
   Georgi Stoychev        : AutoAux, RI-MP2 NMR, DLPNO-MP2 response
   Willem Van den Heuvel  : Paramagnetic NMR
   Boris Wezisla          : Elementary symmetry handling
   Frank Wennmohs         : Technical directorship


 We gratefully acknowledge several colleagues who have allowed us to
 interface, adapt or use parts of their codes:
   Stefan Grimme, W. Hujo, H. Kruse, P. Pracht,  : VdW corrections, initial TS optimization,
                  C. Bannwarth, S. Ehlert          DFT functionals, gCP, sTDA/sTD-DF
   Ed Valeev, F. Pavosevic, A. Kumar             : LibInt (2-el integral package), F12 methods
   Garnet Chan, S. Sharma, J. Yang, R. Olivares  : DMRG
   Ulf Ekstrom                                   : XCFun DFT Library
   Mihaly Kallay                                 : mrcc  (arbitrary order and MRCC methods)
   Jiri Pittner, Ondrej Demel                    : Mk-CCSD
   Frank Weinhold                                : gennbo (NPA and NBO analysis)
   Christopher J. Cramer and Donald G. Truhlar   : smd solvation model
   Lars Goerigk                                  : TD-DFT with DH, B97 family of functionals
   V. Asgeirsson, H. Jonsson                     : NEB implementation
   FAccTs GmbH                                   : IRC, NEB, NEB-TS, DLPNO-Multilevel, CI-OPT
                                                   MM, QMMM, 2- and 3-layer-ONIOM, Crystal-QMMM,
                                                   LR-CPCM, SF, NACMEs, symmetry and pop. for TD-DFT,
                                                   nearIR, NL-DFT gradient (VV10), updates on ESD,
                                                   ML-optimized integration grids
   S Lehtola, MJT Oliveira, MAL Marques          : LibXC Library
   Liviu Ungur et al                             : ANISO software


 Your calculation uses the libint2 library for the computation of 2-el integrals
 For citations please refer to: http://libint.valeyev.net

 Your ORCA version has been built with support for libXC version: 5.1.0
 For citations please refer to: https://tddft.org/programs/libxc/

 This ORCA versions uses:
   CBLAS   interface :  Fast vector & matrix operations
   LAPACKE interface :  Fast linear algebra routines
   SCALAPACK package :  Parallel linear algebra routines
   Shared memory     :  Shared parallel matrices
   BLAS/LAPACK       :  OpenBLAS 0.3.15  USE64BITINT DYNAMIC_ARCH NO_AFFINITY Zen SINGLE_THREADED
        Core in use  :  Zen
   Copyright (c) 2011-2014, The OpenBLAS Project




***************************************
The coordinates will be read from file: opt.xyz
***************************************


================================================================================

----- Orbital basis set information -----
Your calculation utilizes the basis: def2-TZVP
   F. Weigend and R. Ahlrichs, Phys. Chem. Chem. Phys. 7, 3297 (2005).

----- AuxJ basis set information -----
Your calculation utilizes the auxiliary basis: def2/J
   F. Weigend, Phys. Chem. Chem. Phys. 8, 1057 (2006).

================================================================================
                                        WARNINGS
                       Please study these warnings very carefully!
================================================================================


INFO   : the flag for use of the SHARK integral package has been found!


WARNING: TRAH does not support fractional occupations!
  ===> : Turning TRAH off
================================================================================
                                       INPUT FILE
================================================================================
NAME = FOD.inp
|  1> ! FOD
|  2> 
|  3> * xyzfile 0 1 opt.xyz
|  4> 
|  5>                          ****END OF INPUT****
================================================================================

                       ****************************
                       * Single Point Calculation *
                       ****************************

---------------------------------
CARTESIAN COORDINATES (ANGSTROEM)
---------------------------------
  S      0.776227    0.642566    0.000000
  O      2.243739    0.542270    0.000000
  O      0.191813    1.992429    0.000000

----------------------------
CARTESIAN COORDINATES (A.U.)
----------------------------
  NO LB      ZA    FRAG     MASS         X           Y           Z
   0 S    16.0000    0    32.060    1.466857    1.214275    0.000000
   1 O     8.0000    0    15.999    4.240051    1.024741    0.000000
   2 O     8.0000    0    15.999    0.362475    3.765144    0.000000

--------------------------------
INTERNAL COORDINATES (ANGSTROEM)
--------------------------------
 S      0   0   0     0.000000000000     0.00000000     0.00000000
 O      1   0   0     1.470934773200     0.00000000     0.00000000
 O      1   2   0     1.470940969228   117.31967123     0.00000000

---------------------------
INTERNAL COORDINATES (A.U.)
---------------------------
 S      0   0   0     0.000000000000     0.00000000     0.00000000
 O      1   0   0     2.779663882209     0.00000000     0.00000000
 O      1   2   0     2.779675591005   117.31967123     0.00000000

---------------------
BASIS SET INFORMATION
---------------------
There are 2 groups of distinct atoms

 Group   1 Type S   : 14s9p3d1f contracted to 5s5p2d1f pattern {73211/51111/21/1}
 Group   2 Type O   : 11s6p2d1f contracted to 5s3p2d1f pattern {62111/411/11/1}

Atom   0S    basis set group =>   1
Atom   1O    basis set group =>   2
Atom   2O    basis set group =>   2
---------------------------------
AUXILIARY/J BASIS SET INFORMATION
---------------------------------
There are 2 groups of distinct atoms

 Group   1 Type S   : 14s5p5d2f1g contracted to 8s4p3d1f1g pattern {71111111/2111/311/2/1}
 Group   2 Type O   : 12s5p4d2f1g contracted to 6s4p3d1f1g pattern {711111/2111/211/2/1}

Atom   0S    basis set group =>   1
Atom   1O    basis set group =>   2
Atom   2O    basis set group =>   2
------------------------------------------------------------------------------
                           ORCA GTO INTEGRAL CALCULATION
                           -- RI-GTO INTEGRALS CHOSEN --
------------------------------------------------------------------------------
------------------------------------------------------------------------------
                   ___                                                        
                  /   \      - P O W E R E D   B Y -                         
                 /     \                                                     
                 |  |  |   _    _      __       _____    __    __             
                 |  |  |  | |  | |    /  \     |  _  \  |  |  /  |          
                  \  \/   | |  | |   /    \    | | | |  |  | /  /          
                 / \  \   | |__| |  /  /\  \   | |_| |  |  |/  /          
                |  |  |   |  __  | /  /__\  \  |    /   |      \           
                |  |  |   | |  | | |   __   |  |    \   |  |\   \          
                \     /   | |  | | |  |  |  |  | |\  \  |  | \   \       
                 \___/    |_|  |_| |__|  |__|  |_| \__\ |__|  \__/        
                                                                              
                      - O R C A' S   B I G   F R I E N D -                    
                                      &                                       
                       - I N T E G R A L  F E E D E R -                       
                                                                              
 v1 FN, 2020, v2 2021                                                         
------------------------------------------------------------------------------


Reading SHARK input file FOD.SHARKINP.tmp ... ok
----------------------
SHARK INTEGRAL PACKAGE
----------------------

Number of atoms                             ...      3
Number of basis functions                   ...     99
Number of shells                            ...     35
Maximum angular momentum                    ...      3
Integral batch strategy                     ... SHARK/LIBINT Hybrid
RI-J (if used) integral strategy            ... SPLIT-RIJ (Revised 2003 algorithm where possible)
Printlevel                                  ...      1
Contraction scheme used                     ... SEGMENTED contraction
Coulomb Range Separation                    ... NOT USED
Exchange Range Separation                   ... NOT USED
Finite Nucleus Model                        ... NOT USED
Auxiliary Coulomb fitting basis             ... AVAILABLE
   # of basis functions in Aux-J            ...    149
   # of shells in Aux-J                     ...     47
   Maximum angular momentum in Aux-J        ...      4
Auxiliary J/K fitting basis                 ... NOT available
Auxiliary Correlation fitting basis         ... NOT available
Auxiliary 'external' fitting basis          ... NOT available
Integral threshold                          ...     2.500000e-11
Primitive cut-off                           ...     2.500000e-12
Primitive pair pre-selection threshold      ...     2.500000e-12

Calculating pre-screening integrals         ... done (  0.0 sec) Dimension = 35
Organizing shell pair data                  ... done (  0.0 sec)
Shell pair information
Total number of shell pairs                 ...       630
Shell pairs after pre-screening             ...       612
Total number of primitive shell pairs       ...      2358
Primitive shell pairs kept                  ...      1889
          la=0 lb=0:    110 shell pairs
          la=1 lb=0:    161 shell pairs
          la=1 lb=1:     66 shell pairs
          la=2 lb=0:     86 shell pairs
          la=2 lb=1:     66 shell pairs
          la=2 lb=2:     21 shell pairs
          la=3 lb=0:     45 shell pairs
          la=3 lb=1:     33 shell pairs
          la=3 lb=2:     18 shell pairs
          la=3 lb=3:      6 shell pairs

Calculating one electron integrals          ... done (  0.0 sec)
Calculating RI/J V-Matrix + Cholesky decomp.... done (  0.0 sec)
Calculating Nuclear repulsion               ... done (  0.0 sec) ENN=    105.576061431211 Eh

SHARK setup successfully completed in   0.1 seconds

Maximum memory used throughout the entire GTOINT-calculation: 18.1 MB
-------------------------------------------------------------------------------
                                 ORCA SCF
-------------------------------------------------------------------------------

------------
SCF SETTINGS
------------
Hamiltonian:
 Density Functional     Method          .... DFT(GTOs)
 Exchange Functional    Exchange        .... TPSS
 Correlation Functional Correlation     .... TPSS
 LDA part of GGA corr.  LDAOpt          .... PW91-LDA
 Gradients option       PostSCFGGA      .... off
   Density functional embedding theory  .... OFF
   NL short-range parameter             ....  5.000000
 RI-approximation to the Coulomb term is turned on
   Number of AuxJ basis functions       .... 149


General Settings:
 Integral files         IntName         .... FOD
 Hartree-Fock type      HFTyp           .... RHF
 Total Charge           Charge          ....    0
 Multiplicity           Mult            ....    1
 Number of Electrons    NEL             ....   32
 Basis Dimension        Dim             ....   99
 Nuclear Repulsion      ENuc            ....    105.5760614312 Eh

Convergence Acceleration:
 DIIS                   CNVDIIS         .... on
   Start iteration      DIISMaxIt       ....    12
   Startup error        DIISStart       ....  0.200000
   # of expansion vecs  DIISMaxEq       ....     5
   Bias factor          DIISBfac        ....   1.050
   Max. coefficient     DIISMaxC        ....  10.000
 Trust-Rad. Augm. Hess. CNVTRAH         .... off
 SOSCF                  CNVSOSCF        .... on
   Start iteration      SOSCFMaxIt      ....   150
   Startup grad/error   SOSCFStart      ....  0.003300
 Level Shifting         CNVShift        .... on
   Level shift para.    LevelShift      ....    0.2500
   Turn off err/grad.   ShiftErr        ....    0.0010
 Zerner damping         CNVZerner       .... off
 Static damping         CNVDamp         .... on
   Fraction old density DampFac         ....    0.7000
   Max. Damping (<1)    DampMax         ....    0.9800
   Min. Damping (>=0)   DampMin         ....    0.0000
   Turn off err/grad.   DampErr         ....    0.1000
 Fernandez-Rico         CNVRico         .... off

SCF Procedure:
 Maximum # iterations   MaxIter         ....   125
 SCF integral mode      SCFMode         .... Direct
   Integral package                     .... SHARK and LIBINT hybrid scheme
 Reset frequency        DirectResetFreq ....    20
 Integral Threshold     Thresh          ....  2.500e-11 Eh
 Primitive CutOff       TCut            ....  2.500e-12 Eh

Convergence Tolerance:
 Convergence Check Mode ConvCheckMode   .... Total+1el-Energy
 Convergence forced     ConvForced      .... 0
 Energy Change          TolE            ....  1.000e-08 Eh
 1-El. energy change                    ....  1.000e-05 Eh
 Orbital Gradient       TolG            ....  1.000e-05
 Orbital Rotation angle TolX            ....  1.000e-05
 DIIS Error             TolErr          ....  5.000e-07


Diagonalization of the overlap matrix:
Smallest eigenvalue                        ... 2.135e-03
Time for diagonalization                   ...    0.001 sec
Threshold for overlap eigenvalues          ... 1.000e-08
Number of eigenvalues below threshold      ... 0
Time for construction of square roots      ...    0.000 sec
Total time needed                          ...    0.001 sec

Time for model grid setup =    0.018 sec

------------------------------
INITIAL GUESS: MODEL POTENTIAL
------------------------------
Loading Hartree-Fock densities                     ... done
Calculating cut-offs                               ... done
Initializing the effective Hamiltonian             ... done
Setting up the integral package (SHARK)            ... done
Starting the Coulomb interaction                   ... done (   0.0 sec)
Reading the grid                                   ... done
Mapping shells                                     ... done
Starting the XC term evaluation                    ... done (   0.0 sec)
  promolecular density results
     # of electrons  =     32.000056765
     EX              =    -41.192060305
     EC              =     -1.166083853
     EX+EC           =    -42.358144159
Transforming the Hamiltonian                       ... done (   0.0 sec)
Diagonalizing the Hamiltonian                      ... done (   0.0 sec)
Back transforming the eigenvectors                 ... done (   0.0 sec)
Now organizing SCF variables                       ... done
                      ------------------
                      INITIAL GUESS DONE (   0.0 sec)
                      ------------------
-------------------
DFT GRID GENERATION
-------------------

General Integration Accuracy     IntAcc      ... 4.388
Radial Grid Type                 RadialGrid  ... OptM3 with GC (2021)
Angular Grid (max. ang.)         AngularGrid ... 4 (Lebedev-302)
Angular grid pruning method      GridPruning ... 4 (adaptive)
Weight generation scheme         WeightScheme... Becke
Basis function cutoff            BFCut       ... 1.0000e-11
Integration weight cutoff        WCut        ... 1.0000e-14
Angular grids for H and He will be reduced by one unit
Partially contracted basis set               ... off
Rotationally invariant grid construction     ... off

Total number of grid points                  ...    19611
Total number of batches                      ...      308
Average number of points per batch           ...       63
Average number of grid points per atom       ...     6537
Time for grid setup =    0.090 sec


Warning: Fractional occupation numbers cannot be used with CNVRico or SOSCF

--------------
SCF ITERATIONS
--------------
ITER       Energy         Delta-E        Max-DP      RMS-DP      [F,P]     Damp
               ***  Starting incremental Fock matrix formation  ***
  0   -548.5405887934   0.000000000000 0.10814656  0.00330629  0.2829109 0.7000
  1   -548.6587686484  -0.118179855088 0.05291254  0.00146845  0.0773765 0.7000
                               ***Turning on DIIS***
  2   -548.6859303920  -0.027161743534 0.07038596  0.00127549  0.0331427 0.0000
  3   -548.7508989897  -0.064968597707 0.03980290  0.00088824  0.0237186 0.0000
  4   -548.7532223536  -0.002323363894 0.00910223  0.00026335  0.0084737 0.0000
  5   -548.7534968038  -0.000274450224 0.00314150  0.00009437  0.0041409 0.0000
  6   -548.7535437652  -0.000046961413 0.00149679  0.00005586  0.0009426 0.0000
  7   -548.7535401167   0.000003648512 0.00065318  0.00002223  0.0015513 0.0000
  8   -548.7535462901  -0.000006173360 0.00015020  0.00000676  0.0001420 0.0000
  9   -548.7535463802  -0.000000090108 0.00004790  0.00000139  0.0000718 0.0000
 10   -548.7535463943  -0.000000014119 0.00001017  0.00000037  0.0000116 0.0000
 11   -548.7535463950  -0.000000000734 0.00000591  0.00000020  0.0000096 0.0000
 12   -548.7535463968  -0.000000001781 0.00000473  0.00000011  0.0000047 0.0000
                 **** Energy Check signals convergence ****
Fermi smearing:E(HOMO(Eh)) = -0.297546 MUE = -0.229491 gap=   3.550 eV

N_FOD =  0.076202

               *****************************************************
               *                     SUCCESS                       *
               *           SCF CONVERGED AFTER  13 CYCLES          *
               *****************************************************


----------------
TOTAL SCF ENERGY
----------------

Total Energy       :         -548.75354639 Eh          -14932.34314 eV

Components:
Nuclear Repulsion  :          105.57606143 Eh            2872.87069 eV
Electronic Energy  :         -654.32960783 Eh          -17805.21383 eV
One Electron Energy:         -965.78972448 Eh          -26280.47448 eV
Two Electron Energy:          311.46011665 Eh            8475.26065 eV

Virial components:
Potential Energy   :        -1096.11843041 Eh          -29826.89886 eV
Kinetic Energy     :          547.36488401 Eh           14894.55572 eV
Virial Ratio       :            2.00253700


DFT components:
N(Alpha)           :       15.999997819091 electrons
N(Beta)            :       15.999997819091 electrons
N(Total)           :       31.999995638181 electrons
E(X)               :      -41.718649008274 Eh       
E(C)               :       -1.177828803064 Eh       
E(XC)              :      -42.896477811338 Eh       
DFET-embed. en.    :        0.000000000000 Eh       

---------------
SCF CONVERGENCE
---------------

  Last Energy change         ...    2.6355e-09  Tolerance :   1.0000e-08
  Last MAX-Density change    ...    1.1678e-06  Tolerance :   1.0000e-07
  Last RMS-Density change    ...    2.1523e-08  Tolerance :   5.0000e-09
  Last DIIS Error            ...    9.2762e-07  Tolerance :   5.0000e-07

             **** THE GBW FILE WAS UPDATED (FOD.gbw) ****
             **** DENSITY FOD.scfp WAS UPDATED ****
             **** ENERGY FILE WAS UPDATED (FOD.en.tmp) ****
             **** THE GBW FILE WAS UPDATED (FOD.gbw) ****
             **** DENSITY FOD.scfp WAS UPDATED ****
----------------
ORBITAL ENERGIES
----------------

  NO   OCC          E(Eh)            E(eV) 
   0   2.0000     -88.526254     -2408.9218 
   1   2.0000     -18.943634      -515.4825 
   2   2.0000     -18.943624      -515.4822 
   3   2.0000      -7.888500      -214.6570 
   4   2.0000      -5.868298      -159.6845 
   5   2.0000      -5.867975      -159.6757 
   6   2.0000      -5.861758      -159.5066 
   7   2.0000      -1.058701       -28.8087 
   8   2.0000      -0.973854       -26.4999 
   9   2.0000      -0.594832       -16.1862 
  10   2.0000      -0.445594       -12.1252 
  11   2.0000      -0.444015       -12.0822 
  12   2.0000      -0.439595       -11.9620 
  13   1.9977      -0.336927        -9.1682 
  14   1.9910      -0.314967        -8.5707 
  15   1.9732      -0.297546        -8.0966 
  16   0.0381      -0.167081        -4.5465 
  17   0.0000       0.002875         0.0782 
  18   0.0000       0.023137         0.6296 
  19   0.0000       0.208152         5.6641 
  20   0.0000       0.210587         5.7304 
  21   0.0000       0.255319         6.9476 
  22   0.0000       0.292846         7.9688 
  23   0.0000       0.302852         8.2410 
  24   0.0000       0.314101         8.5471 
  25   0.0000       0.347199         9.4478 
  26   0.0000       0.381710        10.3869 
  27   0.0000       0.386425        10.5152 
  28   0.0000       0.438322        11.9273 
  29   0.0000       0.456963        12.4346 
  30   0.0000       0.492314        13.3966 
  31   0.0000       0.525766        14.3068 
  32   0.0000       0.568101        15.4588 
  33   0.0000       0.662860        18.0373 
  34   0.0000       0.771593        20.9961 
  35   0.0000       0.803561        21.8660 
  36   0.0000       1.199504        32.6402 
  37   0.0000       1.233416        33.5630 
  38   0.0000       1.269168        34.5358 
  39   0.0000       1.271081        34.5879 
  40   0.0000       1.372213        37.3398 
  41   0.0000       1.386151        37.7191 
  42   0.0000       1.396288        37.9949 
  43   0.0000       1.430242        38.9189 
  44   0.0000       1.472668        40.0733 
  45   0.0000       1.474109        40.1126 
  46   0.0000       1.509578        41.0777 
  47   0.0000       1.565343        42.5952 
  48   0.0000       1.659883        45.1677 
  49   0.0000       1.697311        46.1862 
  50   0.0000       1.730832        47.0983 
  51   0.0000       1.746546        47.5259 
  52   0.0000       1.844426        50.1894 
  53   0.0000       1.997585        54.3570 
  54   0.0000       2.068132        56.2767 
  55   0.0000       2.088663        56.8354 
  56   0.0000       2.178795        59.2880 
  57   0.0000       2.193610        59.6912 
  58   0.0000       2.225120        60.5486 
  59   0.0000       2.316353        63.0312 
  60   0.0000       2.331617        63.4465 
  61   0.0000       2.678849        72.8952 
  62   0.0000       2.703876        73.5762 
  63   0.0000       2.715255        73.8858 
  64   0.0000       2.756813        75.0167 
  65   0.0000       2.783922        75.7544 
  66   0.0000       2.895092        78.7795 
  67   0.0000       3.282465        89.3204 
  68   0.0000       3.856856       104.9504 
  69   0.0000       5.059385       137.6729 
  70   0.0000       5.070327       137.9706 
  71   0.0000       5.077084       138.1545 
  72   0.0000       5.078123       138.1828 
  73   0.0000       5.081185       138.2661 
  74   0.0000       5.129979       139.5938 
  75   0.0000       5.147139       140.0608 
  76   0.0000       5.174815       140.8139 
  77   0.0000       5.216879       141.9585 
  78   0.0000       5.329843       145.0324 
  79   0.0000       5.338449       145.2666 
  80   0.0000       5.436732       147.9410 
  81   0.0000       5.501045       149.6910 
  82   0.0000       5.585852       151.9988 
  83   0.0000       6.129975       166.8051 
  84   0.0000       6.175690       168.0491 
  85   0.0000       6.184340       168.2844 
  86   0.0000       6.206086       168.8762 
  87   0.0000       6.527447       177.6209 
  88   0.0000       6.536151       177.8577 
  89   0.0000       6.583861       179.1560 
  90   0.0000       6.628130       180.3606 
  91   0.0000       6.728248       183.0849 
  92   0.0000       6.804503       185.1599 
  93   0.0000       8.600723       234.0376 
  94   0.0000       8.897682       242.1182 
  95   0.0000       9.412384       256.1240 
  96   0.0000      20.651043       561.9434 
  97   0.0000      43.050355      1171.4597 
  98   0.0000      43.262589      1177.2349 

                    ********************************
                    * MULLIKEN POPULATION ANALYSIS *
                    ********************************

------------------------------------------
FOD BASED MULLIKEN REDUCED ORBITAL CHARGES
------------------------------------------
  0 S s       :     0.003763  s :     0.003763
      pz      :     0.019479  p :     0.022111
      px      :     0.000971
      py      :     0.001661
      dz2     :     0.000287  d :     0.004570
      dxz     :     0.000609
      dyz     :     0.001034
      dx2y2   :     0.000645
      dxy     :     0.001995
      f0      :     0.000143  f :     0.000351
      f+1     :     0.000000
      f-1     :     0.000000
      f+2     :     0.000016
      f-2     :     0.000031
      f+3     :     0.000056
      f-3     :     0.000105
  1 O s       :    -0.000002  s :    -0.000002
      pz      :     0.009519  p :     0.022689
      px      :     0.003896
      py      :     0.009274
      dz2     :    -0.000000  d :     0.000015
      dxz     :     0.000004
      dyz     :     0.000001
      dx2y2   :     0.000002
      dxy     :     0.000008
      f0      :     0.000000  f :     0.000001
      f+1     :     0.000000
      f-1     :    -0.000000
      f+2     :    -0.000000
      f-2     :     0.000000
      f+3     :     0.000001
      f-3     :     0.000000
  2 O s       :    -0.000002  s :    -0.000002
      pz      :     0.009519  p :     0.022689
      px      :     0.006364
      py      :     0.006805
      dz2     :    -0.000000  d :     0.000015
      dxz     :    -0.000003
      dyz     :     0.000008
      dx2y2   :     0.000004
      dxy     :     0.000006
      f0      :     0.000000  f :     0.000001
      f+1     :     0.000000
      f-1     :     0.000000
      f+2     :     0.000000
      f-2     :    -0.000000
      f+3     :     0.000000
      f-3     :     0.000001


                     *******************************
                     * LOEWDIN POPULATION ANALYSIS *
                     *******************************

----------------------
LOEWDIN ATOMIC CHARGES
----------------------
   0 S :    0.522064
   1 O :   -0.261029
   2 O :   -0.261034

-------------------------------
LOEWDIN REDUCED ORBITAL CHARGES
-------------------------------
  0 S s       :     5.285115  s :     5.285115
      pz      :     2.954901  p :     8.984174
      px      :     2.950103
      py      :     3.079169
      dz2     :     0.105149  d :     1.026891
      dxz     :     0.162045
      dyz     :     0.093814
      dx2y2   :     0.318185
      dxy     :     0.347698
      f0      :     0.007274  f :     0.181757
      f+1     :     0.013119
      f-1     :     0.009869
      f+2     :     0.029954
      f-2     :     0.006101
      f+3     :     0.050984
      f-3     :     0.064456
  1 O s       :     3.643772  s :     3.643772
      pz      :     1.373516  p :     4.542875
      px      :     1.539735
      py      :     1.629624
      dz2     :     0.009526  d :     0.070200
      dxz     :     0.016104
      dyz     :     0.000201
      dx2y2   :     0.030325
      dxy     :     0.014043
      f0      :     0.000361  f :     0.004183
      f+1     :     0.000722
      f-1     :     0.000054
      f+2     :     0.000659
      f-2     :     0.000035
      f+3     :     0.001451
      f-3     :     0.000900
  2 O s       :     3.643777  s :     3.643777
      pz      :     1.373515  p :     4.542876
      px      :     1.596212
      py      :     1.573149
      dz2     :     0.009526  d :     0.070198
      dxz     :     0.002825
      dyz     :     0.013480
      dx2y2   :     0.024150
      dxy     :     0.020217
      f0      :     0.000362  f :     0.004183
      f+1     :     0.000158
      f-1     :     0.000618
      f+2     :     0.000302
      f-2     :     0.000392
      f+3     :     0.001313
      f-3     :     0.001038


                      *****************************
                      * MAYER POPULATION ANALYSIS *
                      *****************************

  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence

  ATOM       NA         ZA         QA         VA         BVA        FA
  0 S     15.3896    16.0000     0.6104     3.8255     3.7650     0.0605
  1 O      8.3052     8.0000    -0.3052     2.1179     2.0731     0.0448
  2 O      8.3052     8.0000    -0.3052     2.1179     2.0731     0.0448

  Mayer bond orders larger than 0.100000
B(  0-S ,  1-O ) :   1.8825 B(  0-S ,  2-O ) :   1.8825 B(  1-O ,  2-O ) :   0.1906 


-------
TIMINGS
-------

Total SCF time: 0 days 0 hours 0 min 2 sec 

Total time                  ....       2.276 sec
Sum of individual times     ....       2.241 sec  ( 98.5%)

Fock matrix formation       ....       2.087 sec  ( 91.7%)
  Split-RI-J                ....       0.261 sec  ( 12.5% of F)
  XC integration            ....       1.823 sec  ( 87.3% of F)
    Basis function eval.    ....       0.329 sec  ( 18.0% of XC)
    Density eval.           ....       0.481 sec  ( 26.4% of XC)
    XC-Functional eval.     ....       0.302 sec  ( 16.6% of XC)
    XC-Potential eval.      ....       0.687 sec  ( 37.7% of XC)
Diagonalization             ....       0.011 sec  (  0.5%)
Density matrix formation    ....       0.003 sec  (  0.1%)
Population analysis         ....       0.001 sec  (  0.0%)
Initial guess               ....       0.028 sec  (  1.2%)
Orbital Transformation      ....       0.000 sec  (  0.0%)
Orbital Orthonormalization  ....       0.000 sec  (  0.0%)
DIIS solution               ....       0.003 sec  (  0.1%)
Grid generation             ....       0.108 sec  (  4.7%)

Maximum memory used throughout the entire SCF-calculation: 24.4 MB

-------------------------   --------------------
FINAL SINGLE POINT ENERGY      -548.753546394175
-------------------------   --------------------


                            ***************************************
                            *     ORCA property calculations      *
                            ***************************************

                                    ---------------------
                                    Active property flags
                                    ---------------------
   (+) Dipole Moment


------------------------------------------------------------------------------
                       ORCA ELECTRIC PROPERTIES CALCULATION
------------------------------------------------------------------------------

Dipole Moment Calculation                       ... on
Quadrupole Moment Calculation                   ... off
Polarizability Calculation                      ... off
GBWName                                         ... FOD.gbw
Electron density                                ... FOD.scfp
The origin for moment calculation is the CENTER OF MASS  = ( 1.883656,  1.804037  0.000000)

-------------
DIPOLE MOMENT
-------------
                                X             Y             Z
Electronic contribution:     -0.39605      -0.56042      -0.00000
Nuclear contribution   :      0.01292       0.01828       0.00000
                        -----------------------------------------
Total Dipole Moment    :     -0.38313      -0.54214      -0.00000
                        -----------------------------------------
Magnitude (a.u.)       :      0.66385
Magnitude (Debye)      :      1.68737



--------------------
Rotational spectrum 
--------------------
 
Rotational constants in cm-1:     1.798422     0.333790     0.281537 
Rotational constants in MHz : 53915.329793 10006.779367  8440.253565 

 Dipole components along the rotational axes: 
x,y,z [a.u.] :    -0.000015    -0.663850    -0.000000 
x,y,z [Debye]:    -0.000037    -1.687372    -0.000000 

 

Timings for individual modules:

Sum of individual times         ...        2.366 sec (=   0.039 min)
GTO integral calculation        ...        0.086 sec (=   0.001 min)   3.6 %
SCF iterations                  ...        2.280 sec (=   0.038 min)  96.4 %
                             ****ORCA TERMINATED NORMALLY****
TOTAL RUN TIME: 0 days 0 hours 0 minutes 2 seconds 411 msec
