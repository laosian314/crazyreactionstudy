-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -948.6164986526
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                 46.9999934812 
   Number of Beta  Electrons                 46.9999934812 
   Total number of  Electrons                93.9999869624 
   Exchange energy                          -95.0505630235 
   Correlation energy                        -3.2640128199 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -98.3145758435 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -948.6164986526 
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 17
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 21
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.0938     6.0000    -0.0938     3.9200     3.9048     0.0152
  1   0     6.1469     6.0000    -0.1469     3.9905     3.9624     0.0281
  2   0     6.0756     6.0000    -0.0756     3.9260     3.8938     0.0322
  3   0     5.7799     6.0000     0.2201     3.7090     3.6769     0.0321
  4   0     6.1568     6.0000    -0.1568     3.9898     3.9611     0.0287
  5   0     6.0919     6.0000    -0.0919     3.9131     3.8983     0.0148
  6   0     0.8718     1.0000     0.1282     0.9724     0.9716     0.0008
  7   0     5.8150     6.0000     0.1850     4.2377     4.2193     0.0184
  8   0     0.8814     1.0000     0.1186     0.9648     0.9643     0.0005
  9   0     0.8809     1.0000     0.1191     0.9633     0.9630     0.0003
 10   0     0.8664     1.0000     0.1336     0.9715     0.9708     0.0007
 11   0     0.8802     1.0000     0.1198     0.9647     0.9642     0.0005
 12   0     7.0926     7.0000    -0.0926     2.6596     2.5136     0.1460
 13   0     8.3230     8.0000    -0.3230     2.1093     2.0714     0.0379
 14   0     8.3133     8.0000    -0.3133     2.1959     2.1330     0.0629
 15   0    15.3894    16.0000     0.6106     3.9681     3.9188     0.0493
 16   0     8.3411     8.0000    -0.3411     2.1113     2.0712     0.0401
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.423117
                0             6               2            6                1.379474
                0             6               8            1                0.966417
                1             6               3            6                1.312996
                1             6               6            1                0.968399
                2             6               5            6                1.374751
                2             6               9            1                0.963212
                3             6               4            6                1.311182
                3             6               7            6                0.762137
                3             6              12            7                0.257881
                4             6               5            6                1.422268
                4             6              10            1                0.967550
                5             6              11            1                0.966177
                7             6              12            7                1.717528
                7             6              14            8                1.574108
               12             7              13            8                0.256568
               12             7              14            8                0.116106
               13             8              15           16                1.591863
               13             8              16            8                0.103354
               14             8              15           16                0.367211
               15            16              16            8                1.858350
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 1
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       17
     number of electrons:                   0
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             408
     number of aux C basis functions:       0
     number of aux J basis functions:       645
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           -948.616499
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 1
   prop. index: 1
       Filename                          : FOD.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        5.3797624067
        Electronic Contribution:
                  0    
      0       9.473621
      1       8.267999
      2      -1.031047
        Nuclear Contribution:
                  0    
      0     -11.274239
      1      -9.187858
      2       1.656575
        Total Dipole moment:
                  0    
      0      -1.800618
      1      -0.919858
      2       0.625528
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     1 
    Coordinates:
               0 C     -2.418344497991   -0.438062056044    0.166963638218
               1 C     -1.426794432613    0.496390758016   -0.096744545223
               2 C     -2.074763102220   -1.765671194335    0.423495584721
               3 C     -0.093438094594    0.081567290693   -0.059812211615
               4 C      0.265031928255   -1.251688576645    0.157109839983
               5 C     -0.739318601851   -2.171946185681    0.418705989634
               6 H     -1.667461942364    1.531707336547   -0.315946963586
               7 C      1.020657600926    1.142224141412   -0.116579941679
               8 H     -3.460705453000   -0.135135348230    0.158489357933
               9 H     -2.856621972487   -2.495240387209    0.613470318073
              10 H      1.310267578812   -1.543709065635    0.133333578419
              11 H     -0.483505764030   -3.210182374350    0.605805732238
              12 N      1.101238667465    1.090131658297   -1.372497641072
              13 O      2.484424230525    2.576401211543   -1.481823979098
              14 O      1.555591808065    1.724550455654    0.864455317429
              15 S      3.219189294371    2.691520551582   -0.156089661963
              16 O      4.264550752730    1.677138784384    0.057666587589
