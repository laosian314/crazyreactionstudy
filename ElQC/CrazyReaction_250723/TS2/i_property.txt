-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -946.8852648287
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 1
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                997
     Surface Area:         729.6783151700
     Dielectric Energy:     -0.0103376106
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 17
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 19
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.0069     6.0000    -0.0069     3.8460     3.8460    -0.0000
  1   0     6.0010     6.0000    -0.0010     3.8336     3.8336    -0.0000
  2   0     5.9957     6.0000     0.0043     3.8859     3.8859    -0.0000
  3   0     5.8068     6.0000     0.1932     3.5723     3.5723    -0.0000
  4   0     6.0023     6.0000    -0.0023     3.8371     3.8371     0.0000
  5   0     6.0063     6.0000    -0.0063     3.8425     3.8425     0.0000
  6   0     0.9755     1.0000     0.0245     1.0072     1.0072     0.0000
  7   0     5.6863     6.0000     0.3137     4.2015     4.2015    -0.0000
  8   0     0.9710     1.0000     0.0290     0.9964     0.9964     0.0000
  9   0     0.9705     1.0000     0.0295     0.9955     0.9955    -0.0000
 10   0     0.9723     1.0000     0.0277     1.0082     1.0082     0.0000
 11   0     0.9706     1.0000     0.0294     0.9966     0.9966     0.0000
 12   0     7.0721     7.0000    -0.0721     2.2592     2.2592    -0.0000
 13   0     8.5555     8.0000    -0.5555     1.8688     1.8688    -0.0000
 14   0     8.4640     8.0000    -0.4640     1.9797     1.9797    -0.0000
 15   0    15.0335    16.0000     0.9665     3.8737     3.8737    -0.0000
 16   0     8.5097     8.0000    -0.5097     1.9551     1.9551     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.399931
                0             6               2            6                1.380022
                0             6               8            1                0.997832
                1             6               3            6                1.279702
                1             6               6            1                0.997964
                2             6               5            6                1.375003
                2             6               9            1                0.998313
                3             6               4            6                1.279358
                3             6               7            6                0.809181
                3             6              12            7                0.164034
                4             6               5            6                1.401493
                4             6              10            1                0.998908
                5             6              11            1                0.998002
                7             6              12            7                1.749340
                7             6              14            8                1.583097
               12             7              13            8                0.148640
               13             8              15           16                1.625515
               14             8              15           16                0.305886
               15            16              16            8                1.878253
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 1
   prop. index: 1
        Total number of Electrons                                                     94
        Number of correlated Electrons                                                62
        Number of alpha correlated Electrons                                          31
        Number of beta correlated Electrons                                           31
        Reference Energy:                                                -946.8852648114
        Total Correlation Energy:                                          -2.4510666923
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1362552584
        Total MDCI Energy:                                               -949.3363315037
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 1
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       17
     number of electrons:                   94
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             863
     number of aux C basis functions:       1998
     number of aux J basis functions:       8822
     number of aux JK basis functions:      8822
     number of aux CABS basis functions:    0
     Total Energy                           -949.336332
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 1
   prop. index: 1
       Filename                          : i.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        6.3184093566
        Electronic Contribution:
                  0    
      0       9.052597
      1       8.236944
      2      -1.074091
        Nuclear Contribution:
                  0    
      0     -11.274239
      1      -9.187858
      2       1.656575
        Total Dipole moment:
                  0    
      0      -2.221642
      1      -0.950914
      2       0.582484
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     1 
    Coordinates:
               0 C     -2.418344497991   -0.438062056044    0.166963638218
               1 C     -1.426794432613    0.496390758016   -0.096744545223
               2 C     -2.074763102220   -1.765671194335    0.423495584721
               3 C     -0.093438094594    0.081567290693   -0.059812211615
               4 C      0.265031928255   -1.251688576645    0.157109839983
               5 C     -0.739318601851   -2.171946185681    0.418705989634
               6 H     -1.667461942364    1.531707336547   -0.315946963586
               7 C      1.020657600926    1.142224141412   -0.116579941679
               8 H     -3.460705453000   -0.135135348230    0.158489357933
               9 H     -2.856621972487   -2.495240387209    0.613470318073
              10 H      1.310267578812   -1.543709065635    0.133333578419
              11 H     -0.483505764030   -3.210182374350    0.605805732238
              12 N      1.101238667465    1.090131658297   -1.372497641072
              13 O      2.484424230525    2.576401211543   -1.481823979098
              14 O      1.555591808065    1.724550455654    0.864455317429
              15 S      3.219189294371    2.691520551582   -0.156089661963
              16 O      4.264550752730    1.677138784384    0.057666587589
