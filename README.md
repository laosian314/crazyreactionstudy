# Crazy Reaction Repository

Here, we will store a citizen-computational-chemistry investigation of the Tiemann Rearrangement reaction.

## Project progress

[Checkout for works by Andrescorp](https://docs.google.com/spreadsheets/d/1NrFvF-v1yOZDaPIBpctpgFdDWu_XUCvVG4IMyFPYwqs/edit?usp=sharing)

### Simple reaction

The initial system contains the following stable structures:
 - Reagent
 - Catalyst (SO_2)
 - Product
 - Pre-intermediate (complex of Reagent + Catalyst)
 - Intermediate (5-memeber cycle)
 - Post-intermeditate (complex of Product + Catalyst)
 
For each of them a structure optimization + Hessian computation needed.

Also, a search of the transition states between the 
 - Pre-intermediate and Intermediate (TS1) 
 - Intermediate and Post-intermediate (TS2)
has been done by various methods.

These results were obtained by the following methods:
 - B3LYP-D3(BJ)/6-31G (gas). **Done by:** [madschumacher](https://gitlab.com/madschumacher/crazyreactionstudy/-/tree/main/MadSchumacherQC/b3lyp-d3bj-6-31g), [Gess](https://gitlab.com/madschumacher/crazyreactionstudy/-/tree/main/GessQC/b3lyp-d3bj-6-31g).
 - r2SCAN-3c (gas). **Done by:** [Dzhvar](https://gitlab.com/madschumacher/crazyreactionstudy/-/tree/main/DzhvarQC/R2SCAN-3c-D4), [Geom](https://gitlab.com/madschumacher/crazyreactionstudy/-/tree/main/GeomQC/r2SCAN-3c), [Gruppasymmetry25](https://gitlab.com/madschumacher/crazyreactionstudy/-/tree/main/Grupasymmetry25QC/r2scan_gas)
 - r2SCAN-D3/TZ2P (gas). **Done (in AMS) by:** [Rostov](https://gitlab.com/madschumacher/crazyreactionstudy/-/tree/main/RostovQC/plams_workdir.002)
 - r2SCAN-3c+CPCM(benzene). **Done by:** [Dzhvar](https://gitlab.com/madschumacher/crazyreactionstudy/-/tree/main/DzhvarQC/R2SCAN-3c-D4), [Pe4ka](https://gitlab.com/madschumacher/crazyreactionstudy/-/tree/main/Pe4kaQC/wB97XD_R2SCAN3c_Solvation), [Grupasymmetry25](https://gitlab.com/madschumacher/crazyreactionstudy/-/tree/main/Grupasymmetry25QC/r2scan_geometry), [Andrescorp](https://gitlab.com/madschumacher/crazyreactionstudy/-/tree/main/AndrescorpQC/opt)
 - r2SCAN-3c (Toluene). **Done by:** [Pe4ka](https://gitlab.com/madschumacher/crazyreactionstudy/-/tree/main/Pe4kaQC/wB97XD_R2SCAN3c_Solvation
 - wB97X-D3BJ def2-TZVP(Toluene and Benzene). **Done by:** [Pe4ka](https://gitlab.com/madschumacher/crazyreactionstudy/-/tree/main/Pe4kaQC/wB97XD_R2SCAN3c_Solvation
 - wB97X-D3BJ def2-TZVPPD(Toluene). **Done by:** [Grupasymmetry25](https://gitlab.com/madschumacher/crazyreactionstudy/-/tree/main/Grupasymmetry25QC/wB97x_gas)
 - DLPNO-CCSD(T)/CBS(cc-pVTZ/cc-pVQZ)+CPCM(Benzene)//r2SCAN-3c+CPCM(Benzene). **Done by:** [Grupasymmetry25](https://gitlab.com/madschumacher/crazyreactionstudy/-/tree/main/Grupasymmetry25QC/CCSD_electronic_energy_wB97X_benzene)
 - DLPNO-CCSD(T)/CBS(cc-pVTZ/cc-pVQZ)+CPCM(Toluene)//wB97X-D3BJ def2-TZVPPD(Toluene). **Done by:** [Grupasymmetry25](https://gitlab.com/madschumacher/crazyreactionstudy/-/tree/main/Grupasymmetry25QC/CCSD_electronic_energy_wB97X_Toluene)

### IRC verification of TS1 and TS2
 - wB97X-D3BJ def2-TZVP CPCM(Toluene, Benzene). **Done by:** [Pe4ka] (https://gitlab.com/madschumacher/crazyreactionstudy/-/tree/main/Pe4kaQC/IRC/wb97xd)
 - r2SCAN-3c CPCM(Toluene, Benzene). **Done by:** [Pe4ka] (https://gitlab.com/madschumacher/crazyreactionstudy/-/tree/main/Pe4kaQC/IRC/r2scan3c)

### Metadynamics simulations for the simple reaction

Starting from intermediate state, three metadynamics (MTD) simulations were preformed with three different collective variables (CV) to describe the reaction, five trajectories were started for each of them.
MTD simulations were done for 5 ps with 1 fs step, 10 fs update period for the bias potential, and Berendsen thermostat at 300 K. The method was B3LYP-D3(BJ)/6-31G (in gas).
*Done by:* [madschumacher](https://gitlab.com/madschumacher/crazyreactionstudy/-/tree/main/MadSchumacherQC/b3lyp-d3bj-6-31g/MTD)


### Substituted reaction

The same stuff as before, but with different substitutions were also done.

 1. Para-chloro-substituted reagent
    - r2SCAN-3c (gas). **Done by:**  [BAV](https://gitlab.com/madschumacher/crazyreactionstudy/-/tree/main/BAVQC/p-Cl)
    - r2SCAN-3c+CPCM(benzene). **Done by:**  [BAV](https://gitlab.com/madschumacher/crazyreactionstudy/-/tree/main/BAVQC/p-Cl_CPCM)


### Alternative reaction with SO2

Full path calculated by [MAK](https://gitlab.com/madschumacher/crazyreactionstudy/-/tree/main/MAKQC/AlternativeMechSO2) at B3LYP-D3/6-31G level of theory (gas).

### Alternative reaction with O3

Started by [MAK](https://gitlab.com/madschumacher/crazyreactionstudy/-/tree/main/MAKQC/OzoneReaction) at B3LYP-D3/6-31G level of theory (gas).

### Alternative reaction with SeO2
r2SCAN-3c level of theory (gas). **Done by:** [Pe4ka] (https://gitlab.com/madschumacher/crazyreactionstudy/-/tree/main/Pe4kaQC/SeO2_Catalyst)

r2SCAN-3c CPCM(Benzene) level of theory. **Done by:** [Pe4ka] (https://gitlab.com/madschumacher/crazyreactionstudy/-/tree/main/Pe4kaQC/SeO2_Catalyst) with postfix *_phh

r2SCAN-3c CPCM(Benzene) level of theory IRC. **Done by:** [Pe4ka] (https://gitlab.com/madschumacher/crazyreactionstudy/-/tree/main/Pe4kaQC/SeO2_Catalyst/IRC_Phh)

### Alternative reaction with RCNS reagent

r2SCAN-3c level of theory (gas). **Done by:** [Pe4ka, Dmitry Sharapa] (https://gitlab.com/igorgordiy2000/crazyreactionstudy/-/tree/main/Pe4kaQC/Reaction%20SO2/RCNS/gas)
r2SCAN-3c level of theory IRC (gas). **Done by:** [Pe4ka] (https://gitlab.com/igorgordiy2000/crazyreactionstudy/-/tree/main/Pe4kaQC/Reaction%20SO2/RCNS/gas)
r2SCAN-3c level of theory (Benzene). **WIP:** [Pe4ka] (https://gitlab.com/igorgordiy2000/crazyreactionstudy/-/tree/main/Pe4kaQC/Reaction%20SO2/RCNS/phh)


