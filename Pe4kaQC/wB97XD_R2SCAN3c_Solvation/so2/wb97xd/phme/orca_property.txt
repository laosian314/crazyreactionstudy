-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -548.6675182266
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                 16.0000007447 
   Number of Beta  Electrons                 16.0000007447 
   Total number of  Electrons                32.0000014893 
   Exchange energy                          -29.9848655429 
   Correlation energy                        -1.5427912756 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -31.5276568185 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -548.6675182266 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 1
   prop. index: 1
     Epsilon:                2.4000000000
     Refrac:                 1.4970000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                254
     Surface Area:         303.5589469572
     Dielectric Energy:     -0.0055505433
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 3
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 3
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0    15.2468    16.0000     0.7532     3.6601     3.6601    -0.0000
  1   0     8.3766     8.0000    -0.3766     2.0534     2.0534    -0.0000
  2   0     8.3766     8.0000    -0.3766     2.0534     2.0534    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0            16               1            8                1.830055
                0            16               2            8                1.830055
                1             8               2            8                0.223313
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 1
   prop. index: 1
        Van der Waals Correction:       -0.0038148528
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:     -548.7143754354
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 2
   prop. index: 1
   Number of Alpha Electrons                 16.0000001409 
   Number of Beta  Electrons                 16.0000001409 
   Total number of  Electrons                32.0000002819 
   Exchange energy                          -30.0796802790 
   Correlation energy                        -1.5516126302 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -31.6312929093 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -548.7143754354 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 2
   prop. index: 1
     Epsilon:                2.4000000000
     Refrac:                 1.4970000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                254
     Surface Area:         292.2182788597
     Dielectric Energy:     -0.0048434369
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 2
   prop. index: 1
        Van der Waals Correction:       -0.0038932082
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 3
   prop. index: 1
        SCF Energy:     -548.7236117400
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 3
   prop. index: 1
   Number of Alpha Electrons                 15.9999998399 
   Number of Beta  Electrons                 15.9999998399 
   Total number of  Electrons                31.9999996799 
   Exchange energy                          -30.2075151582 
   Correlation energy                        -1.5618488871 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -31.7693640453 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -548.7236117400 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 3
   prop. index: 1
     Epsilon:                2.4000000000
     Refrac:                 1.4970000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                250
     Surface Area:         281.7758574834
     Dielectric Energy:     -0.0037743815
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 3
   prop. index: 1
        Van der Waals Correction:       -0.0039531143
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 4
   prop. index: 1
        SCF Energy:     -548.7271812433
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 4
   prop. index: 1
   Number of Alpha Electrons                 16.0000002028 
   Number of Beta  Electrons                 16.0000002028 
   Total number of  Electrons                32.0000004055 
   Exchange energy                          -30.1527287445 
   Correlation energy                        -1.5576280969 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -31.7103568414 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -548.7271812433 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 4
   prop. index: 1
     Epsilon:                2.4000000000
     Refrac:                 1.4970000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                254
     Surface Area:         285.8124263105
     Dielectric Energy:     -0.0040452840
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 4
   prop. index: 1
        Van der Waals Correction:       -0.0039260017
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 5
   prop. index: 1
        SCF Energy:     -548.7273441216
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 5
   prop. index: 1
   Number of Alpha Electrons                 16.0000001325 
   Number of Beta  Electrons                 16.0000001325 
   Total number of  Electrons                32.0000002649 
   Exchange energy                          -30.1611612463 
   Correlation energy                        -1.5582896396 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -31.7194508858 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -548.7273441216 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 5
   prop. index: 1
     Epsilon:                2.4000000000
     Refrac:                 1.4970000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                250
     Surface Area:         285.1523885636
     Dielectric Energy:     -0.0039591800
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 5
   prop. index: 1
        Van der Waals Correction:       -0.0039291898
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 6
   prop. index: 1
        SCF Energy:     -548.7273460841
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 6
   prop. index: 1
   Number of Alpha Electrons                 16.0000001233 
   Number of Beta  Electrons                 16.0000001233 
   Total number of  Electrons                32.0000002466 
   Exchange energy                          -30.1622385031 
   Correlation energy                        -1.5583734863 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -31.7206119894 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -548.7273460841 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 6
   prop. index: 1
     Epsilon:                2.4000000000
     Refrac:                 1.4970000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                250
     Surface Area:         285.0689750375
     Dielectric Energy:     -0.0039448674
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 6
   prop. index: 1
        Van der Waals Correction:       -0.0039294802
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 7
   prop. index: 1
        SCF Energy:     -548.7273461881
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 7
   prop. index: 1
   Number of Alpha Electrons                 16.0000001236 
   Number of Beta  Electrons                 16.0000001236 
   Total number of  Electrons                32.0000002473 
   Exchange energy                          -30.1622083373 
   Correlation energy                        -1.5583708520 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -31.7205791892 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -548.7273461881 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 7
   prop. index: 1
     Epsilon:                2.4000000000
     Refrac:                 1.4970000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                250
     Surface Area:         285.0716334805
     Dielectric Energy:     -0.0039446762
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 7
   prop. index: 1
     Number of atoms                     : 3
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 3
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0    15.2947    16.0000     0.7053     3.8824     3.8824    -0.0000
  1   0     8.3526     8.0000    -0.3526     2.0688     2.0688     0.0000
  2   0     8.3526     8.0000    -0.3526     2.0688     2.0688    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0            16               1            8                1.941224
                0            16               2            8                1.941224
                1             8               2            8                0.127546
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 7
   prop. index: 1
        Van der Waals Correction:       -0.0039294468
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 7
   prop. index: 1
       Filename                          : orca.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        2.0360787188
        Electronic Contribution:
                  0    
      0       0.000000
      1      -0.000000
      2       0.822580
        Nuclear Contribution:
                  0    
      0       0.000000
      1      -0.000000
      2      -0.021541
        Total Dipole moment:
                  0    
      0       0.000000
      1      -0.000000
      2       0.801039
# -----------------------------------------------------------
$ Hessian
   description: Details about the Hessian
   geom. index: 7
   prop. index: 1
Normal modes:
Number of Rows: 9 Number of Columns: 9
                  0          1          2          3          4          5    
      0       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      1       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      2       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      3       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      4       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      5       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      6       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      7       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      8       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
                  6          7          8    
      0       0.000000   0.000000  -0.000000
      1      -0.000000  -0.000000  -0.517758
      2       0.403674  -0.350263   0.000000
      3      -0.000000  -0.000000   0.000000
      4      -0.504915  -0.561692   0.518761
      5      -0.404456   0.350942  -0.311209
      6       0.000000   0.000000  -0.000000
      7       0.504915   0.561692   0.518761
      8      -0.404456   0.350942   0.311208
# -----------------------------------------------------------
$ THERMOCHEMISTRY_Energies
   description: The Thermochemistry energies
   geom. index: 7
   prop. index: 1
        Temperature (Kelvin)           :        298.1500000000
        Pressure (atm)                 :          1.0000000000
        Total Mass (AMU)               :         64.0580000000
        Spin Degeneracy                :          1.0000000000
        Electronic Energy (Hartree)    :       -548.7312756348
        Translational Energy (Hartree) :          0.0014162714
        Rotational Energy (Hartree)    :          0.0014162714
        Vibrational Energy (Hartree)   :          0.0002205932
        Number of frequencies          :     9      
        Scaling Factor for frequencies :          1.0000000000
        Vibrational frequencies        :     
                  0    
      0       0.000000
      1       0.000000
      2       0.000000
      3       0.000000
      4       0.000000
      5       0.000000
      6     535.223038
      7     1238.322078
      8     1418.710230
        Zero Point Energy (Hartree)    :          0.0072724928
        Inner Energy (Hartree)         :       -548.7209500060
        Enthalpy (Hartree)             :       -548.7200057969
        Electronic entropy             :          0.0000000000
        Rotational entropy             :          0.0096124761
        Vibrational entropy            :          0.0002985436
        Translational entropy          :          0.0096124761
        Entropy                        :          0.0281514810
        Gibbs Energy (Hartree)         :       -548.7481572779
        Is Linear                      :                 false
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    3 
    Geometry Index:     1 
    Coordinates:
               0 S      0.000000000000   -0.000000070661    0.464422083483
               1 O      0.000000000000    1.351921804805   -0.413795062960
               2 O      0.000000000000   -1.351921734143   -0.413795020523
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    3 
    Geometry Index:     2 
    Coordinates:
               0 S      0.000000000000   -0.000000021910    0.422910303364
               1 O      0.000000000000    1.266160280981   -0.393039157347
               2 O      0.000000000000   -1.266160259071   -0.393039146017
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    3 
    Geometry Index:     3 
    Coordinates:
               0 S      0.000000000000   -0.000000005113    0.363315360003
               1 O      0.000000000000    1.191558483884   -0.363241680694
               2 O      0.000000000000   -1.191558478771   -0.363241679309
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    3 
    Geometry Index:     4 
    Coordinates:
               0 S      0.000000000000   -0.000000004441    0.375790963387
               1 O      0.000000000000    1.230873052837   -0.369479482164
               2 O      0.000000000000   -1.230873048395   -0.369479481223
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    3 
    Geometry Index:     5 
    Coordinates:
               0 S      0.000000000000    0.000000003832    0.370616521424
               1 O      0.000000000000    1.227219362549   -0.366892258690
               2 O      0.000000000000   -1.227219366381   -0.366892262734
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    3 
    Geometry Index:     6 
    Coordinates:
               0 S      0.000000000000    0.000000021019    0.369705418542
               1 O      0.000000000000    1.226971711144   -0.366436702083
               2 O      0.000000000000   -1.226971732162   -0.366436716459
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    3 
    Geometry Index:     7 
    Coordinates:
               0 S      0.000000000000    0.000000007177    0.369678975875
               1 O      0.000000000000    1.227026821160   -0.366423484901
               2 O      0.000000000000   -1.227026828337   -0.366423490974
