#! /bin/bash

#cp mol_nvt_berendsen.trj  mol.trj
#molden -j 5000 -l mol_nvt_berendsen.trj

for a in $(ls | grep 'mol[0-9]\+.gif'); do
   n=$(echo $a | grep -o '[0-9]\+' | awk '{printf "%05i", $1}')
   echo $n 
   convert $a mol$n.png
done

ffmpeg -framerate 50 -pattern_type glob -i 'mol*.png' -c:v libx264 -pix_fmt yuv420p out.mp4

rm -f mol.trj