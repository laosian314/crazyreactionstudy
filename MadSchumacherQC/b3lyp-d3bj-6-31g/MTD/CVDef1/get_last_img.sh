#! /bin/bash


printf "" > last_frames.xyz
printf "" > first_frames.xyz
for f in $(ls | grep '^[0-9]\+$'); do
    echo $f
    N=$(head -1  $f/mol_nv*.trj | awk '{printf "%i", $1}')
    np1=$(echo $N "+1" | bc )
    T=$(tail -$np1 $f/mol_nv*.trj | head -1 | grep -o '[0-9]\+\.[0-9]\+')
    #echo $T $np1
    printf "%i\n  %s %s\n" $N $f $T >> last_frames.xyz
    printf "%i\n  %s %s\n" $N $f $T >> first_frames.xyz
    tail -$N $f/mol_nv*.trj   >> last_frames.xyz
    head -$(echo "2 + $N" | bc) $f/mol_nv*.trj  | tail -$N  >> first_frames.xyz
#    get_rot_const_distr.py  -t $f/mol_nv*.trj #--UseWvn
done

grep '^ \+[0-9]\+ \+[0-9]\+\.' last_frames.xyz | grep -v '4999\.0'