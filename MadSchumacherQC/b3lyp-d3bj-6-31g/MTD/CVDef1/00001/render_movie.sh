#! /bin/bash

# filename of the trajectory
FILE="mol_nvt_berendsen.trj"
# number of frames to take
NFRAMES=4600

# filename of the final movie
MOV="jmol_movie"

rm -f movie*.jpg $MOV.mp4 tmp.trj

NAT=$(head -1 $FILE | grep -o '[0-9]\+')
echo $NAT

head -$(echo "($NAT + 2) *$NFRAMES"  | bc)  $FILE > tmp.trj

# based on this: https://wiki.jmol.org/index.php/Creating_Movies
echo '''

# this keeps the connectivity
#load trajectory "tmp.trj"
# this recalculates the connectivity for each frame
load  "tmp.trj"

color background [xFFFFFF]

set cameradepth 0.2

frame 1
#frame 3000
num_frames = getProperty("modelInfo.modelCount")
#num_frames = 3500

for (var i = 1; i <= num_frames; i = i+1)
   var filename = "movie"+("00000"+i)[-4][0]+".jpg"
   write IMAGE 500 500 JPG @filename
   frame next
end for

exitjmol
''' > script.jmol

jmol -s script.jmol

ffmpeg -framerate 100 -pattern_type glob -i 'movie*.jpg' -c:v libx264 -pix_fmt yuv420p $MOV.mp4

rm -f movie*.jpg script.jmol tmp.trj
