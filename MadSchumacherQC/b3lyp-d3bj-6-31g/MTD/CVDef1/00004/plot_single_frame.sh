#! /bin/bash

# filename of the trajectory
FILE="mol_nvt_berendsen.trj"
# number of frames to take
NFRAMES=3000

# filename of the final movie
MOV="jmol_movie"

rm -f movie*.jpg $MOV.mp4 tmp.trj

NAT=$(head -1 $FILE | grep -o '[0-9]\+')
echo $NAT

head -$(echo "($NAT + 2) *$NFRAMES"  | bc)  $FILE > tmp.trj

# based on this: https://wiki.jmol.org/index.php/Creating_Movies
echo '''

# this keeps the connectivity
#load trajectory "tmp.trj"
# this recalculates the connectivity for each frame
load  "tmp.trj"

color background [xFFFFFF]

set cameradepth 0.2

frame 1
num_frames = getProperty("modelInfo.modelCount")
var filename = "first_frame.jpg"
write IMAGE 1000 1000 JPG @filename

frame LAST
var filename = "last_frame.jpg"
write IMAGE 1000 1000 JPG @filename

exitjmol
''' > script.jmol

jmol -s script.jmol


rm -f script.jmol tmp.trj
