#!/bin/bash


#SBATCH --partition=allcpu,maxcpu   # this is the default partition.
#SBATCH --time=30:00:00        # default is 1h. The maximum is partition dependent, have a look at sview or scontrol for details.
#SBATCH --nodes=1              # Number of nodes. If your job can consume variable number of nodes you might want to use something like 
#SBATCH --job-name  BOMoND!
#SBATCH --output     job.out  # File to which STDOUT will be written
#SBATCH --error      job.err  # File to which STDERR will be written
#SBATCH --mail-type END                 # Type of email notification- BEGIN,END,FAIL,ALL

export LD_PRELOAD=""
source /etc/profile.d/modules.sh
module load maxwell orca
PYRPATH="/home/tikhonov/pyramd"

# time step in fs
DT=1.0
# simulation time in ps
ST=5.0
# charge
Q=0
# multiplicity
M=1

ADDOPT="   --FreezeTranslation  --FreezeRotation "
ADDOPT+="  --charge $Q --mult $M "
ADDOPT+="  --dt $DT --QCInterface orca5 --SimTime $ST --IniT 300.0  "
ADDOPT+="  --UseClassicalSampling  "
ADDOPT+="  --RelaxTime 100  --ThermoT 300.0  "
ADDOPT+="  --MTDDefFile  cv.def "
ADDOPT+="  --width 0.05 "
ADDOPT+="  --UpdateTime  10.0 "

$PYRPATH/bbbmtd.py --inifile mol.eqg $ADDOPT
