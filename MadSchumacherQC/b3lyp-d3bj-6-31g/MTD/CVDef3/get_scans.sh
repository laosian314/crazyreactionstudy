#! /bin/bash

Xmin=-3
Xmax=3
Npts=200

for d in $(ls | grep '^[0-9]\+$'); do
    
E=$(head -1 $d/mtd.pts | grep -o 'E = \+[0-9]\+\.[0-9]\+'     | grep -o '[0-9]\+\.[0-9]\+')
S=$(head -1 $d/mtd.pts | grep -o 'sigma = \+[0-9]\+\.[0-9]\+' | grep -o '[0-9]\+\.[0-9]\+')

echo $d $E $S

python <<-EOF
import numpy as np

S=float("$S")
E=float("$E")
DW = np.sqrt(2.0)*S


xmin=float("$Xmin") 
xmax=float("$Xmax") 

N = int("$Npts")

def BiasFunc(X):
    return (1.0 - X**2/(2.*S**2))

data = np.loadtxt("$d/mtd.pts")

X = np.linspace(xmin,xmax,N)
V = np.zeros(X.shape, dtype=float)

for cv in data:
    #print(cv)
    tmpV = np.where( np.abs(cv[1] - X) <= DW, BiasFunc(X-cv[1]), 0.0 )
    V -= tmpV


V *= E
data = np.stack([X, V],axis=-1)
np.savetxt("$d/recalc_mtd.pes", data) 

EOF
    
done


python3 <<-EOF
import numpy as np
import os

Dirs = os.listdir(path=".")

# directory to number number
def D2N(x):
    try:
        return int(x)
    except:
        pass


Data = []
for d in Dirs:
    try:
        f2l = d+"/recalc_mtd.pes"
        data = np.loadtxt(f2l)
        Data.append(data)
    except:
        print("Failed to process dir "+d)

X = Data[0][:,0]
PTS = np.array([d[:,1] for d in Data]).T
Y = np.mean(PTS, axis=1)
eY = np.std(PTS, axis=1)/np.sqrt(len(Data))

res = np.stack([X,Y,eY], axis=-1)
np.savetxt("avg.pes", res)


EOF



gnuplot <<-EOF

set terminal pngcairo
set output "current_mtd_pes.png"

set xlabel "CV, Angstroems"
set ylabel "Bias potential, cm-1"

plot for [i=0:20] sprintf("%05i/recalc_mtd.pes", i) w l title sprintf("%i", i),\
     'avg.pes' u 1:(\$2-\$3):(\$2+\$3) w filledcu lc rgb "red" notitle,\
     ''        u 1:2 w l lw 3 notitle

EOF





















