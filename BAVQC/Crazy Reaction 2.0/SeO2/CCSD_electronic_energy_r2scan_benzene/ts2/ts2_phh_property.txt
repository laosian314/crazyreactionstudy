-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:    -2946.9044264188
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 1
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                975
     Surface Area:         744.6660855487
     Dielectric Energy:     -0.0146396180
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 17
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 19
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.1530     6.0000    -0.1530     3.7656     3.7656     0.0000
  1   0     6.1161     6.0000    -0.1161     3.8347     3.8347     0.0000
  2   0     6.1132     6.0000    -0.1132     3.8007     3.8007    -0.0000
  3   0     5.9818     6.0000     0.0182     3.7532     3.7532    -0.0000
  4   0     6.1178     6.0000    -0.1178     3.8383     3.8383    -0.0000
  5   0     6.1543     6.0000    -0.1543     3.7697     3.7697    -0.0000
  6   0     0.8087     1.0000     0.1913     0.9724     0.9724    -0.0000
  7   0     5.6443     6.0000     0.3557     4.2791     4.2791    -0.0000
  8   0     0.8184     1.0000     0.1816     0.9685     0.9685    -0.0000
  9   0     0.8167     1.0000     0.1833     0.9688     0.9688    -0.0000
 10   0     0.8121     1.0000     0.1879     0.9730     0.9730    -0.0000
 11   0     0.8192     1.0000     0.1808     0.9686     0.9686    -0.0000
 12   0     7.0101     7.0000    -0.0101     2.3269     2.3269    -0.0000
 13   0     8.6614     8.0000    -0.6614     1.8351     1.8351    -0.0000
 14   0     8.4905     8.0000    -0.4905     2.0083     2.0083    -0.0000
 15   0    32.8507    34.0000     1.1493     3.7579     3.7579    -0.0000
 16   0     8.6317     8.0000    -0.6317     1.8970     1.8970    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.391888
                0             6               2            6                1.349344
                0             6               8            1                0.967550
                1             6               3            6                1.293448
                1             6               6            1                0.956834
                2             6               5            6                1.353161
                2             6               9            1                0.968671
                3             6               4            6                1.298018
                3             6               7            6                0.856684
                3             6              12            7                0.286879
                4             6               5            6                1.391479
                4             6              10            1                0.959528
                5             6              11            1                0.967689
                7             6              12            7                1.745481
                7             6              14            8                1.593895
               12             7              13            8                0.124502
               13             8              15           34                1.589754
               14             8              15           34                0.331760
               15            34              16            8                1.800696
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 1
   prop. index: 1
        Total number of Electrons                                                    112
        Number of correlated Electrons                                                72
        Number of alpha correlated Electrons                                          36
        Number of beta correlated Electrons                                           36
        Reference Energy:                                               -2946.9044261789
        Total Correlation Energy:                                          -2.3560511771
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1200417241
        Total MDCI Energy:                                              -2949.2604773561
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:    -2946.9484331380
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 2
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                975
     Surface Area:         744.6660855487
     Dielectric Energy:     -0.0146430445
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 2
   prop. index: 1
     Number of atoms                     : 17
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 19
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.1206     6.0000    -0.1206     3.7617     3.7617     0.0000
  1   0     6.1000     6.0000    -0.1000     3.8533     3.8533    -0.0000
  2   0     6.1288     6.0000    -0.1288     3.8307     3.8307     0.0000
  3   0     6.0023     6.0000    -0.0023     3.6504     3.6504    -0.0000
  4   0     6.0967     6.0000    -0.0967     3.8498     3.8498    -0.0000
  5   0     6.1236     6.0000    -0.1236     3.7659     3.7659     0.0000
  6   0     0.8175     1.0000     0.1825     0.9817     0.9817    -0.0000
  7   0     5.5885     6.0000     0.4115     4.3093     4.3093     0.0000
  8   0     0.8190     1.0000     0.1810     0.9741     0.9741     0.0000
  9   0     0.8079     1.0000     0.1921     0.9710     0.9710     0.0000
 10   0     0.8196     1.0000     0.1804     0.9807     0.9807    -0.0000
 11   0     0.8200     1.0000     0.1800     0.9741     0.9741     0.0000
 12   0     7.0745     7.0000    -0.0745     2.3746     2.3746     0.0000
 13   0     8.6899     8.0000    -0.6899     1.8152     1.8152    -0.0000
 14   0     8.5257     8.0000    -0.5257     1.9686     1.9686     0.0000
 15   0    32.7919    34.0000     1.2081     3.7664     3.7664     0.0000
 16   0     8.6735     8.0000    -0.6735     1.8681     1.8681     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.404789
                0             6               2            6                1.369094
                0             6               8            1                0.975236
                1             6               3            6                1.265817
                1             6               6            1                0.975191
                2             6               5            6                1.373055
                2             6               9            1                0.973278
                3             6               4            6                1.266328
                3             6               7            6                0.894918
                3             6              12            7                0.272953
                4             6               5            6                1.405266
                4             6              10            1                0.977180
                5             6              11            1                0.975157
                7             6              12            7                1.802991
                7             6              14            8                1.562505
               12             7              13            8                0.112415
               13             8              15           34                1.604431
               14             8              15           34                0.329878
               15            34              16            8                1.792268
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 2
   prop. index: 1
        Total number of Electrons                                                    112
        Number of correlated Electrons                                                72
        Number of alpha correlated Electrons                                          36
        Number of beta correlated Electrons                                           36
        Reference Energy:                                               -2946.9484333625
        Total Correlation Energy:                                          -2.5497978095
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1322546947
        Total MDCI Energy:                                              -2949.4982311720
# -----------------------------------------------------------
$ Energy_Extrapolation
   description: The Extrapolation of the energies
   geom. index: 2
   prop. index: 1
       Do EP1                            : 1 
       Do EP2                            : 0 
       Do EP3                            : 0 
       Number of energies                : 2 
       SCF energy with basis  cc-pVTZ :  -2946.9044261789 
       SCF energy with basis  cc-pVQZ :  -2946.9484333625 
       CBS SCF Energy                    :  -2946.9616928996 
       Correlation energy with basis  cc-pVTZ :  -2946.9044261789 
       Correlation energy with basis  cc-pVQZ :  -2946.9484333625 
       CBS Correlation Energy            :     -2.6877239996 
       CBS Total Energy                  :  -2949.6494168992 
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 2
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       17
     number of electrons:                   112
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             443
     number of aux C basis functions:       1953
     number of aux J basis functions:       0
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           0.000000
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 2
   prop. index: 1
       Filename                          : /scratch/obeletsan/orcajob__14398-5tNB/ts2_phh.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        7.6813973814
        Electronic Contribution:
                  0    
      0      33.247403
      1      20.145481
      2      -7.472813
        Nuclear Contribution:
                  0    
      0     -34.646140
      1     -22.763900
      2       8.038584
        Total Dipole moment:
                  0    
      0      -1.398738
      1      -2.618419
      2       0.565772
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     1 
    Coordinates:
               0 C      0.259206000000    0.042000000000   -0.484706000000
               1 C      1.355251000000    0.889122000000   -0.532426000000
               2 C      0.256755000000   -1.046947000000    0.389500000000
               3 C      2.456423000000    0.600415000000    0.281525000000
               4 C      2.453212000000   -0.463913000000    1.189155000000
               5 C      1.347355000000   -1.299822000000    1.223218000000
               6 H      1.379435000000    1.752653000000   -1.189664000000
               7 C      3.788089000000    1.350527000000    0.010310000000
               8 H     -0.601416000000    0.236702000000   -1.116891000000
               9 H     -0.612682000000   -1.696481000000    0.433060000000
              10 H      3.307646000000   -0.625211000000    1.838613000000
              11 H      1.326592000000   -2.140450000000    1.909604000000
              12 N      3.456147000000    2.168039000000    0.897605000000
              13 O      5.269296000000    3.288214000000    0.731258000000
              14 O      4.673818000000    1.072072000000   -0.836331000000
              15 Se     5.964385000000    2.910409000000   -0.732839000000
              16 O      5.294890000000    3.759672000000   -1.940290000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     2 
    Coordinates:
               0 C      0.259206000000    0.042000000000   -0.484706000000
               1 C      1.355251000000    0.889122000000   -0.532426000000
               2 C      0.256755000000   -1.046947000000    0.389500000000
               3 C      2.456423000000    0.600415000000    0.281525000000
               4 C      2.453212000000   -0.463913000000    1.189155000000
               5 C      1.347355000000   -1.299822000000    1.223218000000
               6 H      1.379435000000    1.752653000000   -1.189664000000
               7 C      3.788089000000    1.350527000000    0.010310000000
               8 H     -0.601416000000    0.236702000000   -1.116891000000
               9 H     -0.612682000000   -1.696481000000    0.433060000000
              10 H      3.307646000000   -0.625211000000    1.838613000000
              11 H      1.326592000000   -2.140450000000    1.909604000000
              12 N      3.456147000000    2.168039000000    0.897605000000
              13 O      5.269296000000    3.288214000000    0.731258000000
              14 O      4.673818000000    1.072072000000   -0.836331000000
              15 Se     5.964385000000    2.910409000000   -0.732839000000
              16 O      5.294890000000    3.759672000000   -1.940290000000
