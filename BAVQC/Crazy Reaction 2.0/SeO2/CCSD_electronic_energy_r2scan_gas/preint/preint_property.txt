-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:    -2946.8995469777
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 17
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 19
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.1334     6.0000    -0.1334     3.7931     3.7931     0.0000
  1   0     6.1588     6.0000    -0.1588     3.8491     3.8491    -0.0000
  2   0     6.1417     6.0000    -0.1417     3.8283     3.8283     0.0000
  3   0     5.9305     6.0000     0.0695     3.7616     3.7616    -0.0000
  4   0     6.1517     6.0000    -0.1517     3.8218     3.8218    -0.0000
  5   0     6.1293     6.0000    -0.1293     3.7732     3.7732    -0.0000
  6   0     0.8353     1.0000     0.1647     0.9808     0.9808    -0.0000
  7   0     5.9687     6.0000     0.0313     3.9400     3.9400    -0.0000
  8   0     0.8381     1.0000     0.1619     0.9755     0.9755     0.0000
  9   0     0.8360     1.0000     0.1640     0.9763     0.9763     0.0000
 10   0     0.8087     1.0000     0.1913     1.0024     1.0024     0.0000
 11   0     0.8344     1.0000     0.1656     0.9744     0.9744     0.0000
 12   0     6.6914     7.0000     0.3086     3.9474     3.9474    -0.0000
 13   0     8.4839     8.0000    -0.4839     1.6755     1.6755    -0.0000
 14   0     8.6067     8.0000    -0.6067     1.9271     1.9271     0.0000
 15   0    32.8947    34.0000     1.1053     3.7582     3.7582    -0.0000
 16   0     8.5569     8.0000    -0.5569     1.9787     1.9787    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.395755
                0             6               2            6                1.368689
                0             6               8            1                0.974336
                1             6               3            6                1.315118
                1             6               6            1                0.973838
                2             6               5            6                1.365363
                2             6               9            1                0.975453
                3             6               4            6                1.340460
                3             6               7            6                1.067873
                4             6               5            6                1.377579
                4             6              10            1                0.944794
                5             6              11            1                0.973887
                7             6              12            7                2.597032
                7             6              13            8                0.224260
               12             7              13            8                1.292569
               13             8              15           34                0.111891
               14             8              15           34                1.772536
               14             8              16            8                0.102893
               15            34              16            8                1.860267
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 1
   prop. index: 1
        Total number of Electrons                                                    112
        Number of correlated Electrons                                                72
        Number of alpha correlated Electrons                                          36
        Number of beta correlated Electrons                                           36
        Reference Energy:                                               -2946.8995469800
        Total Correlation Energy:                                          -2.3638588530
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1185325161
        Total MDCI Energy:                                              -2949.2634058330
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:    -2946.9434006528
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 2
   prop. index: 1
     Number of atoms                     : 17
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 18
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.0994     6.0000    -0.0994     3.7927     3.7927    -0.0000
  1   0     6.1641     6.0000    -0.1641     3.8690     3.8690    -0.0000
  2   0     6.1431     6.0000    -0.1431     3.8493     3.8493    -0.0000
  3   0     5.9585     6.0000     0.0415     3.7547     3.7547     0.0000
  4   0     6.1705     6.0000    -0.1705     3.8384     3.8384    -0.0000
  5   0     6.0955     6.0000    -0.0955     3.7670     3.7670     0.0000
  6   0     0.8432     1.0000     0.1568     0.9922     0.9922    -0.0000
  7   0     5.7996     6.0000     0.2004     3.8363     3.8363     0.0000
  8   0     0.8463     1.0000     0.1537     0.9869     0.9869     0.0000
  9   0     0.8361     1.0000     0.1639     0.9843     0.9843     0.0000
 10   0     0.8134     1.0000     0.1866     1.0129     1.0129    -0.0000
 11   0     0.8417     1.0000     0.1583     0.9839     0.9839     0.0000
 12   0     6.8178     7.0000     0.1822     3.9014     3.9014     0.0000
 13   0     8.4948     8.0000    -0.4948     1.6942     1.6942     0.0000
 14   0     8.6510     8.0000    -0.6510     1.9109     1.9109     0.0000
 15   0    32.8366    34.0000     1.1634     3.7848     3.7848    -0.0000
 16   0     8.5884     8.0000    -0.5884     1.9638     1.9638    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.419362
                0             6               2            6                1.385156
                0             6               8            1                0.986108
                1             6               3            6                1.292866
                1             6               6            1                0.986442
                2             6               5            6                1.376169
                2             6               9            1                0.984948
                3             6               4            6                1.327716
                3             6               7            6                1.135281
                4             6               5            6                1.397914
                4             6              10            1                0.954531
                5             6              11            1                0.984304
                7             6              12            7                2.524925
                7             6              13            8                0.184205
               12             7              13            8                1.323489
               13             8              15           34                0.126140
               14             8              15           34                1.782854
               15            34              16            8                1.859586
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 2
   prop. index: 1
        Total number of Electrons                                                    112
        Number of correlated Electrons                                                72
        Number of alpha correlated Electrons                                          36
        Number of beta correlated Electrons                                           36
        Reference Energy:                                               -2946.9434006530
        Total Correlation Energy:                                          -2.5567092061
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1306553052
        Total MDCI Energy:                                              -2949.5001098591
# -----------------------------------------------------------
$ Energy_Extrapolation
   description: The Extrapolation of the energies
   geom. index: 2
   prop. index: 1
       Do EP1                            : 1 
       Do EP2                            : 0 
       Do EP3                            : 0 
       Number of energies                : 2 
       SCF energy with basis  cc-pVTZ :  -2946.8995469800 
       SCF energy with basis  cc-pVQZ :  -2946.9434006530 
       CBS SCF Energy                    :  -2946.9566139367 
       Correlation energy with basis  cc-pVTZ :  -2946.8995469800 
       Correlation energy with basis  cc-pVQZ :  -2946.9434006530 
       CBS Correlation Energy            :     -2.6939973444 
       CBS Total Energy                  :  -2949.6506112811 
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 2
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       17
     number of electrons:                   112
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             443
     number of aux C basis functions:       1953
     number of aux J basis functions:       0
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           0.000000
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 2
   prop. index: 1
       Filename                          : /scratch/obeletsan/orcajob__14392-87ld/preint.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        5.3489571050
        Electronic Contribution:
                  0    
      0      36.097987
      1      20.885611
      2       6.271264
        Nuclear Contribution:
                  0    
      0     -37.940334
      1     -21.681992
      2      -5.638785
        Total Dipole moment:
                  0    
      0      -1.842347
      1      -0.796381
      2       0.632479
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     1 
    Coordinates:
               0 C     -0.066963000000    0.057546000000   -0.215227000000
               1 C      0.693954000000    1.209133000000   -0.350662000000
               2 C      0.481377000000   -1.082779000000    0.367699000000
               3 C      2.022055000000    1.215768000000    0.104045000000
               4 C      2.578137000000    0.067838000000    0.692905000000
               5 C      1.799562000000   -1.073103000000    0.818577000000
               6 H      0.275493000000    2.101755000000   -0.803944000000
               7 C      2.822678000000    2.376208000000   -0.022532000000
               8 H     -1.094073000000    0.050143000000   -0.566890000000
               9 H     -0.120242000000   -1.980842000000    0.470275000000
              10 H      3.606161000000    0.097052000000    1.038694000000
              11 H      2.226636000000   -1.962035000000    1.272621000000
              12 N      3.509732000000    3.304938000000   -0.123427000000
              13 O      4.296340000000    4.234651000000   -0.181932000000
              14 O      5.392782000000    1.883764000000    1.492266000000
              15 Se     6.394017000000    3.038090000000    0.920398000000
              16 O      7.190954000000    2.578374000000   -0.415466000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     2 
    Coordinates:
               0 C     -0.066963000000    0.057546000000   -0.215227000000
               1 C      0.693954000000    1.209133000000   -0.350662000000
               2 C      0.481377000000   -1.082779000000    0.367699000000
               3 C      2.022055000000    1.215768000000    0.104045000000
               4 C      2.578137000000    0.067838000000    0.692905000000
               5 C      1.799562000000   -1.073103000000    0.818577000000
               6 H      0.275493000000    2.101755000000   -0.803944000000
               7 C      2.822678000000    2.376208000000   -0.022532000000
               8 H     -1.094073000000    0.050143000000   -0.566890000000
               9 H     -0.120242000000   -1.980842000000    0.470275000000
              10 H      3.606161000000    0.097052000000    1.038694000000
              11 H      2.226636000000   -1.962035000000    1.272621000000
              12 N      3.509732000000    3.304938000000   -0.123427000000
              13 O      4.296340000000    4.234651000000   -0.181932000000
              14 O      5.392782000000    1.883764000000    1.492266000000
              15 Se     6.394017000000    3.038090000000    0.920398000000
              16 O      7.190954000000    2.578374000000   -0.415466000000
