-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -944.5885911380
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 17
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 19
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.1377     6.0000    -0.1377     3.7855     3.7855    -0.0000
  1   0     6.1273     6.0000    -0.1273     3.8604     3.8604    -0.0000
  2   0     6.1244     6.0000    -0.1244     3.8257     3.8257     0.0000
  3   0     5.9618     6.0000     0.0382     3.7623     3.7623     0.0000
  4   0     6.1257     6.0000    -0.1257     3.8548     3.8548     0.0000
  5   0     6.1369     6.0000    -0.1369     3.7806     3.7806     0.0000
  6   0     0.8293     1.0000     0.1707     0.9800     0.9800    -0.0000
  7   0     5.6775     6.0000     0.3225     4.2899     4.2899     0.0000
  8   0     0.8364     1.0000     0.1636     0.9753     0.9753    -0.0000
  9   0     0.8347     1.0000     0.1653     0.9761     0.9761     0.0000
 10   0     0.8247     1.0000     0.1753     0.9791     0.9791     0.0000
 11   0     0.8355     1.0000     0.1645     0.9752     0.9752     0.0000
 12   0     6.9910     7.0000     0.0090     2.2122     2.2122     0.0000
 13   0     8.5416     8.0000    -0.5416     1.9364     1.9364     0.0000
 14   0     8.4755     8.0000    -0.4755     2.0253     2.0253     0.0000
 15   0    15.0577    16.0000     0.9423     3.8828     3.8828     0.0000
 16   0     8.4824     8.0000    -0.4824     2.0266     2.0266     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.387164
                0             6               2            6                1.366088
                0             6               8            1                0.973058
                1             6               3            6                1.327536
                1             6               6            1                0.965349
                2             6               5            6                1.362360
                2             6               9            1                0.974625
                3             6               4            6                1.322148
                3             6               7            6                0.877084
                3             6              12            7                0.213787
                4             6               5            6                1.386723
                4             6              10            1                0.962441
                5             6              11            1                0.972924
                7             6              12            7                1.675354
                7             6              14            8                1.668223
               12             7              13            8                0.164323
               13             8              15           16                1.657924
               14             8              15           16                0.269649
               15            16              16            8                1.922143
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 1
   prop. index: 1
        Total number of Electrons                                                     94
        Number of correlated Electrons                                                62
        Number of alpha correlated Electrons                                          31
        Number of beta correlated Electrons                                           31
        Reference Energy:                                                -944.5885911446
        Total Correlation Energy:                                          -2.3062755561
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1173193290
        Total MDCI Energy:                                               -946.8948667007
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:     -944.6370738359
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 2
   prop. index: 1
     Number of atoms                     : 17
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 19
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.0992     6.0000    -0.0992     3.7759     3.7759    -0.0000
  1   0     6.1008     6.0000    -0.1008     3.8722     3.8722    -0.0000
  2   0     6.1286     6.0000    -0.1286     3.8497     3.8497     0.0000
  3   0     5.9917     6.0000     0.0083     3.6704     3.6704     0.0000
  4   0     6.1040     6.0000    -0.1040     3.8764     3.8764     0.0000
  5   0     6.0966     6.0000    -0.0966     3.7699     3.7699    -0.0000
  6   0     0.8458     1.0000     0.1542     0.9938     0.9938     0.0000
  7   0     5.6108     6.0000     0.3892     4.3196     4.3196     0.0000
  8   0     0.8463     1.0000     0.1537     0.9855     0.9855    -0.0000
  9   0     0.8349     1.0000     0.1651     0.9841     0.9841     0.0000
 10   0     0.8426     1.0000     0.1574     0.9941     0.9941     0.0000
 11   0     0.8452     1.0000     0.1548     0.9855     0.9855    -0.0000
 12   0     7.0531     7.0000    -0.0531     2.2690     2.2690     0.0000
 13   0     8.5775     8.0000    -0.5775     1.8915     1.8915     0.0000
 14   0     8.4970     8.0000    -0.4970     2.0015     2.0015    -0.0000
 15   0    14.9863    16.0000     1.0137     3.8736     3.8736    -0.0000
 16   0     8.5396     8.0000    -0.5396     1.9655     1.9655     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.395795
                0             6               2            6                1.380797
                0             6               8            1                0.985427
                1             6               3            6                1.293404
                1             6               6            1                0.988400
                2             6               5            6                1.376796
                2             6               9            1                0.984852
                3             6               4            6                1.294218
                3             6               7            6                0.919000
                3             6              12            7                0.197767
                4             6               5            6                1.393797
                4             6              10            1                0.986261
                5             6              11            1                0.985499
                7             6              12            7                1.738005
                7             6              14            8                1.629895
               12             7              13            8                0.156422
               13             8              15           16                1.650758
               14             8              15           16                0.291080
               15            16              16            8                1.891804
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 2
   prop. index: 1
        Total number of Electrons                                                     94
        Number of correlated Electrons                                                62
        Number of alpha correlated Electrons                                          31
        Number of beta correlated Electrons                                           31
        Reference Energy:                                                -944.6370738369
        Total Correlation Energy:                                          -2.4471322675
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1279867644
        Total MDCI Energy:                                               -947.0842061044
# -----------------------------------------------------------
$ Energy_Extrapolation
   description: The Extrapolation of the energies
   geom. index: 2
   prop. index: 1
       Do EP1                            : 1 
       Do EP2                            : 0 
       Do EP3                            : 0 
       Number of energies                : 2 
       SCF energy with basis  cc-pVTZ :   -944.5885911446 
       SCF energy with basis  cc-pVQZ :   -944.6370738369 
       CBS SCF Energy                    :   -944.6516818623 
       Correlation energy with basis  cc-pVTZ :   -944.5885911446 
       Correlation energy with basis  cc-pVQZ :   -944.6370738369 
       CBS Correlation Energy            :     -2.5474066790 
       CBS Total Energy                  :   -947.1990885414 
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 2
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       17
     number of electrons:                   94
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             434
     number of aux C basis functions:       1885
     number of aux J basis functions:       0
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           0.000000
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 2
   prop. index: 1
       Filename                          : /scratch/obeletsan/orcajob__14388-yp78/cc_ts2.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        5.3160057308
        Electronic Contribution:
                  0    
      0       9.404242
      1       8.368005
      2      -1.188506
        Nuclear Contribution:
                  0    
      0     -11.270596
      1      -9.192865
      2       1.647233
        Total Dipole moment:
                  0    
      0      -1.866354
      1      -0.824860
      2       0.458727
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     1 
    Coordinates:
               0 C     -2.418875000000   -0.444954000000    0.161002000000
               1 C     -1.429819000000    0.493735000000   -0.095209000000
               2 C     -2.071626000000   -1.770899000000    0.416499000000
               3 C     -0.095579000000    0.087663000000   -0.053622000000
               4 C      0.265507000000   -1.242846000000    0.165406000000
               5 C     -0.735013000000   -2.169490000000    0.418041000000
               6 H     -1.672385000000    1.527869000000   -0.316790000000
               7 C      1.019063000000    1.158141000000   -0.099213000000
               8 H     -3.462466000000   -0.146751000000    0.146942000000
               9 H     -2.851028000000   -2.504369000000    0.600590000000
              10 H      1.312393000000   -1.528134000000    0.144560000000
              11 H     -0.475109000000   -3.207076000000    0.602691000000
              12 N      1.094145000000    1.082448000000   -1.354248000000
              13 O      2.480081000000    2.569269000000   -1.487510000000
              14 O      1.535101000000    1.742441000000    0.883493000000
              15 S      3.237708000000    2.688426000000   -0.179329000000
              16 O      4.267902000000    1.664527000000    0.046697000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     2 
    Coordinates:
               0 C     -2.418875000000   -0.444954000000    0.161002000000
               1 C     -1.429819000000    0.493735000000   -0.095209000000
               2 C     -2.071626000000   -1.770899000000    0.416499000000
               3 C     -0.095579000000    0.087663000000   -0.053622000000
               4 C      0.265507000000   -1.242846000000    0.165406000000
               5 C     -0.735013000000   -2.169490000000    0.418041000000
               6 H     -1.672385000000    1.527869000000   -0.316790000000
               7 C      1.019063000000    1.158141000000   -0.099213000000
               8 H     -3.462466000000   -0.146751000000    0.146942000000
               9 H     -2.851028000000   -2.504369000000    0.600590000000
              10 H      1.312393000000   -1.528134000000    0.144560000000
              11 H     -0.475109000000   -3.207076000000    0.602691000000
              12 N      1.094145000000    1.082448000000   -1.354248000000
              13 O      2.480081000000    2.569269000000   -1.487510000000
              14 O      1.535101000000    1.742441000000    0.883493000000
              15 S      3.237708000000    2.688426000000   -0.179329000000
              16 O      4.267902000000    1.664527000000    0.046697000000
