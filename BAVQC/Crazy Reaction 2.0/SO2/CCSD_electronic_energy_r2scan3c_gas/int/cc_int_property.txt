-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -944.6451128610
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 17
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 18
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.1428     6.0000    -0.1428     3.7845     3.7845    -0.0000
  1   0     6.1397     6.0000    -0.1397     3.8384     3.8384    -0.0000
  2   0     6.1388     6.0000    -0.1388     3.8203     3.8203     0.0000
  3   0     5.9517     6.0000     0.0483     3.7655     3.7655     0.0000
  4   0     6.1552     6.0000    -0.1552     3.8464     3.8464    -0.0000
  5   0     6.1426     6.0000    -0.1426     3.7892     3.7892    -0.0000
  6   0     0.8257     1.0000     0.1743     0.9915     0.9915    -0.0000
  7   0     5.7164     6.0000     0.2836     4.0782     4.0782    -0.0000
  8   0     0.8402     1.0000     0.1598     0.9763     0.9763    -0.0000
  9   0     0.8387     1.0000     0.1613     0.9766     0.9766     0.0000
 10   0     0.8282     1.0000     0.1718     0.9898     0.9898     0.0000
 11   0     0.8416     1.0000     0.1584     0.9771     0.9771     0.0000
 12   0     7.0963     7.0000    -0.0963     2.9023     2.9023     0.0000
 13   0     8.3515     8.0000    -0.3515     1.9253     1.9253     0.0000
 14   0     8.3979     8.0000    -0.3979     2.0546     2.0546    -0.0000
 15   0    15.0957    16.0000     0.9043     3.8746     3.8746     0.0000
 16   0     8.4968     8.0000    -0.4968     2.0066     2.0066     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.398048
                0             6               2            6                1.351587
                0             6               8            1                0.977098
                1             6               3            6                1.325784
                1             6               6            1                0.961580
                2             6               5            6                1.385207
                2             6               9            1                0.976673
                3             6               4            6                1.356487
                3             6               7            6                1.064935
                4             6               5            6                1.368707
                4             6              10            1                0.962368
                5             6              11            1                0.977546
                7             6              12            7                1.903933
                7             6              14            8                1.066447
               12             7              13            8                0.865241
               13             8              15           16                0.976280
               14             8              15           16                0.918116
               15            16              16            8                1.923530
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 1
   prop. index: 1
        Total number of Electrons                                                     94
        Number of correlated Electrons                                                62
        Number of alpha correlated Electrons                                          31
        Number of beta correlated Electrons                                           31
        Reference Energy:                                                -944.6451128695
        Total Correlation Energy:                                          -2.3029277038
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1125906367
        Total MDCI Energy:                                               -946.9480405733
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:     -944.6925873473
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 2
   prop. index: 1
     Number of atoms                     : 17
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 18
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.1116     6.0000    -0.1116     3.7930     3.7930    -0.0000
  1   0     6.1242     6.0000    -0.1242     3.7968     3.7968     0.0000
  2   0     6.1325     6.0000    -0.1325     3.8286     3.8286    -0.0000
  3   0     6.0575     6.0000    -0.0575     3.7842     3.7842    -0.0000
  4   0     6.1296     6.0000    -0.1296     3.7960     3.7960     0.0000
  5   0     6.1118     6.0000    -0.1118     3.8017     3.8017     0.0000
  6   0     0.8266     1.0000     0.1734     1.0030     1.0030     0.0000
  7   0     5.6239     6.0000     0.3761     4.0715     4.0715     0.0000
  8   0     0.8470     1.0000     0.1530     0.9847     0.9847     0.0000
  9   0     0.8404     1.0000     0.1596     0.9853     0.9853    -0.0000
 10   0     0.8357     1.0000     0.1643     1.0075     1.0075     0.0000
 11   0     0.8493     1.0000     0.1507     0.9864     0.9864    -0.0000
 12   0     7.1301     7.0000    -0.1301     2.9242     2.9242     0.0000
 13   0     8.3812     8.0000    -0.3812     1.9072     1.9072    -0.0000
 14   0     8.4552     8.0000    -0.4552     1.9903     1.9903     0.0000
 15   0    14.9851    16.0000     1.0149     3.8209     3.8209     0.0000
 16   0     8.5583     8.0000    -0.5583     1.9503     1.9503    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.413209
                0             6               2            6                1.366571
                0             6               8            1                0.986321
                1             6               3            6                1.293841
                1             6               6            1                0.971869
                2             6               5            6                1.400787
                2             6               9            1                0.987666
                3             6               4            6                1.316547
                3             6               7            6                1.144961
                4             6               5            6                1.386518
                4             6              10            1                0.975580
                5             6              11            1                0.987474
                7             6              12            7                1.903364
                7             6              14            8                1.038175
               12             7              13            8                0.868493
               13             8              15           16                0.966001
               14             8              15           16                0.892458
               15            16              16            8                1.898860
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 2
   prop. index: 1
        Total number of Electrons                                                     94
        Number of correlated Electrons                                                62
        Number of alpha correlated Electrons                                          31
        Number of beta correlated Electrons                                           31
        Reference Energy:                                                -944.6925873415
        Total Correlation Energy:                                          -2.4440433714
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1231307829
        Total MDCI Energy:                                               -947.1366307129
# -----------------------------------------------------------
$ Energy_Extrapolation
   description: The Extrapolation of the energies
   geom. index: 2
   prop. index: 1
       Do EP1                            : 1 
       Do EP2                            : 0 
       Do EP3                            : 0 
       Number of energies                : 2 
       SCF energy with basis  cc-pVTZ :   -944.6451128695 
       SCF energy with basis  cc-pVQZ :   -944.6925873415 
       CBS SCF Energy                    :   -944.7068915862 
       Correlation energy with basis  cc-pVTZ :   -944.6451128695 
       Correlation energy with basis  cc-pVQZ :   -944.6925873415 
       CBS Correlation Energy            :     -2.5445021311 
       CBS Total Energy                  :   -947.2513937173 
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 2
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       17
     number of electrons:                   94
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             434
     number of aux C basis functions:       1885
     number of aux J basis functions:       0
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           0.000000
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 2
   prop. index: 1
       Filename                          : /scratch/obeletsan/orcajob__14383-QpXH/cc_int.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        4.0822709304
        Electronic Contribution:
                  0    
      0      10.333580
      1       7.962213
      2      -0.635952
        Nuclear Contribution:
                  0    
      0     -11.173224
      1      -8.820987
      2       1.702217
        Total Dipole moment:
                  0    
      0      -0.839644
      1      -0.858774
      2       1.066266
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     1 
    Coordinates:
               0 C      0.080939000000    0.089224000000   -0.096399000000
               1 C      1.066144000000    1.053886000000   -0.232948000000
               2 C      0.385478000000   -1.156396000000    0.451921000000
               3 C      2.376079000000    0.771880000000    0.177857000000
               4 C      2.682517000000   -0.476726000000    0.727737000000
               5 C      1.684674000000   -1.434602000000    0.862630000000
               6 H      0.836295000000    2.026532000000   -0.655234000000
               7 C      3.415652000000    1.785966000000    0.033438000000
               8 H     -0.932833000000    0.309274000000   -0.416954000000
               9 H     -0.391430000000   -1.907685000000    0.558059000000
              10 H      3.697373000000   -0.694535000000    1.041986000000
              11 H      1.926110000000   -2.403472000000    1.288971000000
              12 N      3.256906000000    2.904650000000   -0.577358000000
              13 O      4.445167000000    3.648731000000   -0.498639000000
              14 O      4.639541000000    1.525509000000    0.594044000000
              15 S      5.786763000000    2.670401000000   -0.001825000000
              16 O      6.419025000000    2.084363000000   -1.186585000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     2 
    Coordinates:
               0 C      0.080939000000    0.089224000000   -0.096399000000
               1 C      1.066144000000    1.053886000000   -0.232948000000
               2 C      0.385478000000   -1.156396000000    0.451921000000
               3 C      2.376079000000    0.771880000000    0.177857000000
               4 C      2.682517000000   -0.476726000000    0.727737000000
               5 C      1.684674000000   -1.434602000000    0.862630000000
               6 H      0.836295000000    2.026532000000   -0.655234000000
               7 C      3.415652000000    1.785966000000    0.033438000000
               8 H     -0.932833000000    0.309274000000   -0.416954000000
               9 H     -0.391430000000   -1.907685000000    0.558059000000
              10 H      3.697373000000   -0.694535000000    1.041986000000
              11 H      1.926110000000   -2.403472000000    1.288971000000
              12 N      3.256906000000    2.904650000000   -0.577358000000
              13 O      4.445167000000    3.648731000000   -0.498639000000
              14 O      4.639541000000    1.525509000000    0.594044000000
              15 S      5.786763000000    2.670401000000   -0.001825000000
              16 O      6.419025000000    2.084363000000   -1.186585000000
