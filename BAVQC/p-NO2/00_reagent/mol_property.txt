-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -604.0608729021
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                 41.9999927958 
   Number of Beta  Electrons                 41.9999927958 
   Total number of  Electrons                83.9999855915 
   Exchange energy                          -75.8875631724 
   Correlation energy                        -2.8116991033 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -78.6992622756 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -604.0608729021 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 1
   prop. index: 1
        Van der Waals Correction:       -0.0059426434
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:     -604.0608727167
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 2
   prop. index: 1
   Number of Alpha Electrons                 41.9999927799 
   Number of Beta  Electrons                 41.9999927799 
   Total number of  Electrons                83.9999855598 
   Exchange energy                          -75.8875829182 
   Correlation energy                        -2.8116993904 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -78.6992823086 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -604.0608727167 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 2
   prop. index: 1
        Van der Waals Correction:       -0.0059426432
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 2
   prop. index: 1
       Filename                          : mol.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        1.2676207748
        Electronic Contribution:
                  0    
      0       0.695093
      1       0.838408
      2       0.046338
        Nuclear Contribution:
                  0    
      0      -0.383683
      1      -0.449803
      2      -0.019465
        Total Dipole moment:
                  0    
      0       0.311410
      1       0.388605
      2       0.026872
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 3
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 3
   prop. index: 1
        SCF Energy:     -604.0608727097
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 3
   prop. index: 1
   Number of Alpha Electrons                 41.9999927798 
   Number of Beta  Electrons                 41.9999927798 
   Total number of  Electrons                83.9999855597 
   Exchange energy                          -75.8875774282 
   Correlation energy                        -2.8116994598 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -78.6992768880 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -604.0608727097 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 3
   prop. index: 1
        Van der Waals Correction:       -0.0059426432
# -----------------------------------------------------------
$ THERMOCHEMISTRY_Energies
   description: The Thermochemistry energies
   geom. index: 3
   prop. index: 1
        Temperature (Kelvin)           :        298.1500000000
        Pressure (atm)                 :          1.0000000000
        Total Mass (AMU)               :        164.0221919965
        Spin Degeneracy                :          1.0000000000
        Electronic Energy (Hartree)    :       -604.0558072856
        Translational Energy (Hartree) :          0.0014162714
        Rotational Energy (Hartree)    :          0.0014162714
        Vibrational Energy (Hartree)   :          0.0071200692
        Number of frequencies          :     48      
        Scaling Factor for frequencies :          1.0000000000
        Vibrational frequencies        :     
                  0    
      0       0.000000
      1       0.000000
      2       0.000000
      3       0.000000
      4       0.000000
      5       0.000000
      6      17.833210
      7      49.291679
      8      59.105396
      9     142.186308
     10     239.101521
     11     306.468200
     12     308.645300
     13     412.279101
     14     412.634554
     15     465.341206
     16     484.335161
     17     485.937936
     18     539.306403
     19     541.767801
     20     649.707355
     21     686.936844
     22     758.472398
     23     758.780121
     24     839.855983
     25     865.019668
     26     869.676357
     27     978.007381
     28     985.027230
     29     1034.643812
     30     1112.845929
     31     1133.389578
     32     1135.074433
     33     1206.121378
     34     1315.780296
     35     1350.318412
     36     1352.734318
     37     1433.822845
     38     1459.423129
     39     1534.328816
     40     1564.851262
     41     1625.943353
     42     1628.377574
     43     2431.660864
     44     3198.073324
     45     3198.642830
     46     3220.009885
     47     3220.967310
        Zero Point Energy (Hartree)    :          0.1048247040
        Inner Energy (Hartree)         :       -603.9410299696
        Enthalpy (Hartree)             :       -603.9400857605
        Electronic entropy             :          0.0000000000
        Rotational entropy             :          0.0146126541
        Vibrational entropy            :          0.0125488458
        Translational entropy          :          0.0146126541
        Entropy                        :          0.0467335577
        Gibbs Energy (Hartree)         :       -603.9868193183
        Is Linear                      :                 false
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    16 
    Geometry Index:     1 
    Coordinates:
               0 C     -0.117478610568   -0.068105614508    0.349180653144
               1 C      0.756864701756    1.001823072548    0.414121504196
               2 C      0.394515494709   -1.359580320643    0.404347562442
               3 C      2.141293797877    0.776879487750    0.533991853136
               4 C      2.631695570446   -0.541641109739    0.588018088441
               5 C      1.756809578140   -1.611043350418    0.522717881882
               6 H      0.382412832186    2.018769356671    0.373210052791
               7 C      3.034242170788    1.868808737464    0.600192251899
               8 H     -1.186589092135    0.081683614555    0.256659525981
               9 N     -0.537994320715   -2.500195641461    0.335215772723
              10 H      3.698755445860   -0.711502165052    0.680894108816
              11 H      2.112101500860   -2.633908740006    0.561893774288
              12 N      3.781251220286    2.762555937631    0.647045948151
              13 O      4.547749579530    3.683707443169    0.696179669812
              14 O     -1.732108393804   -2.247793324882    0.233956018383
              15 O     -0.057451475217   -3.625617383079    0.383635333915
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    16 
    Geometry Index:     2 
    Coordinates:
               0 C     -0.117464285037   -0.068087229380    0.349149730025
               1 C      0.756954126829    1.001796372208    0.414088694366
               2 C      0.394500371963   -1.359569345335    0.404335930200
               3 C      2.141371940952    0.776807513464    0.533948106474
               4 C      2.631718758322   -0.541714835518    0.587972096267
               5 C      1.756797595515   -1.611120452197    0.522703954438
               6 H      0.382491812678    2.018740445243    0.373174478097
               7 C      3.034342134924    1.868711364324    0.600154992654
               8 H     -1.186544697748    0.081875278576    0.256625384157
               9 N     -0.538014515929   -2.500179867816    0.335234741003
              10 H      3.698768089049   -0.711658257255    0.680833924336
              11 H      2.112142375281   -2.633952798379    0.561903145296
              12 N      3.781189268287    2.762594501319    0.647051951534
              13 O      4.547440415429    3.683941066204    0.696327050880
              14 O     -1.732110726904   -2.247733814210    0.234066145806
              15 O     -0.057512663613   -3.625609941248    0.383689674468
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    16 
    Geometry Index:     3 
    Coordinates:
               0 C     -0.117464285037   -0.068087229380    0.349149730025
               1 C      0.756954126829    1.001796372208    0.414088694366
               2 C      0.394500371963   -1.359569345335    0.404335930200
               3 C      2.141371940952    0.776807513464    0.533948106474
               4 C      2.631718758322   -0.541714835518    0.587972096267
               5 C      1.756797595515   -1.611120452197    0.522703954438
               6 H      0.382491812678    2.018740445243    0.373174478097
               7 C      3.034342134924    1.868711364324    0.600154992654
               8 H     -1.186544697748    0.081875278576    0.256625384157
               9 N     -0.538014515929   -2.500179867816    0.335234741003
              10 H      3.698768089049   -0.711658257255    0.680833924336
              11 H      2.112142375281   -2.633952798379    0.561903145296
              12 N      3.781189268287    2.762594501319    0.647051951534
              13 O      4.547440415429    3.683941066204    0.696327050880
              14 O     -1.732110726904   -2.247733814210    0.234066145806
              15 O     -0.057512663613   -3.625609941248    0.383689674468
