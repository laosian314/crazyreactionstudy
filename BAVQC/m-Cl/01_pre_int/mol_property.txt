-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:    -1407.7129198210
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                 55.0000075029 
   Number of Beta  Electrons                 55.0000075029 
   Total number of  Electrons               110.0000150057 
   Exchange energy                         -121.7744573735 
   Correlation energy                        -3.8457186779 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -125.6201760514 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1407.7129198210 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 1
   prop. index: 1
        Van der Waals Correction:       -0.0070418355
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:    -1407.7280716174
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 2
   prop. index: 1
   Number of Alpha Electrons                 55.0000053318 
   Number of Beta  Electrons                 55.0000053318 
   Total number of  Electrons               110.0000106637 
   Exchange energy                         -121.8536114944 
   Correlation energy                        -3.8504587778 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -125.7040702721 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1407.7280716174 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 2
   prop. index: 1
        Van der Waals Correction:       -0.0070877208
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 3
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 3
   prop. index: 1
        SCF Energy:    -1407.7292152447
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 3
   prop. index: 1
   Number of Alpha Electrons                 55.0000059432 
   Number of Beta  Electrons                 55.0000059432 
   Total number of  Electrons               110.0000118863 
   Exchange energy                         -121.8723017203 
   Correlation energy                        -3.8521701295 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -125.7244718498 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1407.7292152447 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 3
   prop. index: 1
        Van der Waals Correction:       -0.0071020808
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 4
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 4
   prop. index: 1
        SCF Energy:    -1407.7293039101
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 4
   prop. index: 1
   Number of Alpha Electrons                 55.0000049548 
   Number of Beta  Electrons                 55.0000049548 
   Total number of  Electrons               110.0000099095 
   Exchange energy                         -121.8647811801 
   Correlation energy                        -3.8516968308 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -125.7164780108 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1407.7293039101 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 4
   prop. index: 1
        Van der Waals Correction:       -0.0070973097
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 5
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 5
   prop. index: 1
        SCF Energy:    -1407.7292558954
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 5
   prop. index: 1
   Number of Alpha Electrons                 55.0000052687 
   Number of Beta  Electrons                 55.0000052687 
   Total number of  Electrons               110.0000105374 
   Exchange energy                         -121.8622437022 
   Correlation energy                        -3.8514760512 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -125.7137197534 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1407.7292558954 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 5
   prop. index: 1
        Van der Waals Correction:       -0.0070896318
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 6
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 6
   prop. index: 1
        SCF Energy:    -1407.7293034859
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 6
   prop. index: 1
   Number of Alpha Electrons                 55.0000048193 
   Number of Beta  Electrons                 55.0000048193 
   Total number of  Electrons               110.0000096386 
   Exchange energy                         -121.8626521865 
   Correlation energy                        -3.8514718761 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -125.7141240626 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1407.7293034859 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 6
   prop. index: 1
        Van der Waals Correction:       -0.0070932907
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 7
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 7
   prop. index: 1
        SCF Energy:    -1407.7293191811
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 7
   prop. index: 1
   Number of Alpha Electrons                 55.0000056000 
   Number of Beta  Electrons                 55.0000056000 
   Total number of  Electrons               110.0000111999 
   Exchange energy                         -121.8643065120 
   Correlation energy                        -3.8516296565 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -125.7159361684 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1407.7293191811 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 7
   prop. index: 1
        Van der Waals Correction:       -0.0070950039
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 8
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 8
   prop. index: 1
        SCF Energy:    -1407.7293180060
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 8
   prop. index: 1
   Number of Alpha Electrons                 55.0000058229 
   Number of Beta  Electrons                 55.0000058229 
   Total number of  Electrons               110.0000116458 
   Exchange energy                         -121.8638900709 
   Correlation energy                        -3.8515838130 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -125.7154738838 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1407.7293180060 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 8
   prop. index: 1
        Van der Waals Correction:       -0.0070941526
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 9
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 9
   prop. index: 1
        SCF Energy:    -1407.7293179862
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 9
   prop. index: 1
   Number of Alpha Electrons                 55.0000059760 
   Number of Beta  Electrons                 55.0000059760 
   Total number of  Electrons               110.0000119519 
   Exchange energy                         -121.8639119514 
   Correlation energy                        -3.8515834983 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -125.7154954498 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1407.7293179862 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 9
   prop. index: 1
        Van der Waals Correction:       -0.0070941711
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 9
   prop. index: 1
       Filename                          : mol.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        2.8685874898
        Electronic Contribution:
                  0    
      0      -0.269094
      1       5.005255
      2      -1.518886
        Nuclear Contribution:
                  0    
      0      -0.449324
      1      -5.717264
      2       2.019466
        Total Dipole moment:
                  0    
      0      -0.718418
      1      -0.712010
      2       0.500580
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 10
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 10
   prop. index: 1
        SCF Energy:    -1407.7293179818
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 10
   prop. index: 1
   Number of Alpha Electrons                 55.0000059759 
   Number of Beta  Electrons                 55.0000059759 
   Total number of  Electrons               110.0000119519 
   Exchange energy                         -121.8639108593 
   Correlation energy                        -3.8515835514 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -125.7154944107 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1407.7293179818 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 10
   prop. index: 1
        Van der Waals Correction:       -0.0070941711
# -----------------------------------------------------------
$ THERMOCHEMISTRY_Energies
   description: The Thermochemistry energies
   geom. index: 10
   prop. index: 1
        Temperature (Kelvin)           :        298.1500000000
        Pressure (atm)                 :          1.0000000000
        Total Mass (AMU)               :        216.9600418485
        Spin Degeneracy                :          1.0000000000
        Electronic Energy (Hartree)    :      -1407.7234850157
        Translational Energy (Hartree) :          0.0014162714
        Rotational Energy (Hartree)    :          0.0014162714
        Vibrational Energy (Hartree)   :          0.0104605364
        Number of frequencies          :     51      
        Scaling Factor for frequencies :          1.0000000000
        Vibrational frequencies        :     
                  0    
      0       0.000000
      1       0.000000
      2       0.000000
      3       0.000000
      4       0.000000
      5       0.000000
      6      17.026165
      7      35.197160
      8      50.869227
      9      64.133506
     10      78.232986
     11      99.604063
     12     144.748092
     13     185.663924
     14     194.385977
     15     262.296350
     16     265.686390
     17     396.882107
     18     424.740767
     19     432.260059
     20     436.148041
     21     457.098567
     22     494.524777
     23     503.637015
     24     544.754989
     25     677.439127
     26     688.516131
     27     792.779321
     28     816.063173
     29     883.014461
     30     920.077264
     31     985.895043
     32     1017.399298
     33     1100.233817
     34     1107.762929
     35     1111.868877
     36     1142.020639
     37     1198.781963
     38     1274.127519
     39     1300.810613
     40     1346.754143
     41     1411.566653
     42     1451.527758
     43     1508.811303
     44     1588.327938
     45     1621.184440
     46     2432.446785
     47     3183.263374
     48     3203.592322
     49     3208.607465
     50     3209.556510
        Zero Point Energy (Hartree)    :          0.1008552078
        Inner Energy (Hartree)         :      -1407.6093367287
        Enthalpy (Hartree)             :      -1407.6083925196
        Electronic entropy             :          0.0000000000
        Rotational entropy             :          0.0155359908
        Vibrational entropy            :          0.0193106742
        Translational entropy          :          0.0155359908
        Entropy                        :          0.0548148702
        Gibbs Energy (Hartree)         :      -1407.6632073898
        Is Linear                      :                 false
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     1 
    Coordinates:
               0 C     -0.072470000000    0.061610000000   -0.194660000000
               1 C      0.686800000000    1.210780000000   -0.345440000000
               2 C      0.505400000000   -1.064520000000    0.384120000000
               3 C      2.023580000000    1.233150000000    0.082060000000
               4 C      2.590360000000    0.086870000000    0.664930000000
               5 C      1.829340000000   -1.060840000000    0.814580000000
               6 H      0.249290000000    2.095740000000   -0.795140000000
               7 C      2.811740000000    2.398270000000   -0.064920000000
               8 Cl    -1.861620000000    0.148310000000   -0.768370000000
               9 H     -0.144380000000   -2.081830000000    0.504930000000
              10 H      3.623170000000    0.113220000000    0.995570000000
              11 H      2.259020000000   -1.949820000000    1.263100000000
              12 N      3.521520000000    3.312580000000   -0.158960000000
              13 O      4.296150000000    4.243120000000   -0.231700000000
              14 O      5.478960000000    1.949530000000    1.466610000000
              15 S      6.414980000000    2.962790000000    0.933070000000
              16 O      7.172790000000    2.591870000000   -0.272230000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     2 
    Coordinates:
               0 C     -0.064327990568    0.059161579928   -0.188792481689
               1 C      0.679693472190    1.214660570211   -0.346592702748
               2 C      0.488955545474   -1.073102498705    0.381732153239
               3 C      2.015654265585    1.229975341725    0.081727707803
               4 C      2.581223107530    0.082547895363    0.663861927771
               5 C      1.812837086136   -1.063458970577    0.808908847184
               6 H      0.239362248877    2.098092676801   -0.796125312685
               7 C      2.803852869332    2.394521545155   -0.065115699201
               8 Cl    -1.768919978578    0.103239050953   -0.734896437093
               9 H     -0.105812155668   -1.977979749906    0.488061692263
              10 H      3.614266169741    0.107799634909    0.994764665014
              11 H      2.240240291888   -1.953707047138    1.255815661232
              12 N      3.514065486748    3.308198671043   -0.158997210631
              13 O      4.288456863269    4.237584291089   -0.231334840408
              14 O      5.471619704866    1.942782150307    1.465605951026
              15 S      6.408259164157    2.955564800121    0.932376368149
              16 O      7.165203849022    2.584950058720   -0.273450289226
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     3 
    Coordinates:
               0 C     -0.064564890922    0.064018318175   -0.186863123149
               1 C      0.677114647427    1.223084950553   -0.348095326074
               2 C      0.477847578393   -1.077602716005    0.380703291849
               3 C      2.013254639652    1.230957795004    0.080505441042
               4 C      2.578469038149    0.083005324130    0.661337615663
               5 C      1.804846496514   -1.059715020643    0.803809297414
               6 H      0.237900060035    2.106655145617   -0.796451505769
               7 C      2.803668909601    2.394687643690   -0.066079117353
               8 Cl    -1.722889540103    0.070265701091   -0.720129324331
               9 H     -0.119454870957   -1.968862146854    0.490173019576
              10 H      3.611522166166    0.107717649798    0.991083763579
              11 H      2.233703593063   -1.950436304558    1.250281534932
              12 N      3.513998858502    3.308433272829   -0.159065673186
              13 O      4.287785836526    4.238189295902   -0.229724734921
              14 O      5.473714781523    1.940673431473    1.465335658737
              15 S      6.410322947536    2.954207597809    0.933541183215
              16 O      7.167389748894    2.585550061989   -0.272812001223
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     4 
    Coordinates:
               0 C     -0.066054566045    0.065614064381   -0.188507002553
               1 C      0.676679071159    1.226052740072   -0.346254925881
               2 C      0.478978554784   -1.078632539894    0.378910368403
               3 C      2.012621940460    1.232795339409    0.083290381882
               4 C      2.579988681203    0.084796336328    0.661361395596
               5 C      1.806361007006   -1.057430177324    0.801312735905
               6 H      0.237215774569    2.110559943750   -0.792122558337
               7 C      2.801821889605    2.398318653288   -0.060066734098
               8 Cl    -1.730080389913    0.059518269571   -0.722909345191
               9 H     -0.123045484032   -1.974991837680    0.489673369643
              10 H      3.612906022478    0.110463386847    0.990773038350
              11 H      2.237392174624   -1.948323833321    1.246419009668
              12 N      3.514434857504    3.309670462983   -0.159412004995
              13 O      4.290806347658    4.236813310001   -0.237493112242
              14 O      5.474204623200    1.946733274490    1.472409283173
              15 S      6.413014291667    2.953897401268    0.932526599891
              16 O      7.167385204072    2.574975205831   -0.272360499213
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     5 
    Coordinates:
               0 C     -0.071443725574    0.070797981628   -0.186218935527
               1 C      0.684063701215    1.221049032998   -0.359871872511
               2 C      0.465131194078   -1.074448131579    0.393349606675
               3 C      2.021910558344    1.216924957876    0.063599675967
               4 C      2.581242063262    0.069980281809    0.651006102737
               5 C      1.794865950518   -1.060715772981    0.807664934898
               6 H      0.251874874220    2.105448800866   -0.812938746424
               7 C      2.822792658391    2.372688719373   -0.097817913316
               8 Cl    -1.741568794464    0.066462570683   -0.710580000305
               9 H     -0.148081712622   -1.961157790844    0.518586365242
              10 H      3.616156349410    0.088829465421    0.974013199110
              11 H      2.219793968136   -1.950971932266    1.260715756867
              12 N      3.517803201766    3.300879224099   -0.159151242028
              13 O      4.273344352617    4.247783695986   -0.192275537877
              14 O      5.493623131647    1.919438802432    1.440639819213
              15 S      6.414830104870    2.966644652691    0.948933645452
              16 O      7.188292124184    2.651195441808   -0.262104858173
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     6 
    Coordinates:
               0 C     -0.067967026571    0.068511534395   -0.189715752658
               1 C      0.677611871037    1.227269111504   -0.346812457652
               2 C      0.477249098563   -1.079042110115    0.379821092102
               3 C      2.013576449655    1.230181710114    0.082340823413
               4 C      2.581912602552    0.082356711317    0.659008827501
               5 C      1.804804953180   -1.056665207106    0.799468318027
               6 H      0.238700634272    2.112818412709   -0.791182734112
               7 C      2.803231977736    2.396341430714   -0.061133141198
               8 Cl    -1.736455000462    0.049748602031   -0.719335688755
               9 H     -0.128433046901   -1.970206215835    0.492235478812
              10 H      3.615248462748    0.107713576259    0.986335330900
              11 H      2.236717377175   -1.947864476749    1.244354860267
              12 N      3.514793018438    3.308566882318   -0.160555516425
              13 O      4.289945371309    4.236763510815   -0.239024366053
              14 O      5.477273853404    1.949547543650    1.475389603664
              15 S      6.415404581005    2.957015695127    0.935177962388
              16 O      7.171014822859    2.577773288852   -0.268822640221
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     7 
    Coordinates:
               0 C     -0.067207899656    0.067985471731   -0.188244572089
               1 C      0.677934030716    1.225891477173   -0.347473159847
               2 C      0.476853286004   -1.078287365848    0.380920657959
               3 C      2.014232646384    1.229374772502    0.080729494225
               4 C      2.581332854042    0.081584917512    0.658883927792
               5 C      1.804244908279   -1.057926858796    0.800655307595
               6 H      0.239190680170    2.110889033677   -0.793271650234
               7 C      2.804563173408    2.394473259249   -0.064873131750
               8 Cl    -1.733372543285    0.056493179362   -0.718318051673
               9 H     -0.129300913563   -1.970966270677    0.493051576654
              10 H      3.614785129978    0.106443425589    0.986253481695
              11 H      2.235071213073   -1.949041903171    1.246141422610
              12 N      3.514931601473    3.307943195856   -0.160942946785
              13 O      4.288573229959    4.237682238000   -0.235153444495
              14 O      5.477091341927    1.946730264606    1.471699874652
              15 S      6.414155253573    2.957626827095    0.936045096380
              16 O      7.171552007517    2.583934336139   -0.268553882690
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     8 
    Coordinates:
               0 C     -0.067738918304    0.068472649855   -0.188088532016
               1 C      0.678232544816    1.225814263518   -0.347948394272
               2 C      0.476210823623   -1.078215573456    0.381647280521
               3 C      2.014747367959    1.228443838098    0.079732956594
               4 C      2.581701555484    0.080687454714    0.657917576179
               5 C      1.803755779075   -1.058102985604    0.800533289152
               6 H      0.239776529960    2.110932774739   -0.793751334002
               7 C      2.805546844643    2.393231602882   -0.066736289419
               8 Cl    -1.735006426590    0.055924134045   -0.717016145498
               9 H     -0.130756758532   -1.969726093506    0.494494524282
              10 H      3.615322953888    0.105267449290    0.984645835122
              11 H      2.234574614807   -1.949203340651    1.246231660694
              12 N      3.515223590310    3.307386451121   -0.161463510316
              13 O      4.287978173371    4.238007171442   -0.234032864320
              14 O      5.477794726438    1.946326403743    1.471209544865
              15 S      6.414364583923    2.958599884257    0.937348542925
              16 O      7.172902015129    2.586983915514   -0.267174140490
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     9 
    Coordinates:
               0 C     -0.067811488812    0.068589435182   -0.187766843324
               1 C      0.678105637842    1.225881967334   -0.347697354758
               2 C      0.476346142130   -1.078247955613    0.381689261150
               3 C      2.014794564323    1.228392423244    0.079582569070
               4 C      2.581912277197    0.080607936088    0.657442917073
               5 C      1.803926413027   -1.058223988552    0.800146719031
               6 H      0.239503515207    2.111050830208   -0.793256707735
               7 C      2.805474847151    2.393251033488   -0.067049224276
               8 Cl    -1.735271433360    0.055975613610   -0.716090508028
               9 H     -0.130880480611   -1.969557328623    0.494431129564
              10 H      3.615625714624    0.105217575784    0.983864075886
              11 H      2.234975843676   -1.949326400524    1.245627466252
              12 N      3.515328987839    3.307223776486   -0.162140439948
              13 O      4.288221833154    4.237696784201   -0.235112973112
              14 O      5.477171808278    1.947180248706    1.472113130967
              15 S      6.414186161550    2.958839680864    0.937905027786
              16 O      7.173019656785    2.586278368117   -0.266138245599
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     10 
    Coordinates:
               0 C     -0.067811488812    0.068589435182   -0.187766843324
               1 C      0.678105637842    1.225881967334   -0.347697354758
               2 C      0.476346142130   -1.078247955613    0.381689261150
               3 C      2.014794564323    1.228392423244    0.079582569070
               4 C      2.581912277197    0.080607936088    0.657442917073
               5 C      1.803926413027   -1.058223988552    0.800146719031
               6 H      0.239503515207    2.111050830208   -0.793256707735
               7 C      2.805474847151    2.393251033488   -0.067049224276
               8 Cl    -1.735271433360    0.055975613610   -0.716090508028
               9 H     -0.130880480611   -1.969557328623    0.494431129564
              10 H      3.615625714624    0.105217575784    0.983864075886
              11 H      2.234975843676   -1.949326400524    1.245627466252
              12 N      3.515328987839    3.307223776486   -0.162140439948
              13 O      4.288221833154    4.237696784201   -0.235112973112
              14 O      5.477171808278    1.947180248706    1.472113130967
              15 S      6.414186161550    2.958839680864    0.937905027786
              16 O      7.173019656785    2.586278368117   -0.266138245599
