#!/bin/sh

#======
ready() 
#======
{

if test ! -z "$SCM_RESULTDIR" -a ! -z "$JOBNAME"
then

#  special case to make sure results from chained jobs are copied back, sub jobs have path in JOBNAME
   AAA=`dirname "$JOBNAME"`
   JOBNAME=`basename "$JOBNAME"`
   if [ "$AAA" != "." ]; then
      SCM_RESULTDIR=$SCM_RESULTDIR/$AAA
   fi

    # -------------------------------------------
    # copy any files produced to result directory
    # -------------------------------------------

    for n in 13 21 41 10 11 15
    do
        cp -f -p "TAPE$n" "$SCM_RESULTDIR/$JOBNAME.t$n" 2>/dev/null
        rm -f "TAPE$n"
    done

    cp -f -p RUNKF "$SCM_RESULTDIR/$JOBNAME.runkf" 2>/dev/null
    cp -f -p COSKF "$SCM_RESULTDIR/$JOBNAME.coskf" 2>/dev/null
    cp -f -p CRSKF "$SCM_RESULTDIR/$JOBNAME.crskf" 2>/dev/null
    cp -f -p QUILDKF "$SCM_RESULTDIR/$JOBNAME.qkf" 2>/dev/null
    cp -f -p quildjob.qkf "$SCM_RESULTDIR/$JOBNAME.qkf" 2>/dev/null
    rm -f  QUILDKF quildjob.qkf COSKF CRSKF RUNKF

    if test ! -d "$SCM_RESULTDIR/$JOBNAME.results"
    then
        mkdir -p "$SCM_RESULTDIR/$JOBNAME.results"
    fi
    if test -d "$SCM_RESULTDIR/$JOBNAME.results"
    then
        for n in 13 21 41 10 11 15
        do
            cp -f -p "TAPE$n" "$SCM_RESULTDIR/$JOBNAME.t$n" 2>/dev/null
            rm -f "TAPE$n"
        done
        touch thisfileshouldbeignored
        cp -rf -p * "$SCM_RESULTDIR/$JOBNAME.results"
    else
        echo Problem copying result files to \"$SCM_RESULTDIR/$JOBNAME.results\"
    fi

    for f in "$SCM_RESULTDIR/$JOBNAME.results"/* thisfiledoesnotexist
    do
        if test -L "$f"; then
            rm -f "$f"
        fi
    done
    rm -f "$SCM_RESULTDIR/$JOBNAME.results"/thisfileshouldbeignored

    if test -z "$1"
    then
        echo "Job $JOBNAME has finished" >> "$SCM_RESULTDIR/$JOBNAME.logfile"
    else
        echo "Job $JOBNAME: $1" >> "$SCM_RESULTDIR/$JOBNAME.logfile"
    fi
    cd "$OWD" 2>/dev/null
    rm -rf "$SCM_JOBDIR"
    if test -z "$SCM_NOREADY"; then
        echo "# status: ready" >> "$SCM_RESULTDIR/$JOBNAME.pid"
#       densf runs from AMSview remove the line above, for that reason an extra NOP line is needed
        DUMMY=blurb
    fi
fi
}


#-----------------------------------------
# keep track of original working directory
#-----------------------------------------

OWD="`pwd`"
export OWD

# -------------------------------------------------
# insert prolog command as defined in AMSjobs queue
# -------------------------------------------------

# <<PROLOG>>

# ---------------------------------------------------
# execute arguments (possibly to set environment etc)
# ---------------------------------------------------

if test ! -z "$1"
then
    if test "$1" != "export NSCM=" -a "$1" != "NSCM= ; export NSCM"; then
       eval $1
    fi
fi

# ---------------
# name of the job
# ---------------

JOBNAME=`basename "$0" .job`
export JOBNAME

# -----------------------------------------------------------------------------------
# on a batch system, the start.cmd will replace the next line to set JOBNAME properly
# since the batch system may make a copy of the job script
# -----------------------------------------------------------------------------------

# <<JOBNAME>>

# -------------------------------------------------
# determine DIRN: full path of the script directory
# -------------------------------------------------

DN="`dirname "$0"`"
case "$DN" in
/*)
DIRN="$DN"
;;
?:*)
DIRN="$DN"
;;
*)
DIRN="`pwd`/$DN"
;;
esac

# --------------------------------------------------------------------------------
# on a batch system, the start.cmd will replace the next line to set DIRN properly
# since the batch system may make a copy of the job script
# --------------------------------------------------------------------------------

# <<FULL PATH OF SCRIPT DIRECTORY DIRN>>

# ------------------------------------------------------------------------------------
# set SCM_RESULTDIR to DIRN, so results end up in the full path as required by ADFJOBS
# ------------------------------------------------------------------------------------

SCM_RESULTDIR="$DIRN"
export SCM_RESULTDIR

#----------------------------
# for AMS, set AMS_RESULTSDIR
#----------------------------

AMS_RESULTSDIR="$SCM_RESULTDIR/$JOBNAME.results"
export AMS_RESULTSDIR
rm -rf "$AMS_RESULTSDIR"
mkdir -p "$AMS_RESULTSDIR"
unset AMS_SWITCH_LOGFILE_AND_STDOUT

# ----------------------------------
# pass the location of the info file
# ----------------------------------

SCM_INFOFILE="$SCM_RESULTDIR/$JOBNAME.info"
export SCM_INFOFILE

# ---------------------------------
# update the status in the pid file
# ---------------------------------

echo "# pid: $$" >> "$SCM_RESULTDIR/$JOBNAME.pid"
echo "# status: running" >> "$SCM_RESULTDIR/$JOBNAME.pid"


# ---------------
# prepare logfile
# ---------------

if test -s "$SCM_RESULTDIR/$JOBNAME.logfile"
then
    cp "$SCM_RESULTDIR/$JOBNAME.logfile" "$SCM_RESULTDIR/$JOBNAME.logfile~"
fi
cat /dev/null > "$SCM_RESULTDIR/$JOBNAME.logfile"

SCM_LOGFILE="$SCM_RESULTDIR/$JOBNAME.logfile"
export SCM_LOGFILE


# <<FRAGMENT JOB SCRIPTS>>


# ------------------------------------------------------------------
# within SCM_RESULTDIR, make a new empty directory just for this job
# ------------------------------------------------------------------

XX=0
SCM_JOBDIR="$SCM_RESULTDIR/tmp.$USER.$$.$XX.noindex"
while test -d "$SCM_JOBDIR" 
do
   XX=`expr $XX + 1`
   SCM_JOBDIR="$SCM_RESULTDIR/tmp.$USER.$$.$XX.noindex"
done
export SCM_JOBDIR
mkdir "$SCM_JOBDIR"
echo "# jobdir: localhost:$SCM_JOBDIR" >> "$SCM_INFOFILE"

if test ! -d "$SCM_JOBDIR"
then
    echo "Could not create temporary directory $SCM_JOBDIR" >> "$SCM_RESULTDIR/$JOBNAME.logfile"
    echo "# status: killed" >> "$SCM_RESULTDIR/$JOBNAME.pid"
    exit
fi

# -----------------------------------------------------
# nodeinfo may be needed, and keep backup of old output
# -----------------------------------------------------

if test -f nodeinfo 
then
    cp nodeinfo "$SCM_JOBDIR/nodeinfo"
fi
cd "$SCM_JOBDIR"

if test -s "$SCM_RESULTDIR/$JOBNAME.out"
then
    mv "$SCM_RESULTDIR/$JOBNAME.out" "$SCM_RESULTDIR/$JOBNAME.out~"
fi

# <<COPY FRAGMENT FILES>>

# -------------------------------------------------------
# on normal or abnormal exits, clean up and update status
# -------------------------------------------------------

trap "ready \"abnormal exit (signal 1)\"; exit"  1 
trap "ready \"abnormal exit (signal 2)\"; exit"  2 
trap "ready \"abnormal exit (signal 3)\"; exit"  3
trap "ready \"abnormal exit (signal 15)\"; exit" 15

# -------------------------------------
# define easy vars for output and error
# -------------------------------------

SCM_OUT="$SCM_RESULTDIR/$JOBNAME.out"
export SCM_OUT
SCM_ERR="$SCM_RESULTDIR/$JOBNAME.err"
export SCM_ERR


# ----------------------
# execute the run script
# ----------------------

cat << \EORTHEJOB > job
#!/bin/sh

"$AMSBIN/ams" << eor

Task GeometryOptimization
UseSymmetry No
Properties
    NormalModes Yes
End
NormalModes
    Hessian Numerical
End
System
    Atoms
        C 1.31345376814531 0.11905178250098 1.71310371684857 adf.R=1.700
        C 2.15665325260215 0.75629228144299 0.80113837992149 adf.R=1.700
        C 0.48434561034831 -0.93268703602353 1.30296605919931 adf.R=1.700
        C 2.16150068843991 0.32812329635502 -0.53742140285648 adf.R=1.700
        C 1.33738600925956 -0.72529614863869 -0.95850323826276 adf.R=1.700
        C 0.500658066246 -1.3523946422646 -0.0324172798414 adf.R=1.700
        C 3.79090422861094 1.74837636506946 -1.85178758878132 adf.R=1.700
        N 2.98939554673704 0.95255967356226 -1.46847692117091 adf.R=1.608
        O 6.86857766433636 3.89652111203135 0.1744914023874 adf.R=1.517
        O 4.59766318873247 2.5228911434019 -2.32180870904942 adf.R=1.517
        S 5.32396723984347 4.18920219112083 -0.1669036448653 adf.R=1.792
        O 4.30165790667784 3.27325454163252 0.6821606879891901 adf.R=1.517
        H 2.8070989922022 1.56999798456158 1.1014847851629 adf.R=1.350
        H 1.30591070779215 0.44666568117239 2.74729367516886 adf.R=1.350
        H -0.16791113600342 -1.42168676955135 2.01862557655726 adf.R=1.350
        H 1.36156401745979 -1.03778137924803 -1.99575804021669 adf.R=1.350
        H -0.1373257514302 -2.16819107712509 -0.35528545819073 adf.R=1.350
    End
    BondOrders
         1 2 1.5
         1 3 1.5
         1 14 1.0
         2 4 1.5
         2 13 1.0
         3 6 1.5
         3 15 1.0
         4 5 1.5
         4 8 1.0
         5 6 1.5
         5 16 1.0
         6 17 1.0
         7 8 2.0
         7 10 2.0
         9 11 2.0
         11 12 2.0
    End
End

Engine ADF
    Basis
        Type TZ2P
        Core None
    End
    XC
        LibXC r2SCAN
        DISPERSION GRIMME4
    End
    Solvation
        Surf Delley
        Solv name=Toluene cav0=0.0 cav1=0.0067639
        Charged method=CONJ
        C-Mat POT
        SCF VAR ALL 
        CSMRSP
    End
    NumericalQuality Good
EndEngine
eor



EORTHEJOB

chmod u+x job
./job >>"$SCM_RESULTDIR/$JOBNAME.out" 2>> "$SCM_RESULTDIR/$JOBNAME.err"
rm job

# <<EPILOG>>

ready

