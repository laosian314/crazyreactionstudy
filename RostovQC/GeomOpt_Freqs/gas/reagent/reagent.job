#!/bin/sh

#======
ready() 
#======
{

if test ! -z "$SCM_RESULTDIR" -a ! -z "$JOBNAME"
then

#  special case to make sure results from chained jobs are copied back, sub jobs have path in JOBNAME
   AAA=`dirname "$JOBNAME"`
   JOBNAME=`basename "$JOBNAME"`
   if [ "$AAA" != "." ]; then
      SCM_RESULTDIR=$SCM_RESULTDIR/$AAA
   fi

    # -------------------------------------------
    # copy any files produced to result directory
    # -------------------------------------------

    for n in 13 21 41 10 11 15
    do
        cp -f -p "TAPE$n" "$SCM_RESULTDIR/$JOBNAME.t$n" 2>/dev/null
        rm -f "TAPE$n"
    done

    cp -f -p RUNKF "$SCM_RESULTDIR/$JOBNAME.runkf" 2>/dev/null
    cp -f -p COSKF "$SCM_RESULTDIR/$JOBNAME.coskf" 2>/dev/null
    cp -f -p CRSKF "$SCM_RESULTDIR/$JOBNAME.crskf" 2>/dev/null
    cp -f -p QUILDKF "$SCM_RESULTDIR/$JOBNAME.qkf" 2>/dev/null
    cp -f -p quildjob.qkf "$SCM_RESULTDIR/$JOBNAME.qkf" 2>/dev/null
    rm -f  QUILDKF quildjob.qkf COSKF CRSKF RUNKF

    if test ! -d "$SCM_RESULTDIR/$JOBNAME.results"
    then
        mkdir -p "$SCM_RESULTDIR/$JOBNAME.results"
    fi
    if test -d "$SCM_RESULTDIR/$JOBNAME.results"
    then
        for n in 13 21 41 10 11 15
        do
            cp -f -p "TAPE$n" "$SCM_RESULTDIR/$JOBNAME.t$n" 2>/dev/null
            rm -f "TAPE$n"
        done
        touch thisfileshouldbeignored
        cp -rf -p * "$SCM_RESULTDIR/$JOBNAME.results"
    else
        echo Problem copying result files to \"$SCM_RESULTDIR/$JOBNAME.results\"
    fi

    for f in "$SCM_RESULTDIR/$JOBNAME.results"/* thisfiledoesnotexist
    do
        if test -L "$f"; then
            rm -f "$f"
        fi
    done
    rm -f "$SCM_RESULTDIR/$JOBNAME.results"/thisfileshouldbeignored

    if test -z "$1"
    then
        echo "Job $JOBNAME has finished" >> "$SCM_RESULTDIR/$JOBNAME.logfile"
    else
        echo "Job $JOBNAME: $1" >> "$SCM_RESULTDIR/$JOBNAME.logfile"
    fi
    cd "$OWD" 2>/dev/null
    rm -rf "$SCM_JOBDIR"
    if test -z "$SCM_NOREADY"; then
        echo "# status: ready" >> "$SCM_RESULTDIR/$JOBNAME.pid"
#       densf runs from AMSview remove the line above, for that reason an extra NOP line is needed
        DUMMY=blurb
    fi
fi
}


#-----------------------------------------
# keep track of original working directory
#-----------------------------------------

OWD="`pwd`"
export OWD

# -------------------------------------------------
# insert prolog command as defined in AMSjobs queue
# -------------------------------------------------

# <<PROLOG>>

# ---------------------------------------------------
# execute arguments (possibly to set environment etc)
# ---------------------------------------------------

if test ! -z "$1"
then
    if test "$1" != "export NSCM=" -a "$1" != "NSCM= ; export NSCM"; then
       eval $1
    fi
fi

# ---------------
# name of the job
# ---------------

JOBNAME=`basename "$0" .job`
export JOBNAME

# -----------------------------------------------------------------------------------
# on a batch system, the start.cmd will replace the next line to set JOBNAME properly
# since the batch system may make a copy of the job script
# -----------------------------------------------------------------------------------

# <<JOBNAME>>

# -------------------------------------------------
# determine DIRN: full path of the script directory
# -------------------------------------------------

DN="`dirname "$0"`"
case "$DN" in
/*)
DIRN="$DN"
;;
?:*)
DIRN="$DN"
;;
*)
DIRN="`pwd`/$DN"
;;
esac

# --------------------------------------------------------------------------------
# on a batch system, the start.cmd will replace the next line to set DIRN properly
# since the batch system may make a copy of the job script
# --------------------------------------------------------------------------------

# <<FULL PATH OF SCRIPT DIRECTORY DIRN>>

# ------------------------------------------------------------------------------------
# set SCM_RESULTDIR to DIRN, so results end up in the full path as required by ADFJOBS
# ------------------------------------------------------------------------------------

SCM_RESULTDIR="$DIRN"
export SCM_RESULTDIR

#----------------------------
# for AMS, set AMS_RESULTSDIR
#----------------------------

AMS_RESULTSDIR="$SCM_RESULTDIR/$JOBNAME.results"
export AMS_RESULTSDIR
rm -rf "$AMS_RESULTSDIR"
mkdir -p "$AMS_RESULTSDIR"
unset AMS_SWITCH_LOGFILE_AND_STDOUT

# ----------------------------------
# pass the location of the info file
# ----------------------------------

SCM_INFOFILE="$SCM_RESULTDIR/$JOBNAME.info"
export SCM_INFOFILE

# ---------------------------------
# update the status in the pid file
# ---------------------------------

echo "# pid: $$" >> "$SCM_RESULTDIR/$JOBNAME.pid"
echo "# status: running" >> "$SCM_RESULTDIR/$JOBNAME.pid"


# ---------------
# prepare logfile
# ---------------

if test -s "$SCM_RESULTDIR/$JOBNAME.logfile"
then
    cp "$SCM_RESULTDIR/$JOBNAME.logfile" "$SCM_RESULTDIR/$JOBNAME.logfile~"
fi
cat /dev/null > "$SCM_RESULTDIR/$JOBNAME.logfile"

SCM_LOGFILE="$SCM_RESULTDIR/$JOBNAME.logfile"
export SCM_LOGFILE


# <<FRAGMENT JOB SCRIPTS>>


# ------------------------------------------------------------------
# within SCM_RESULTDIR, make a new empty directory just for this job
# ------------------------------------------------------------------

XX=0
SCM_JOBDIR="$SCM_RESULTDIR/tmp.$USER.$$.$XX.noindex"
while test -d "$SCM_JOBDIR" 
do
   XX=`expr $XX + 1`
   SCM_JOBDIR="$SCM_RESULTDIR/tmp.$USER.$$.$XX.noindex"
done
export SCM_JOBDIR
mkdir "$SCM_JOBDIR"
echo "# jobdir: localhost:$SCM_JOBDIR" >> "$SCM_INFOFILE"

if test ! -d "$SCM_JOBDIR"
then
    echo "Could not create temporary directory $SCM_JOBDIR" >> "$SCM_RESULTDIR/$JOBNAME.logfile"
    echo "# status: killed" >> "$SCM_RESULTDIR/$JOBNAME.pid"
    exit
fi

# -----------------------------------------------------
# nodeinfo may be needed, and keep backup of old output
# -----------------------------------------------------

if test -f nodeinfo 
then
    cp nodeinfo "$SCM_JOBDIR/nodeinfo"
fi
cd "$SCM_JOBDIR"

if test -s "$SCM_RESULTDIR/$JOBNAME.out"
then
    mv "$SCM_RESULTDIR/$JOBNAME.out" "$SCM_RESULTDIR/$JOBNAME.out~"
fi

# <<COPY FRAGMENT FILES>>

# -------------------------------------------------------
# on normal or abnormal exits, clean up and update status
# -------------------------------------------------------

trap "ready \"abnormal exit (signal 1)\"; exit"  1 
trap "ready \"abnormal exit (signal 2)\"; exit"  2 
trap "ready \"abnormal exit (signal 3)\"; exit"  3
trap "ready \"abnormal exit (signal 15)\"; exit" 15

# -------------------------------------
# define easy vars for output and error
# -------------------------------------

SCM_OUT="$SCM_RESULTDIR/$JOBNAME.out"
export SCM_OUT
SCM_ERR="$SCM_RESULTDIR/$JOBNAME.err"
export SCM_ERR


# ----------------------
# execute the run script
# ----------------------

cat << \EORTHEJOB > job
#!/bin/sh

"$AMSBIN/ams" << eor

Task GeometryOptimization
UseSymmetry No
Properties
    NormalModes Yes
End
NormalModes
    Hessian Numerical
End
System
    Atoms
        C -0.08273470783009999 -0.07064211449714 0.35237960735861 
        C 0.78247266588008 1.02193517515067 0.41755395533647 
        C 0.42119737063369 -1.37623666190761 0.40610294357462 
        C 2.17388521297619 0.81307936650769 0.53803774388804 
        C 2.67667997781601 -0.50569807194797 0.59233294430546 
        C 1.80025764351845 -1.58889004203788 0.52600996556119 
        H 0.39704961504994 2.03435633900075 0.37635565364327 
        C 3.06562294944186 1.92091025673637 0.60364286550601 
        H -1.15054157018085 0.09650526885021 0.25982060482484 
        H -0.25592754975466 -2.22220292058534 0.35483738156043 
        H 3.74504125187502 -0.66443846361238 0.68513846533755 
        H 2.19363218139665 -2.59893347032894 0.56775123759861 
        N 3.81484593697027 2.82403650031812 0.64943859245018 
        O 4.61481902220746 3.78451883835344 0.6965980390547299 
    End
    BondOrders
         1 2 1.5
         1 3 1.5
         1 9 1.0
         2 4 1.5
         2 7 1.0
         3 6 1.5
         3 10 1.0
         4 5 1.5
         4 8 1.0
         5 6 1.5
         5 11 1.0
         6 12 1.0
         8 13 2.0
         13 14 1.0
    End
End

Engine ADF
    Basis
        Type TZ2P
        Core None
    End
    XC
        LibXC r2SCAN
        DISPERSION GRIMME4
    End
    NumericalQuality Good
EndEngine
eor



EORTHEJOB

chmod u+x job
./job >>"$SCM_RESULTDIR/$JOBNAME.out" 2>> "$SCM_RESULTDIR/$JOBNAME.err"
rm job

# <<EPILOG>>

ready

