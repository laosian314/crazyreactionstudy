-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -399.6494095156
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                 31.0000204813 
   Number of Beta  Electrons                 31.0000204813 
   Total number of  Electrons                62.0000409625 
   Exchange energy                          -53.0821733532 
   Correlation energy                        -2.0233252291 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -55.1054985822 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -399.6494095156 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 1
   prop. index: 1
        Van der Waals Correction:       -0.0045807797
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:     -399.6514730377
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 2
   prop. index: 1
   Number of Alpha Electrons                 31.0000191227 
   Number of Beta  Electrons                 31.0000191227 
   Total number of  Electrons                62.0000382455 
   Exchange energy                          -53.1349377446 
   Correlation energy                        -2.0258442040 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -55.1607819486 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -399.6514730377 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 2
   prop. index: 1
        Van der Waals Correction:       -0.0046034330
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 3
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 3
   prop. index: 1
        SCF Energy:     -399.6517675446
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 3
   prop. index: 1
   Number of Alpha Electrons                 31.0000162255 
   Number of Beta  Electrons                 31.0000162255 
   Total number of  Electrons                62.0000324509 
   Exchange energy                          -53.1474592027 
   Correlation energy                        -2.0264230540 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -55.1738822567 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -399.6517675446 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 3
   prop. index: 1
        Van der Waals Correction:       -0.0046111641
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 4
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 4
   prop. index: 1
        SCF Energy:     -399.6520786769
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 4
   prop. index: 1
   Number of Alpha Electrons                 31.0000103040 
   Number of Beta  Electrons                 31.0000103040 
   Total number of  Electrons                62.0000206081 
   Exchange energy                          -53.1531226690 
   Correlation energy                        -2.0266365928 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -55.1797592618 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -399.6520786769 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 4
   prop. index: 1
        Van der Waals Correction:       -0.0046202703
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 5
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 5
   prop. index: 1
        SCF Energy:     -399.6525738381
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 5
   prop. index: 1
   Number of Alpha Electrons                 30.9999961140 
   Number of Beta  Electrons                 30.9999961140 
   Total number of  Electrons                61.9999922280 
   Exchange energy                          -53.1546662325 
   Correlation energy                        -2.0265587780 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -55.1812250105 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -399.6525738381 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 5
   prop. index: 1
        Van der Waals Correction:       -0.0046440770
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 6
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 6
   prop. index: 1
        SCF Energy:     -399.6527248462
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 6
   prop. index: 1
   Number of Alpha Electrons                 30.9999940342 
   Number of Beta  Electrons                 30.9999940342 
   Total number of  Electrons                61.9999880684 
   Exchange energy                          -53.1450114936 
   Correlation energy                        -2.0260420691 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -55.1710535627 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -399.6527248462 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 6
   prop. index: 1
        Van der Waals Correction:       -0.0046454728
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 7
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 7
   prop. index: 1
        SCF Energy:     -399.6527778647
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 7
   prop. index: 1
   Number of Alpha Electrons                 30.9999942018 
   Number of Beta  Electrons                 30.9999942018 
   Total number of  Electrons                61.9999884037 
   Exchange energy                          -53.1350930045 
   Correlation energy                        -2.0255105648 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -55.1606035693 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -399.6527778647 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 7
   prop. index: 1
        Van der Waals Correction:       -0.0046415473
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 8
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 8
   prop. index: 1
        SCF Energy:     -399.6527817322
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 8
   prop. index: 1
   Number of Alpha Electrons                 30.9999939534 
   Number of Beta  Electrons                 30.9999939534 
   Total number of  Electrons                61.9999879068 
   Exchange energy                          -53.1353849719 
   Correlation energy                        -2.0255156194 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -55.1609005914 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -399.6527817322 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 8
   prop. index: 1
        Van der Waals Correction:       -0.0046425371
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 9
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 9
   prop. index: 1
        SCF Energy:     -399.6527852418
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 9
   prop. index: 1
   Number of Alpha Electrons                 30.9999940906 
   Number of Beta  Electrons                 30.9999940906 
   Total number of  Electrons                61.9999881811 
   Exchange energy                          -53.1358530131 
   Correlation energy                        -2.0255338702 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -55.1613868833 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -399.6527852418 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 9
   prop. index: 1
        Van der Waals Correction:       -0.0046424938
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 10
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 10
   prop. index: 1
        SCF Energy:     -399.6527858402
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 10
   prop. index: 1
   Number of Alpha Electrons                 30.9999941727 
   Number of Beta  Electrons                 30.9999941727 
   Total number of  Electrons                61.9999883455 
   Exchange energy                          -53.1359308168 
   Correlation energy                        -2.0255385817 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -55.1614693985 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -399.6527858402 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 10
   prop. index: 1
        Van der Waals Correction:       -0.0046423188
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 10
   prop. index: 1
       Filename                          : mol.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        2.3299540866
        Electronic Contribution:
                  0    
      0       6.506798
      1       3.463403
      2      -1.702197
        Nuclear Contribution:
                  0    
      0      -7.295419
      1      -3.882145
      2       1.909534
        Total Dipole moment:
                  0    
      0      -0.788622
      1      -0.418742
      2       0.207337
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 11
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 11
   prop. index: 1
        SCF Energy:     -399.6527858397
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 11
   prop. index: 1
   Number of Alpha Electrons                 30.9999941727 
   Number of Beta  Electrons                 30.9999941727 
   Total number of  Electrons                61.9999883455 
   Exchange energy                          -53.1359256139 
   Correlation energy                        -2.0255386155 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -55.1614642294 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -399.6527858397 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 11
   prop. index: 1
        Van der Waals Correction:       -0.0046423188
# -----------------------------------------------------------
$ THERMOCHEMISTRY_Energies
   description: The Thermochemistry energies
   geom. index: 11
   prop. index: 1
        Temperature (Kelvin)           :        298.1500000000
        Pressure (atm)                 :          1.0000000000
        Total Mass (AMU)               :        119.0371137851
        Spin Degeneracy                :          1.0000000000
        Electronic Energy (Hartree)    :       -399.6528336241
        Translational Energy (Hartree) :          0.0014162714
        Rotational Energy (Hartree)    :          0.0014162714
        Vibrational Energy (Hartree)   :          0.0042570018
        Number of frequencies          :     42      
        Scaling Factor for frequencies :          1.0000000000
        Vibrational frequencies        :     
                  0    
      0       0.000000
      1       0.000000
      2       0.000000
      3       0.000000
      4       0.000000
      5       0.000000
      6      61.311204
      7      98.092829
      8     237.146721
      9     393.266061
     10     414.534812
     11     474.227863
     12     497.650339
     13     569.322783
     14     634.964318
     15     656.868599
     16     696.862555
     17     761.935372
     18     785.372990
     19     836.289203
     20     909.612801
     21     969.001645
     22     987.302469
     23     1025.080722
     24     1049.098937
     25     1103.539659
     26     1149.589745
     27     1186.534051
     28     1201.109106
     29     1329.928922
     30     1359.233584
     31     1469.710271
     32     1482.589879
     33     1542.442026
     34     1617.920151
     35     1636.172173
     36     2348.646914
     37     3165.511317
     38     3172.933983
     39     3180.368213
     40     3190.490419
     41     3197.734153
        Zero Point Energy (Hartree)    :          0.1034114890
        Inner Energy (Hartree)         :       -399.5423325905
        Enthalpy (Hartree)             :       -399.5413883814
        Electronic entropy             :          0.0000000000
        Rotational entropy             :          0.0136537662
        Vibrational entropy            :          0.0070198742
        Translational entropy          :          0.0136537662
        Entropy                        :          0.0397916893
        Gibbs Energy (Hartree)         :       -399.5811800707
        Is Linear                      :                 false
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    14 
    Geometry Index:     1 
    Coordinates:
               0 C      1.806997169610   -0.419591538559    0.362265295650
               1 C      2.922676222062    0.352003236506    0.030274049797
               2 C      1.949607537721   -1.779650144518    0.662170749277
               3 C      4.193171294638   -0.242406052097   -0.001789539377
               4 C      4.343102390871   -1.607583811207    0.298592994518
               5 C      3.219814238926   -2.368147260640    0.628522450976
               6 H      2.828883114510    1.405481814938   -0.204693992363
               7 C      6.472869090323    0.673488638073   -0.531235217573
               8 H      0.825828438535    0.042593415873    0.386511838283
               9 H      1.080460649215   -2.375177987282    0.919769504802
              10 H      5.329927437246   -2.057214373673    0.271485033478
              11 H      3.338469638104   -3.421524762413    0.859683455849
              12 N      5.292558457564    0.545181058330   -0.335680990166
              13 O      7.629234320676    0.923147766670   -0.754875633151
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    14 
    Geometry Index:     2 
    Coordinates:
               0 C      1.816360205095   -0.420750226749    0.361236555467
               1 C      2.923713249320    0.351705303065    0.029967984890
               2 C      1.962406489107   -1.774235710140    0.659353215351
               3 C      4.189785940215   -0.238222386129   -0.002985621325
               4 C      4.345001090411   -1.597116764208    0.295450256803
               5 C      3.228733722139   -2.356568035518    0.624792641312
               6 H      2.825600853510    1.406399236565   -0.204359405219
               7 C      6.454899832714    0.656561900696   -0.524570882357
               8 H      0.833306289284    0.039003381727    0.386506754719
               9 H      1.094623001276   -2.372644450091    0.917530906707
              10 H      5.334158500712   -2.043616206061    0.266937628533
              11 H      3.350597889259   -3.410411892559    0.855806819462
              12 N      5.280115596160    0.549712315242   -0.335487869525
              13 O      7.594297340798    0.880783534159   -0.739178984818
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    14 
    Geometry Index:     3 
    Coordinates:
               0 C      1.816993773092   -0.422655728580    0.361619967580
               1 C      2.919410596997    0.353770030026    0.030086951858
               2 C      1.967804445266   -1.773691724982    0.658574373510
               3 C      4.186854747725   -0.230265462802   -0.004484316981
               4 C      4.346628727413   -1.587576228007    0.292924827578
               5 C      3.234698506337   -2.350056257574    0.622500619272
               6 H      2.815829343280    1.408327798068   -0.203367925219
               7 C      6.451262157185    0.644777232778   -0.521300994755
               8 H      0.831882754564    0.033582930133    0.387948849820
               9 H      1.102340565044   -2.375708647531    0.917206902143
              10 H      5.337718985078   -2.030431718155    0.263054754509
              11 H      3.360755549984   -3.403929459783    0.852982738721
              12 N      5.272655761949    0.561801584523   -0.337629265321
              13 O      7.588764086085    0.842655651885   -0.729117482714
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    14 
    Geometry Index:     4 
    Coordinates:
               0 C      1.815130071158   -0.425570182109    0.362665668100
               1 C      2.910541183916    0.358648494847    0.030096178356
               2 C      1.974844411591   -1.774507928881    0.657956372821
               3 C      4.181899883455   -0.215164570008   -0.007592644894
               4 C      4.350045567586   -1.571339563571    0.288424934352
               5 C      3.244798397538   -2.340991969500    0.619018825447
               6 H      2.797601041698    1.412703849366   -0.201894077046
               7 C      6.446234327254    0.624179699020   -0.515665062115
               8 H      0.826812860193    0.024291326423    0.390980619146
               9 H      1.113926876062   -2.382887367941    0.917498791085
              10 H      5.344362213887   -2.007511989639    0.256253491482
              11 H      3.378735826830   -3.394441313576    0.848395642936
              12 N      5.260975259128    0.586391590644   -0.342467130708
              13 O      7.587692079703    0.776799924924   -0.712671608962
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    14 
    Geometry Index:     5 
    Coordinates:
               0 C      1.809552311668   -0.431685467593    0.365297897952
               1 C      2.888287458604    0.372852323793    0.029186705588
               2 C      1.993204752806   -1.777052818889    0.656768101502
               3 C      4.170050508079   -0.175071828574   -0.016750862735
               4 C      4.361074823575   -1.528998234661    0.275801862449
               5 C      3.272711407088   -2.318809465534    0.609917600836
               6 H      2.752089819654    1.425105701402   -0.199219400449
               7 C      6.426812384089    0.566675673584   -0.498742419923
               8 H      0.813602344956    0.001437458254    0.399198206992
               9 H      1.144786382744   -2.401696152600    0.919163135437
              10 H      5.363359278939   -1.947635016271    0.237612756605
              11 H      3.427620512047   -3.370366993319    0.836238336900
              12 N      5.232756008111    0.653874007094   -0.356900387416
              13 O      7.577692007640    0.601970813315   -0.666571533738
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    14 
    Geometry Index:     6 
    Coordinates:
               0 C      1.806671243261   -0.432226196469    0.365937326357
               1 C      2.883896955542    0.375916580256    0.028959920393
               2 C      1.995144629128   -1.778330930045    0.656956595474
               3 C      4.167656940466   -0.168215002525   -0.018215323072
               4 C      4.363467292111   -1.522176945038    0.273659098308
               5 C      3.277466389775   -2.316621962988    0.608720776720
               6 H      2.744744403646    1.427752430351   -0.199137647601
               7 C      6.423970078548    0.556516098705   -0.495791169985
               8 H      0.809716737501   -0.002125295425    0.400830911238
               9 H      1.149135312929   -2.405715308529    0.919878553742
              10 H      5.366834893015   -1.938222866068    0.234569151139
              11 H      3.436158025710   -3.367506517195    0.834284628958
              12 N      5.229113551294    0.668680949104   -0.360410181232
              13 O      7.579623547071    0.572874965866   -0.659242640436
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    14 
    Geometry Index:     7 
    Coordinates:
               0 C      1.804348448857   -0.431349630641    0.366061955592
               1 C      2.883767151550    0.376194047994    0.028932927382
               2 C      1.993173091363   -1.779101211424    0.657367621397
               3 C      4.166999476566   -0.170048332881   -0.017612458895
               4 C      4.363740463620   -1.524136561818    0.274127009314
               5 C      3.276447420705   -2.319294740491    0.609452720125
               6 H      2.747137715470    1.428078412614   -0.199693065409
               7 C      6.425814424526    0.557613790458   -0.496295232612
               8 H      0.807902846898   -0.001034095112    0.400865267678
               9 H      1.147008525796   -2.405967084382    0.920259971393
              10 H      5.366376148192   -1.941517977681    0.235569064765
              11 H      3.434577332238   -3.369887937444    0.834905238134
              12 N      5.230625320846    0.670283427637   -0.360868971369
              13 O      7.585681633373    0.580767893171   -0.662072047496
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    14 
    Geometry Index:     8 
    Coordinates:
               0 C      1.804228532068   -0.431258739174    0.366062809884
               1 C      2.883372535864    0.376670621404    0.028852225960
               2 C      1.993931379585   -1.778932208917    0.657236850426
               3 C      4.166472576209   -0.169448676425   -0.017724907328
               4 C      4.364375245465   -1.523124107475    0.273769942035
               5 C      3.277328115119   -2.318768816011    0.609197595437
               6 H      2.746831710988    1.428546024973   -0.199780194304
               7 C      6.424573213294    0.555173691778   -0.495542065737
               8 H      0.807549254182   -0.001546817789    0.401069611056
               9 H      1.148064626274   -2.406199115946    0.920215890245
              10 H      5.367004500096   -1.940428542077    0.235210589309
              11 H      3.435815999609   -3.369308026402    0.834591107562
              12 N      5.229640452901    0.672087836857   -0.361192626246
              13 O      7.584411858345    0.577136875203   -0.660966828299
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    14 
    Geometry Index:     9 
    Coordinates:
               0 C      1.804307496379   -0.430996543378    0.365967600102
               1 C      2.883732773935    0.376553137965    0.028853738467
               2 C      1.993978028560   -1.778632353558    0.657127529646
               3 C      4.166358011134   -0.170423186804   -0.017425979981
               4 C      4.364508934633   -1.523764167621    0.273949331282
               5 C      3.276998447468   -2.319024111190    0.609305318536
               6 H      2.748264417938    1.428509212410   -0.199928524101
               7 C      6.424562684602    0.554896113928   -0.495541008124
               8 H      0.807598984568   -0.001359828034    0.400973008766
               9 H      1.147900860718   -2.405696002063    0.920055076496
              10 H      5.366773319067   -1.941851393723    0.235671122872
              11 H      3.434937628147   -3.369630198252    0.834781831096
              12 N      5.229669344273    0.671284920571   -0.360886721996
              13 O      7.584009068577    0.580734399747   -0.661902323062
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    14 
    Geometry Index:     10 
    Coordinates:
               0 C      1.804372580812   -0.430949015861    0.365940752104
               1 C      2.883883382553    0.376435396674    0.028869162558
               2 C      1.993875061692   -1.778577112038    0.657116586616
               3 C      4.166461403547   -0.170760124147   -0.017343611723
               4 C      4.364439479912   -1.524099729753    0.274056836103
               5 C      3.276740323914   -2.319150291908    0.609372695705
               6 H      2.748756834987    1.428424869997   -0.199957690411
               7 C      6.424809127049    0.555112354477   -0.495670548481
               8 H      0.807663700816   -0.001293566663    0.400930800486
               9 H      1.147685309485   -2.405515823484    0.920013940243
              10 H      5.366571138300   -1.942506436131    0.235881560279
              11 H      3.434434735712   -3.369792606169    0.834893364388
              12 N      5.229816928462    0.670830221371   -0.360749306637
              13 O      7.584089992758    0.582441863636   -0.662354541232
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    14 
    Geometry Index:     11 
    Coordinates:
               0 C      1.804372580812   -0.430949015861    0.365940752104
               1 C      2.883883382553    0.376435396674    0.028869162558
               2 C      1.993875061692   -1.778577112038    0.657116586616
               3 C      4.166461403547   -0.170760124147   -0.017343611723
               4 C      4.364439479912   -1.524099729753    0.274056836103
               5 C      3.276740323914   -2.319150291908    0.609372695705
               6 H      2.748756834987    1.428424869997   -0.199957690411
               7 C      6.424809127049    0.555112354477   -0.495670548481
               8 H      0.807663700816   -0.001293566663    0.400930800486
               9 H      1.147685309485   -2.405515823484    0.920013940243
              10 H      5.366571138300   -1.942506436131    0.235881560279
              11 H      3.434434735712   -3.369792606169    0.834893364388
              12 N      5.229816928462    0.670830221371   -0.360749306637
              13 O      7.584089992758    0.582441863636   -0.662354541232
