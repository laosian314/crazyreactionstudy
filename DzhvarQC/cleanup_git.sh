#! /bin/bash
 
tree -a . # print the tree of files
 
du -h -d 0 # print initial size of the data in the current folder
 
find . -type f -name '*.gbw' -exec git rm -f {} \; # remove the *.gbw files, that contain DFT/HF orbitals from Orca. They are only needed for those, who wants to visualize them
find . -type f -name '*.err' -exec git rm -f {} \; # remove the *.err files of the calculations
find . -type f -name '*.loc' -exec git rm -f {} \; # remove *.loc files, that are produced from the orbital localizations procedures of the DLPNO-calculations
find . -type f -name '*.cis' -exec git rm -f {} \; # rempve *.cis files, storing the excited states wavefunctions from Orca
find . -type f -name '*.tmp' -exec git rm -f {} \; # remove the *.tmp files that were left from Orca job crashing
find . -type f -name '*.tmp.*' -exec git rm -f {} \; # remove more tmp files from there
find . -type f -name '*.zip' -exec git rm -f {} \; # remove the zip-archives, if there are any
find . -type f -name '*.cis1' -exec git rm -f {} \; # remove the *cis1 files, if there are any
find . -type f -name '*.densities' -exec git rm -f {} \; # remove the densities, if there are any find . -type f -name '*.zip' -exec rm {} \;
#find . -type f -name '*.zip' -exec rm {} \;
 
 
#tree -a
 
du -h -d 0

