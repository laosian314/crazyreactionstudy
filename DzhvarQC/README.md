# R2SCAN-3c calculations

Calculations are done in Orca-5.0.3 with R2SCAN-3c method and mTZVP(default) basis set. For more detaill see 

https://chemrxiv.org/engage/api-gateway/chemrxiv/assets/orp/resource/item/60c752f6bb8c1a21633dbf6c/original/r2scan-3c-an-efficient-swiss-army-knife-composite-electronic-structure-method.pdf

The structures are taken from output of b3lyp-d3bj-6-31g level calculations in MadSchumacherQC directory

## Short description from TM

1. Пересчитал найденные в b3lyp минимумы в r2SCAN-3c с поправкой в D4 с гессианами. В том же методе запустил НЭБ1 и получил ПС1. НЭБ2 полностью в r2SCAN-3c не привел к ПС2, но тот нашелся из ПС2 в b3lyp. 

2. Пересчитал все упомянутое в п.1 в том же методе с растворителем cpcm(benzene)

Считаю:

Полученные геометрии из п.2 в dlpno-ccsd с cpcm(benzene)

