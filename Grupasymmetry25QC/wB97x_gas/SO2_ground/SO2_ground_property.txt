-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -548.6709701477
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                 15.9999905876 
   Number of Beta  Electrons                 15.9999905876 
   Total number of  Electrons                31.9999811752 
   Exchange energy                          -29.9778383383 
   Correlation energy                        -1.5416882075 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -31.5195265459 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -548.6709701477 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 1
   prop. index: 1
     Epsilon:                2.4000000000
     Refrac:                 1.4970000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                254
     Surface Area:         303.5589469497
     Dielectric Energy:     -0.0054159074
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 3
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 3
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0    15.2598    16.0000     0.7402     3.5863     3.5863    -0.0000
  1   0     8.3701     8.0000    -0.3701     2.0108     2.0108    -0.0000
  2   0     8.3701     8.0000    -0.3701     2.0109     2.0109    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0            16               1            8                1.793165
                0            16               2            8                1.793179
                1             8               2            8                0.217680
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 1
   prop. index: 1
        Van der Waals Correction:       -0.0038148528
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:     -548.7177139126
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 2
   prop. index: 1
   Number of Alpha Electrons                 15.9999976404 
   Number of Beta  Electrons                 15.9999976404 
   Total number of  Electrons                31.9999952808 
   Exchange energy                          -30.0721813616 
   Correlation energy                        -1.5504544786 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -31.6226358402 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -548.7177139126 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 2
   prop. index: 1
     Epsilon:                2.4000000000
     Refrac:                 1.4970000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                254
     Surface Area:         292.2891641407
     Dielectric Energy:     -0.0046859386
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 2
   prop. index: 1
        Van der Waals Correction:       -0.0038923859
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 3
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 3
   prop. index: 1
        SCF Energy:     -548.7273917233
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 3
   prop. index: 1
   Number of Alpha Electrons                 16.0000019656 
   Number of Beta  Electrons                 16.0000019656 
   Total number of  Electrons                32.0000039313 
   Exchange energy                          -30.1993937098 
   Correlation energy                        -1.5606230587 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -31.7600167685 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -548.7273917233 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 3
   prop. index: 1
     Epsilon:                2.4000000000
     Refrac:                 1.4970000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                250
     Surface Area:         281.8490144990
     Dielectric Energy:     -0.0035948828
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 3
   prop. index: 1
        Van der Waals Correction:       -0.0039519321
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 4
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 4
   prop. index: 1
        SCF Energy:     -548.7307256229
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 4
   prop. index: 1
   Number of Alpha Electrons                 15.9999984525 
   Number of Beta  Electrons                 15.9999984525 
   Total number of  Electrons                31.9999969050 
   Exchange energy                          -30.1454517399 
   Correlation energy                        -1.5564742344 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -31.7019259743 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -548.7307256229 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 4
   prop. index: 1
     Epsilon:                2.4000000000
     Refrac:                 1.4970000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                254
     Surface Area:         285.8179922633
     Dielectric Energy:     -0.0038592155
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 4
   prop. index: 1
        Van der Waals Correction:       -0.0039249672
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 5
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 5
   prop. index: 1
        SCF Energy:     -548.7308976798
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 5
   prop. index: 1
   Number of Alpha Electrons                 15.9999989305 
   Number of Beta  Electrons                 15.9999989305 
   Total number of  Electrons                31.9999978609 
   Exchange energy                          -30.1537491717 
   Correlation energy                        -1.5571225100 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -31.7108716817 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -548.7308976798 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 5
   prop. index: 1
     Epsilon:                2.4000000000
     Refrac:                 1.4970000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                250
     Surface Area:         285.1615988897
     Dielectric Energy:     -0.0037688557
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 5
   prop. index: 1
        Van der Waals Correction:       -0.0039279820
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 6
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 6
   prop. index: 1
        SCF Energy:     -548.7309003378
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 6
   prop. index: 1
   Number of Alpha Electrons                 15.9999989766 
   Number of Beta  Electrons                 15.9999989766 
   Total number of  Electrons                31.9999979533 
   Exchange energy                          -30.1548094938 
   Correlation energy                        -1.5572046867 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -31.7120141806 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -548.7309003378 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 6
   prop. index: 1
     Epsilon:                2.4000000000
     Refrac:                 1.4970000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                250
     Surface Area:         285.0771179950
     Dielectric Energy:     -0.0037529268
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 6
   prop. index: 1
        Van der Waals Correction:       -0.0039282221
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 7
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 7
   prop. index: 1
        SCF Energy:     -548.7309002448
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 7
   prop. index: 1
   Number of Alpha Electrons                 15.9999989715 
   Number of Beta  Electrons                 15.9999989715 
   Total number of  Electrons                31.9999979431 
   Exchange energy                          -30.1547754186 
   Correlation energy                        -1.5572016953 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -31.7119771139 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -548.7309002448 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 7
   prop. index: 1
     Epsilon:                2.4000000000
     Refrac:                 1.4970000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                250
     Surface Area:         285.0800346789
     Dielectric Energy:     -0.0037525619
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 7
   prop. index: 1
     Number of atoms                     : 3
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 3
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0    15.2541    16.0000     0.7459     3.8346     3.8346     0.0000
  1   0     8.3729     8.0000    -0.3729     2.0667     2.0667     0.0000
  2   0     8.3729     8.0000    -0.3729     2.0667     2.0667     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0            16               1            8                1.917320
                0            16               2            8                1.917321
                1             8               2            8                0.149367
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 7
   prop. index: 1
        Van der Waals Correction:       -0.0039281816
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 7
   prop. index: 1
       Filename                          : SO2_ground/SO2_ground.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        1.9798213441
        Electronic Contribution:
                  0    
      0       0.000000
      1       0.000000
      2       0.800330
        Nuclear Contribution:
                  0    
      0       0.000000
      1      -0.000000
      2      -0.021424
        Total Dipole moment:
                  0    
      0       0.000000
      1       0.000000
      2       0.778906
# -----------------------------------------------------------
$ Hessian
   description: Details about the Hessian
   geom. index: 7
   prop. index: 1
Normal modes:
Number of Rows: 9 Number of Columns: 9
                  0          1          2          3          4          5    
      0       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      1       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      2       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      3       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      4       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      5       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      6       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      7       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      8       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
                  6          7          8    
      0       0.000000   0.000000   0.000000
      1       0.000000  -0.000001   0.518500
      2      -0.405931  -0.347832  -0.000001
      3      -0.000000  -0.000000  -0.000000
      4       0.502186  -0.563958  -0.519506
      5       0.406717   0.348506   0.309345
      6       0.000000   0.000000   0.000000
      7      -0.502187   0.563960  -0.519504
      8       0.406717   0.348506  -0.309344
# -----------------------------------------------------------
$ THERMOCHEMISTRY_Energies
   description: The Thermochemistry energies
   geom. index: 7
   prop. index: 1
        Temperature (Kelvin)           :        298.1500000000
        Pressure (atm)                 :          1.0000000000
        Total Mass (AMU)               :         64.0580000000
        Spin Degeneracy                :          1.0000000000
        Electronic Energy (Hartree)    :       -548.7348284263
        Translational Energy (Hartree) :          0.0014162714
        Rotational Energy (Hartree)    :          0.0014162714
        Vibrational Energy (Hartree)   :          0.0002226146
        Number of frequencies          :     9      
        Scaling Factor for frequencies :          1.0000000000
        Vibrational frequencies        :     
                  0    
      0       0.000000
      1       0.000000
      2       0.000000
      3       0.000000
      4       0.000000
      5       0.000000
      6     532.783699
      7     1233.413353
      8     1415.255318
        Zero Point Energy (Hartree)    :          0.0072478818
        Inner Energy (Hartree)         :       -548.7245253871
        Enthalpy (Hartree)             :       -548.7235811780
        Electronic entropy             :          0.0000000000
        Rotational entropy             :          0.0096099557
        Vibrational entropy            :          0.0003015600
        Translational entropy          :          0.0096099557
        Entropy                        :          0.0281519770
        Gibbs Energy (Hartree)         :       -548.7517331550
        Is Linear                      :                 false
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    3 
    Geometry Index:     1 
    Coordinates:
               0 S      0.000000000000   -0.000000071000    0.464422083000
               1 O      0.000000000000    1.351921805000   -0.413795063000
               2 O      0.000000000000   -1.351921734000   -0.413795021000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    3 
    Geometry Index:     2 
    Coordinates:
               0 S      0.000000000000    0.000000114112    0.422047320651
               1 O      0.000000000000    1.267471909816   -0.392607622058
               2 O      0.000000000000   -1.267472023928   -0.392607699594
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    3 
    Geometry Index:     3 
    Coordinates:
               0 S      0.000000000000   -0.000000044689    0.361561318694
               1 O      0.000000000000    1.193819661802   -0.362364670281
               2 O      0.000000000000   -1.193819617113   -0.362364649412
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    3 
    Geometry Index:     4 
    Coordinates:
               0 S      0.000000000000    0.000000003188    0.373515714998
               1 O      0.000000000000    1.232859417969   -0.368341853923
               2 O      0.000000000000   -1.232859421157   -0.368341862075
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    3 
    Geometry Index:     5 
    Coordinates:
               0 S      0.000000000000   -0.000000013247    0.368061828770
               1 O      0.000000000000    1.229525162736   -0.365614915754
               2 O      0.000000000000   -1.229525149489   -0.365614914015
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    3 
    Geometry Index:     6 
    Coordinates:
               0 S      0.000000000000    0.000000042041    0.367035467602
               1 O      0.000000000000    1.229375089924   -0.365101718683
               2 O      0.000000000000   -1.229375131965   -0.365101749919
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    3 
    Geometry Index:     7 
    Coordinates:
               0 S      0.000000000000    0.000000047471    0.366999984976
               1 O      0.000000000000    1.229442256661   -0.365083975751
               2 O      0.000000000000   -1.229442304132   -0.365084010225
