-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:    -2549.5696001604
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 1
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                250
     Surface Area:         309.2837689655
     Dielectric Energy:     -0.0096601988
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 3
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 3
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0    32.8737    34.0000     1.1263     3.7034     3.7034    -0.0000
  1   0     8.5632     8.0000    -0.5632     1.9741     1.9741    -0.0000
  2   0     8.5632     8.0000    -0.5632     1.9741     1.9741    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0            34               1            8                1.851717
                0            34               2            8                1.851717
                1             8               2            8                0.122348
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 1
   prop. index: 1
        Total number of Electrons                                                     50
        Number of correlated Electrons                                                28
        Number of alpha correlated Electrons                                          14
        Number of beta correlated Electrons                                           14
        Reference Energy:                                               -2549.5696000868
        Total Correlation Energy:                                          -0.7443717573
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.0370422826
        Total MDCI Energy:                                              -2550.3139718441
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:    -2549.5880021520
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 2
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                250
     Surface Area:         309.2837689655
     Dielectric Energy:     -0.0096905497
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 2
   prop. index: 1
     Number of atoms                     : 3
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 3
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0    32.8083    34.0000     1.1917     3.6942     3.6942     0.0000
  1   0     8.5958     8.0000    -0.5958     1.9538     1.9538     0.0000
  2   0     8.5958     8.0000    -0.5958     1.9538     1.9538     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0            34               1            8                1.847085
                0            34               2            8                1.847085
                1             8               2            8                0.106727
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 2
   prop. index: 1
        Total number of Electrons                                                     50
        Number of correlated Electrons                                                28
        Number of alpha correlated Electrons                                          14
        Number of beta correlated Electrons                                           14
        Reference Energy:                                               -2549.5880020789
        Total Correlation Energy:                                          -0.8469897669
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.0424531932
        Total MDCI Energy:                                              -2550.4349918459
# -----------------------------------------------------------
$ Energy_Extrapolation
   description: The Extrapolation of the energies
   geom. index: 2
   prop. index: 1
       Do EP1                            : 1 
       Do EP2                            : 0 
       Do EP3                            : 0 
       Number of energies                : 2 
       SCF energy with basis  cc-pVTZ :  -2549.5696000868 
       SCF energy with basis  cc-pVQZ :  -2549.5880020789 
       CBS SCF Energy                    :  -2549.5935466713 
       Correlation energy with basis  cc-pVTZ :  -2549.5696000868 
       Correlation energy with basis  cc-pVQZ :  -2549.5880020789 
       CBS Correlation Energy            :     -0.9200424488 
       CBS Total Energy                  :  -2550.5135891201 
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 2
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       3
     number of electrons:                   50
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             103
     number of aux C basis functions:       490
     number of aux J basis functions:       0
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           0.000000
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 2
   prop. index: 1
       Filename                          : /scratch/obeletsan/orcajob__14396-YyrS/seo2_phh.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        3.9554403449
        Electronic Contribution:
                  0    
      0      -0.000000
      1      -0.000000
      2       4.216644
        Nuclear Contribution:
                  0    
      0       0.000000
      1       0.000000
      2      -2.660485
        Total Dipole moment:
                  0    
      0      -0.000000
      1      -0.000000
      2       1.556158
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    3 
    Geometry Index:     1 
    Coordinates:
               0 Se     0.000000000000    0.000000000000    0.472594000000
               1 O      0.000000000000    1.354840000000   -0.417881000000
               2 O      0.000000000000   -1.354840000000   -0.417881000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    3 
    Geometry Index:     2 
    Coordinates:
               0 Se     0.000000000000    0.000000000000    0.472594000000
               1 O      0.000000000000    1.354840000000   -0.417881000000
               2 O      0.000000000000   -1.354840000000   -0.417881000000
