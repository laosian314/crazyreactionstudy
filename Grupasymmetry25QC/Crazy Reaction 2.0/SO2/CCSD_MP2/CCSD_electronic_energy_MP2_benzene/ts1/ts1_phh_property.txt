-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -944.5791239271
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 1
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                999
     Surface Area:         720.0560658809
     Dielectric Energy:     -0.0085230097
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 17
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 19
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.1487     6.0000    -0.1487     3.7760     3.7760    -0.0000
  1   0     6.1465     6.0000    -0.1465     3.8317     3.8317    -0.0000
  2   0     6.1533     6.0000    -0.1533     3.8080     3.8080    -0.0000
  3   0     5.9411     6.0000     0.0589     3.7645     3.7645    -0.0000
  4   0     6.1596     6.0000    -0.1596     3.8207     3.8207     0.0000
  5   0     6.1482     6.0000    -0.1482     3.7679     3.7679     0.0000
  6   0     0.8195     1.0000     0.1805     0.9808     0.9808    -0.0000
  7   0     5.7596     6.0000     0.2404     3.8323     3.8323     0.0000
  8   0     0.8190     1.0000     0.1810     0.9681     0.9681    -0.0000
  9   0     0.8181     1.0000     0.1819     0.9691     0.9691    -0.0000
 10   0     0.8093     1.0000     0.1907     0.9907     0.9907     0.0000
 11   0     0.8194     1.0000     0.1806     0.9686     0.9686     0.0000
 12   0     6.8908     7.0000     0.1092     3.4602     3.4602     0.0000
 13   0     8.4385     8.0000    -0.4385     1.8014     1.8014     0.0000
 14   0     8.5392     8.0000    -0.5392     1.9572     1.9572     0.0000
 15   0    15.0507    16.0000     0.9493     3.8936     3.8936     0.0000
 16   0     8.5386     8.0000    -0.5386     1.9671     1.9671    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.396195
                0             6               2            6                1.353934
                0             6               8            1                0.968851
                1             6               3            6                1.294134
                1             6               6            1                0.966069
                2             6               5            6                1.372791
                2             6               9            1                0.969838
                3             6               4            6                1.338295
                3             6               7            6                1.079588
                4             6               5            6                1.371067
                4             6              10            1                0.952252
                5             6              11            1                0.969817
                7             6              12            7                2.190751
                7             6              13            8                0.128630
                7             6              14            8                0.369502
               12             7              13            8                1.130345
               13             8              15           16                0.459833
               14             8              15           16                1.496330
               15            16              16            8                1.857705
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 1
   prop. index: 1
        Total number of Electrons                                                     94
        Number of correlated Electrons                                                62
        Number of alpha correlated Electrons                                          31
        Number of beta correlated Electrons                                           31
        Reference Energy:                                                -944.5791239347
        Total Correlation Energy:                                          -2.3236588010
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1187950704
        Total MDCI Energy:                                               -946.9027827357
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:     -944.6274640022
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 2
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                999
     Surface Area:         720.0560658809
     Dielectric Energy:     -0.0084584141
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 2
   prop. index: 1
     Number of atoms                     : 17
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 19
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.1196     6.0000    -0.1196     3.7770     3.7770     0.0000
  1   0     6.1459     6.0000    -0.1459     3.8502     3.8502     0.0000
  2   0     6.1654     6.0000    -0.1654     3.8356     3.8356    -0.0000
  3   0     6.0372     6.0000    -0.0372     3.7684     3.7684     0.0000
  4   0     6.1652     6.0000    -0.1652     3.8034     3.8034     0.0000
  5   0     6.1133     6.0000    -0.1133     3.7715     3.7715    -0.0000
  6   0     0.8197     1.0000     0.1803     0.9908     0.9908     0.0000
  7   0     5.6361     6.0000     0.3639     3.7935     3.7935    -0.0000
  8   0     0.8173     1.0000     0.1827     0.9736     0.9736     0.0000
  9   0     0.8100     1.0000     0.1900     0.9723     0.9723    -0.0000
 10   0     0.8091     1.0000     0.1909     1.0031     1.0031    -0.0000
 11   0     0.8200     1.0000     0.1800     0.9742     0.9742    -0.0000
 12   0     6.9607     7.0000     0.0393     3.4108     3.4108    -0.0000
 13   0     8.4301     8.0000    -0.4301     1.8230     1.8230    -0.0000
 14   0     8.5856     8.0000    -0.5856     1.9070     1.9070    -0.0000
 15   0    14.9622    16.0000     1.0378     3.8669     3.8669    -0.0000
 16   0     8.6026     8.0000    -0.6026     1.8953     1.8953     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.421608
                0             6               2            6                1.372285
                0             6               8            1                0.974838
                1             6               3            6                1.273021
                1             6               6            1                0.978870
                2             6               5            6                1.392557
                2             6               9            1                0.974193
                3             6               4            6                1.306498
                3             6               7            6                1.162303
                4             6               5            6                1.399444
                4             6              10            1                0.957487
                5             6              11            1                0.975673
                7             6              12            7                2.121393
                7             6              13            8                0.114174
                7             6              14            8                0.391341
               12             7              13            8                1.140553
               13             8              15           16                0.483923
               14             8              15           16                1.478043
               15            16              16            8                1.822586
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 2
   prop. index: 1
        Total number of Electrons                                                     94
        Number of correlated Electrons                                                62
        Number of alpha correlated Electrons                                          31
        Number of beta correlated Electrons                                           31
        Reference Energy:                                                -944.6274640471
        Total Correlation Energy:                                          -2.4652639920
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1296344310
        Total MDCI Energy:                                               -947.0927280392
# -----------------------------------------------------------
$ Energy_Extrapolation
   description: The Extrapolation of the energies
   geom. index: 2
   prop. index: 1
       Do EP1                            : 1 
       Do EP2                            : 0 
       Do EP3                            : 0 
       Number of energies                : 2 
       SCF energy with basis  cc-pVTZ :   -944.5791239347 
       SCF energy with basis  cc-pVQZ :   -944.6274640471 
       CBS SCF Energy                    :   -944.6420291126 
       Correlation energy with basis  cc-pVTZ :   -944.5791239347 
       Correlation energy with basis  cc-pVQZ :   -944.6274640471 
       CBS Correlation Energy            :     -2.5660712383 
       CBS Total Energy                  :   -947.2081003509 
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 2
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       17
     number of electrons:                   94
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             434
     number of aux C basis functions:       1885
     number of aux J basis functions:       0
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           0.000000
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 2
   prop. index: 1
       Filename                          : /scratch/obeletsan/orcajob__14427-8CwK/ts1_phh.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        6.4349379696
        Electronic Contribution:
                  0    
      0       9.000327
      1       8.095283
      2       0.847172
        Nuclear Contribution:
                  0    
      0     -10.936098
      1      -9.519508
      2      -0.051174
        Total Dipole moment:
                  0    
      0      -1.935772
      1      -1.424225
      2       0.795998
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     1 
    Coordinates:
               0 C     -2.365424899000   -0.747956260000   -0.451892003000
               1 C     -1.495679274000    0.332776061000   -0.513498879000
               2 C     -1.937835281000   -1.968714083000    0.071666989000
               3 C     -0.181301903000    0.178570739000   -0.045854014000
               4 C      0.255956540000   -1.042478784000    0.481875693000
               5 C     -0.631458176000   -2.111650634000    0.535930643000
               6 H     -1.815140287000    1.283766831000   -0.915564624000
               7 C      0.721297701000    1.284035462000   -0.110477448000
               8 H     -3.378138558000   -0.635910537000   -0.811193253000
               9 H     -2.621443519000   -2.803952540000    0.117506748000
              10 H      1.271132711000   -1.137064619000    0.835917487000
              11 H     -0.300244733000   -3.056833315000    0.940684225000
              12 N      0.913248835000    2.395351888000   -0.561838541000
              13 O      1.842695475000    3.251909975000   -0.469996952000
              14 O      2.260207251000    1.082913625000    0.969491500000
              15 S      3.329820777000    2.079750291000    0.533704407000
              16 O      4.132307341000    1.615487900000   -0.606459980000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     2 
    Coordinates:
               0 C     -2.365424899000   -0.747956260000   -0.451892003000
               1 C     -1.495679274000    0.332776061000   -0.513498879000
               2 C     -1.937835281000   -1.968714083000    0.071666989000
               3 C     -0.181301903000    0.178570739000   -0.045854014000
               4 C      0.255956540000   -1.042478784000    0.481875693000
               5 C     -0.631458176000   -2.111650634000    0.535930643000
               6 H     -1.815140287000    1.283766831000   -0.915564624000
               7 C      0.721297701000    1.284035462000   -0.110477448000
               8 H     -3.378138558000   -0.635910537000   -0.811193253000
               9 H     -2.621443519000   -2.803952540000    0.117506748000
              10 H      1.271132711000   -1.137064619000    0.835917487000
              11 H     -0.300244733000   -3.056833315000    0.940684225000
              12 N      0.913248835000    2.395351888000   -0.561838541000
              13 O      1.842695475000    3.251909975000   -0.469996952000
              14 O      2.260207251000    1.082913625000    0.969491500000
              15 S      3.329820777000    2.079750291000    0.533704407000
              16 O      4.132307341000    1.615487900000   -0.606459980000
