-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -944.6617819370
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 1
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                965
     Surface Area:         696.0891150393
     Dielectric Energy:     -0.0080046561
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 17
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 18
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.1562     6.0000    -0.1562     3.7708     3.7708     0.0000
  1   0     6.1518     6.0000    -0.1518     3.8237     3.8237    -0.0000
  2   0     6.1491     6.0000    -0.1491     3.8026     3.8026     0.0000
  3   0     5.9462     6.0000     0.0538     3.7213     3.7213    -0.0000
  4   0     6.1619     6.0000    -0.1619     3.8301     3.8301    -0.0000
  5   0     6.1572     6.0000    -0.1572     3.7725     3.7725     0.0000
  6   0     0.8189     1.0000     0.1811     0.9876     0.9876    -0.0000
  7   0     5.7057     6.0000     0.2943     4.0745     4.0745     0.0000
  8   0     0.8222     1.0000     0.1778     0.9692     0.9692     0.0000
  9   0     0.8196     1.0000     0.1804     0.9692     0.9692    -0.0000
 10   0     0.8175     1.0000     0.1825     0.9854     0.9854    -0.0000
 11   0     0.8228     1.0000     0.1772     0.9698     0.9698    -0.0000
 12   0     7.1249     7.0000    -0.1249     2.8711     2.8711     0.0000
 13   0     8.3517     8.0000    -0.3517     1.9319     1.9319     0.0000
 14   0     8.3762     8.0000    -0.3762     2.0732     2.0732    -0.0000
 15   0    15.0780    16.0000     0.9220     3.9148     3.9148    -0.0000
 16   0     8.5399     8.0000    -0.5399     1.9650     1.9650    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.394611
                0             6               2            6                1.349696
                0             6               8            1                0.971232
                1             6               3            6                1.312718
                1             6               6            1                0.961703
                2             6               5            6                1.376265
                2             6               9            1                0.970376
                3             6               4            6                1.338582
                3             6               7            6                1.063887
                4             6               5            6                1.369544
                4             6              10            1                0.960620
                5             6              11            1                0.971510
                7             6              12            7                1.892826
                7             6              14            8                1.067566
               12             7              13            8                0.847632
               13             8              15           16                1.014721
               14             8              15           16                0.943905
               15            16              16            8                1.894480
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 1
   prop. index: 1
        Total number of Electrons                                                     94
        Number of correlated Electrons                                                62
        Number of alpha correlated Electrons                                          31
        Number of beta correlated Electrons                                           31
        Reference Energy:                                                -944.6617820167
        Total Correlation Energy:                                          -2.2939058324
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1107830577
        Total MDCI Energy:                                               -946.9556878491
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:     -944.7098296742
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 2
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                965
     Surface Area:         696.0891150393
     Dielectric Energy:     -0.0079249135
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 2
   prop. index: 1
     Number of atoms                     : 17
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 18
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.1291     6.0000    -0.1291     3.7810     3.7810    -0.0000
  1   0     6.1393     6.0000    -0.1393     3.7949     3.7949    -0.0000
  2   0     6.1541     6.0000    -0.1541     3.8221     3.8221     0.0000
  3   0     6.0513     6.0000    -0.0513     3.7355     3.7355     0.0000
  4   0     6.1429     6.0000    -0.1429     3.7952     3.7952     0.0000
  5   0     6.1308     6.0000    -0.1308     3.7868     3.7868    -0.0000
  6   0     0.8162     1.0000     0.1838     0.9958     0.9958     0.0000
  7   0     5.6089     6.0000     0.3911     4.0500     4.0500    -0.0000
  8   0     0.8214     1.0000     0.1786     0.9741     0.9741    -0.0000
  9   0     0.8128     1.0000     0.1872     0.9730     0.9730     0.0000
 10   0     0.8204     1.0000     0.1796     0.9993     0.9993     0.0000
 11   0     0.8225     1.0000     0.1775     0.9751     0.9751     0.0000
 12   0     7.1596     7.0000    -0.1596     2.8854     2.8854    -0.0000
 13   0     8.3962     8.0000    -0.3962     1.8955     1.8955    -0.0000
 14   0     8.4454     8.0000    -0.4454     2.0007     2.0007    -0.0000
 15   0    14.9410    16.0000     1.0590     3.8254     3.8254    -0.0000
 16   0     8.6079     8.0000    -0.6079     1.8909     1.8909    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.416586
                0             6               2            6                1.369864
                0             6               8            1                0.976222
                1             6               3            6                1.285279
                1             6               6            1                0.969642
                2             6               5            6                1.397631
                2             6               9            1                0.975775
                3             6               4            6                1.304746
                3             6               7            6                1.142817
                4             6               5            6                1.393237
                4             6              10            1                0.971494
                5             6              11            1                0.976828
                7             6              12            7                1.886507
                7             6              14            8                1.043070
               12             7              13            8                0.849147
               13             8              15           16                0.991823
               14             8              15           16                0.910888
               15            16              16            8                1.857729
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 2
   prop. index: 1
        Total number of Electrons                                                     94
        Number of correlated Electrons                                                62
        Number of alpha correlated Electrons                                          31
        Number of beta correlated Electrons                                           31
        Reference Energy:                                                -944.7098295993
        Total Correlation Energy:                                          -2.4354536475
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1212581906
        Total MDCI Energy:                                               -947.1452832467
# -----------------------------------------------------------
$ Energy_Extrapolation
   description: The Extrapolation of the energies
   geom. index: 2
   prop. index: 1
       Do EP1                            : 1 
       Do EP2                            : 0 
       Do EP3                            : 0 
       Number of energies                : 2 
       SCF energy with basis  cc-pVTZ :   -944.6617820167 
       SCF energy with basis  cc-pVQZ :   -944.7098295993 
       CBS SCF Energy                    :   -944.7243065245 
       Correlation energy with basis  cc-pVTZ :   -944.6617820167 
       Correlation energy with basis  cc-pVQZ :   -944.7098295993 
       CBS Correlation Energy            :     -2.5362200485 
       CBS Total Energy                  :   -947.2605265729 
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 2
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       17
     number of electrons:                   94
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             434
     number of aux C basis functions:       1885
     number of aux J basis functions:       0
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           0.000000
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 2
   prop. index: 1
       Filename                          : /scratch/obeletsan/orcajob__14421-h9r9/int_phh.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        5.0194193684
        Electronic Contribution:
                  0    
      0       9.954653
      1       7.740448
      2      -0.405927
        Nuclear Contribution:
                  0    
      0     -11.080403
      1      -8.759029
      2       1.668791
        Total Dipole moment:
                  0    
      0      -1.125750
      1      -1.018581
      2       1.262864
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     1 
    Coordinates:
               0 C      0.080651751000    0.092932213000   -0.086307336000
               1 C      1.059555752000    1.069017138000   -0.220361600000
               2 C      0.399150848000   -1.157278295000    0.446008001000
               3 C      2.371724483000    0.784687307000    0.176013318000
               4 C      2.697882682000   -0.464917158000    0.711018731000
               5 C      1.705520750000   -1.431900445000    0.843876587000
               6 H      0.820925396000    2.042928747000   -0.623575885000
               7 C      3.407022069000    1.799097407000    0.024218161000
               8 H     -0.932855386000    0.308029198000   -0.392590107000
               9 H     -0.368088491000   -1.910536164000    0.550874845000
              10 H      3.714464928000   -0.673467563000    1.010754879000
              11 H      1.954396581000   -2.398975248000    1.256248546000
              12 N      3.278827644000    2.886760671000   -0.654141986000
              13 O      4.484433053000    3.624552361000   -0.513526402000
              14 O      4.605611916000    1.585790010000    0.654503107000
              15 S      5.744168591000    2.652853907000    0.015259641000
              16 O      6.351007433000    1.987425910000   -1.127572498000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     2 
    Coordinates:
               0 C      0.080651751000    0.092932213000   -0.086307336000
               1 C      1.059555752000    1.069017138000   -0.220361600000
               2 C      0.399150848000   -1.157278295000    0.446008001000
               3 C      2.371724483000    0.784687307000    0.176013318000
               4 C      2.697882682000   -0.464917158000    0.711018731000
               5 C      1.705520750000   -1.431900445000    0.843876587000
               6 H      0.820925396000    2.042928747000   -0.623575885000
               7 C      3.407022069000    1.799097407000    0.024218161000
               8 H     -0.932855386000    0.308029198000   -0.392590107000
               9 H     -0.368088491000   -1.910536164000    0.550874845000
              10 H      3.714464928000   -0.673467563000    1.010754879000
              11 H      1.954396581000   -2.398975248000    1.256248546000
              12 N      3.278827644000    2.886760671000   -0.654141986000
              13 O      4.484433053000    3.624552361000   -0.513526402000
              14 O      4.605611916000    1.585790010000    0.654503107000
              15 S      5.744168591000    2.652853907000    0.015259641000
              16 O      6.351007433000    1.987425910000   -1.127572498000
