-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -944.6171593613
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 1
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1058
     Surface Area:         765.1861664021
     Dielectric Energy:     -0.0094114616
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 17
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 17
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.1453     6.0000    -0.1453     3.7711     3.7711     0.0000
  1   0     6.1652     6.0000    -0.1652     3.8287     3.8287    -0.0000
  2   0     6.1550     6.0000    -0.1550     3.8105     3.8105     0.0000
  3   0     5.9334     6.0000     0.0666     3.7539     3.7539     0.0000
  4   0     6.1652     6.0000    -0.1652     3.8154     3.8154    -0.0000
  5   0     6.1420     6.0000    -0.1420     3.7634     3.7634    -0.0000
  6   0     0.8189     1.0000     0.1811     0.9746     0.9746     0.0000
  7   0     5.9677     6.0000     0.0323     3.9351     3.9351     0.0000
  8   0     0.8205     1.0000     0.1795     0.9681     0.9681    -0.0000
  9   0     0.8181     1.0000     0.1819     0.9691     0.9691     0.0000
 10   0     0.8125     1.0000     0.1875     0.9924     0.9924     0.0000
 11   0     0.8191     1.0000     0.1809     0.9680     0.9680    -0.0000
 12   0     6.7064     7.0000     0.2936     3.9797     3.9797     0.0000
 13   0     8.4955     8.0000    -0.4955     1.6722     1.6722     0.0000
 14   0     8.5064     8.0000    -0.5064     2.0053     2.0053    -0.0000
 15   0    15.0417    16.0000     0.9583     3.8712     3.8712    -0.0000
 16   0     8.4870     8.0000    -0.4870     2.0205     2.0205     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.386997
                0             6               2            6                1.363467
                0             6               8            1                0.968957
                1             6               3            6                1.311533
                1             6               6            1                0.969052
                2             6               5            6                1.361654
                2             6               9            1                0.969402
                3             6               4            6                1.324789
                3             6               7            6                1.076451
                4             6               5            6                1.379242
                4             6              10            1                0.955035
                5             6              11            1                0.968889
                7             6              12            7                2.588487
                7             6              13            8                0.238433
               12             7              13            8                1.324946
               14             8              15           16                1.878571
               15            16              16            8                1.915335
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 1
   prop. index: 1
        Total number of Electrons                                                     94
        Number of correlated Electrons                                                62
        Number of alpha correlated Electrons                                          31
        Number of beta correlated Electrons                                           31
        Reference Energy:                                                -944.6171593672
        Total Correlation Energy:                                          -2.3060720463
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1138119737
        Total MDCI Energy:                                               -946.9232314135
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:     -944.6663619420
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 2
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1058
     Surface Area:         765.1861664021
     Dielectric Energy:     -0.0093644460
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 2
   prop. index: 1
     Number of atoms                     : 17
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 17
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.1186     6.0000    -0.1186     3.7767     3.7767     0.0000
  1   0     6.1780     6.0000    -0.1780     3.8562     3.8562     0.0000
  2   0     6.1672     6.0000    -0.1672     3.8421     3.8421     0.0000
  3   0     5.9598     6.0000     0.0402     3.7321     3.7321    -0.0000
  4   0     6.1911     6.0000    -0.1911     3.8364     3.8364    -0.0000
  5   0     6.1135     6.0000    -0.1135     3.7631     3.7631    -0.0000
  6   0     0.8170     1.0000     0.1830     0.9813     0.9813     0.0000
  7   0     5.8160     6.0000     0.1840     3.8209     3.8209    -0.0000
  8   0     0.8203     1.0000     0.1797     0.9751     0.9751    -0.0000
  9   0     0.8093     1.0000     0.1907     0.9720     0.9720    -0.0000
 10   0     0.8097     1.0000     0.1903     0.9993     0.9993     0.0000
 11   0     0.8188     1.0000     0.1812     0.9740     0.9740    -0.0000
 12   0     6.8211     7.0000     0.1789     3.9004     3.9004    -0.0000
 13   0     8.5143     8.0000    -0.5143     1.6760     1.6760    -0.0000
 14   0     8.5706     8.0000    -0.5706     1.9394     1.9394    -0.0000
 15   0    14.9324    16.0000     1.0676     3.8093     3.8093     0.0000
 16   0     8.5422     8.0000    -0.5422     1.9517     1.9517     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.417506
                0             6               2            6                1.385048
                0             6               8            1                0.976011
                1             6               3            6                1.294324
                1             6               6            1                0.975889
                2             6               5            6                1.379606
                2             6               9            1                0.972995
                3             6               4            6                1.310333
                3             6               7            6                1.135522
                4             6               5            6                1.405030
                4             6              10            1                0.961289
                5             6              11            1                0.975248
                7             6              12            7                2.505231
                7             6              13            8                0.202454
               12             7              13            8                1.344898
               14             8              15           16                1.849498
               15            16              16            8                1.875903
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 2
   prop. index: 1
        Total number of Electrons                                                     94
        Number of correlated Electrons                                                62
        Number of alpha correlated Electrons                                          31
        Number of beta correlated Electrons                                           31
        Reference Energy:                                                -944.6663619614
        Total Correlation Energy:                                          -2.4467140063
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1244682249
        Total MDCI Energy:                                               -947.1130759677
# -----------------------------------------------------------
$ Energy_Extrapolation
   description: The Extrapolation of the energies
   geom. index: 2
   prop. index: 1
       Do EP1                            : 1 
       Do EP2                            : 0 
       Do EP3                            : 0 
       Number of energies                : 2 
       SCF energy with basis  cc-pVTZ :   -944.6171593672 
       SCF energy with basis  cc-pVQZ :   -944.6663619614 
       CBS SCF Energy                    :   -944.6811868961 
       Correlation energy with basis  cc-pVTZ :   -944.6171593672 
       Correlation energy with basis  cc-pVQZ :   -944.6663619614 
       CBS Correlation Energy            :     -2.5468355386 
       CBS Total Energy                  :   -947.2280224347 
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 2
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       17
     number of electrons:                   94
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             434
     number of aux C basis functions:       1885
     number of aux J basis functions:       0
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           0.000000
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 2
   prop. index: 1
       Filename                          : /scratch/obeletsan/orcajob__14423-s8MG/preint_phh.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        6.2709184170
        Electronic Contribution:
                  0    
      0      10.265959
      1       8.331345
      2       0.858777
        Nuclear Contribution:
                  0    
      0     -12.220883
      1      -9.702724
      2      -0.238881
        Total Dipole moment:
                  0    
      0      -1.954923
      1      -1.371379
      2       0.619896
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     1 
    Coordinates:
               0 C     -2.556969419000   -0.873249372000   -0.445082362000
               1 C     -1.770292983000    0.263631147000   -0.594114998000
               2 C     -2.023045659000   -2.033315404000    0.114037095000
               3 C     -0.433633579000    0.229866092000   -0.176257399000
               4 C      0.110623043000   -0.931761518000    0.389152381000
               5 C     -0.692284645000   -2.058028261000    0.529664822000
               6 H     -2.175905525000    1.167004833000   -1.025819770000
               7 C      0.392305952000    1.382157443000   -0.321121165000
               8 H     -3.588212377000   -0.851537521000   -0.766081012000
               9 H     -2.640713214000   -2.912205678000    0.226130972000
              10 H      1.143279133000   -0.934325542000    0.707721275000
              11 H     -0.277382778000   -2.955476294000    0.965350246000
              12 N      1.074106245000    2.329320107000   -0.459837400000
              13 O      1.833746799000    3.275100625000   -0.558782143000
              14 O      2.982196283000    1.074071512000    1.202374441000
              15 S      3.907931150000    2.094006165000    0.691286420000
              16 O      4.714251573000    1.734741665000   -0.478621407000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     2 
    Coordinates:
               0 C     -2.556969419000   -0.873249372000   -0.445082362000
               1 C     -1.770292983000    0.263631147000   -0.594114998000
               2 C     -2.023045659000   -2.033315404000    0.114037095000
               3 C     -0.433633579000    0.229866092000   -0.176257399000
               4 C      0.110623043000   -0.931761518000    0.389152381000
               5 C     -0.692284645000   -2.058028261000    0.529664822000
               6 H     -2.175905525000    1.167004833000   -1.025819770000
               7 C      0.392305952000    1.382157443000   -0.321121165000
               8 H     -3.588212377000   -0.851537521000   -0.766081012000
               9 H     -2.640713214000   -2.912205678000    0.226130972000
              10 H      1.143279133000   -0.934325542000    0.707721275000
              11 H     -0.277382778000   -2.955476294000    0.965350246000
              12 N      1.074106245000    2.329320107000   -0.459837400000
              13 O      1.833746799000    3.275100625000   -0.558782143000
              14 O      2.982196283000    1.074071512000    1.202374441000
              15 S      3.907931150000    2.094006165000    0.691286420000
              16 O      4.714251573000    1.734741665000   -0.478621407000
