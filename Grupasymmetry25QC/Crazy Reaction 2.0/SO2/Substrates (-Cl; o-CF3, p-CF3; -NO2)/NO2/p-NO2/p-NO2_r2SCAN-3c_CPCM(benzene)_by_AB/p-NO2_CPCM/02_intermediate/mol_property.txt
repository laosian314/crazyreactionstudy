-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:    -1152.6892237554
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                 57.9999635859 
   Number of Beta  Electrons                 57.9999635859 
   Total number of  Electrons               115.9999271718 
   Exchange energy                         -117.5004379443 
   Correlation energy                        -4.0110457941 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -121.5114837384 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1152.6892237554 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 1
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1066
     Surface Area:         811.9021476144
     Dielectric Energy:     -0.0079029810
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 1
   prop. index: 1
        Van der Waals Correction:       -0.0082287591
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:    -1152.6892965285
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 2
   prop. index: 1
   Number of Alpha Electrons                 57.9999630129 
   Number of Beta  Electrons                 57.9999630129 
   Total number of  Electrons               115.9999260259 
   Exchange energy                         -117.4980652865 
   Correlation energy                        -4.0108445434 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -121.5089098299 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1152.6892965285 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 2
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1068
     Surface Area:         812.3469793099
     Dielectric Energy:     -0.0080016932
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 2
   prop. index: 1
        Van der Waals Correction:       -0.0082280147
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 3
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 3
   prop. index: 1
        SCF Energy:    -1152.6893131619
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 3
   prop. index: 1
   Number of Alpha Electrons                 57.9999622779 
   Number of Beta  Electrons                 57.9999622779 
   Total number of  Electrons               115.9999245558 
   Exchange energy                         -117.4971044515 
   Correlation energy                        -4.0107768950 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -121.5078813466 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1152.6893131619 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 3
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1069
     Surface Area:         812.6006964048
     Dielectric Energy:     -0.0080723723
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 3
   prop. index: 1
        Van der Waals Correction:       -0.0082291036
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 4
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 4
   prop. index: 1
        SCF Energy:    -1152.6892761713
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 4
   prop. index: 1
   Number of Alpha Electrons                 57.9999621374 
   Number of Beta  Electrons                 57.9999621374 
   Total number of  Electrons               115.9999242747 
   Exchange energy                         -117.4957126940 
   Correlation energy                        -4.0106884901 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -121.5064011841 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1152.6892761713 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 4
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1068
     Surface Area:         812.4871986003
     Dielectric Energy:     -0.0081991975
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 4
   prop. index: 1
        Van der Waals Correction:       -0.0082316231
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 5
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 5
   prop. index: 1
        SCF Energy:    -1152.6893267007
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 5
   prop. index: 1
   Number of Alpha Electrons                 57.9999624778 
   Number of Beta  Electrons                 57.9999624778 
   Total number of  Electrons               115.9999249556 
   Exchange energy                         -117.4973333472 
   Correlation energy                        -4.0107974837 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -121.5081308309 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1152.6893267007 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 5
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1069
     Surface Area:         812.4277081289
     Dielectric Energy:     -0.0080731089
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 5
   prop. index: 1
        Van der Waals Correction:       -0.0082295855
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 6
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 6
   prop. index: 1
        SCF Energy:    -1152.6893270834
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 6
   prop. index: 1
   Number of Alpha Electrons                 57.9999624188 
   Number of Beta  Electrons                 57.9999624188 
   Total number of  Electrons               115.9999248376 
   Exchange energy                         -117.4973705813 
   Correlation energy                        -4.0107976968 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -121.5081682782 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1152.6893270834 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 6
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1069
     Surface Area:         812.4296302592
     Dielectric Energy:     -0.0080769214
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 6
   prop. index: 1
        Van der Waals Correction:       -0.0082296176
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 6
   prop. index: 1
       Filename                          : mol.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        3.5847872065
        Electronic Contribution:
                  0    
      0       5.298173
      1       3.660221
      2      -0.018993
        Nuclear Contribution:
                  0    
      0      -4.318827
      1      -2.937527
      2       0.731483
        Total Dipole moment:
                  0    
      0       0.979345
      1       0.722693
      2       0.712490
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 7
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 7
   prop. index: 1
        SCF Energy:    -1152.6893270944
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 7
   prop. index: 1
   Number of Alpha Electrons                 57.9999624190 
   Number of Beta  Electrons                 57.9999624190 
   Total number of  Electrons               115.9999248380 
   Exchange energy                         -117.4973708148 
   Correlation energy                        -4.0107980098 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -121.5081688247 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1152.6893270944 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 7
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1069
     Surface Area:         812.4296302592
     Dielectric Energy:     -0.0080768482
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 7
   prop. index: 1
        Van der Waals Correction:       -0.0082296176
# -----------------------------------------------------------
$ THERMOCHEMISTRY_Energies
   description: The Thermochemistry energies
   geom. index: 7
   prop. index: 1
        Temperature (Kelvin)           :        298.1500000000
        Pressure (atm)                 :          1.0000000000
        Total Mass (AMU)               :        227.9840924101
        Spin Degeneracy                :          1.0000000000
        Electronic Energy (Hartree)    :      -1152.6721648301
        Translational Energy (Hartree) :          0.0014162714
        Rotational Energy (Hartree)    :          0.0014162714
        Vibrational Energy (Hartree)   :          0.0095792893
        Number of frequencies          :     57      
        Scaling Factor for frequencies :          1.0000000000
        Vibrational frequencies        :     
                  0    
      0       0.000000
      1       0.000000
      2       0.000000
      3       0.000000
      4       0.000000
      5       0.000000
      6      29.047551
      7      49.949690
      8      51.309943
      9      97.542529
     10     120.530554
     11     195.394405
     12     238.701029
     13     259.489682
     14     284.603570
     15     317.023064
     16     396.220196
     17     404.514468
     18     428.820351
     19     459.485314
     20     484.277084
     21     501.019391
     22     540.412517
     23     639.784500
     24     644.220456
     25     648.515899
     26     671.479913
     27     695.430108
     28     740.449350
     29     764.994909
     30     838.450703
     31     867.041782
     32     870.524961
     33     905.096060
     34     940.815337
     35     977.036896
     36     986.963053
     37     1033.878028
     38     1072.838612
     39     1119.720043
     40     1139.009317
     41     1203.702495
     42     1221.210352
     43     1321.843217
     44     1338.240801
     45     1349.594678
     46     1372.991983
     47     1433.555547
     48     1519.446939
     49     1552.052768
     50     1606.357814
     51     1631.720612
     52     1639.648309
     53     3202.802474
     54     3206.941225
     55     3222.583194
     56     3223.287597
        Zero Point Energy (Hartree)    :          0.1149576400
        Inner Energy (Hartree)         :      -1152.5447953579
        Enthalpy (Hartree)             :      -1152.5438511488
        Electronic entropy             :          0.0000000000
        Rotational entropy             :          0.0154579977
        Vibrational entropy            :          0.0167088480
        Translational entropy          :          0.0154579977
        Entropy                        :          0.0522052452
        Gibbs Energy (Hartree)         :      -1152.5960563940
        Is Linear                      :                 false
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     1 
    Coordinates:
               0 C      0.086992576317    0.113240841879   -0.101669002065
               1 C      1.080148538909    1.065932617290   -0.236895813704
               2 C      0.410869400047   -1.118124838152    0.459142413403
               3 C      2.387634709407    0.779871628188    0.182814759407
               4 C      2.690054226559   -0.466395650295    0.742775372580
               5 C      1.695918282763   -1.423194458485    0.883749430382
               6 H      0.855053167660    2.035439573816   -0.667758171768
               7 C      3.430849134058    1.789628459908    0.040034059102
               8 H     -0.930083917283    0.308618747835   -0.419496941348
               9 N     -0.650753278238   -2.136589609858    0.607595130159
              10 H      3.702007854876   -0.686001207500    1.063540724696
              11 H      1.904601506096   -2.395458454615    1.313554468778
              12 N      3.262564363528    2.916203018839   -0.553592598800
              13 O      4.443266334888    3.654983948650   -0.497455875804
              14 O      4.660098483626    1.519136438080    0.577971206551
              15 S      5.804635629192    2.670805061521   -0.020665229621
              16 O      6.417194039715    2.103262609548   -1.222201506139
              17 O     -1.773787786923   -1.839392402307    0.222703756469
              18 O     -0.336923265198   -3.209576324342    1.105653817721
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     2 
    Coordinates:
               0 C      0.085626546502    0.113454680274   -0.102449366640
               1 C      1.078136537040    1.066215935785   -0.238409415629
               2 C      0.411394052352   -1.117889724127    0.458570457876
               3 C      2.386279413498    0.779899965850    0.180405385317
               4 C      2.690532079281   -0.466266445770    0.740280999045
               5 C      1.697271640031   -1.423226740835    0.882407137783
               6 H      0.850300222524    2.035616690631   -0.668103793138
               7 C      3.429934839229    1.789711027525    0.038118357661
               8 H     -0.931414955601    0.310691192372   -0.419383282552
               9 N     -0.649101524616   -2.135296249372    0.609524835679
              10 H      3.702538570516   -0.686279389601    1.060923111049
              11 H      1.908845459493   -2.394595414612    1.312918718089
              12 N      3.264232888822    2.916642276562   -0.556108110363
              13 O      4.446886293853    3.654827773800   -0.496827935105
              14 O      4.658248347672    1.518672642696    0.576473436223
              15 S      5.806089152720    2.670145024164   -0.014477908425
              16 O      6.416917177432    2.102056626831   -1.217893846951
              17 O     -1.774527771126   -1.842147571875    0.226312679833
              18 O     -0.337848969621   -3.209842300298    1.107518540248
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     3 
    Coordinates:
               0 C      0.084985771832    0.113004040025   -0.103896371003
               1 C      1.077149358116    1.066135054617   -0.239745792466
               2 C      0.412188672756   -1.118416518719    0.456586888304
               3 C      2.385449339061    0.779622085106    0.178475969150
               4 C      2.691072407370   -0.466617975426    0.737739602531
               5 C      1.698371548760   -1.424027892290    0.880123989684
               6 H      0.846748873218    2.035282410263   -0.668633388972
               7 C      3.429244819083    1.789519648643    0.036786866501
               8 H     -0.931598273212    0.312189874624   -0.420933069766
               9 N     -0.646382391163   -2.134621116069    0.607826326126
              10 H      3.702901937167   -0.687486618288    1.058166886311
              11 H      1.912869250747   -2.394754764659    1.310533272562
              12 N      3.264814154870    2.916891353418   -0.557752957902
              13 O      4.448447896078    3.655127053479   -0.496113134825
              14 O      4.656136943229    1.518126695308    0.576573684007
              15 S      5.805196371605    2.669350403053   -0.009064047882
              16 O      6.416358701580    2.101144578940   -1.213451901067
              17 O     -1.775260136295   -1.840734067301    0.233060308493
              18 O     -0.338355244801   -3.207344244722    1.113516870213
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     4 
    Coordinates:
               0 C      0.082722427714    0.114497211839   -0.100596070131
               1 C      1.074965243867    1.067426300139   -0.238664635939
               2 C      0.411948454822   -1.116330186525    0.460659797274
               3 C      2.383630660510    0.780268044906    0.177526979322
               4 C      2.691272649658   -0.465485873722    0.737174121762
               5 C      1.698911927656   -1.422837765287    0.882430362774
               6 H      0.839961250091    2.035476505054   -0.667488902534
               7 C      3.428266375026    1.789273384747    0.034082080958
               8 H     -0.933019608951    0.317872086623   -0.417264961894
               9 N     -0.643615898994   -2.128791500419    0.615547264840
              10 H      3.702855195089   -0.688469696213    1.056442989667
              11 H      1.918782484385   -2.391785128261    1.313851238675
              12 N      3.266077306874    2.916468174717   -0.562865401295
              13 O      4.451959541261    3.654089180819   -0.499516170920
              14 O      4.652804189576    1.517323326868    0.575949915140
              15 S      5.803787675461    2.665655000135   -0.004293967215
              16 O      6.413678399564    2.094560731440   -1.210199412160
              17 O     -1.771022509174   -1.846059712573    0.223571809981
              18 O     -0.333625764436   -3.210760084286    1.103452961696
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     5 
    Coordinates:
               0 C      0.084848015464    0.113447505298   -0.102427669135
               1 C      1.077308147274    1.066501303042   -0.238718961062
               2 C      0.411689205670   -1.117874772763    0.458349101030
               3 C      2.385487150348    0.780051291219    0.179282291157
               4 C      2.690899696974   -0.466079629915    0.738733606194
               5 C      1.697952430506   -1.423414013287    0.881569347608
               6 H      0.846419923960    2.035341117140   -0.668002482898
               7 C      3.429378209444    1.789781763495    0.036987327769
               8 H     -0.931461935358    0.313409265045   -0.419655762727
               9 N     -0.646684329644   -2.133551637374    0.610000766021
              10 H      3.702621820320   -0.687480726980    1.059038620235
              11 H      1.913145748334   -2.393827215034    1.312197636519
              12 N      3.264818311192    2.916577907886   -0.558507529819
              13 O      4.448468703512    3.654776135226   -0.497121846299
              14 O      4.656176089180    1.518808038968    0.577285115309
              15 S      5.804906745654    2.668421848442   -0.010538308604
              16 O      6.414992900362    2.099003210202   -1.215033812482
              17 O     -1.773813297113   -1.842405968266    0.227577548299
              18 O     -0.336813536078   -3.209095422346    1.108785012884
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     6 
    Coordinates:
               0 C      0.084804249510    0.113395314657   -0.102357901148
               1 C      1.077266191783    1.066513817619   -0.238639193118
               2 C      0.411622297056   -1.117995632533    0.458275329129
               3 C      2.385463402954    0.780089949133    0.179169029720
               4 C      2.690909268564   -0.466084023074    0.738473153563
               5 C      1.697970243062   -1.423461100537    0.881331261530
               6 H      0.845962927003    2.035294554718   -0.667829560626
               7 C      3.429343440940    1.789848419736    0.036876183762
               8 H     -0.931411988238    0.313782605683   -0.419605535962
               9 N     -0.646582818826   -2.133589496615    0.609902969289
              10 H      3.702592969051   -0.687737187942    1.058724014204
              11 H      1.913655341488   -2.393783697139    1.311909780640
              12 N      3.264873179586    2.916488346511   -0.558919685796
              13 O      4.448568555373    3.654737637779   -0.497519430319
              14 O      4.656045134156    1.519098981195    0.577559151240
              15 S      5.804731283363    2.668419842070   -0.010158187493
              16 O      6.415196688662    2.098725254249   -1.214398792250
              17 O     -1.773851495083   -1.842371644657    0.227861353776
              18 O     -0.336818870404   -3.208981940854    1.109146059859
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     7 
    Coordinates:
               0 C      0.084804249510    0.113395314657   -0.102357901148
               1 C      1.077266191783    1.066513817619   -0.238639193118
               2 C      0.411622297056   -1.117995632533    0.458275329129
               3 C      2.385463402954    0.780089949133    0.179169029720
               4 C      2.690909268564   -0.466084023074    0.738473153563
               5 C      1.697970243062   -1.423461100537    0.881331261530
               6 H      0.845962927003    2.035294554718   -0.667829560626
               7 C      3.429343440940    1.789848419736    0.036876183762
               8 H     -0.931411988238    0.313782605683   -0.419605535962
               9 N     -0.646582818826   -2.133589496615    0.609902969289
              10 H      3.702592969051   -0.687737187942    1.058724014204
              11 H      1.913655341488   -2.393783697139    1.311909780640
              12 N      3.264873179586    2.916488346511   -0.558919685796
              13 O      4.448568555373    3.654737637779   -0.497519430319
              14 O      4.656045134156    1.519098981195    0.577559151240
              15 S      5.804731283363    2.668419842070   -0.010158187493
              16 O      6.415196688662    2.098725254249   -1.214398792250
              17 O     -1.773851495083   -1.842371644657    0.227861353776
              18 O     -0.336818870404   -3.208981940854    1.109146059859
