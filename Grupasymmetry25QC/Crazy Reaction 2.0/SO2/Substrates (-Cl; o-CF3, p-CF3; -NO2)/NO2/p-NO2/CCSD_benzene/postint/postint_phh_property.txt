-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:    -1148.2754569955
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 1
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1170
     Surface Area:         888.1415343131
     Dielectric Energy:     -0.0121814850
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 19
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 20
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.1221     6.0000    -0.1221     3.8439     3.8439    -0.0000
  1   0     6.1947     6.0000    -0.1947     3.7914     3.7914    -0.0000
  2   0     5.9669     6.0000     0.0331     3.7523     3.7523    -0.0000
  3   0     5.7571     6.0000     0.2429     3.8596     3.8596    -0.0000
  4   0     6.2231     6.0000    -0.2231     3.7501     3.7501    -0.0000
  5   0     6.1321     6.0000    -0.1321     3.8496     3.8496    -0.0000
  6   0     0.8198     1.0000     0.1802     0.9824     0.9824     0.0000
  7   0     5.5871     6.0000     0.4129     4.2627     4.2627     0.0000
  8   0     0.7927     1.0000     0.2073     0.9753     0.9753    -0.0000
  9   0     6.5039     7.0000     0.4961     4.2433     4.2433    -0.0000
 10   0     0.8174     1.0000     0.1826     0.9862     0.9862    -0.0000
 11   0     0.7913     1.0000     0.2087     0.9767     0.9767    -0.0000
 12   0     7.2109     7.0000    -0.2109     3.0792     3.0792    -0.0000
 13   0     8.3311     8.0000    -0.3311     2.2667     2.2667     0.0000
 14   0     8.4668     8.0000    -0.4668     2.0493     2.0493    -0.0000
 15   0    15.0398    16.0000     0.9602     3.8385     3.8385    -0.0000
 16   0     8.4761     8.0000    -0.4761     2.0383     2.0383    -0.0000
 17   0     8.3830     8.0000    -0.3830     1.8745     1.8745    -0.0000
 18   0     8.3839     8.0000    -0.3839     1.8742     1.8742    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.385917
                0             6               2            6                1.361020
                0             6               8            1                0.944731
                1             6               3            6                1.347724
                1             6               6            1                0.964635
                2             6               5            6                1.357202
                2             6               9            7                0.964304
                3             6               4            6                1.314839
                3             6              12            7                1.056219
                4             6               5            6                1.385798
                4             6              10            1                0.959985
                5             6              11            1                0.943264
                7             6              12            7                1.979897
                7             6              13            8                2.168710
                9             7              17            8                1.641220
                9             7              18            8                1.640465
               14             8              15           16                1.918949
               14             8              16            8                0.113979
               15            16              16            8                1.910274
               17             8              18            8                0.149266
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 1
   prop. index: 1
        Total number of Electrons                                                    116
        Number of correlated Electrons                                                78
        Number of alpha correlated Electrons                                          39
        Number of beta correlated Electrons                                           39
        Reference Energy:                                               -1148.2754569749
        Total Correlation Energy:                                          -2.9711937442
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1450930957
        Total MDCI Energy:                                              -1151.2466507191
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:    -1148.3393698141
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 2
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1170
     Surface Area:         888.1415343131
     Dielectric Energy:     -0.0120917240
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 2
   prop. index: 1
     Number of atoms                     : 19
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 19
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.0832     6.0000    -0.0832     3.8320     3.8320     0.0000
  1   0     6.1757     6.0000    -0.1757     3.8072     3.8072     0.0000
  2   0     6.0173     6.0000    -0.0173     3.7502     3.7502     0.0000
  3   0     5.7344     6.0000     0.2656     3.8012     3.8012     0.0000
  4   0     6.1717     6.0000    -0.1717     3.7487     3.7487    -0.0000
  5   0     6.1066     6.0000    -0.1066     3.8238     3.8238    -0.0000
  6   0     0.8085     1.0000     0.1915     0.9838     0.9838     0.0000
  7   0     5.3178     6.0000     0.6822     4.2632     4.2632     0.0000
  8   0     0.7730     1.0000     0.2270     0.9690     0.9690     0.0000
  9   0     6.5326     7.0000     0.4674     4.2730     4.2730    -0.0000
 10   0     0.8331     1.0000     0.1669     0.9965     0.9965     0.0000
 11   0     0.7708     1.0000     0.2292     0.9707     0.9707    -0.0000
 12   0     7.5061     7.0000    -0.5061     3.1155     3.1155     0.0000
 13   0     8.3985     8.0000    -0.3985     2.2397     2.2397    -0.0000
 14   0     8.5292     8.0000    -0.5292     1.9828     1.9828    -0.0000
 15   0    14.9210    16.0000     1.0790     3.7696     3.7696    -0.0000
 16   0     8.5391     8.0000    -0.5391     1.9730     1.9730     0.0000
 17   0     8.3902     8.0000    -0.3902     1.8718     1.8718    -0.0000
 18   0     8.3911     8.0000    -0.3911     1.8703     1.8703     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.404555
                0             6               2            6                1.362551
                0             6               8            1                0.936612
                1             6               3            6                1.357928
                1             6               6            1                0.969527
                2             6               5            6                1.351525
                2             6               9            7                0.988508
                3             6               4            6                1.318183
                3             6              12            7                1.028072
                4             6               5            6                1.393393
                4             6              10            1                0.984825
                5             6              11            1                0.937441
                7             6              12            7                2.054916
                7             6              13            8                2.118300
                9             7              17            8                1.657342
                9             7              18            8                1.655664
               14             8              15           16                1.886523
               15            16              16            8                1.875906
               17             8              18            8                0.143821
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 2
   prop. index: 1
        Total number of Electrons                                                    116
        Number of correlated Electrons                                                78
        Number of alpha correlated Electrons                                          39
        Number of beta correlated Electrons                                           39
        Reference Energy:                                               -1148.3393698741
        Total Correlation Energy:                                          -3.1588968855
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1587925671
        Total MDCI Energy:                                              -1151.4982667596
# -----------------------------------------------------------
$ Energy_Extrapolation
   description: The Extrapolation of the energies
   geom. index: 2
   prop. index: 1
       Do EP1                            : 1 
       Do EP2                            : 0 
       Do EP3                            : 0 
       Number of energies                : 2 
       SCF energy with basis  cc-pVTZ :  -1148.2754569749 
       SCF energy with basis  cc-pVQZ :  -1148.3393698741 
       CBS SCF Energy                    :  -1148.3586270814 
       Correlation energy with basis  cc-pVTZ :  -1148.2754569749 
       Correlation energy with basis  cc-pVQZ :  -1148.3393698741 
       CBS Correlation Energy            :     -3.2925207778 
       CBS Total Energy                  :  -1151.6511478592 
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 2
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       19
     number of electrons:                   116
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             510
     number of aux C basis functions:       2226
     number of aux J basis functions:       0
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           0.000000
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 2
   prop. index: 1
       Filename                          : /scratch/obeletsan/orcajob__14567-V0Nm/postint_phh.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        5.1745380496
        Electronic Contribution:
                  0    
      0       6.018729
      1       4.217494
      2      -0.050104
        Nuclear Contribution:
                  0    
      0      -4.308749
      1      -3.372957
      2      -0.662019
        Total Dipole moment:
                  0    
      0       1.709980
      1       0.844537
      2      -0.712123
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     1 
    Coordinates:
               0 C      1.019930000000   -1.437054000000    0.143720000000
               1 C      1.669695000000   -0.803955000000   -0.901691000000
               2 C      0.794789000000   -0.732790000000    1.322317000000
               3 C      2.091001000000    0.522451000000   -0.759822000000
               4 C      1.850981000000    1.220290000000    0.432521000000
               5 C      1.201241000000    0.589950000000    1.477902000000
               6 H      1.860844000000   -1.327201000000   -1.832383000000
               7 C      3.341210000000    2.150763000000   -2.017862000000
               8 H      0.689626000000   -2.464951000000    0.054229000000
               9 N      0.109234000000   -1.401269000000    2.431763000000
              10 H      2.176183000000    2.251165000000    0.531889000000
              11 H      1.006582000000    1.111708000000    2.407394000000
              12 N      2.739027000000    1.117079000000   -1.835875000000
              13 O      3.944409000000    3.099330000000   -2.339075000000
              14 O      4.506323000000    3.650792000000    1.055636000000
              15 S      5.596589000000    2.677476000000    0.885524000000
              16 O      5.282793000000    1.449508000000    0.136408000000
              17 O     -0.242838000000   -2.566318000000    2.270846000000
              18 O     -0.075149000000   -0.760174000000    3.462659000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     2 
    Coordinates:
               0 C      1.019930000000   -1.437054000000    0.143720000000
               1 C      1.669695000000   -0.803955000000   -0.901691000000
               2 C      0.794789000000   -0.732790000000    1.322317000000
               3 C      2.091001000000    0.522451000000   -0.759822000000
               4 C      1.850981000000    1.220290000000    0.432521000000
               5 C      1.201241000000    0.589950000000    1.477902000000
               6 H      1.860844000000   -1.327201000000   -1.832383000000
               7 C      3.341210000000    2.150763000000   -2.017862000000
               8 H      0.689626000000   -2.464951000000    0.054229000000
               9 N      0.109234000000   -1.401269000000    2.431763000000
              10 H      2.176183000000    2.251165000000    0.531889000000
              11 H      1.006582000000    1.111708000000    2.407394000000
              12 N      2.739027000000    1.117079000000   -1.835875000000
              13 O      3.944409000000    3.099330000000   -2.339075000000
              14 O      4.506323000000    3.650792000000    1.055636000000
              15 S      5.596589000000    2.677476000000    0.885524000000
              16 O      5.282793000000    1.449508000000    0.136408000000
              17 O     -0.242838000000   -2.566318000000    2.270846000000
              18 O     -0.075149000000   -0.760174000000    3.462659000000
