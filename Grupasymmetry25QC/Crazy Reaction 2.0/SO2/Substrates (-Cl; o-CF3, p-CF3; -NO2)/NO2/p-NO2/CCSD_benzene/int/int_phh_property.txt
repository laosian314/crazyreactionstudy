-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:    -1148.1935992940
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 1
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1069
     Surface Area:         812.4296423448
     Dielectric Energy:     -0.0114411085
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 19
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 21
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.1484     6.0000    -0.1484     3.8465     3.8465    -0.0000
  1   0     6.1336     6.0000    -0.1336     3.7934     3.7934    -0.0000
  2   0     5.9286     6.0000     0.0714     3.7685     3.7685    -0.0000
  3   0     5.9223     6.0000     0.0777     3.7514     3.7514    -0.0000
  4   0     6.1503     6.0000    -0.1503     3.8043     3.8043    -0.0000
  5   0     6.1409     6.0000    -0.1409     3.8457     3.8457     0.0000
  6   0     0.8113     1.0000     0.1887     0.9858     0.9858    -0.0000
  7   0     5.7126     6.0000     0.2874     4.0855     4.0855    -0.0000
  8   0     0.7892     1.0000     0.2108     0.9744     0.9744    -0.0000
  9   0     6.5038     7.0000     0.4962     4.2404     4.2404     0.0000
 10   0     0.8104     1.0000     0.1896     0.9828     0.9828    -0.0000
 11   0     0.7897     1.0000     0.2103     0.9751     0.9751     0.0000
 12   0     7.0978     7.0000    -0.0978     2.9177     2.9177    -0.0000
 13   0     8.3575     8.0000    -0.3575     1.9151     1.9151    -0.0000
 14   0     8.3921     8.0000    -0.3921     2.0653     2.0653    -0.0000
 15   0    15.0527    16.0000     0.9473     3.8368     3.8368     0.0000
 16   0     8.5164     8.0000    -0.5164     1.9882     1.9882     0.0000
 17   0     8.3706     8.0000    -0.3706     1.8871     1.8871    -0.0000
 18   0     8.3716     8.0000    -0.3716     1.8865     1.8865     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.387877
                0             6               2            6                1.353225
                0             6               8            1                0.947253
                1             6               3            6                1.314686
                1             6               6            1                0.959799
                2             6               5            6                1.382665
                2             6               9            7                0.950863
                3             6               4            6                1.349504
                3             6               7            6                1.058632
                4             6               5            6                1.358825
                4             6              10            1                0.959180
                5             6              11            1                0.946891
                7             6              12            7                1.910523
                7             6              14            8                1.077041
                9             7              17            8                1.647597
                9             7              18            8                1.647164
               12             7              13            8                0.873567
               13             8              15           16                0.956160
               14             8              15           16                0.919184
               15            16              16            8                1.906404
               17             8              18            8                0.158092
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 1
   prop. index: 1
        Total number of Electrons                                                    116
        Number of correlated Electrons                                                78
        Number of alpha correlated Electrons                                          39
        Number of beta correlated Electrons                                           39
        Reference Energy:                                               -1148.1935993027
        Total Correlation Energy:                                          -2.9870596647
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1465601660
        Total MDCI Energy:                                              -1151.1806589674
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:    -1148.2562551889
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 2
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1069
     Surface Area:         812.4296423448
     Dielectric Energy:     -0.0113678436
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 2
   prop. index: 1
     Number of atoms                     : 19
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 21
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.0885     6.0000    -0.0885     3.8193     3.8193    -0.0000
  1   0     6.1184     6.0000    -0.1184     3.7491     3.7491    -0.0000
  2   0     6.0030     6.0000    -0.0030     3.7775     3.7775    -0.0000
  3   0     6.0085     6.0000    -0.0085     3.7668     3.7668     0.0000
  4   0     6.1278     6.0000    -0.1278     3.7508     3.7508    -0.0000
  5   0     6.0795     6.0000    -0.0795     3.8218     3.8218    -0.0000
  6   0     0.8026     1.0000     0.1974     0.9893     0.9893    -0.0000
  7   0     5.6384     6.0000     0.3616     4.0787     4.0787     0.0000
  8   0     0.7731     1.0000     0.2269     0.9712     0.9712    -0.0000
  9   0     6.5337     7.0000     0.4663     4.2704     4.2704    -0.0000
 10   0     0.8091     1.0000     0.1909     0.9933     0.9933    -0.0000
 11   0     0.7748     1.0000     0.2252     0.9728     0.9728    -0.0000
 12   0     7.1304     7.0000    -0.1304     2.9320     2.9320     0.0000
 13   0     8.3868     8.0000    -0.3868     1.8960     1.8960    -0.0000
 14   0     8.4478     8.0000    -0.4478     2.0016     2.0016    -0.0000
 15   0    14.9407    16.0000     1.0593     3.7783     3.7783    -0.0000
 16   0     8.5824     8.0000    -0.5824     1.9229     1.9229    -0.0000
 17   0     8.3767     8.0000    -0.3767     1.8858     1.8858     0.0000
 18   0     8.3777     8.0000    -0.3777     1.8850     1.8850     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.403933
                0             6               2            6                1.354031
                0             6               8            1                0.943399
                1             6               3            6                1.284394
                1             6               6            1                0.964025
                2             6               5            6                1.385423
                2             6               9            7                0.976729
                3             6               4            6                1.307682
                3             6               7            6                1.142657
                4             6               5            6                1.377719
                4             6              10            1                0.967534
                5             6              11            1                0.943516
                7             6              12            7                1.907279
                7             6              14            8                1.048788
                9             7              17            8                1.664385
                9             7              18            8                1.663902
               12             7              13            8                0.875143
               13             8              15           16                0.946118
               14             8              15           16                0.895510
               15            16              16            8                1.874929
               17             8              18            8                0.152907
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 2
   prop. index: 1
        Total number of Electrons                                                    116
        Number of correlated Electrons                                                78
        Number of alpha correlated Electrons                                          39
        Number of beta correlated Electrons                                           39
        Reference Energy:                                               -1148.2562551633
        Total Correlation Energy:                                          -3.1759381844
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1604474539
        Total MDCI Energy:                                              -1151.4321933476
# -----------------------------------------------------------
$ Energy_Extrapolation
   description: The Extrapolation of the energies
   geom. index: 2
   prop. index: 1
       Do EP1                            : 1 
       Do EP2                            : 0 
       Do EP3                            : 0 
       Number of energies                : 2 
       SCF energy with basis  cc-pVTZ :  -1148.1935993027 
       SCF energy with basis  cc-pVQZ :  -1148.2562551633 
       CBS SCF Energy                    :  -1148.2751336199 
       Correlation energy with basis  cc-pVTZ :  -1148.1935993027 
       Correlation energy with basis  cc-pVQZ :  -1148.2562551633 
       CBS Correlation Energy            :     -3.3103988162 
       CBS Total Energy                  :  -1151.5855324362 
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 2
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       19
     number of electrons:                   116
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             510
     number of aux C basis functions:       2226
     number of aux J basis functions:       0
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           0.000000
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 2
   prop. index: 1
       Filename                          : /scratch/obeletsan/orcajob__14566-nxG4/int_phh.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        3.8963925180
        Electronic Contribution:
                  0    
      0       5.455575
      1       3.779963
      2       0.250163
        Nuclear Contribution:
                  0    
      0      -4.520172
      1      -3.067953
      2       0.733671
        Total Dipole moment:
                  0    
      0       0.935403
      1       0.712011
      2       0.983834
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     1 
    Coordinates:
               0 C      0.084804000000    0.113395000000   -0.102358000000
               1 C      1.077266000000    1.066514000000   -0.238639000000
               2 C      0.411622000000   -1.117996000000    0.458275000000
               3 C      2.385463000000    0.780090000000    0.179169000000
               4 C      2.690909000000   -0.466084000000    0.738473000000
               5 C      1.697970000000   -1.423461000000    0.881331000000
               6 H      0.845963000000    2.035295000000   -0.667830000000
               7 C      3.429343000000    1.789848000000    0.036876000000
               8 H     -0.931412000000    0.313783000000   -0.419606000000
               9 N     -0.646583000000   -2.133589000000    0.609903000000
              10 H      3.702593000000   -0.687737000000    1.058724000000
              11 H      1.913655000000   -2.393784000000    1.311910000000
              12 N      3.264873000000    2.916488000000   -0.558920000000
              13 O      4.448569000000    3.654738000000   -0.497519000000
              14 O      4.656045000000    1.519099000000    0.577559000000
              15 S      5.804731000000    2.668420000000   -0.010158000000
              16 O      6.415197000000    2.098725000000   -1.214399000000
              17 O     -1.773851000000   -1.842372000000    0.227861000000
              18 O     -0.336819000000   -3.208982000000    1.109146000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     2 
    Coordinates:
               0 C      0.084804000000    0.113395000000   -0.102358000000
               1 C      1.077266000000    1.066514000000   -0.238639000000
               2 C      0.411622000000   -1.117996000000    0.458275000000
               3 C      2.385463000000    0.780090000000    0.179169000000
               4 C      2.690909000000   -0.466084000000    0.738473000000
               5 C      1.697970000000   -1.423461000000    0.881331000000
               6 H      0.845963000000    2.035295000000   -0.667830000000
               7 C      3.429343000000    1.789848000000    0.036876000000
               8 H     -0.931412000000    0.313783000000   -0.419606000000
               9 N     -0.646583000000   -2.133589000000    0.609903000000
              10 H      3.702593000000   -0.687737000000    1.058724000000
              11 H      1.913655000000   -2.393784000000    1.311910000000
              12 N      3.264873000000    2.916488000000   -0.558920000000
              13 O      4.448569000000    3.654738000000   -0.497519000000
              14 O      4.656045000000    1.519099000000    0.577559000000
              15 S      5.804731000000    2.668420000000   -0.010158000000
              16 O      6.415197000000    2.098725000000   -1.214399000000
              17 O     -1.773851000000   -1.842372000000    0.227861000000
              18 O     -0.336819000000   -3.208982000000    1.109146000000
