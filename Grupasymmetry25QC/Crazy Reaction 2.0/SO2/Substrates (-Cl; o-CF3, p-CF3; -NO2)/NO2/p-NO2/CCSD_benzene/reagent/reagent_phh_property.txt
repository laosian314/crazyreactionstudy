-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -600.8719790879
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 1
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                947
     Surface Area:         691.9460801380
     Dielectric Energy:     -0.0116336369
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 16
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 18
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.1258     6.0000    -0.1258     3.8445     3.8445    -0.0000
  1   0     6.1471     6.0000    -0.1471     3.7873     3.7873    -0.0000
  2   0     5.9402     6.0000     0.0598     3.7863     3.7863     0.0000
  3   0     5.9087     6.0000     0.0913     3.7725     3.7725    -0.0000
  4   0     6.1467     6.0000    -0.1467     3.7866     3.7866    -0.0000
  5   0     6.1258     6.0000    -0.1258     3.8436     3.8436     0.0000
  6   0     0.8113     1.0000     0.1887     0.9708     0.9708    -0.0000
  7   0     6.0342     6.0000    -0.0342     3.8989     3.8989    -0.0000
  8   0     0.7879     1.0000     0.2121     0.9733     0.9733    -0.0000
  9   0     6.5044     7.0000     0.4956     4.2390     4.2390     0.0000
 10   0     0.8109     1.0000     0.1891     0.9706     0.9706     0.0000
 11   0     0.7877     1.0000     0.2123     0.9732     0.9732     0.0000
 12   0     6.6745     7.0000     0.3255     4.0528     4.0528     0.0000
 13   0     8.4533     8.0000    -0.4533     1.7577     1.7577    -0.0000
 14   0     8.3708     8.0000    -0.3708     1.8860     1.8860     0.0000
 15   0     8.3706     8.0000    -0.3706     1.8863     1.8863    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.381320
                0             6               2            6                1.368195
                0             6               8            1                0.943723
                1             6               3            6                1.310896
                1             6               6            1                0.967587
                2             6               5            6                1.367860
                2             6               9            7                0.951493
                3             6               4            6                1.310755
                3             6               7            6                1.081393
                4             6               5            6                1.380890
                4             6              10            1                0.967484
                5             6              11            1                0.943702
                7             6              12            7                2.552595
                7             6              13            8                0.268262
                9             7              14            8                1.645284
                9             7              15            8                1.645594
               12             7              13            8                1.441201
               14             8              15            8                0.158228
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 1
   prop. index: 1
        Total number of Electrons                                                     84
        Number of correlated Electrons                                                60
        Number of alpha correlated Electrons                                          30
        Number of beta correlated Electrons                                           30
        Reference Energy:                                                -600.8719791648
        Total Correlation Energy:                                          -2.2997094412
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1151695732
        Total MDCI Energy:                                               -603.1716886060
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:     -600.9137734914
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 2
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                947
     Surface Area:         691.9460801380
     Dielectric Energy:     -0.0117706020
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 2
   prop. index: 1
     Number of atoms                     : 16
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 18
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.0756     6.0000    -0.0756     3.8115     3.8115     0.0000
  1   0     6.1484     6.0000    -0.1484     3.7824     3.7824    -0.0000
  2   0     6.0033     6.0000    -0.0033     3.7919     3.7919    -0.0000
  3   0     5.9511     6.0000     0.0489     3.8236     3.8236     0.0000
  4   0     6.1466     6.0000    -0.1466     3.7816     3.7816    -0.0000
  5   0     6.0772     6.0000    -0.0772     3.8121     3.8121    -0.0000
  6   0     0.8046     1.0000     0.1954     0.9722     0.9722    -0.0000
  7   0     5.9392     6.0000     0.0608     3.8270     3.8270     0.0000
  8   0     0.7733     1.0000     0.2267     0.9721     0.9721     0.0000
  9   0     6.5358     7.0000     0.4642     4.2703     4.2703    -0.0000
 10   0     0.8033     1.0000     0.1967     0.9716     0.9716    -0.0000
 11   0     0.7728     1.0000     0.2272     0.9718     0.9718     0.0000
 12   0     6.7405     7.0000     0.2595     3.9798     3.9798     0.0000
 13   0     8.4746     8.0000    -0.4746     1.7565     1.7565    -0.0000
 14   0     8.3769     8.0000    -0.3769     1.8854     1.8854     0.0000
 15   0     8.3767     8.0000    -0.3767     1.8857     1.8857    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.402509
                0             6               2            6                1.364556
                0             6               8            1                0.942418
                1             6               3            6                1.291628
                1             6               6            1                0.973189
                2             6               5            6                1.364491
                2             6               9            7                0.976952
                3             6               4            6                1.291082
                3             6               7            6                1.185985
                4             6               5            6                1.402633
                4             6              10            1                0.972548
                5             6              11            1                0.942071
                7             6              12            7                2.456653
                7             6              13            8                0.226043
                9             7              14            8                1.662622
                9             7              15            8                1.662888
               12             7              13            8                1.463101
               14             8              15            8                0.153211
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 2
   prop. index: 1
        Total number of Electrons                                                     84
        Number of correlated Electrons                                                60
        Number of alpha correlated Electrons                                          30
        Number of beta correlated Electrons                                           30
        Reference Energy:                                                -600.9137732058
        Total Correlation Energy:                                          -2.4366667046
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1250533213
        Total MDCI Energy:                                               -603.3504399104
# -----------------------------------------------------------
$ Energy_Extrapolation
   description: The Extrapolation of the energies
   geom. index: 2
   prop. index: 1
       Do EP1                            : 1 
       Do EP2                            : 0 
       Do EP3                            : 0 
       Number of energies                : 2 
       SCF energy with basis  cc-pVTZ :   -600.8719791648 
       SCF energy with basis  cc-pVQZ :   -600.9137732058 
       CBS SCF Energy                    :   -600.9263659143 
       Correlation energy with basis  cc-pVTZ :   -600.8719791648 
       Correlation energy with basis  cc-pVQZ :   -600.9137732058 
       CBS Correlation Energy            :     -2.5341651401 
       CBS Total Energy                  :   -603.4605310544 
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 2
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       16
     number of electrons:                   84
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             416
     number of aux C basis functions:       1804
     number of aux J basis functions:       0
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           0.000000
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 2
   prop. index: 1
       Filename                          : /scratch/obeletsan/orcajob__14570-4HUA/reagent_phh.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        0.5857501426
        Electronic Contribution:
                  0    
      0       0.232029
      1       0.280572
      2       0.016189
        Nuclear Contribution:
                  0    
      0      -0.385072
      1      -0.452845
      2      -0.018624
        Total Dipole moment:
                  0    
      0      -0.153043
      1      -0.172273
      2      -0.002435
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    16 
    Geometry Index:     1 
    Coordinates:
               0 C     -0.118576000000   -0.067117000000    0.349051000000
               1 C      0.756001000000    1.002549000000    0.413845000000
               2 C      0.395003000000   -1.358874000000    0.404689000000
               3 C      2.139554000000    0.774005000000    0.533960000000
               4 C      2.632821000000   -0.543092000000    0.588759000000
               5 C      1.757813000000   -1.612331000000    0.523430000000
               6 H      0.381173000000    2.019381000000    0.372421000000
               7 C      3.033067000000    1.866093000000    0.599737000000
               8 H     -1.186630000000    0.088958000000    0.256006000000
               9 N     -0.535008000000   -2.496368000000    0.335171000000
              10 H      3.699761000000   -0.713499000000    0.681931000000
              11 H      2.118637000000   -2.633099000000    0.563410000000
              12 N      3.778077000000    2.759162000000    0.646366000000
              13 O      4.546085000000    3.684437000000    0.695956000000
              14 O     -1.732240000000   -2.249554000000    0.237443000000
              15 O     -0.059469000000   -3.625811000000    0.379084000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    16 
    Geometry Index:     2 
    Coordinates:
               0 C     -0.118576000000   -0.067117000000    0.349051000000
               1 C      0.756001000000    1.002549000000    0.413845000000
               2 C      0.395003000000   -1.358874000000    0.404689000000
               3 C      2.139554000000    0.774005000000    0.533960000000
               4 C      2.632821000000   -0.543092000000    0.588759000000
               5 C      1.757813000000   -1.612331000000    0.523430000000
               6 H      0.381173000000    2.019381000000    0.372421000000
               7 C      3.033067000000    1.866093000000    0.599737000000
               8 H     -1.186630000000    0.088958000000    0.256006000000
               9 N     -0.535008000000   -2.496368000000    0.335171000000
              10 H      3.699761000000   -0.713499000000    0.681931000000
              11 H      2.118637000000   -2.633099000000    0.563410000000
              12 N      3.778077000000    2.759162000000    0.646366000000
              13 O      4.546085000000    3.684437000000    0.695956000000
              14 O     -1.732240000000   -2.249554000000    0.237443000000
              15 O     -0.059469000000   -3.625811000000    0.379084000000
