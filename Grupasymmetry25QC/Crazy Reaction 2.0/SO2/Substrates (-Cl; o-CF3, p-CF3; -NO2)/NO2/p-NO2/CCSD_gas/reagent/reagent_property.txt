-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -600.8615272974
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 16
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 18
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.1266     6.0000    -0.1266     3.8533     3.8533    -0.0000
  1   0     6.1474     6.0000    -0.1474     3.7913     3.7913    -0.0000
  2   0     5.9580     6.0000     0.0420     3.8235     3.8235    -0.0000
  3   0     5.8954     6.0000     0.1046     3.7805     3.7805    -0.0000
  4   0     6.1467     6.0000    -0.1467     3.7912     3.7912     0.0000
  5   0     6.1267     6.0000    -0.1267     3.8529     3.8529     0.0000
  6   0     0.8226     1.0000     0.1774     0.9758     0.9758     0.0000
  7   0     6.0827     6.0000    -0.0827     3.8868     3.8868     0.0000
  8   0     0.7910     1.0000     0.2090     0.9752     0.9752     0.0000
  9   0     6.5155     7.0000     0.4845     4.2409     4.2409     0.0000
 10   0     0.8226     1.0000     0.1774     0.9757     0.9757     0.0000
 11   0     0.7909     1.0000     0.2091     0.9752     0.9752     0.0000
 12   0     6.6643     7.0000     0.3357     4.0596     4.0596     0.0000
 13   0     8.4065     8.0000    -0.4065     1.8142     1.8142    -0.0000
 14   0     8.3516     8.0000    -0.3516     1.9008     1.9008     0.0000
 15   0     8.3516     8.0000    -0.3516     1.9008     1.9008    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.377227
                0             6               2            6                1.381938
                0             6               8            1                0.943274
                1             6               3            6                1.317504
                1             6               6            1                0.971028
                2             6               5            6                1.381549
                2             6               9            7                0.949918
                3             6               4            6                1.317015
                3             6               7            6                1.079800
                4             6               5            6                1.377216
                4             6              10            1                0.971054
                5             6              11            1                0.943253
                7             6              12            7                2.524529
                7             6              13            8                0.285598
                9             7              14            8                1.649437
                9             7              15            8                1.649433
               12             7              13            8                1.477764
               14             8              15            8                0.164661
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 1
   prop. index: 1
        Total number of Electrons                                                     84
        Number of correlated Electrons                                                60
        Number of alpha correlated Electrons                                          30
        Number of beta correlated Electrons                                           30
        Reference Energy:                                                -600.8615273000
        Total Correlation Energy:                                          -2.3038576225
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1160609101
        Total MDCI Energy:                                               -603.1653849225
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:     -600.9032398521
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 2
   prop. index: 1
     Number of atoms                     : 16
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 18
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.0735     6.0000    -0.0735     3.8187     3.8187     0.0000
  1   0     6.1409     6.0000    -0.1409     3.7884     3.7884     0.0000
  2   0     6.0208     6.0000    -0.0208     3.8279     3.8279     0.0000
  3   0     5.9479     6.0000     0.0521     3.8375     3.8375    -0.0000
  4   0     6.1384     6.0000    -0.1384     3.7885     3.7885    -0.0000
  5   0     6.0748     6.0000    -0.0748     3.8190     3.8190     0.0000
  6   0     0.8235     1.0000     0.1765     0.9824     0.9824     0.0000
  7   0     5.9822     6.0000     0.0178     3.8218     3.8218    -0.0000
  8   0     0.7788     1.0000     0.2212     0.9767     0.9767     0.0000
  9   0     6.5548     7.0000     0.4452     4.2909     4.2909    -0.0000
 10   0     0.8233     1.0000     0.1767     0.9822     0.9822    -0.0000
 11   0     0.7787     1.0000     0.2213     0.9767     0.9767    -0.0000
 12   0     6.7325     7.0000     0.2675     4.0065     4.0065     0.0000
 13   0     8.4217     8.0000    -0.4217     1.8247     1.8247     0.0000
 14   0     8.3541     8.0000    -0.3541     1.9091     1.9091     0.0000
 15   0     8.3541     8.0000    -0.3541     1.9091     1.9091    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.396430
                0             6               2            6                1.377259
                0             6               8            1                0.943282
                1             6               3            6                1.295628
                1             6               6            1                0.981909
                2             6               5            6                1.376826
                2             6               9            7                0.977395
                3             6               4            6                1.294764
                3             6               7            6                1.189269
                4             6               5            6                1.396314
                4             6              10            1                0.982024
                5             6              11            1                0.943210
                7             6              12            7                2.432800
                7             6              13            8                0.237132
                9             7              14            8                1.675115
                9             7              15            8                1.675078
               12             7              13            8                1.515057
               14             8              15            8                0.159590
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 2
   prop. index: 1
        Total number of Electrons                                                     84
        Number of correlated Electrons                                                60
        Number of alpha correlated Electrons                                          30
        Number of beta correlated Electrons                                           30
        Reference Energy:                                                -600.9032398468
        Total Correlation Energy:                                          -2.4404857439
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1259167780
        Total MDCI Energy:                                               -603.3437255907
# -----------------------------------------------------------
$ Energy_Extrapolation
   description: The Extrapolation of the energies
   geom. index: 2
   prop. index: 1
       Do EP1                            : 1 
       Do EP2                            : 0 
       Do EP3                            : 0 
       Number of energies                : 2 
       SCF energy with basis  cc-pVTZ :   -600.8615273000 
       SCF energy with basis  cc-pVQZ :   -600.9032398468 
       CBS SCF Energy                    :   -600.9158080008 
       Correlation energy with basis  cc-pVTZ :   -600.8615273000 
       Correlation energy with basis  cc-pVQZ :   -600.9032398468 
       CBS Correlation Energy            :     -2.5377498667 
       CBS Total Energy                  :   -603.4535578675 
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 2
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       16
     number of electrons:                   84
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             416
     number of aux C basis functions:       1804
     number of aux J basis functions:       0
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           0.000000
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 2
   prop. index: 1
       Filename                          : /scratch/obeletsan/orcajob__14563-G1OY/reagent.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        0.3457239360
        Electronic Contribution:
                  0    
      0       0.299139
      1       0.362173
      2       0.020505
        Nuclear Contribution:
                  0    
      0      -0.392604
      1      -0.460988
      2      -0.020249
        Total Dipole moment:
                  0    
      0      -0.093465
      1      -0.098815
      2       0.000256
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    16 
    Geometry Index:     1 
    Coordinates:
               0 C     -0.117464000000   -0.068087000000    0.349150000000
               1 C      0.756954000000    1.001796000000    0.414089000000
               2 C      0.394500000000   -1.359569000000    0.404336000000
               3 C      2.141372000000    0.776808000000    0.533948000000
               4 C      2.631719000000   -0.541715000000    0.587972000000
               5 C      1.756798000000   -1.611120000000    0.522704000000
               6 H      0.382492000000    2.018740000000    0.373174000000
               7 C      3.034342000000    1.868711000000    0.600155000000
               8 H     -1.186545000000    0.081875000000    0.256625000000
               9 N     -0.538015000000   -2.500180000000    0.335235000000
              10 H      3.698768000000   -0.711658000000    0.680834000000
              11 H      2.112142000000   -2.633953000000    0.561903000000
              12 N      3.781189000000    2.762595000000    0.647052000000
              13 O      4.547440000000    3.683941000000    0.696327000000
              14 O     -1.732111000000   -2.247734000000    0.234066000000
              15 O     -0.057513000000   -3.625610000000    0.383690000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    16 
    Geometry Index:     2 
    Coordinates:
               0 C     -0.117464000000   -0.068087000000    0.349150000000
               1 C      0.756954000000    1.001796000000    0.414089000000
               2 C      0.394500000000   -1.359569000000    0.404336000000
               3 C      2.141372000000    0.776808000000    0.533948000000
               4 C      2.631719000000   -0.541715000000    0.587972000000
               5 C      1.756798000000   -1.611120000000    0.522704000000
               6 H      0.382492000000    2.018740000000    0.373174000000
               7 C      3.034342000000    1.868711000000    0.600155000000
               8 H     -1.186545000000    0.081875000000    0.256625000000
               9 N     -0.538015000000   -2.500180000000    0.335235000000
              10 H      3.698768000000   -0.711658000000    0.680834000000
              11 H      2.112142000000   -2.633953000000    0.561903000000
              12 N      3.781189000000    2.762595000000    0.647052000000
              13 O      4.547440000000    3.683941000000    0.696327000000
              14 O     -1.732111000000   -2.247734000000    0.234066000000
              15 O     -0.057513000000   -3.625610000000    0.383690000000
