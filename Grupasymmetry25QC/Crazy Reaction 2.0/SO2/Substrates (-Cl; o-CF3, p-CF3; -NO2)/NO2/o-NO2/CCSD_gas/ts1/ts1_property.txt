-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:    -1148.0913710316
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 19
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 22
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.1594     6.0000    -0.1594     3.8582     3.8582     0.0000
  1   0     5.9224     6.0000     0.0776     3.8480     3.8480     0.0000
  2   0     6.1197     6.0000    -0.1197     3.7895     3.7895     0.0000
  3   0     5.9150     6.0000     0.0850     3.8719     3.8719     0.0000
  4   0     6.0853     6.0000    -0.0853     3.8093     3.8093     0.0000
  5   0     6.1277     6.0000    -0.1277     3.7964     3.7964    -0.0000
  6   0     6.5124     7.0000     0.4876     4.1874     4.1874     0.0000
  7   0     5.8484     6.0000     0.1516     3.8576     3.8576     0.0000
  8   0     0.7970     1.0000     0.2030     0.9800     0.9800    -0.0000
  9   0     0.8229     1.0000     0.1771     0.9722     0.9722     0.0000
 10   0     0.8152     1.0000     0.1848     0.9746     0.9746     0.0000
 11   0     0.8256     1.0000     0.1744     0.9727     0.9727    -0.0000
 12   0     6.8251     7.0000     0.1749     3.6032     3.6032     0.0000
 13   0     8.4486     8.0000    -0.4486     1.7921     1.7921    -0.0000
 14   0     8.5484     8.0000    -0.5484     1.9282     1.9282     0.0000
 15   0    15.0583    16.0000     0.9417     3.8724     3.8724     0.0000
 16   0     8.4947     8.0000    -0.4947     2.0136     2.0136     0.0000
 17   0     8.3437     8.0000    -0.3437     1.9001     1.9001     0.0000
 18   0     8.3305     8.0000    -0.3305     1.8998     1.8998    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.414932
                0             6               2            6                1.344342
                0             6               8            1                0.947307
                1             6               3            6                1.341764
                1             6               6            7                0.917061
                2             6               5            6                1.375573
                2             6               9            1                0.969616
                3             6               4            6                1.327873
                3             6               7            6                1.061843
                4             6               5            6                1.369387
                4             6              10            1                0.959999
                5             6              11            1                0.970556
                6             7              17            8                1.634044
                6             7              18            8                1.624021
                7             6              12            7                2.275589
                7             6              13            8                0.170580
                7             6              14            8                0.260361
               12             7              13            8                1.218660
               13             8              15           16                0.322695
               14             8              15           16                1.594079
               15            16              16            8                1.895096
               17             8              18            8                0.171846
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 1
   prop. index: 1
        Total number of Electrons                                                    116
        Number of correlated Electrons                                                78
        Number of alpha correlated Electrons                                          39
        Number of beta correlated Electrons                                           39
        Reference Energy:                                               -1148.0913710295
        Total Correlation Energy:                                          -3.0212054418
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1552338439
        Total MDCI Energy:                                              -1151.1125764713
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:    -1148.1552522150
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 2
   prop. index: 1
     Number of atoms                     : 19
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 22
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.1262     6.0000    -0.1262     3.8625     3.8625    -0.0000
  1   0     5.9986     6.0000     0.0014     3.8481     3.8481    -0.0000
  2   0     6.0931     6.0000    -0.0931     3.8069     3.8069     0.0000
  3   0     5.9683     6.0000     0.0317     3.8997     3.8997     0.0000
  4   0     6.0575     6.0000    -0.0575     3.7898     3.7898     0.0000
  5   0     6.0937     6.0000    -0.0937     3.7998     3.7998    -0.0000
  6   0     6.5403     7.0000     0.4597     4.2309     4.2309    -0.0000
  7   0     5.7083     6.0000     0.2917     3.8706     3.8706    -0.0000
  8   0     0.7905     1.0000     0.2095     0.9870     0.9870     0.0000
  9   0     0.8237     1.0000     0.1763     0.9771     0.9771     0.0000
 10   0     0.8228     1.0000     0.1772     0.9873     0.9873     0.0000
 11   0     0.8302     1.0000     0.1698     0.9805     0.9805    -0.0000
 12   0     6.9044     7.0000     0.0956     3.5704     3.5704     0.0000
 13   0     8.4349     8.0000    -0.4349     1.8209     1.8209    -0.0000
 14   0     8.5851     8.0000    -0.5851     1.8758     1.8758    -0.0000
 15   0    14.9857    16.0000     1.0143     3.8587     3.8587     0.0000
 16   0     8.5511     8.0000    -0.5511     1.9544     1.9544    -0.0000
 17   0     8.3521     8.0000    -0.3521     1.9138     1.9138     0.0000
 18   0     8.3335     8.0000    -0.3335     1.9166     1.9166     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.432374
                0             6               2            6                1.361378
                0             6               8            1                0.949509
                1             6               3            6                1.299379
                1             6               6            7                0.950356
                2             6               5            6                1.389611
                2             6               9            1                0.977285
                3             6               4            6                1.291033
                3             6               7            6                1.182823
                4             6               5            6                1.383011
                4             6              10            1                0.973598
                5             6              11            1                0.980755
                6             7              17            8                1.662489
                6             7              18            8                1.657507
                7             6              12            7                2.205845
                7             6              13            8                0.151766
                7             6              14            8                0.277501
               12             7              13            8                1.237457
               13             8              15           16                0.349922
               14             8              15           16                1.571564
               15            16              16            8                1.873208
               17             8              18            8                0.168487
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 2
   prop. index: 1
        Total number of Electrons                                                    116
        Number of correlated Electrons                                                78
        Number of alpha correlated Electrons                                          39
        Number of beta correlated Electrons                                           39
        Reference Energy:                                               -1148.1552522128
        Total Correlation Energy:                                          -3.2100045704
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1694290273
        Total MDCI Energy:                                              -1151.3652567831
# -----------------------------------------------------------
$ Energy_Extrapolation
   description: The Extrapolation of the energies
   geom. index: 2
   prop. index: 1
       Do EP1                            : 1 
       Do EP2                            : 0 
       Do EP3                            : 0 
       Number of energies                : 2 
       SCF energy with basis  cc-pVTZ :  -1148.0913710295 
       SCF energy with basis  cc-pVQZ :  -1148.1552522128 
       CBS SCF Energy                    :  -1148.1744998640 
       Correlation energy with basis  cc-pVTZ :  -1148.0913710295 
       Correlation energy with basis  cc-pVQZ :  -1148.1552522128 
       CBS Correlation Energy            :     -3.3444086846 
       CBS Total Energy                  :  -1151.5189085485 
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 2
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       19
     number of electrons:                   116
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             510
     number of aux C basis functions:       2226
     number of aux J basis functions:       0
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           0.000000
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 2
   prop. index: 1
       Filename                          : /scratch/obeletsan/orcajob__14550-Ewxe/ts1.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        7.3452595171
        Electronic Contribution:
                  0    
      0       4.020180
      1       9.174388
      2      -0.449654
        Nuclear Contribution:
                  0    
      0      -5.244653
      1     -11.790664
      2       0.531177
        Total Dipole moment:
                  0    
      0      -1.224473
      1      -2.616276
      2       0.081523
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     1 
    Coordinates:
               0 C     -2.069031000000   -0.946878000000    0.230258000000
               1 C     -1.182494000000    0.057597000000   -0.118164000000
               2 C     -1.611664000000   -2.256931000000    0.313553000000
               3 C      0.173941000000   -0.208039000000   -0.359525000000
               4 C      0.621187000000   -1.532291000000   -0.236448000000
               5 C     -0.270710000000   -2.547825000000    0.075180000000
               6 N     -1.710729000000    1.429335000000   -0.266062000000
               7 C      1.118265000000    0.822028000000   -0.621687000000
               8 H     -3.105431000000   -0.692259000000    0.419281000000
               9 H     -2.305514000000   -3.050880000000    0.570390000000
              10 H      1.675050000000   -1.740991000000   -0.386331000000
              11 H      0.087522000000   -3.569655000000    0.150283000000
              12 N      2.060375000000    1.186536000000   -1.278490000000
              13 O      2.937700000000    2.072397000000   -1.228831000000
              14 O      1.216997000000    2.153140000000    0.870964000000
              15 S      2.579951000000    2.839176000000    0.895212000000
              16 O      3.621376000000    2.100787000000    1.633993000000
              17 O     -2.740176000000    1.696263000000    0.336687000000
              18 O     -1.096614000000    2.188490000000   -1.000263000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     2 
    Coordinates:
               0 C     -2.069031000000   -0.946878000000    0.230258000000
               1 C     -1.182494000000    0.057597000000   -0.118164000000
               2 C     -1.611664000000   -2.256931000000    0.313553000000
               3 C      0.173941000000   -0.208039000000   -0.359525000000
               4 C      0.621187000000   -1.532291000000   -0.236448000000
               5 C     -0.270710000000   -2.547825000000    0.075180000000
               6 N     -1.710729000000    1.429335000000   -0.266062000000
               7 C      1.118265000000    0.822028000000   -0.621687000000
               8 H     -3.105431000000   -0.692259000000    0.419281000000
               9 H     -2.305514000000   -3.050880000000    0.570390000000
              10 H      1.675050000000   -1.740991000000   -0.386331000000
              11 H      0.087522000000   -3.569655000000    0.150283000000
              12 N      2.060375000000    1.186536000000   -1.278490000000
              13 O      2.937700000000    2.072397000000   -1.228831000000
              14 O      1.216997000000    2.153140000000    0.870964000000
              15 S      2.579951000000    2.839176000000    0.895212000000
              16 O      3.621376000000    2.100787000000    1.633993000000
              17 O     -2.740176000000    1.696263000000    0.336687000000
              18 O     -1.096614000000    2.188490000000   -1.000263000000
