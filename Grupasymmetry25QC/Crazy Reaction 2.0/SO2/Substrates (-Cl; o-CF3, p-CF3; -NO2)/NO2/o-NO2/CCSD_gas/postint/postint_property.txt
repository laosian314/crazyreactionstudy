-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:    -1148.2493288191
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 19
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 20
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.1162     6.0000    -0.1162     3.8478     3.8478    -0.0000
  1   0     5.9913     6.0000     0.0087     3.7801     3.7801    -0.0000
  2   0     6.1663     6.0000    -0.1663     3.7844     3.7844     0.0000
  3   0     5.8841     6.0000     0.1159     3.9908     3.9908     0.0000
  4   0     6.1286     6.0000    -0.1286     3.7960     3.7960    -0.0000
  5   0     6.0992     6.0000    -0.0992     3.8021     3.8021    -0.0000
  6   0     6.4842     7.0000     0.5158     4.1584     4.1584    -0.0000
  7   0     5.5506     6.0000     0.4494     4.2714     4.2714    -0.0000
  8   0     0.8026     1.0000     0.1974     0.9831     0.9831    -0.0000
  9   0     0.8350     1.0000     0.1650     0.9766     0.9766     0.0000
 10   0     0.8297     1.0000     0.1703     0.9915     0.9915    -0.0000
 11   0     0.8320     1.0000     0.1680     0.9729     0.9729    -0.0000
 12   0     7.2646     7.0000    -0.2646     2.9742     2.9742    -0.0000
 13   0     8.4619     8.0000    -0.4619     2.0368     2.0368     0.0000
 14   0     8.2789     8.0000    -0.2789     2.3257     2.3257     0.0000
 15   0    15.0913    16.0000     0.9087     3.8720     3.8720     0.0000
 16   0     8.4489     8.0000    -0.4489     2.0592     2.0592    -0.0000
 17   0     8.3328     8.0000    -0.3328     1.9227     1.9227    -0.0000
 18   0     8.4019     8.0000    -0.4019     1.8192     1.8192    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.357041
                0             6               2            6                1.392998
                0             6               8            1                0.943673
                1             6               3            6                1.334656
                1             6               6            7                0.956023
                2             6               5            6                1.342568
                2             6               9            1                0.974302
                3             6               4            6                1.362039
                3             6              12            7                1.078751
                4             6               5            6                1.393620
                4             6              10            1                0.960877
                5             6              11            1                0.972575
                6             7              17            8                1.672090
                6             7              18            8                1.519185
                7             6              12            7                1.853012
                7             6              14            8                2.232590
               13             8              15           16                1.908249
               13             8              16            8                0.113895
               15            16              16            8                1.933405
               17             8              18            8                0.152226
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 1
   prop. index: 1
        Total number of Electrons                                                    116
        Number of correlated Electrons                                                78
        Number of alpha correlated Electrons                                          39
        Number of beta correlated Electrons                                           39
        Reference Energy:                                               -1148.2493288231
        Total Correlation Energy:                                          -2.9823672948
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1470858906
        Total MDCI Energy:                                              -1151.2316961179
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:    -1148.3136627197
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 2
   prop. index: 1
     Number of atoms                     : 19
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 19
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.0898     6.0000    -0.0898     3.8411     3.8411     0.0000
  1   0     6.0739     6.0000    -0.0739     3.8363     3.8363    -0.0000
  2   0     6.1269     6.0000    -0.1269     3.8100     3.8100    -0.0000
  3   0     5.7494     6.0000     0.2506     3.8583     3.8583     0.0000
  4   0     6.1322     6.0000    -0.1322     3.8012     3.8012     0.0000
  5   0     6.0777     6.0000    -0.0777     3.8169     3.8169     0.0000
  6   0     6.5028     7.0000     0.4972     4.2043     4.2043     0.0000
  7   0     5.3400     6.0000     0.6600     4.3035     4.3035    -0.0000
  8   0     0.7974     1.0000     0.2026     0.9924     0.9924    -0.0000
  9   0     0.8393     1.0000     0.1607     0.9834     0.9834     0.0000
 10   0     0.8246     1.0000     0.1754     1.0009     1.0009     0.0000
 11   0     0.8313     1.0000     0.1687     0.9773     0.9773     0.0000
 12   0     7.5381     7.0000    -0.5381     3.0004     3.0004     0.0000
 13   0     8.5117     8.0000    -0.5117     1.9812     1.9812    -0.0000
 14   0     8.3274     8.0000    -0.3274     2.3163     2.3163    -0.0000
 15   0    14.9985    16.0000     1.0015     3.8268     3.8268     0.0000
 16   0     8.5017     8.0000    -0.5017     2.0000     2.0000    -0.0000
 17   0     8.3380     8.0000    -0.3380     1.9303     1.9303     0.0000
 18   0     8.3993     8.0000    -0.3993     1.8324     1.8324    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.362200
                0             6               2            6                1.406918
                0             6               8            1                0.945471
                1             6               3            6                1.312193
                1             6               6            7                1.000311
                2             6               5            6                1.366497
                2             6               9            1                0.984056
                3             6               4            6                1.391451
                3             6              12            7                1.040130
                4             6               5            6                1.396966
                4             6              10            1                0.971181
                5             6              11            1                0.976384
                6             7              17            8                1.696456
                6             7              18            8                1.548289
                7             6              12            7                1.912632
                7             6              14            8                2.214265
               13             8              15           16                1.883298
               15            16              16            8                1.904882
               17             8              18            8                0.147012
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 2
   prop. index: 1
        Total number of Electrons                                                    116
        Number of correlated Electrons                                                78
        Number of alpha correlated Electrons                                          39
        Number of beta correlated Electrons                                           39
        Reference Energy:                                               -1148.3136627058
        Total Correlation Energy:                                          -3.1703737576
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1608881422
        Total MDCI Energy:                                              -1151.4840364634
# -----------------------------------------------------------
$ Energy_Extrapolation
   description: The Extrapolation of the energies
   geom. index: 2
   prop. index: 1
       Do EP1                            : 1 
       Do EP2                            : 0 
       Do EP3                            : 0 
       Number of energies                : 2 
       SCF energy with basis  cc-pVTZ :  -1148.2493288231 
       SCF energy with basis  cc-pVQZ :  -1148.3136627058 
       CBS SCF Energy                    :  -1148.3330467571 
       Correlation energy with basis  cc-pVTZ :  -1148.2493288231 
       Correlation energy with basis  cc-pVQZ :  -1148.3136627058 
       CBS Correlation Energy            :     -3.3042135813 
       CBS Total Energy                  :  -1151.6372603384 
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 2
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       19
     number of electrons:                   116
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             510
     number of aux C basis functions:       2226
     number of aux J basis functions:       0
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           0.000000
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 2
   prop. index: 1
       Filename                          : /scratch/obeletsan/orcajob__14546-0Q5N/postint.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        4.2376675894
        Electronic Contribution:
                  0    
      0       4.207558
      1       8.545462
      2      -0.821414
        Nuclear Contribution:
                  0    
      0      -3.795604
      1     -10.147739
      2       1.027654
        Total Dipole moment:
                  0    
      0       0.411954
      1      -1.602277
      2       0.206240
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     1 
    Coordinates:
               0 C      0.608931000000   -0.013902000000   -0.477035000000
               1 C      1.316163000000    1.086184000000    0.003542000000
               2 C      1.060365000000   -1.298377000000   -0.233974000000
               3 C      2.507611000000    0.914201000000    0.741276000000
               4 C      2.954580000000   -0.397770000000    0.951138000000
               5 C      2.242762000000   -1.486206000000    0.479423000000
               6 N      0.769275000000    2.404958000000   -0.317715000000
               7 C      3.283255000000    3.109348000000    1.416163000000
               8 H     -0.296408000000    0.167264000000   -1.043572000000
               9 H      0.497773000000   -2.147468000000   -0.607120000000
              10 H      3.886379000000   -0.529042000000    1.490417000000
              11 H      2.618005000000   -2.487884000000    0.664668000000
              12 N      3.347517000000    1.904120000000    1.219073000000
              13 O      3.584886000000    1.751483000000   -2.036645000000
              14 O      3.476339000000    4.219899000000    1.703490000000
              15 S      4.901751000000    1.850668000000   -1.382881000000
              16 O      5.579445000000    0.589535000000   -1.041849000000
              17 O     -0.102702000000    2.475794000000   -1.167707000000
              18 O      1.207756000000    3.380097000000    0.298457000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     2 
    Coordinates:
               0 C      0.608931000000   -0.013902000000   -0.477035000000
               1 C      1.316163000000    1.086184000000    0.003542000000
               2 C      1.060365000000   -1.298377000000   -0.233974000000
               3 C      2.507611000000    0.914201000000    0.741276000000
               4 C      2.954580000000   -0.397770000000    0.951138000000
               5 C      2.242762000000   -1.486206000000    0.479423000000
               6 N      0.769275000000    2.404958000000   -0.317715000000
               7 C      3.283255000000    3.109348000000    1.416163000000
               8 H     -0.296408000000    0.167264000000   -1.043572000000
               9 H      0.497773000000   -2.147468000000   -0.607120000000
              10 H      3.886379000000   -0.529042000000    1.490417000000
              11 H      2.618005000000   -2.487884000000    0.664668000000
              12 N      3.347517000000    1.904120000000    1.219073000000
              13 O      3.584886000000    1.751483000000   -2.036645000000
              14 O      3.476339000000    4.219899000000    1.703490000000
              15 S      4.901751000000    1.850668000000   -1.382881000000
              16 O      5.579445000000    0.589535000000   -1.041849000000
              17 O     -0.102702000000    2.475794000000   -1.167707000000
              18 O      1.207756000000    3.380097000000    0.298457000000
