-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:    -1152.6791547199
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                 58.0000782411 
   Number of Beta  Electrons                 58.0000782411 
   Total number of  Electrons               116.0001564822 
   Exchange energy                         -117.4937927734 
   Correlation energy                        -4.0111315613 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -121.5049243347 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1152.6791547199 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 1
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1075
     Surface Area:         809.8595082340
     Dielectric Energy:     -0.0085681011
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 1
   prop. index: 1
        Van der Waals Correction:       -0.0087206396
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:    -1152.6791814192
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 2
   prop. index: 1
   Number of Alpha Electrons                 58.0000841430 
   Number of Beta  Electrons                 58.0000841430 
   Total number of  Electrons               116.0001682860 
   Exchange energy                         -117.4900346216 
   Correlation energy                        -4.0107610384 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -121.5007956600 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1152.6791814192 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 2
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1075
     Surface Area:         811.1553511953
     Dielectric Energy:     -0.0086941561
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 2
   prop. index: 1
        Van der Waals Correction:       -0.0087178510
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 3
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 3
   prop. index: 1
        SCF Energy:    -1152.6788952435
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 3
   prop. index: 1
   Number of Alpha Electrons                 58.0000912062 
   Number of Beta  Electrons                 58.0000912062 
   Total number of  Electrons               116.0001824123 
   Exchange energy                         -117.4839744154 
   Correlation energy                        -4.0101664062 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -121.4941408217 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1152.6788952435 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 3
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1072
     Surface Area:         813.1532898593
     Dielectric Energy:     -0.0088905933
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 3
   prop. index: 1
        Van der Waals Correction:       -0.0087101377
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 4
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 4
   prop. index: 1
        SCF Energy:    -1152.6792462075
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 4
   prop. index: 1
   Number of Alpha Electrons                 58.0000822399 
   Number of Beta  Electrons                 58.0000822399 
   Total number of  Electrons               116.0001644799 
   Exchange energy                         -117.4888796805 
   Correlation energy                        -4.0107731599 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -121.4996528404 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1152.6792462075 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 4
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1073
     Surface Area:         810.3229203007
     Dielectric Energy:     -0.0087690366
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 4
   prop. index: 1
        Van der Waals Correction:       -0.0087243926
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 5
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 5
   prop. index: 1
        SCF Energy:    -1152.6792128083
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 5
   prop. index: 1
   Number of Alpha Electrons                 58.0000878154 
   Number of Beta  Electrons                 58.0000878154 
   Total number of  Electrons               116.0001756308 
   Exchange energy                         -117.4903666285 
   Correlation energy                        -4.0107968801 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -121.5011635087 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1152.6792128083 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 5
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1075
     Surface Area:         811.3591872488
     Dielectric Energy:     -0.0087287598
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 5
   prop. index: 1
        Van der Waals Correction:       -0.0087193586
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 6
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 6
   prop. index: 1
        SCF Energy:    -1152.6792463222
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 6
   prop. index: 1
   Number of Alpha Electrons                 58.0000868418 
   Number of Beta  Electrons                 58.0000868418 
   Total number of  Electrons               116.0001736836 
   Exchange energy                         -117.4896620223 
   Correlation energy                        -4.0107864981 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -121.5004485204 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1152.6792463222 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 6
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1074
     Surface Area:         810.9721100016
     Dielectric Energy:     -0.0087614943
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 6
   prop. index: 1
        Van der Waals Correction:       -0.0087219280
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 7
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 7
   prop. index: 1
        SCF Energy:    -1152.6792532733
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 7
   prop. index: 1
   Number of Alpha Electrons                 58.0000877462 
   Number of Beta  Electrons                 58.0000877462 
   Total number of  Electrons               116.0001754924 
   Exchange energy                         -117.4896910308 
   Correlation energy                        -4.0108028145 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -121.5004938453 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1152.6792532733 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 7
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1070
     Surface Area:         810.9555724542
     Dielectric Energy:     -0.0087721705
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 7
   prop. index: 1
        Van der Waals Correction:       -0.0087224939
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 8
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 8
   prop. index: 1
        SCF Energy:    -1152.6792621740
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 8
   prop. index: 1
   Number of Alpha Electrons                 58.0000904499 
   Number of Beta  Electrons                 58.0000904499 
   Total number of  Electrons               116.0001808997 
   Exchange energy                         -117.4898987956 
   Correlation energy                        -4.0108402422 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -121.5007390378 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1152.6792621740 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 8
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1069
     Surface Area:         811.2037084772
     Dielectric Energy:     -0.0087975182
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 8
   prop. index: 1
        Van der Waals Correction:       -0.0087226858
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 9
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 9
   prop. index: 1
        SCF Energy:    -1152.6792618352
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 9
   prop. index: 1
   Number of Alpha Electrons                 58.0000909580 
   Number of Beta  Electrons                 58.0000909580 
   Total number of  Electrons               116.0001819159 
   Exchange energy                         -117.4899721688 
   Correlation energy                        -4.0108429114 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -121.5008150802 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1152.6792618352 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 9
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1070
     Surface Area:         811.2977120094
     Dielectric Energy:     -0.0087945453
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 9
   prop. index: 1
        Van der Waals Correction:       -0.0087218878
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 9
   prop. index: 1
       Filename                          : mol.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        5.2492831593
        Electronic Contribution:
                  0    
      0       5.322642
      1       9.167599
      2      -1.253753
        Nuclear Contribution:
                  0    
      0      -6.136006
      1     -10.982167
      2       1.811223
        Total Dipole moment:
                  0    
      0      -0.813364
      1      -1.814568
      2       0.557470
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 10
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 10
   prop. index: 1
        SCF Energy:    -1152.6792618571
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 10
   prop. index: 1
   Number of Alpha Electrons                 58.0000909578 
   Number of Beta  Electrons                 58.0000909578 
   Total number of  Electrons               116.0001819156 
   Exchange energy                         -117.4899859213 
   Correlation energy                        -4.0108427171 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -121.5008286385 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1152.6792618571 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 10
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1070
     Surface Area:         811.2977120094
     Dielectric Energy:     -0.0087947110
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 10
   prop. index: 1
        Van der Waals Correction:       -0.0087218878
# -----------------------------------------------------------
$ THERMOCHEMISTRY_Energies
   description: The Thermochemistry energies
   geom. index: 10
   prop. index: 1
        Temperature (Kelvin)           :        298.1500000000
        Pressure (atm)                 :          1.0000000000
        Total Mass (AMU)               :        227.9840924101
        Spin Degeneracy                :          1.0000000000
        Electronic Energy (Hartree)    :      -1152.6627950971
        Translational Energy (Hartree) :          0.0014162714
        Rotational Energy (Hartree)    :          0.0014162714
        Vibrational Energy (Hartree)   :          0.0095187927
        Number of frequencies          :     57      
        Scaling Factor for frequencies :          1.0000000000
        Vibrational frequencies        :     
                  0    
      0       0.000000
      1       0.000000
      2       0.000000
      3       0.000000
      4       0.000000
      5       0.000000
      6      30.401970
      7      66.646919
      8      73.526308
      9     100.596734
     10     143.581727
     11     163.287489
     12     237.155308
     13     271.918739
     14     288.254524
     15     311.878469
     16     359.041457
     17     408.267927
     18     422.279400
     19     440.003278
     20     449.906021
     21     535.811213
     22     569.948188
     23     625.877471
     24     649.098239
     25     668.871766
     26     684.079285
     27     714.933442
     28     733.166130
     29     759.544671
     30     802.275194
     31     868.078710
     32     880.974950
     33     900.772924
     34     939.682743
     35     980.759896
     36     1008.589639
     37     1051.414845
     38     1077.113603
     39     1118.366364
     40     1167.763806
     41     1188.102896
     42     1218.219353
     43     1282.707388
     44     1339.251149
     45     1358.510108
     46     1364.225184
     47     1469.580303
     48     1507.436837
     49     1561.768476
     50     1605.363314
     51     1636.797424
     52     1652.275773
     53     3185.151696
     54     3195.962888
     55     3203.664412
     56     3215.677384
        Zero Point Energy (Hartree)    :          0.1150214120
        Inner Energy (Hartree)         :      -1152.5354223496
        Enthalpy (Hartree)             :      -1152.5344781405
        Electronic entropy             :          0.0000000000
        Rotational entropy             :          0.0153136241
        Vibrational entropy            :          0.0165371846
        Translational entropy          :          0.0153136241
        Entropy                        :          0.0518892081
        Gibbs Energy (Hartree)         :      -1152.5863673486
        Is Linear                      :                 false
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     1 
    Coordinates:
               0 C      0.132005732744    0.088326845083    0.340148689689
               1 C      1.115312788811    1.005689785311    0.001847106226
               2 C      0.480506906760   -1.245202819850    0.515053383566
               3 C      2.456887667939    0.633593243879   -0.132642313148
               4 C      2.787901865377   -0.708691309404    0.059668464288
               5 C      1.804907077656   -1.642995839685    0.364837779447
               6 N      0.682526090637    2.384474264200   -0.301331349416
               7 C      3.542281117110    1.590284534364   -0.390474979460
               8 H     -0.892818037954    0.427564562582    0.438542146701
               9 H     -0.286009071947   -1.971996706457    0.764239253081
              10 H      3.829152690337   -1.002341925995   -0.020862581312
              11 H      2.080700659481   -2.683880739450    0.501390251996
              12 N      4.464536402342    1.364423981814   -1.248601936293
              13 O      5.384730699152    2.424345382566   -1.216519408542
              14 O      3.617736564964    2.705306421395    0.391271277029
              15 S      5.186807275143    3.421491420290    0.191398802569
              16 O      6.039037412725    2.904026389461    1.264127489674
              17 O     -0.313557676589    2.798915406254    0.273758071163
              18 O      1.331040835312    2.999569103642   -1.136702147259
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     2 
    Coordinates:
               0 C      0.129715483329    0.086327206992    0.338122072189
               1 C      1.112122061799    1.004786767138   -0.000850185740
               2 C      0.481298274165   -1.245925268073    0.517459438498
               3 C      2.455073726277    0.634984390579   -0.133818184793
               4 C      2.788889750377   -0.706062654907    0.062491121438
               5 C      1.806981138535   -1.641350687462    0.369942305687
               6 N      0.677773107453    2.380149362499   -0.311341225728
               7 C      3.539427549807    1.593687239186   -0.389202813169
               8 H     -0.897461821374    0.420947551711    0.428821547580
               9 H     -0.284261905050   -1.973792583270    0.766370809054
              10 H      3.831198827620   -0.998157012095   -0.011389666958
              11 H      2.084901020814   -2.681139312471    0.510539141877
              12 N      4.452300370909    1.381852076476   -1.261169343264
              13 O      5.375774120167    2.440976120447   -1.221176038242
              14 O      3.625573817222    2.697889582287    0.406658895773
              15 S      5.192447973383    3.417822207957    0.203052498169
              16 O      6.053516570204    2.877137208787    1.258325931568
              17 O     -0.308237640627    2.806559220113    0.273191401213
              18 O      1.326654574990    2.996210584110   -1.146879705152
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     3 
    Coordinates:
               0 C      0.128132765010    0.087456809617    0.337763194087
               1 C      1.111865250956    1.006526467299    0.002839253767
               2 C      0.481881856715   -1.244194610764    0.519712776469
               3 C      2.455792001730    0.637615085465   -0.130804337282
               4 C      2.791731331431   -0.702256519240    0.069057807992
               5 C      1.809070911821   -1.637981670454    0.376865605428
               6 N      0.681698480529    2.380041902897   -0.306397106182
               7 C      3.538801072949    1.598691385081   -0.383053649364
               8 H     -0.902321420244    0.415713898874    0.416862220840
               9 H     -0.283878411805   -1.972972927473    0.765178837521
              10 H      3.834910147670   -0.994417891568    0.005270676975
              11 H      2.088068924281   -2.677071044900    0.520582666344
              12 N      4.431641737250    1.414204025774   -1.282180369538
              13 O      5.360626673804    2.472519755840   -1.230083444171
              14 O      3.644881882724    2.682979331925    0.437254890353
              15 S      5.205989868366    3.411544704778    0.222963830577
              16 O      6.086151341082    2.829938017657    1.242699182172
              17 O     -0.325664538709    2.800411619187    0.247202316820
              18 O      1.304307124440    2.984153660002   -1.172586352808
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     4 
    Coordinates:
               0 C      0.127871412360    0.085488392528    0.331615795458
               1 C      1.113862370311    1.002882022358    0.000300190959
               2 C      0.478378411237   -1.247098294229    0.512849781513
               3 C      2.456600670892    0.631628595932   -0.129755709662
               4 C      2.789951336915   -0.708495366695    0.071280906330
               5 C      1.805158667065   -1.642948859178    0.374728552782
               6 N      0.688898493944    2.380607278995   -0.300237979135
               7 C      3.539336319621    1.591126069502   -0.387315918680
               8 H     -0.900673705845    0.418151122412    0.414798000398
               9 H     -0.289123744119   -1.974550618018    0.756883750769
              10 H      3.832257799836   -1.002486528288    0.004231878990
              11 H      2.081776585148   -2.682786935654    0.517569188988
              12 N      4.444833731352    1.388956267354   -1.269450486025
              13 O      5.365839140495    2.452451787721   -1.230880708416
              14 O      3.626646081647    2.693387480267    0.410865482783
              15 S      5.187168661152    3.423013634304    0.198206542485
              16 O      6.058914814541    2.877083093506    1.243487314765
              17 O     -0.311574200720    2.802241311776    0.263999598346
              18 O      1.347564154168    3.004251545405   -1.124028182648
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     5 
    Coordinates:
               0 C      0.128257945804    0.086136225468    0.334324808813
               1 C      1.110142988592    1.005361527774   -0.003424471788
               2 C      0.482287879859   -1.245177018485    0.517317504853
               3 C      2.454290867582    0.637475928338   -0.131809779515
               4 C      2.790441696089   -0.701989234475    0.068066694702
               5 C      1.809022665257   -1.638320643960    0.375244749409
               6 N      0.676548622453    2.379239120508   -0.312102824663
               7 C      3.536828719103    1.597713376252   -0.388462644599
               8 H     -0.900298667863    0.417109870024    0.421625022501
               9 H     -0.282709516723   -1.973921133202    0.765379348141
              10 H      3.832897284689   -0.993920673319   -0.003727997992
              11 H      2.088436541924   -2.677308574341    0.518784025126
              12 N      4.441877167748    1.393455886564   -1.270266672773
              13 O      5.366287126333    2.452172348529   -1.228071469884
              14 O      3.633072835030    2.690430122998    0.422004758684
              15 S      5.197476151067    3.410708656525    0.209946054968
              16 O      6.070821345294    2.855445141366    1.248345132082
              17 O     -0.321797288310    2.798075028568    0.257530327525
              18 O      1.329802636071    3.000216044867   -1.141554565591
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     6 
    Coordinates:
               0 C      0.127518094180    0.085461876972    0.331777153931
               1 C      1.110994581272    1.004360451371   -0.002550592487
               2 C      0.480855732173   -1.246046922492    0.515448481528
               3 C      2.454957842548    0.635589963331   -0.130002417350
               4 C      2.790663822083   -0.703634382123    0.071630752568
               5 C      1.807985315084   -1.639556347750    0.377097991756
               6 N      0.681404690322    2.379586260230   -0.307637614633
               7 C      3.537203986333    1.596048291484   -0.387395178386
               8 H     -0.901539116470    0.415862068643    0.415868409168
               9 H     -0.285063970700   -1.974556343886    0.761330477201
              10 H      3.833171379905   -0.996300974886    0.002890856192
              11 H      2.086750449201   -2.678598910831    0.521544321253
              12 N      4.438676788259    1.396377244506   -1.273859824157
              13 O      5.362051753148    2.457086079430   -1.232167245835
              14 O      3.633089382204    2.689380855179    0.422298601373
              15 S      5.195059292363    3.413862977233    0.207259492724
              16 O      6.071749896879    2.856565984864    1.242245061826
              17 O     -0.322654301403    2.797066943640    0.253216566598
              18 O      1.340811382620    3.004346885083   -1.129847293272
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     7 
    Coordinates:
               0 C      0.126948816105    0.084934289634    0.330348523365
               1 C      1.110257234269    1.004332494060   -0.003181107785
               2 C      0.480991933685   -1.246243232441    0.515383284345
               3 C      2.454550610077    0.636033422447   -0.129134207129
               4 C      2.790966843541   -0.702679869080    0.073787912128
               5 C      1.808484383278   -1.639019702492    0.379022198442
               6 N      0.681021968395    2.379532341960   -0.307978380984
               7 C      3.536508943107    1.596936129791   -0.386993265643
               8 H     -0.902541783004    0.414248978858    0.413091917952
               9 H     -0.284812059498   -1.975040445034    0.760780861767
              10 H      3.833610424653   -0.995109409317    0.005988114545
              11 H      2.087763318560   -2.677776885997    0.524546870850
              12 N      4.434614453219    1.400366253060   -1.277376651097
              13 O      5.358439233640    2.460978995210   -1.234984898180
              14 O      3.635509455668    2.687275218213    0.426497448330
              15 S      5.196720085780    3.412044975003    0.208650664375
              16 O      6.077024896694    2.850216962265    1.238245106730
              17 O     -0.328190172873    2.794142765795    0.245779624231
              18 O      1.345818414704    3.007728718065   -1.123326016243
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     8 
    Coordinates:
               0 C      0.125350116821    0.083342022810    0.326810367819
               1 C      1.107270028582    1.004702283674   -0.005563791457
               2 C      0.482151294988   -1.246556573047    0.516252484020
               3 C      2.452733599588    0.638543868030   -0.127106225856
               4 C      2.791871194373   -0.698545297484    0.079471121069
               5 C      1.810755345575   -1.636591798784    0.384798037251
               6 N      0.677134737352    2.379242564712   -0.311312087135
               7 C      3.534003214215    1.600784635712   -0.385936853925
               8 H     -0.905405009890    0.409279071402    0.406155630736
               9 H     -0.282782300428   -1.976467361015    0.761076456836
              10 H      3.835051025677   -0.989553910784    0.013655365302
              11 H      2.092170267063   -2.674331092337    0.533493678434
              12 N      4.421821350872    1.413256525792   -1.287966703186
              13 O      5.348064823908    2.472386945596   -1.242548211434
              14 O      3.644397758769    2.680228228195    0.440946547839
              15 S      5.204249240325    3.404129737229    0.214894857127
              16 O      6.095020040554    2.827090299975    1.227150474973
              17 O     -0.347290466439    2.784463420984    0.221105228502
              18 O      1.357120738095    3.017498429337   -1.106228376915
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     9 
    Coordinates:
               0 C      0.125251727369    0.083164624578    0.326855600725
               1 C      1.106633935429    1.004925081115   -0.006000127928
               2 C      0.482543298333   -1.246488217268    0.516831959780
               3 C      2.452262735346    0.639272958720   -0.127247132048
               4 C      2.791891667591   -0.697655730519    0.079592039705
               5 C      1.811262943057   -1.636069876338    0.385265962985
               6 N      0.675483555137    2.379291212871   -0.312343153876
               7 C      3.533731181805    1.601472154550   -0.385974899562
               8 H     -0.905562127976    0.408869583659    0.406242134283
               9 H     -0.282087304754   -1.976581274429    0.762054606974
              10 H      3.835189996712   -0.988170377960    0.013452854356
              11 H      2.093128556683   -2.673662568721    0.534113494576
              12 N      4.420662499528    1.414337386644   -1.288916227381
              13 O      5.347724009985    2.472760359387   -1.242900905244
              14 O      3.645932992111    2.679304680103    0.442753909429
              15 S      5.206090676603    3.402461492378    0.215889171022
              16 O      6.097446184444    2.823979228867    1.226713793369
              17 O     -0.351178314138    2.782594561246    0.217094021485
              18 O      1.357278786736    3.019096721117   -1.104329102651
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     10 
    Coordinates:
               0 C      0.125251727369    0.083164624578    0.326855600725
               1 C      1.106633935429    1.004925081115   -0.006000127928
               2 C      0.482543298333   -1.246488217268    0.516831959780
               3 C      2.452262735346    0.639272958720   -0.127247132048
               4 C      2.791891667591   -0.697655730519    0.079592039705
               5 C      1.811262943057   -1.636069876338    0.385265962985
               6 N      0.675483555137    2.379291212871   -0.312343153876
               7 C      3.533731181805    1.601472154550   -0.385974899562
               8 H     -0.905562127976    0.408869583659    0.406242134283
               9 H     -0.282087304754   -1.976581274429    0.762054606974
              10 H      3.835189996712   -0.988170377960    0.013452854356
              11 H      2.093128556683   -2.673662568721    0.534113494576
              12 N      4.420662499528    1.414337386644   -1.288916227381
              13 O      5.347724009985    2.472760359387   -1.242900905244
              14 O      3.645932992111    2.679304680103    0.442753909429
              15 S      5.206090676603    3.402461492378    0.215889171022
              16 O      6.097446184444    2.823979228867    1.226713793369
              17 O     -0.351178314138    2.782594561246    0.217094021485
              18 O      1.357278786736    3.019096721117   -1.104329102651
