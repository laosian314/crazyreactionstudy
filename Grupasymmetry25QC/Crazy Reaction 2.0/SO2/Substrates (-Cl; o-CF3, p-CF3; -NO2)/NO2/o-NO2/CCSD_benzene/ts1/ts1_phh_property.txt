-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:    -1148.1048586253
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 1
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1102
     Surface Area:         832.2401274230
     Dielectric Energy:     -0.0141964648
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 19
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 22
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.1527     6.0000    -0.1527     3.8446     3.8446    -0.0000
  1   0     5.9148     6.0000     0.0852     3.8230     3.8230     0.0000
  2   0     6.1199     6.0000    -0.1199     3.7813     3.7813     0.0000
  3   0     5.9252     6.0000     0.0748     3.8631     3.8631     0.0000
  4   0     6.0789     6.0000    -0.0789     3.7948     3.7948     0.0000
  5   0     6.1310     6.0000    -0.1310     3.7840     3.7840    -0.0000
  6   0     6.5002     7.0000     0.4998     4.1864     4.1864    -0.0000
  7   0     5.8221     6.0000     0.1779     3.8571     3.8571     0.0000
  8   0     0.7921     1.0000     0.2079     0.9770     0.9770    -0.0000
  9   0     0.8078     1.0000     0.1922     0.9659     0.9659     0.0000
 10   0     0.8050     1.0000     0.1950     0.9695     0.9695    -0.0000
 11   0     0.8105     1.0000     0.1895     0.9663     0.9663    -0.0000
 12   0     6.8377     7.0000     0.1623     3.5725     3.5725    -0.0000
 13   0     8.4677     8.0000    -0.4677     1.7641     1.7641     0.0000
 14   0     8.5590     8.0000    -0.5590     1.9179     1.9179    -0.0000
 15   0    15.0305    16.0000     0.9695     3.8590     3.8590    -0.0000
 16   0     8.5315     8.0000    -0.5315     1.9740     1.9740    -0.0000
 17   0     8.3592     8.0000    -0.3592     1.8900     1.8900    -0.0000
 18   0     8.3542     8.0000    -0.3542     1.8815     1.8815    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.410955
                0             6               2            6                1.335847
                0             6               8            1                0.946964
                1             6               3            6                1.327327
                1             6               6            7                0.919686
                2             6               5            6                1.375919
                2             6               9            1                0.964280
                3             6               4            6                1.325237
                3             6               7            6                1.070537
                4             6               5            6                1.358402
                4             6              10            1                0.957571
                5             6              11            1                0.965485
                6             7              17            8                1.634829
                6             7              18            8                1.616185
                7             6              12            7                2.278683
                7             6              13            8                0.156589
                7             6              14            8                0.259991
               12             7              13            8                1.184820
               13             8              15           16                0.344932
               14             8              15           16                1.589542
               15            16              16            8                1.862480
               17             8              18            8                0.165520
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 1
   prop. index: 1
        Total number of Electrons                                                    116
        Number of correlated Electrons                                                78
        Number of alpha correlated Electrons                                          39
        Number of beta correlated Electrons                                           39
        Reference Energy:                                               -1148.1048586559
        Total Correlation Energy:                                          -3.0161953185
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1542568025
        Total MDCI Energy:                                              -1151.1210539744
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:    -1148.1686079967
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 2
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1102
     Surface Area:         832.2401274230
     Dielectric Energy:     -0.0141165916
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 2
   prop. index: 1
     Number of atoms                     : 19
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 22
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.1254     6.0000    -0.1254     3.8524     3.8524     0.0000
  1   0     5.9913     6.0000     0.0087     3.8205     3.8205    -0.0000
  2   0     6.1046     6.0000    -0.1046     3.7993     3.7993    -0.0000
  3   0     5.9725     6.0000     0.0275     3.8865     3.8865    -0.0000
  4   0     6.0611     6.0000    -0.0611     3.7752     3.7752     0.0000
  5   0     6.1057     6.0000    -0.1057     3.7908     3.7908    -0.0000
  6   0     6.5217     7.0000     0.4783     4.2152     4.2152     0.0000
  7   0     5.6853     6.0000     0.3147     3.8685     3.8685     0.0000
  8   0     0.7815     1.0000     0.2185     0.9800     0.9800    -0.0000
  9   0     0.7980     1.0000     0.2020     0.9644     0.9644    -0.0000
 10   0     0.8050     1.0000     0.1950     0.9774     0.9774     0.0000
 11   0     0.8046     1.0000     0.1954     0.9681     0.9681     0.0000
 12   0     6.9132     7.0000     0.0868     3.5332     3.5332     0.0000
 13   0     8.4569     8.0000    -0.4569     1.7864     1.7864     0.0000
 14   0     8.5990     8.0000    -0.5990     1.8623     1.8623     0.0000
 15   0    14.9507    16.0000     1.0493     3.8334     3.8334    -0.0000
 16   0     8.5928     8.0000    -0.5928     1.9042     1.9042     0.0000
 17   0     8.3705     8.0000    -0.3705     1.8960     1.8960    -0.0000
 18   0     8.3603     8.0000    -0.3603     1.8914     1.8914     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.429881
                0             6               2            6                1.354680
                0             6               8            1                0.947220
                1             6               3            6                1.284924
                1             6               6            7                0.951596
                2             6               5            6                1.396615
                2             6               9            1                0.965215
                3             6               4            6                1.288380
                3             6               7            6                1.189910
                4             6               5            6                1.376469
                4             6              10            1                0.968006
                5             6              11            1                0.969570
                6             7              17            8                1.656461
                6             7              18            8                1.643967
                7             6              12            7                2.211755
                7             6              13            8                0.139091
                7             6              14            8                0.275077
               12             7              13            8                1.196992
               13             8              15           16                0.371689
               14             8              15           16                1.565726
               15            16              16            8                1.831069
               17             8              18            8                0.162310
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 2
   prop. index: 1
        Total number of Electrons                                                    116
        Number of correlated Electrons                                                78
        Number of alpha correlated Electrons                                          39
        Number of beta correlated Electrons                                           39
        Reference Energy:                                               -1148.1686082598
        Total Correlation Energy:                                          -3.2054403211
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1685134850
        Total MDCI Energy:                                              -1151.3740485809
# -----------------------------------------------------------
$ Energy_Extrapolation
   description: The Extrapolation of the energies
   geom. index: 2
   prop. index: 1
       Do EP1                            : 1 
       Do EP2                            : 0 
       Do EP3                            : 0 
       Number of energies                : 2 
       SCF energy with basis  cc-pVTZ :  -1148.1048586559 
       SCF energy with basis  cc-pVQZ :  -1148.1686082598 
       CBS SCF Energy                    :  -1148.1878162656 
       Correlation energy with basis  cc-pVTZ :  -1148.1048586559 
       Correlation energy with basis  cc-pVQZ :  -1148.1686082598 
       CBS Correlation Energy            :     -3.3401618483 
       CBS Total Energy                  :  -1151.5279781138 
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 2
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       19
     number of electrons:                   116
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             510
     number of aux C basis functions:       2226
     number of aux J basis functions:       0
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           0.000000
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 2
   prop. index: 1
       Filename                          : /scratch/obeletsan/orcajob__14557-kcsc/ts1_phh.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        8.7966976695
        Electronic Contribution:
                  0    
      0       3.750801
      1       8.669666
      2      -0.453616
        Nuclear Contribution:
                  0    
      0      -5.234229
      1     -11.795444
      2       0.532399
        Total Dipole moment:
                  0    
      0      -1.483429
      1      -3.125778
      2       0.078783
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     1 
    Coordinates:
               0 C     -2.075987000000   -0.955520000000    0.220415000000
               1 C     -1.194384000000    0.055227000000   -0.121206000000
               2 C     -1.606777000000   -2.261853000000    0.310557000000
               3 C      0.166794000000   -0.201811000000   -0.352960000000
               4 C      0.625340000000   -1.521068000000   -0.224095000000
               5 C     -0.261981000000   -2.542983000000    0.083896000000
               6 N     -1.726849000000    1.421347000000   -0.268556000000
               7 C      1.108376000000    0.829772000000   -0.616358000000
               8 H     -3.118355000000   -0.716266000000    0.395708000000
               9 H     -2.296949000000   -3.060593000000    0.562402000000
              10 H      1.681352000000   -1.722845000000   -0.368421000000
              11 H      0.103397000000   -3.561968000000    0.162785000000
              12 N      2.035740000000    1.209819000000   -1.285213000000
              13 O      2.913557000000    2.102976000000   -1.218443000000
              14 O      1.245741000000    2.138112000000    0.899029000000
              15 S      2.607444000000    2.828340000000    0.889099000000
              16 O      3.666134000000    2.078519000000    1.597702000000
              17 O     -2.776728000000    1.680143000000    0.303506000000
              18 O     -1.095864000000    2.200651000000   -0.969848000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     2 
    Coordinates:
               0 C     -2.075987000000   -0.955520000000    0.220415000000
               1 C     -1.194384000000    0.055227000000   -0.121206000000
               2 C     -1.606777000000   -2.261853000000    0.310557000000
               3 C      0.166794000000   -0.201811000000   -0.352960000000
               4 C      0.625340000000   -1.521068000000   -0.224095000000
               5 C     -0.261981000000   -2.542983000000    0.083896000000
               6 N     -1.726849000000    1.421347000000   -0.268556000000
               7 C      1.108376000000    0.829772000000   -0.616358000000
               8 H     -3.118355000000   -0.716266000000    0.395708000000
               9 H     -2.296949000000   -3.060593000000    0.562402000000
              10 H      1.681352000000   -1.722845000000   -0.368421000000
              11 H      0.103397000000   -3.561968000000    0.162785000000
              12 N      2.035740000000    1.209819000000   -1.285213000000
              13 O      2.913557000000    2.102976000000   -1.218443000000
              14 O      1.245741000000    2.138112000000    0.899029000000
              15 S      2.607444000000    2.828340000000    0.889099000000
              16 O      3.666134000000    2.078519000000    1.597702000000
              17 O     -2.776728000000    1.680143000000    0.303506000000
              18 O     -1.095864000000    2.200651000000   -0.969848000000
