-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:    -1148.1295710204
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 1
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1068
     Surface Area:         827.7261826839
     Dielectric Energy:     -0.0141889072
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 19
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 22
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.1444     6.0000    -0.1444     3.8340     3.8340     0.0000
  1   0     5.8910     6.0000     0.1090     3.8106     3.8106     0.0000
  2   0     6.1159     6.0000    -0.1159     3.7808     3.7808     0.0000
  3   0     6.0410     6.0000    -0.0410     3.9076     3.9076     0.0000
  4   0     6.0840     6.0000    -0.0840     3.8060     3.8060     0.0000
  5   0     6.1276     6.0000    -0.1276     3.7840     3.7840    -0.0000
  6   0     6.5011     7.0000     0.4989     4.1917     4.1917    -0.0000
  7   0     5.5964     6.0000     0.4036     4.2939     4.2939     0.0000
  8   0     0.7922     1.0000     0.2078     0.9762     0.9762     0.0000
  9   0     0.8083     1.0000     0.1917     0.9661     0.9661     0.0000
 10   0     0.8061     1.0000     0.1939     0.9733     0.9733     0.0000
 11   0     0.8098     1.0000     0.1902     0.9661     0.9661    -0.0000
 12   0     7.0054     7.0000    -0.0054     2.1782     2.1782     0.0000
 13   0     8.5522     8.0000    -0.5522     1.9290     1.9290     0.0000
 14   0     8.4894     8.0000    -0.4894     2.0180     2.0180     0.0000
 15   0    15.0187    16.0000     0.9813     3.8498     3.8498    -0.0000
 16   0     8.5030     8.0000    -0.5030     2.0063     2.0063     0.0000
 17   0     8.3565     8.0000    -0.3565     1.8949     1.8949    -0.0000
 18   0     8.3569     8.0000    -0.3569     1.8846     1.8846    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.409339
                0             6               2            6                1.330040
                0             6               8            1                0.944849
                1             6               3            6                1.314910
                1             6               6            7                0.916615
                2             6               5            6                1.378152
                2             6               9            1                0.964792
                3             6               4            6                1.344116
                3             6               7            6                0.890089
                3             6              12            7                0.242965
                4             6               5            6                1.355559
                4             6              10            1                0.953355
                5             6              11            1                0.965502
                6             7              17            8                1.640217
                6             7              18            8                1.610705
                7             6              12            7                1.641196
                7             6              14            8                1.672903
               12             7              13            8                0.153482
               13             8              15           16                1.667130
               14             8              15           16                0.247788
               15            16              16            8                1.904796
               17             8              18            8                0.165880
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 1
   prop. index: 1
        Total number of Electrons                                                    116
        Number of correlated Electrons                                                78
        Number of alpha correlated Electrons                                          39
        Number of beta correlated Electrons                                           39
        Reference Energy:                                               -1148.1295711626
        Total Correlation Energy:                                          -2.9924315958
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1515721603
        Total MDCI Energy:                                              -1151.1220027584
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:    -1148.1930901277
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 2
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1068
     Surface Area:         827.7261826839
     Dielectric Energy:     -0.0140645234
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 2
   prop. index: 1
     Number of atoms                     : 19
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 22
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.1085     6.0000    -0.1085     3.8236     3.8236    -0.0000
  1   0     5.9588     6.0000     0.0412     3.8579     3.8579    -0.0000
  2   0     6.1132     6.0000    -0.1132     3.8148     3.8148     0.0000
  3   0     6.0200     6.0000    -0.0200     3.8006     3.8006     0.0000
  4   0     6.0530     6.0000    -0.0530     3.7761     3.7761    -0.0000
  5   0     6.0932     6.0000    -0.0932     3.7737     3.7737    -0.0000
  6   0     6.5271     7.0000     0.4729     4.2224     4.2224    -0.0000
  7   0     5.5142     6.0000     0.4858     4.2798     4.2798    -0.0000
  8   0     0.7836     1.0000     0.2164     0.9795     0.9795    -0.0000
  9   0     0.7975     1.0000     0.2025     0.9644     0.9644     0.0000
 10   0     0.8148     1.0000     0.1852     0.9815     0.9815    -0.0000
 11   0     0.8043     1.0000     0.1957     0.9672     0.9672     0.0000
 12   0     7.0695     7.0000    -0.0695     2.2498     2.2498    -0.0000
 13   0     8.5913     8.0000    -0.5913     1.8771     1.8771    -0.0000
 14   0     8.5161     8.0000    -0.5161     1.9936     1.9936     0.0000
 15   0    14.9425    16.0000     1.0575     3.8305     3.8305    -0.0000
 16   0     8.5642     8.0000    -0.5642     1.9372     1.9372    -0.0000
 17   0     8.3660     8.0000    -0.3660     1.9002     1.9002     0.0000
 18   0     8.3623     8.0000    -0.3623     1.8965     1.8965    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.413956
                0             6               2            6                1.349868
                0             6               8            1                0.947510
                1             6               3            6                1.292188
                1             6               6            7                0.954018
                2             6               5            6                1.403432
                2             6               9            1                0.965361
                3             6               4            6                1.300143
                3             6               7            6                0.908119
                3             6              12            7                0.230096
                4             6               5            6                1.359954
                4             6              10            1                0.972126
                5             6              11            1                0.968840
                6             7              17            8                1.662655
                6             7              18            8                1.643207
                7             6              12            7                1.710555
                7             6              14            8                1.626819
               12             7              13            8                0.146918
               13             8              15           16                1.656285
               14             8              15           16                0.270921
               15            16              16            8                1.868104
               17             8              18            8                0.162214
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 2
   prop. index: 1
        Total number of Electrons                                                    116
        Number of correlated Electrons                                                78
        Number of alpha correlated Electrons                                          39
        Number of beta correlated Electrons                                           39
        Reference Energy:                                               -1148.1930903410
        Total Correlation Energy:                                          -3.1810532945
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1656032630
        Total MDCI Energy:                                              -1151.3741436355
# -----------------------------------------------------------
$ Energy_Extrapolation
   description: The Extrapolation of the energies
   geom. index: 2
   prop. index: 1
       Do EP1                            : 1 
       Do EP2                            : 0 
       Do EP3                            : 0 
       Number of energies                : 2 
       SCF energy with basis  cc-pVTZ :  -1148.1295711626 
       SCF energy with basis  cc-pVQZ :  -1148.1930903410 
       CBS SCF Energy                    :  -1148.2122289187 
       Correlation energy with basis  cc-pVTZ :  -1148.1295711626 
       Correlation energy with basis  cc-pVQZ :  -1148.1930903410 
       CBS Correlation Energy            :     -3.3153310982 
       CBS Total Energy                  :  -1151.5275600169 
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 2
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       19
     number of electrons:                   116
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             510
     number of aux C basis functions:       2226
     number of aux J basis functions:       0
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           0.000000
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 2
   prop. index: 1
       Filename                          : /scratch/obeletsan/orcajob__14558-JK7k/ts2_phh.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        7.8664484320
        Electronic Contribution:
                  0    
      0       5.155430
      1       8.254090
      2      -1.181410
        Nuclear Contribution:
                  0    
      0      -6.473432
      1     -10.910158
      2       2.068080
        Total Dipole moment:
                  0    
      0      -1.318002
      1      -2.656068
      2       0.886670
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     1 
    Coordinates:
               0 C      0.069405000000    0.017270000000    0.122533000000
               1 C      1.025934000000    0.995131000000   -0.084803000000
               2 C      0.476831000000   -1.270372000000    0.463977000000
               3 C      2.389724000000    0.704231000000    0.055693000000
               4 C      2.788435000000   -0.601247000000    0.346724000000
               5 C      1.830310000000   -1.579965000000    0.575095000000
               6 N      0.572885000000    2.356058000000   -0.422281000000
               7 C      3.493512000000    1.795669000000    0.033406000000
               8 H     -0.979941000000    0.270616000000    0.022588000000
               9 H     -0.272397000000   -2.037866000000    0.630267000000
              10 H      3.848779000000   -0.828722000000    0.392417000000
              11 H      2.142225000000   -2.589997000000    0.820336000000
              12 N      3.658751000000    1.590369000000   -1.200121000000
              13 O      5.111735000000    3.036584000000   -1.360001000000
              14 O      3.959524000000    2.422810000000    1.005858000000
              15 S      5.779253000000    3.249431000000   -0.017376000000
              16 O      6.753132000000    2.215025000000    0.364102000000
              17 O     -0.541196000000    2.468922000000   -0.912668000000
              18 O      1.336787000000    3.278954000000   -0.176598000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     2 
    Coordinates:
               0 C      0.069405000000    0.017270000000    0.122533000000
               1 C      1.025934000000    0.995131000000   -0.084803000000
               2 C      0.476831000000   -1.270372000000    0.463977000000
               3 C      2.389724000000    0.704231000000    0.055693000000
               4 C      2.788435000000   -0.601247000000    0.346724000000
               5 C      1.830310000000   -1.579965000000    0.575095000000
               6 N      0.572885000000    2.356058000000   -0.422281000000
               7 C      3.493512000000    1.795669000000    0.033406000000
               8 H     -0.979941000000    0.270616000000    0.022588000000
               9 H     -0.272397000000   -2.037866000000    0.630267000000
              10 H      3.848779000000   -0.828722000000    0.392417000000
              11 H      2.142225000000   -2.589997000000    0.820336000000
              12 N      3.658751000000    1.590369000000   -1.200121000000
              13 O      5.111735000000    3.036584000000   -1.360001000000
              14 O      3.959524000000    2.422810000000    1.005858000000
              15 S      5.779253000000    3.249431000000   -0.017376000000
              16 O      6.753132000000    2.215025000000    0.364102000000
              17 O     -0.541196000000    2.468922000000   -0.912668000000
              18 O      1.336787000000    3.278954000000   -0.176598000000
