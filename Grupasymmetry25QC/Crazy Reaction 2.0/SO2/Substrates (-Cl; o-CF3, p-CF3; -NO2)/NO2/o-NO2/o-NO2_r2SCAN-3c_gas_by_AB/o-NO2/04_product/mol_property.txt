-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -604.0960578055
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                 41.9999915240 
   Number of Beta  Electrons                 41.9999915240 
   Total number of  Electrons                83.9999830479 
   Exchange energy                          -75.6960213787 
   Correlation energy                        -2.7935825082 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -78.4896038869 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -604.0960578055 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 1
   prop. index: 1
        Van der Waals Correction:       -0.0061923545
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:     -604.1383108834
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 2
   prop. index: 1
   Number of Alpha Electrons                 41.9999909862 
   Number of Beta  Electrons                 41.9999909862 
   Total number of  Electrons                83.9999819723 
   Exchange energy                          -75.8538873046 
   Correlation energy                        -2.8044652747 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -78.6583525792 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -604.1383108834 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 2
   prop. index: 1
        Van der Waals Correction:       -0.0062409437
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 3
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 3
   prop. index: 1
        SCF Energy:     -604.1396706861
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 3
   prop. index: 1
   Number of Alpha Electrons                 41.9999881984 
   Number of Beta  Electrons                 41.9999881984 
   Total number of  Electrons                83.9999763968 
   Exchange energy                          -76.0056729984 
   Correlation energy                        -2.8128748987 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -78.8185478971 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -604.1396706861 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 3
   prop. index: 1
        Van der Waals Correction:       -0.0062543955
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 4
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 4
   prop. index: 1
        SCF Energy:     -604.1449768098
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 4
   prop. index: 1
   Number of Alpha Electrons                 41.9999908490 
   Number of Beta  Electrons                 41.9999908490 
   Total number of  Electrons                83.9999816980 
   Exchange energy                          -75.9203655091 
   Correlation energy                        -2.8082495316 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -78.7286150407 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -604.1449768098 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 4
   prop. index: 1
        Van der Waals Correction:       -0.0062463147
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 5
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 5
   prop. index: 1
        SCF Energy:     -604.1426484235
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 5
   prop. index: 1
   Number of Alpha Electrons                 41.9999905037 
   Number of Beta  Electrons                 41.9999905037 
   Total number of  Electrons                83.9999810073 
   Exchange energy                          -75.9214419654 
   Correlation energy                        -2.8082395104 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -78.7296814758 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -604.1426484235 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 5
   prop. index: 1
        Van der Waals Correction:       -0.0062409206
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 6
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 6
   prop. index: 1
        SCF Energy:     -604.1452129830
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 6
   prop. index: 1
   Number of Alpha Electrons                 41.9999903270 
   Number of Beta  Electrons                 41.9999903270 
   Total number of  Electrons                83.9999806539 
   Exchange energy                          -75.9323754712 
   Correlation energy                        -2.8088756753 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -78.7412511465 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -604.1452129830 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 6
   prop. index: 1
        Van der Waals Correction:       -0.0062443767
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 7
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 7
   prop. index: 1
        SCF Energy:     -604.1452326661
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 7
   prop. index: 1
   Number of Alpha Electrons                 41.9999897182 
   Number of Beta  Electrons                 41.9999897182 
   Total number of  Electrons                83.9999794363 
   Exchange energy                          -75.9311149093 
   Correlation energy                        -2.8088236068 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -78.7399385160 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -604.1452326661 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 7
   prop. index: 1
        Van der Waals Correction:       -0.0062441694
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 8
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 8
   prop. index: 1
        SCF Energy:     -604.1452421438
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 8
   prop. index: 1
   Number of Alpha Electrons                 41.9999884291 
   Number of Beta  Electrons                 41.9999884291 
   Total number of  Electrons                83.9999768583 
   Exchange energy                          -75.9304546864 
   Correlation energy                        -2.8087876243 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -78.7392423107 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -604.1452421438 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 8
   prop. index: 1
        Van der Waals Correction:       -0.0062432395
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 9
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 9
   prop. index: 1
        SCF Energy:     -604.1452503375
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 9
   prop. index: 1
   Number of Alpha Electrons                 41.9999862842 
   Number of Beta  Electrons                 41.9999862842 
   Total number of  Electrons                83.9999725684 
   Exchange energy                          -75.9300863440 
   Correlation energy                        -2.8087694280 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -78.7388557720 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -604.1452503375 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 9
   prop. index: 1
        Van der Waals Correction:       -0.0062421478
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 10
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 10
   prop. index: 1
        SCF Energy:     -604.1452559203
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 10
   prop. index: 1
   Number of Alpha Electrons                 41.9999843775 
   Number of Beta  Electrons                 41.9999843775 
   Total number of  Electrons                83.9999687549 
   Exchange energy                          -75.9302403772 
   Correlation energy                        -2.8087833485 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -78.7390237257 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -604.1452559203 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 10
   prop. index: 1
        Van der Waals Correction:       -0.0062416154
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 11
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 11
   prop. index: 1
        SCF Energy:     -604.1452608499
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 11
   prop. index: 1
   Number of Alpha Electrons                 41.9999828405 
   Number of Beta  Electrons                 41.9999828405 
   Total number of  Electrons                83.9999656809 
   Exchange energy                          -75.9306358502 
   Correlation energy                        -2.8088142264 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -78.7394500766 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -604.1452608499 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 11
   prop. index: 1
        Van der Waals Correction:       -0.0062415608
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 12
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 12
   prop. index: 1
        SCF Energy:     -604.1452632328
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 12
   prop. index: 1
   Number of Alpha Electrons                 41.9999824673 
   Number of Beta  Electrons                 41.9999824673 
   Total number of  Electrons                83.9999649345 
   Exchange energy                          -75.9308615893 
   Correlation energy                        -2.8088324330 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -78.7396940223 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -604.1452632328 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 12
   prop. index: 1
        Van der Waals Correction:       -0.0062417705
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 13
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 13
   prop. index: 1
        SCF Energy:     -604.1452642141
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 13
   prop. index: 1
   Number of Alpha Electrons                 41.9999824821 
   Number of Beta  Electrons                 41.9999824821 
   Total number of  Electrons                83.9999649641 
   Exchange energy                          -75.9308954913 
   Correlation energy                        -2.8088367442 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -78.7397322355 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -604.1452642141 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 13
   prop. index: 1
        Van der Waals Correction:       -0.0062418912
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 13
   prop. index: 1
       Filename                          : mol.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        5.0868851409
        Electronic Contribution:
                  0    
      0       3.870470
      1       6.369888
      2      -1.767273
        Nuclear Contribution:
                  0    
      0      -4.119198
      1      -8.306237
      2       2.207576
        Total Dipole moment:
                  0    
      0      -0.248728
      1      -1.936348
      2       0.440303
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 14
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 14
   prop. index: 1
        SCF Energy:     -604.1452642135
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 14
   prop. index: 1
   Number of Alpha Electrons                 41.9999824821 
   Number of Beta  Electrons                 41.9999824821 
   Total number of  Electrons                83.9999649642 
   Exchange energy                          -75.9308886631 
   Correlation energy                        -2.8088363528 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -78.7397250159 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -604.1452642135 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 14
   prop. index: 1
        Van der Waals Correction:       -0.0062418912
# -----------------------------------------------------------
$ THERMOCHEMISTRY_Energies
   description: The Thermochemistry energies
   geom. index: 14
   prop. index: 1
        Temperature (Kelvin)           :        298.1500000000
        Pressure (atm)                 :          1.0000000000
        Total Mass (AMU)               :        164.0221919965
        Spin Degeneracy                :          1.0000000000
        Electronic Energy (Hartree)    :       -604.1403423875
        Translational Energy (Hartree) :          0.0014162714
        Rotational Energy (Hartree)    :          0.0014162714
        Vibrational Energy (Hartree)   :          0.0068682707
        Number of frequencies          :     48      
        Scaling Factor for frequencies :          1.0000000000
        Vibrational frequencies        :     
                  0    
      0       0.000000
      1       0.000000
      2       0.000000
      3       0.000000
      4       0.000000
      5       0.000000
      6      44.930151
      7      60.720196
      8      85.003642
      9     128.340222
     10     220.509971
     11     242.144955
     12     360.362618
     13     373.579128
     14     431.544920
     15     506.396183
     16     516.671567
     17     572.424665
     18     578.573922
     19     644.498739
     20     677.899710
     21     695.814732
     22     750.777629
     23     789.661992
     24     796.432496
     25     870.935664
     26     872.735327
     27     962.366592
     28     984.994092
     29     1063.026247
     30     1104.071510
     31     1159.430135
     32     1177.669315
     33     1192.510384
     34     1287.687710
     35     1353.152057
     36     1364.357503
     37     1464.523482
     38     1480.041815
     39     1556.665670
     40     1578.236699
     41     1609.553501
     42     1644.846811
     43     2361.709519
     44     3173.817295
     45     3185.403710
     46     3196.462048
     47     3211.744090
        Zero Point Energy (Hartree)    :          0.1055525834
        Inner Energy (Hartree)         :       -604.0250889906
        Enthalpy (Hartree)             :       -604.0241447815
        Electronic entropy             :          0.0000000000
        Rotational entropy             :          0.0146060473
        Vibrational entropy            :          0.0118810990
        Translational entropy          :          0.0146060473
        Entropy                        :          0.0460592042
        Gibbs Energy (Hartree)         :       -604.0702039857
        Is Linear                      :                 false
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    16 
    Geometry Index:     1 
    Coordinates:
               0 C      1.830141987000   -0.591828029000    0.300050474000
               1 C      2.913881363000    0.242239797000    0.024181983000
               2 C      2.025404789000   -1.939602159000    0.548526477000
               3 C      4.230134946000   -0.261692302000    0.027547887000
               4 C      4.407060286000   -1.625027139000    0.312097209000
               5 C      3.322753387000   -2.453552524000    0.555439829000
               6 N      2.607769059000    1.657108800000   -0.275392999000
               7 C      6.518977689000    0.404682153000   -0.346685972000
               8 H      0.837929999000   -0.156814583000    0.302504802000
               9 H      1.172211431000   -2.585919437000    0.738994321000
              10 H      5.423221758000   -2.014092169000    0.347431647000
              11 H      3.492462641000   -3.506623016000    0.759930469000
              12 N      5.336374336000    0.557642976000   -0.144597087000
              13 O      7.671703286000    0.408342097000   -0.552072163000
              14 O      3.445533997000    2.254142439000   -1.205747460000
              15 O      1.512041530000    2.141624755000    0.406871186000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    16 
    Geometry Index:     2 
    Coordinates:
               0 C      1.826029193571   -0.588806938252    0.298810352363
               1 C      2.913641872332    0.239737572707    0.025246236161
               2 C      2.021572606499   -1.935567083333    0.547773201318
               3 C      4.226464990869   -0.262833225804    0.028782944397
               4 C      4.403590327224   -1.624636705568    0.312886088822
               5 C      3.318593987522   -2.451050256985    0.555579391595
               6 N      2.631360487756    1.632268390320   -0.270791510337
               7 C      6.500961992885    0.427311791909   -0.352137233855
               8 H      0.834874645023   -0.151503033320    0.298125148034
               9 H      1.169369990891   -2.580203901729    0.736910232608
              10 H      5.417198222654   -2.014605149949    0.347805718787
              11 H      3.487062809990   -3.504165897257    0.759742146583
              12 N      5.318193101966    0.566256787168   -0.147810491510
              13 O      7.651477251278    0.442001320585   -0.559782242227
              14 O      3.399787527902    2.200932154407   -1.117684259351
              15 O      1.627423475640    2.135495834101    0.335624879612
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    16 
    Geometry Index:     3 
    Coordinates:
               0 C      1.828052368938   -0.588465833003    0.301587245943
               1 C      2.916948686599    0.236578692375    0.028990200888
               2 C      2.022140560452   -1.936551974361    0.547853532762
               3 C      4.227330089590   -0.269305329394    0.033756844411
               4 C      4.403355491962   -1.631572624190    0.313739368761
               5 C      3.316785216728   -2.455167317098    0.553754449283
               6 N      2.642924804447    1.639697362291   -0.269115801131
               7 C      6.495508721338    0.437653913746   -0.351138394406
               8 H      0.837311086202   -0.148556485309    0.302041983347
               9 H      1.169688708208   -2.579887932254    0.737734488002
              10 H      5.415096594756   -2.025539384357    0.346659161167
              11 H      3.482450419938   -3.508965130572    0.755865971948
              12 N      5.311534736600    0.566142346182   -0.147615806234
              13 O      7.645545417662    0.459183868380   -0.558306683968
              14 O      3.321598143559    2.189006029242   -1.063188164064
              15 O      1.711331437020    2.146381457322    0.266462206291
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    16 
    Geometry Index:     4 
    Coordinates:
               0 C      1.825260349694   -0.593447372866    0.297586264699
               1 C      2.913382607794    0.231802563518    0.025939132910
               2 C      2.021199355992   -1.940692524048    0.548253696704
               3 C      4.224253021353   -0.270611322486    0.034276558419
               4 C      4.402802408478   -1.631894790217    0.317217407927
               5 C      3.316948908789   -2.456947576776    0.557743639813
               6 N      2.637116622576    1.636876061702   -0.279303359336
               7 C      6.491203111302    0.439811179395   -0.356269713364
               8 H      0.833732804081   -0.155225187854    0.294282546827
               9 H      1.169553936787   -2.584910070924    0.738670414000
              10 H      5.415154351902   -2.024142660254    0.350818141678
              11 H      3.484094360398   -3.510029524456    0.762506821426
              12 N      5.306707941177    0.566661259609   -0.154707860665
              13 O      7.641460051067    0.462935747374   -0.562881806748
              14 O      3.381016878844    2.204344215706   -1.079211020675
              15 O      1.683715773770    2.156101661577    0.304159739384
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    16 
    Geometry Index:     5 
    Coordinates:
               0 C      1.835706858078   -0.578835873086    0.313947197125
               1 C      2.928289676393    0.240478861684    0.047811744617
               2 C      2.023691622826   -1.930340147413    0.548878412260
               3 C      4.235890038370   -0.269778879805    0.043922085441
               4 C      4.406643663334   -1.635225285145    0.310150872921
               5 C      3.315744000474   -2.455309997023    0.546088444115
               6 N      2.657829438933    1.654634818061   -0.236478727575
               7 C      6.506200159514    0.432974243427   -0.344085424928
               8 H      0.847254010739   -0.133250378247    0.321417204382
               9 H      1.168799912217   -2.570825699346    0.737386330198
              10 H      5.416135730298   -2.035412717718    0.333926804949
              11 H      3.476366038473   -3.511820533573    0.737989201094
              12 N      5.322066531063    0.563381449340   -0.143005762908
              13 O      7.656767362030    0.451887866672   -0.549702597808
              14 O      3.321300873218    2.186862801485   -1.124627081346
              15 O      1.628916568040    2.121211129685    0.255461900462
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    16 
    Geometry Index:     6 
    Coordinates:
               0 C      1.827710517854   -0.590253445529    0.301529367160
               1 C      2.917151697702    0.230710735976    0.029829139003
               2 C      2.021854341237   -1.938724673652    0.549834694980
               3 C      4.227042692102   -0.272978248075    0.034757792084
               4 C      4.404049274893   -1.634866540198    0.314082115698
               5 C      3.316244395393   -2.457562896063    0.555362310812
               6 N      2.641329474477    1.642268452614   -0.272598147714
               7 C      6.494308934045    0.440504774438   -0.352914039932
               8 H      0.837154010976   -0.149389418429    0.302105260022
               9 H      1.169641112604   -2.581524505931    0.742585932681
              10 H      5.415254313601   -2.030350524531    0.343851887389
              11 H      3.481215908756   -3.511404127239    0.757943551307
              12 N      5.308348610219    0.564983538748   -0.158990472954
              13 O      7.645661452800    0.464519080601   -0.553458450022
              14 O      3.364091435292    2.204719155866   -1.081246633527
              15 O      1.676544312048    2.149980300405    0.286406296012
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    16 
    Geometry Index:     7 
    Coordinates:
               0 C      1.827405359660   -0.589909323367    0.302046745714
               1 C      2.916819207775    0.229704990870    0.028158913330
               2 C      2.021864344437   -1.938419035211    0.551311526343
               3 C      4.226781098227   -0.273613113480    0.031314738716
               4 C      4.404049362885   -1.635184709648    0.311567001236
               5 C      3.316113597485   -2.457258749865    0.555371472398
               6 N      2.639854998447    1.642029634147   -0.275105497610
               7 C      6.494755688693    0.440991028069   -0.351363864251
               8 H      0.837007730762   -0.148802505760    0.303992162769
               9 H      1.169991162261   -2.581070177934    0.746118522242
              10 H      5.415053605381   -2.031250213845    0.340172542287
              11 H      3.481103100314   -3.510960657138    0.758719378137
              12 N      5.307563840956    0.564466691162   -0.164842173761
              13 O      7.647173936205    0.465495572359   -0.545864568543
              14 O      3.365915412102    2.205849543564   -1.080627452973
              15 O      1.676150038409    2.148562685076    0.288111156964
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    16 
    Geometry Index:     8 
    Coordinates:
               0 C      1.827286093100   -0.589292971393    0.303720841968
               1 C      2.916626988135    0.228684381861    0.026831549238
               2 C      2.022163669645   -1.937969407987    0.553774478426
               3 C      4.226594245301   -0.274467387758    0.026879883286
               4 C      4.404129664913   -1.635842257823    0.307471057454
               5 C      3.316098139712   -2.457010061666    0.554908013734
               6 N      2.638502580072    1.642579596893   -0.277230702229
               7 C      6.495773682605    0.441232610626   -0.348218224848
               8 H      0.837105839708   -0.147939536880    0.307947746284
               9 H      1.170792855443   -2.580442226634    0.751400584655
              10 H      5.414755001145   -2.033034345661    0.333439858723
              11 H      3.481090131337   -3.510654447086    0.758643617318
              12 N      5.306642588306    0.563295981764   -0.174098813359
              13 O      7.649887968602    0.466359406411   -0.532657565924
              14 O      3.364560603743    2.206973192424   -1.082181313037
              15 O      1.675592432233    2.148159131908    0.288449591311
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    16 
    Geometry Index:     9 
    Coordinates:
               0 C      1.827316043772   -0.588342935267    0.306748633856
               1 C      2.916361253824    0.227724638178    0.025063260196
               2 C      2.022681362281   -1.937191379172    0.557531470539
               3 C      4.226215917441   -0.275528811662    0.020275874225
               4 C      4.404063437775   -1.636777656277    0.301045334233
               5 C      3.316161151766   -2.456731235581    0.553896455575
               6 N      2.637232360482    1.643148441610   -0.279395856022
               7 C      6.497223676357    0.441483251803   -0.342981452366
               8 H      0.837399490107   -0.146626642302    0.314699459593
               9 H      1.171990569128   -2.579294852603    0.759277864072
              10 H      5.414163794647   -2.035548639291    0.322653202394
              11 H      3.481255889039   -3.510325575722    0.757926350537
              12 N      5.305293516462    0.561316349364   -0.188284185334
              13 O      7.653785169966    0.467840786417   -0.511614976719
              14 O      3.361116827060    2.207318867113   -1.086179777475
              15 O      1.675342023893    2.148167052393    0.288418945695
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    16 
    Geometry Index:     10 
    Coordinates:
               0 C      1.827493757937   -0.587630780620    0.309470427397
               1 C      2.916015158365    0.227480452384    0.023348855738
               2 C      2.023148269848   -1.936447543308    0.560671638083
               3 C      4.225725023106   -0.276184745530    0.014606489554
               4 C      4.403788931953   -1.637425367546    0.295481968880
               5 C      3.316322038003   -2.456546177022    0.552897687116
               6 N      2.636450684628    1.643128390687   -0.280833514274
               7 C      6.498321771260    0.441503576997   -0.338234227062
               8 H      0.837752083274   -0.145627407485    0.320683433676
               9 H      1.172940148515   -2.578107691346    0.765835053901
              10 H      5.413557466742   -2.037225909851    0.313266709549
              11 H      3.481645652101   -3.510078690631    0.757094184598
              12 N      5.304209990135    0.559473102875   -0.200289135661
              13 O      7.656792488946    0.469188499241   -0.493148136699
              14 O      3.357384009003    2.206515757750   -1.090637454177
              15 O      1.676055010183    2.148616192404    0.288866622379
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    16 
    Geometry Index:     11 
    Coordinates:
               0 C      1.827750871291   -0.587238366682    0.311604994687
               1 C      2.915583979049    0.227802503814    0.021755791770
               2 C      2.023522333942   -1.935834108615    0.563057791068
               3 C      4.225132483448   -0.276477089227    0.010117889161
               4 C      4.403367163080   -1.637766251835    0.291127091932
               5 C      3.316561503742   -2.456454379302    0.552100302310
               6 N      2.635957862730    1.642650603292   -0.281783811081
               7 C      6.499031132884    0.441275185798   -0.334403463362
               8 H      0.838112235596   -0.145006019670    0.325349377299
               9 H      1.173591672107   -2.577014402327    0.770825998596
              10 H      5.413033223643   -2.037967754489    0.305841681301
              11 H      3.482208243753   -3.509907726635    0.756400875379
              12 N      5.303348760911    0.557961826080   -0.209689401984
              13 O      7.658875599566    0.470211139913   -0.478377573811
              14 O      3.354148134315    2.205129184666   -1.094684045461
              15 O      1.677377283943    2.149267314220    0.289837105197
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    16 
    Geometry Index:     12 
    Coordinates:
               0 C      1.827893709871   -0.587303551587    0.312027437116
               1 C      2.915345297255    0.228255475630    0.021190619512
               2 C      2.023627584595   -1.935691057561    0.563524426363
               3 C      4.224826858406   -0.276362318057    0.009067125044
               4 C      4.403148188976   -1.637677553607    0.290193884304
               5 C      3.316737869862   -2.456460670727    0.551950745990
               6 N      2.635790702365    1.642268888770   -0.281938066260
               7 C      6.499123796882    0.440884531336   -0.333549264260
               8 H      0.838261112221   -0.144991277316    0.326265212896
               9 H      1.173677979054   -2.576663755374    0.771829756652
              10 H      5.412953207559   -2.037570079504    0.304187908641
              11 H      3.482573600632   -3.509867255568    0.756288378961
              12 N      5.303116729907    0.557673221382   -0.211765693749
              13 O      7.659262913210    0.470099990070   -0.474947113164
              14 O      3.353247437563    2.204518387591   -1.095768825388
              15 O      1.678015495643    2.149518683523    0.290524070341
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    16 
    Geometry Index:     13 
    Coordinates:
               0 C      1.827958187590   -0.587427465508    0.311925744249
               1 C      2.915271378979    0.228477500435    0.021077989264
               2 C      2.023646342280   -1.935711127545    0.563445168052
               3 C      4.224723892574   -0.276264768560    0.009106344866
               4 C      4.403088996885   -1.637577965424    0.290313710669
               5 C      3.316827015637   -2.456492940588    0.552010833147
               6 N      2.635709912618    1.642158053996   -0.281922670395
               7 C      6.499052432485    0.440594737809   -0.333706321898
               8 H      0.838333247149   -0.145053496210    0.326041787254
               9 H      1.173629502073   -2.576610791952    0.771690125902
              10 H      5.413029059195   -2.037140237160    0.304346164205
              11 H      3.482719988123   -3.509878597050    0.756381435094
              12 N      5.303091845757    0.557769195600   -0.211552399208
              13 O      7.659173847992    0.469766621935   -0.475199346568
              14 O      3.353325141108    2.204547546116   -1.095584441121
              15 O      1.678021693554    2.149475393105    0.290706479487
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    16 
    Geometry Index:     14 
    Coordinates:
               0 C      1.827958187590   -0.587427465508    0.311925744249
               1 C      2.915271378979    0.228477500435    0.021077989264
               2 C      2.023646342280   -1.935711127545    0.563445168052
               3 C      4.224723892574   -0.276264768560    0.009106344866
               4 C      4.403088996885   -1.637577965424    0.290313710669
               5 C      3.316827015637   -2.456492940588    0.552010833147
               6 N      2.635709912618    1.642158053996   -0.281922670395
               7 C      6.499052432485    0.440594737809   -0.333706321898
               8 H      0.838333247149   -0.145053496210    0.326041787254
               9 H      1.173629502073   -2.576610791952    0.771690125902
              10 H      5.413029059195   -2.037140237160    0.304346164205
              11 H      3.482719988123   -3.509878597050    0.756381435094
              12 N      5.303091845757    0.557769195600   -0.211552399208
              13 O      7.659173847992    0.469766621935   -0.475199346568
              14 O      3.353325141108    2.204547546116   -1.095584441121
              15 O      1.678021693554    2.149475393105    0.290706479487
