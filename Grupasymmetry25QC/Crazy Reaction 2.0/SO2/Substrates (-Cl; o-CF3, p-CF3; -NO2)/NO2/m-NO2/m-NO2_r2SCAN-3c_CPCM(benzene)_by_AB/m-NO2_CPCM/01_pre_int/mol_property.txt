-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:    -1152.6543970168
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                 58.0000234252 
   Number of Beta  Electrons                 58.0000234252 
   Total number of  Electrons               116.0000468505 
   Exchange energy                         -117.4628528519 
   Correlation energy                        -3.9881926804 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -121.4510455323 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1152.6543970168 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 1
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1158
     Surface Area:         880.2147699109
     Dielectric Energy:     -0.0082366467
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 1
   prop. index: 1
        Van der Waals Correction:       -0.0077936570
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:    -1152.6544806866
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 2
   prop. index: 1
   Number of Alpha Electrons                 58.0000219340 
   Number of Beta  Electrons                 58.0000219340 
   Total number of  Electrons               116.0000438680 
   Exchange energy                         -117.4608735217 
   Correlation energy                        -3.9879442419 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -121.4488177636 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1152.6544806866 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 2
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1158
     Surface Area:         880.9295571305
     Dielectric Energy:     -0.0083473299
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 2
   prop. index: 1
        Van der Waals Correction:       -0.0077882689
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 3
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 3
   prop. index: 1
        SCF Energy:    -1152.6545378587
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 3
   prop. index: 1
   Number of Alpha Electrons                 58.0000202977 
   Number of Beta  Electrons                 58.0000202977 
   Total number of  Electrons               116.0000405953 
   Exchange energy                         -117.4593632426 
   Correlation energy                        -3.9877810569 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -121.4471442994 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1152.6545378587 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 3
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1161
     Surface Area:         881.6179066515
     Dielectric Energy:     -0.0084638034
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 3
   prop. index: 1
        Van der Waals Correction:       -0.0077823102
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 4
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 4
   prop. index: 1
        SCF Energy:    -1152.6545083446
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 4
   prop. index: 1
   Number of Alpha Electrons                 58.0000195105 
   Number of Beta  Electrons                 58.0000195105 
   Total number of  Electrons               116.0000390210 
   Exchange energy                         -117.4587323531 
   Correlation energy                        -3.9877275115 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -121.4464598646 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1152.6545083446 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 4
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1162
     Surface Area:         882.1710995505
     Dielectric Energy:     -0.0085379747
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 4
   prop. index: 1
        Van der Waals Correction:       -0.0077774922
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 5
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 5
   prop. index: 1
        SCF Energy:    -1152.6545766573
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 5
   prop. index: 1
   Number of Alpha Electrons                 58.0000193513 
   Number of Beta  Electrons                 58.0000193513 
   Total number of  Electrons               116.0000387026 
   Exchange energy                         -117.4590407994 
   Correlation energy                        -3.9877683210 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -121.4468091204 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1152.6545766573 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 5
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1162
     Surface Area:         882.1238411739
     Dielectric Energy:     -0.0085306999
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 5
   prop. index: 1
        Van der Waals Correction:       -0.0077768316
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 6
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 6
   prop. index: 1
        SCF Energy:    -1152.6545897303
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 6
   prop. index: 1
   Number of Alpha Electrons                 58.0000193102 
   Number of Beta  Electrons                 58.0000193102 
   Total number of  Electrons               116.0000386204 
   Exchange energy                         -117.4593886047 
   Correlation energy                        -3.9878086898 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -121.4471972946 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1152.6545897303 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 6
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1161
     Surface Area:         882.2279423278
     Dielectric Energy:     -0.0085320415
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 6
   prop. index: 1
        Van der Waals Correction:       -0.0077752202
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 7
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 7
   prop. index: 1
        SCF Energy:    -1152.6546347066
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 7
   prop. index: 1
   Number of Alpha Electrons                 58.0000186188 
   Number of Beta  Electrons                 58.0000186188 
   Total number of  Electrons               116.0000372377 
   Exchange energy                         -117.4604263824 
   Correlation energy                        -3.9879291092 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -121.4483554916 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1152.6546347066 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 7
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1163
     Surface Area:         882.9332940285
     Dielectric Energy:     -0.0085688014
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 7
   prop. index: 1
        Van der Waals Correction:       -0.0077672461
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 8
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 8
   prop. index: 1
        SCF Energy:    -1152.6546477888
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 8
   prop. index: 1
   Number of Alpha Electrons                 58.0000183159 
   Number of Beta  Electrons                 58.0000183159 
   Total number of  Electrons               116.0000366317 
   Exchange energy                         -117.4604690334 
   Correlation energy                        -3.9879320336 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -121.4484010670 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1152.6546477888 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 8
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1164
     Surface Area:         883.3072095504
     Dielectric Energy:     -0.0085904285
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 8
   prop. index: 1
        Van der Waals Correction:       -0.0077638982
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 9
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 9
   prop. index: 1
        SCF Energy:    -1152.6546559092
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 9
   prop. index: 1
   Number of Alpha Electrons                 58.0000181661 
   Number of Beta  Electrons                 58.0000181661 
   Total number of  Electrons               116.0000363323 
   Exchange energy                         -117.4602684006 
   Correlation energy                        -3.9879037208 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -121.4481721215 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1152.6546559092 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 9
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1163
     Surface Area:         883.6687174198
     Dielectric Energy:     -0.0086108448
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 9
   prop. index: 1
        Van der Waals Correction:       -0.0077608812
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 10
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 10
   prop. index: 1
        SCF Energy:    -1152.6546564327
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 10
   prop. index: 1
   Number of Alpha Electrons                 58.0000182784 
   Number of Beta  Electrons                 58.0000182784 
   Total number of  Electrons               116.0000365569 
   Exchange energy                         -117.4601451826 
   Correlation energy                        -3.9878868001 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -121.4480319827 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1152.6546564327 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 10
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1162
     Surface Area:         883.7455646565
     Dielectric Energy:     -0.0086145614
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 10
   prop. index: 1
        Van der Waals Correction:       -0.0077602540
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 10
   prop. index: 1
       Filename                          : mol.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        3.2168835050
        Electronic Contribution:
                  0    
      0       4.042298
      1       6.158974
      2      -0.479276
        Nuclear Contribution:
                  0    
      0      -3.609787
      1      -6.896763
      2       1.412190
        Total Dipole moment:
                  0    
      0       0.432511
      1      -0.737789
      2       0.932914
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 11
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 11
   prop. index: 1
        SCF Energy:    -1152.6546564285
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 11
   prop. index: 1
   Number of Alpha Electrons                 58.0000182785 
   Number of Beta  Electrons                 58.0000182785 
   Total number of  Electrons               116.0000365569 
   Exchange energy                         -117.4601458394 
   Correlation energy                        -3.9878869219 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -121.4480327613 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1152.6546564285 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 11
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1162
     Surface Area:         883.7455646565
     Dielectric Energy:     -0.0086145180
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 11
   prop. index: 1
        Van der Waals Correction:       -0.0077602540
# -----------------------------------------------------------
$ THERMOCHEMISTRY_Energies
   description: The Thermochemistry energies
   geom. index: 11
   prop. index: 1
        Temperature (Kelvin)           :        298.1500000000
        Pressure (atm)                 :          1.0000000000
        Total Mass (AMU)               :        227.9840924101
        Spin Degeneracy                :          1.0000000000
        Electronic Energy (Hartree)    :      -1152.6442974976
        Translational Energy (Hartree) :          0.0014162714
        Rotational Energy (Hartree)    :          0.0014162714
        Vibrational Energy (Hartree)   :          0.0118555906
        Number of frequencies          :     57      
        Scaling Factor for frequencies :          1.0000000000
        Vibrational frequencies        :     
                  0    
      0       0.000000
      1       0.000000
      2       0.000000
      3       0.000000
      4       0.000000
      5       0.000000
      6      14.374574
      7      30.038853
      8      38.465995
      9      46.861242
     10      64.370122
     11      80.468163
     12      99.929397
     13     143.823071
     14     165.742147
     15     186.285661
     16     239.001259
     17     269.193660
     18     362.353914
     19     414.960550
     20     423.496078
     21     437.553394
     22     441.257485
     23     490.918218
     24     500.458341
     25     504.498144
     26     552.523719
     27     673.785894
     28     680.046337
     29     748.291261
     30     779.904496
     31     821.675427
     32     899.891986
     33     915.604496
     34     949.469725
     35     1002.107425
     36     1019.375098
     37     1097.230811
     38     1103.661816
     39     1113.310186
     40     1144.693960
     41     1192.904497
     42     1256.994002
     43     1302.122523
     44     1352.138008
     45     1355.234325
     46     1413.676220
     47     1461.219579
     48     1510.093107
     49     1565.328218
     50     1599.561400
     51     1643.338362
     52     2428.356891
     53     3193.103965
     54     3206.421125
     55     3223.116844
     56     3226.409939
        Zero Point Energy (Hartree)    :          0.1125087708
        Inner Energy (Hartree)         :      -1152.5171005934
        Enthalpy (Hartree)             :      -1152.5161563843
        Electronic entropy             :          0.0000000000
        Rotational entropy             :          0.0157117157
        Vibrational entropy            :          0.0221251143
        Translational entropy          :          0.0157117157
        Entropy                        :          0.0578752295
        Gibbs Energy (Hartree)         :      -1152.5740316138
        Is Linear                      :                 false
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     1 
    Coordinates:
               0 C      0.018297765383   -0.007351522910   -0.127705572693
               1 C      0.722378760024    1.170228028782   -0.312728952041
               2 C      0.584854089111   -1.139066392545    0.444187788459
               3 C      2.060913625829    1.211266882780    0.097489475006
               4 C      2.657538876725    0.077866808432    0.680300452934
               5 C      1.915117990074   -1.082202522421    0.846486040448
               6 H      0.241052214848    2.029355899859   -0.763296779272
               7 C      2.826361904193    2.390385244066   -0.066756145545
               8 N     -1.398854311078   -0.056184524272   -0.559356855078
               9 H     -0.011969039440   -2.034875787516    0.565584584497
              10 H      3.694483637001    0.131642643592    0.994568218157
              11 H      2.378656057258   -1.954222041749    1.296230899142
              12 N      3.538295362539    3.302932117060   -0.162936566983
              13 O      4.303795569445    4.235641397961   -0.241362547470
              14 O      5.488076154080    1.948075449360    1.491695679529
              15 S      6.445870226361    2.940261506011    0.957634925147
              16 O      7.197106265772    2.552788474905   -0.245637991543
              17 O     -1.870132802005    0.955474523942   -1.059320616975
              18 O     -2.001832346120   -1.105656185336   -0.386256035720
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     2 
    Coordinates:
               0 C      0.018073382508   -0.007266254029   -0.127512743148
               1 C      0.724052966213    1.169678103256   -0.312272366769
               2 C      0.582036773927   -1.140917313901    0.444589868726
               3 C      2.061949570204    1.207061181654    0.098679654341
               4 C      2.656366825520    0.072045947129    0.681052715542
               5 C      1.912053788817   -1.086924087047    0.847153175508
               6 H      0.246664939483    2.031129495554   -0.762771818874
               7 C      2.828307507423    2.385873196156   -0.065997814777
               8 N     -1.397751578830   -0.053100113005   -0.559391489634
               9 H     -0.014795288553   -2.036823595404    0.566553042227
              10 H      3.693583791403    0.121896686631    0.994443479603
              11 H      2.373786627827   -1.960082755134    1.296500720641
              12 N      3.537681482356    3.299232084467   -0.163571818324
              13 O      4.301539260506    4.235747734516   -0.245197813561
              14 O      5.496124316768    1.956598526632    1.493599825035
              15 S      6.447989552048    2.952929198771    0.957076525229
              16 O      7.198970124211    2.561267111379   -0.246202161791
              17 O     -1.870801682343    0.958850377824   -1.058534580124
              18 O     -2.005822359486   -1.100835525450   -0.389376399851
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     3 
    Coordinates:
               0 C      0.016866204624   -0.007023949877   -0.127012174239
               1 C      0.726247313571    1.168215735902   -0.311585984699
               2 C      0.576616440144   -1.143169847680    0.444950206744
               3 C      2.063963742769    1.199776231883    0.099848607130
               4 C      2.654688928099    0.062819784301    0.681665897285
               5 C      1.906707909718   -1.093939637637    0.847711043147
               6 H      0.254591304812    2.033010495594   -0.761559972266
               7 C      2.832596600792    2.377362934619   -0.064969675417
               8 N     -1.396581741398   -0.047950893949   -0.558379916479
               9 H     -0.020884837438   -2.038480857917    0.567980770751
              10 H      3.692196245653    0.107068917408    0.995091479319
              11 H      2.365219660653   -1.968756928764    1.296995049020
              12 N      3.538490134784    3.292587791739   -0.164707142888
              13 O      4.298732878056    4.233923080024   -0.249672661561
              14 O      5.507604817281    1.969068180046    1.495469360868
              15 S      6.449574169834    2.972724853166    0.955649183350
              16 O      7.201763742543    2.578511826771   -0.247462518272
              17 O     -1.868893772899    0.965284605686   -1.057555167533
              18 O     -2.009489741596   -1.094672321316   -0.393636384260
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     4 
    Coordinates:
               0 C      0.016634107737   -0.008347369827   -0.130923017034
               1 C      0.729477722737    1.165082905510   -0.314907253349
               2 C      0.572300524869   -1.145910961412    0.442420350409
               3 C      2.066809403366    1.192108230063    0.098475103430
               4 C      2.653594825950    0.054114472355    0.681397300895
               5 C      1.902052592995   -1.100505831970    0.847019691348
               6 H      0.263068924893    2.032268975983   -0.765505736038
               7 C      2.838011853911    2.368170106121   -0.065928073658
               8 N     -1.394600154729   -0.045567148656   -0.564049878223
               9 H     -0.026114495836   -2.040464855766    0.565979501209
              10 H      3.690801197534    0.094062078418    0.996559453038
              11 H      2.357185978640   -1.976491204442    1.297385226445
              12 N      3.540543951830    3.285590563230   -0.167444519689
              13 O      4.297273479462    4.230400712589   -0.254681145233
              14 O      5.515927010298    1.982039073137    1.499757981151
              15 S      6.448934747389    2.992345805171    0.957038436883
              16 O      7.205131470044    2.597206634372   -0.244144123132
              17 O     -1.870453349466    0.975330526999   -1.045098900757
              18 O     -2.016569791624   -1.085072711875   -0.384530397695
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     5 
    Coordinates:
               0 C      0.015046532194   -0.007143456499   -0.127441248631
               1 C      0.728975236123    1.165551761803   -0.311732474714
               2 C      0.570691313091   -1.145357198198    0.444447057406
               3 C      2.067018227876    1.191270013942    0.100029247053
               4 C      2.653511301684    0.052667484722    0.681723655014
               5 C      1.901027046968   -1.101314176214    0.847546409546
               6 H      0.262821617352    2.033424623552   -0.761190779382
               7 C      2.839205758090    2.366582965193   -0.064492341439
               8 N     -1.397101377365   -0.042817161888   -0.558464496335
               9 H     -0.028295192478   -2.039444013980    0.568279710830
              10 H      3.691046394428    0.091593440194    0.995952579939
              11 H      2.355876807938   -1.977960305572    1.296931813879
              12 N      3.541221193780    3.284478249101   -0.166843420842
              13 O      4.297576081073    4.228965307646   -0.254698278125
              14 O      5.518570683831    1.983335873521    1.498866197306
              15 S      6.449427108168    2.994863657085    0.954627510322
              16 O      7.205352818216    2.601309044385   -0.247113699959
              17 O     -1.867546999828    0.973861369120   -1.053335393500
              18 O     -2.014414551142   -1.087507477915   -0.394272048367
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     6 
    Coordinates:
               0 C      0.014258517232   -0.007076075522   -0.127074014471
               1 C      0.729763837075    1.164599057961   -0.311369803294
               2 C      0.568925316225   -1.145880690687    0.444499383105
               3 C      2.068074246177    1.188726413423    0.099950799674
               4 C      2.653383463586    0.049504387451    0.681449541110
               5 C      1.899417248937   -1.103532679863    0.847301582995
               6 H      0.264637141015    2.033181104840   -0.760542656252
               7 C      2.841812446776    2.362916894602   -0.064510220077
               8 N     -1.398398653889   -0.040975792561   -0.557661471089
               9 H     -0.031030689563   -2.039310837987    0.568417114368
              10 H      3.691007840535    0.087147888977    0.995638332591
              11 H      2.353220256595   -1.980830882874    1.296482910357
              12 N      3.542369061178    3.281857722097   -0.168022643442
              13 O      4.298175576694    4.226346664042   -0.256569969899
              14 O      5.521673282536    1.987884466533    1.500540427412
              15 S      6.449002970615    3.001694743702    0.954375980483
              16 O      7.206753920389    2.609418028755   -0.246505073329
              17 O     -1.867121431008    0.976305559887   -1.052594264191
              18 O     -2.015914351106   -1.085615972775   -0.394985956052
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     7 
    Coordinates:
               0 C      0.011327849459   -0.007038094075   -0.126190233503
               1 C      0.733635258262    1.160234322252   -0.310515240338
               2 C      0.560899996015   -1.148777439445    0.444277747329
               3 C      2.072785555506    1.176818518478    0.099179558290
               4 C      2.652463686323    0.034573031895    0.679940973546
               5 C      1.891911781864   -1.114189344096    0.846016927382
               6 H      0.273433231603    2.032008786462   -0.758670822014
               7 C      2.853596182065    2.345928067052   -0.065032706317
               8 N     -1.402913818343   -0.032881875307   -0.555321387885
               9 H     -0.043531075258   -2.039180269561    0.568538169326
              10 H      3.690468912346    0.066280915579    0.993892198380
              11 H      2.340819256252   -1.994337870944    1.294518158680
              12 N      3.547011579127    3.269657277060   -0.174094136174
              13 O      4.300720161856    4.214852995325   -0.265758354838
              14 O      5.536542853569    2.009396186888    1.508791543515
              15 S      6.447140746210    3.034059587636    0.954200371799
              16 O      7.213258369961    2.646291389916   -0.242528364333
              17 O     -1.865880629904    0.988236900741   -1.046798289663
              18 O     -2.023679896910   -1.075573085857   -0.395626113182
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     8 
    Coordinates:
               0 C      0.010575103021   -0.007141828599   -0.125944317456
               1 C      0.734773690587    1.158835354134   -0.310558748976
               2 C      0.558260018271   -1.149971709751    0.444144885320
               3 C      2.074100942629    1.172935535341    0.098392756214
               4 C      2.651985427149    0.029517061015    0.678833646712
               5 C      1.889484971463   -1.117892862604    0.845268163062
               6 H      0.275688910207    2.031366548365   -0.758422515845
               7 C      2.857442136486    2.340236025532   -0.065879511818
               8 N     -1.404150439292   -0.030373673886   -0.554447408735
               9 H     -0.048005967777   -2.039127798128    0.568487282129
              10 H      3.690226374312    0.059585792398    0.992232585124
              11 H      2.336957424632   -1.998898304473    1.293536583087
              12 N      3.548143851977    3.265665308398   -0.177127281749
              13 O      4.301720149665    4.211128572491   -0.269484454017
              14 O      5.542146920217    2.016514261965    1.512321621164
              15 S      6.446951219836    3.045142384946    0.955386540694
              16 O      7.216334994838    2.658576563194   -0.239601566664
              17 O     -1.865903188405    0.992100192325   -1.044091739703
              18 O     -2.026722539814   -1.071837422664   -0.394226518545
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     9 
    Coordinates:
               0 C      0.010116254467   -0.007263909388   -0.125564065528
               1 C      0.735343415840    1.157954286592   -0.310808607734
               2 C      0.556371455556   -1.150972031681    0.444158521496
               3 C      2.074813215264    1.170272635443    0.097197676158
               4 C      2.651547344693    0.025863104087    0.677253048651
               5 C      1.887898988132   -1.120613078333    0.844355420751
               6 H      0.276372727684    2.030674540169   -0.758451894886
               7 C      2.860140675811    2.336154727757   -0.067323146379
               8 N     -1.404995841936   -0.028676797088   -0.553228414905
               9 H     -0.051614656236   -2.038944924153    0.568597337840
              10 H      3.690119533336    0.055135924297    0.989652116433
              11 H      2.334638088995   -2.002121157147    1.292369295072
              12 N      3.548646375942    3.262894766989   -0.180472523806
              13 O      4.302474085219    4.208564615732   -0.272849193279
              14 O      5.546653858141    2.021117474572    1.515414812305
              15 S      6.447230801969    3.053125629850    0.957773906948
              16 O      7.219725991419    2.667637841699   -0.235590856722
              17 O     -1.866176521498    0.994488020417   -1.042005860213
              18 O     -2.029295792799   -1.068931669812   -0.391657572202
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     10 
    Coordinates:
               0 C      0.010074791507   -0.007274919349   -0.125293780611
               1 C      0.735279094876    1.157930471692   -0.310995307748
               2 C      0.556140241787   -1.151156175094    0.444281137229
               3 C      2.074836356189    1.169968160535    0.096490001802
               4 C      2.651514944648    0.025337868044    0.676362426436
               5 C      1.887859704020   -1.121002722448    0.843906792744
               6 H      0.275800450438    2.030411189860   -0.758578852679
               7 C      2.860566941558    2.335549858288   -0.068255009553
               8 N     -1.405164982035   -0.028452734566   -0.552443854509
               9 H     -0.052391285887   -2.038737513591    0.568812400637
              10 H      3.690262690055    0.054767620413    0.988171805817
              11 H      2.334737301572   -2.002503251127    1.291796890105
              12 N      3.548634175325    3.262537547247   -0.181918511843
              13 O      4.302603933036    4.208242070096   -0.273883168132
              14 O      5.547202203345    2.021079257041    1.515985874200
              15 S      6.447272468873    3.054090748783    0.959443619238
              16 O      7.221069828411    2.669381875503   -0.233363226957
              17 O     -1.866317926371    0.994499704250   -1.041785504907
              18 O     -2.029970931349   -1.068309055576   -0.389913731268
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     11 
    Coordinates:
               0 C      0.010074791507   -0.007274919349   -0.125293780611
               1 C      0.735279094876    1.157930471692   -0.310995307748
               2 C      0.556140241787   -1.151156175094    0.444281137229
               3 C      2.074836356189    1.169968160535    0.096490001802
               4 C      2.651514944648    0.025337868044    0.676362426436
               5 C      1.887859704020   -1.121002722448    0.843906792744
               6 H      0.275800450438    2.030411189860   -0.758578852679
               7 C      2.860566941558    2.335549858288   -0.068255009553
               8 N     -1.405164982035   -0.028452734566   -0.552443854509
               9 H     -0.052391285887   -2.038737513591    0.568812400637
              10 H      3.690262690055    0.054767620413    0.988171805817
              11 H      2.334737301572   -2.002503251127    1.291796890105
              12 N      3.548634175325    3.262537547247   -0.181918511843
              13 O      4.302603933036    4.208242070096   -0.273883168132
              14 O      5.547202203345    2.021079257041    1.515985874200
              15 S      6.447272468873    3.054090748783    0.959443619238
              16 O      7.221069828411    2.669381875503   -0.233363226957
              17 O     -1.866317926371    0.994499704250   -1.041785504907
              18 O     -2.029970931349   -1.068309055576   -0.389913731268
