-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:    -1148.1268968831
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 19
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 22
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     5.9469     6.0000     0.0531     3.7697     3.7697     0.0000
  1   0     6.1213     6.0000    -0.1213     3.9004     3.9004     0.0000
  2   0     6.1199     6.0000    -0.1199     3.8749     3.8749     0.0000
  3   0     5.9817     6.0000     0.0183     3.7461     3.7461     0.0000
  4   0     6.0933     6.0000    -0.0933     3.8529     3.8529    -0.0000
  5   0     6.1322     6.0000    -0.1322     3.7453     3.7453    -0.0000
  6   0     0.7833     1.0000     0.2167     0.9768     0.9768     0.0000
  7   0     5.6674     6.0000     0.3326     4.3082     4.3082    -0.0000
  8   0     6.5130     7.0000     0.4870     4.2365     4.2365     0.0000
  9   0     0.7890     1.0000     0.2110     0.9768     0.9768     0.0000
 10   0     0.8150     1.0000     0.1850     0.9763     0.9763    -0.0000
 11   0     0.8249     1.0000     0.1751     0.9726     0.9726    -0.0000
 12   0     6.9900     7.0000     0.0100     2.1955     2.1955    -0.0000
 13   0     8.5317     8.0000    -0.5317     1.9500     1.9500     0.0000
 14   0     8.4726     8.0000    -0.4726     2.0325     2.0325     0.0000
 15   0    15.0498    16.0000     0.9502     3.8657     3.8657     0.0000
 16   0     8.4686     8.0000    -0.4686     2.0421     2.0421     0.0000
 17   0     8.3475     8.0000    -0.3475     1.9060     1.9060     0.0000
 18   0     8.3520     8.0000    -0.3520     1.8995     1.8995     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.386298
                0             6               2            6                1.372894
                0             6               8            7                0.944725
                1             6               3            6                1.313997
                1             6               6            1                0.938745
                2             6               5            6                1.351048
                2             6               9            1                0.941890
                3             6               4            6                1.320502
                3             6               7            6                0.872903
                3             6              12            7                0.231319
                4             6               5            6                1.381891
                4             6              10            1                0.959688
                5             6              11            1                0.968830
                7             6              12            7                1.651951
                7             6              14            8                1.719975
                8             7              17            8                1.653383
                8             7              18            8                1.646812
               12             7              13            8                0.156577
               13             8              15           16                1.676317
               14             8              15           16                0.222533
               15            16              16            8                1.934553
               17             8              18            8                0.164448
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 1
   prop. index: 1
        Total number of Electrons                                                    116
        Number of correlated Electrons                                                78
        Number of alpha correlated Electrons                                          39
        Number of beta correlated Electrons                                           39
        Reference Energy:                                               -1148.1268968877
        Total Correlation Energy:                                          -2.9917327822
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1515353813
        Total MDCI Energy:                                              -1151.1186296699
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:    -1148.1907710374
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 2
   prop. index: 1
     Number of atoms                     : 19
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 22
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.0074     6.0000    -0.0074     3.7603     3.7603     0.0000
  1   0     6.0681     6.0000    -0.0681     3.8788     3.8788     0.0000
  2   0     6.0861     6.0000    -0.0861     3.8378     3.8378    -0.0000
  3   0     6.0078     6.0000    -0.0078     3.6691     3.6691     0.0000
  4   0     6.0665     6.0000    -0.0665     3.8507     3.8507    -0.0000
  5   0     6.0899     6.0000    -0.0899     3.7301     3.7301    -0.0000
  6   0     0.7760     1.0000     0.2240     0.9778     0.9778     0.0000
  7   0     5.5895     6.0000     0.4105     4.3310     4.3310     0.0000
  8   0     6.5497     7.0000     0.4503     4.2883     4.2883    -0.0000
  9   0     0.7733     1.0000     0.2267     0.9770     0.9770     0.0000
 10   0     0.8302     1.0000     0.1698     0.9900     0.9900    -0.0000
 11   0     0.8329     1.0000     0.1671     0.9806     0.9806    -0.0000
 12   0     7.0510     7.0000    -0.0510     2.2572     2.2572     0.0000
 13   0     8.5662     8.0000    -0.5662     1.9070     1.9070     0.0000
 14   0     8.4956     8.0000    -0.4956     2.0081     2.0081    -0.0000
 15   0    14.9805    16.0000     1.0195     3.8584     3.8584    -0.0000
 16   0     8.5246     8.0000    -0.5246     1.9825     1.9825     0.0000
 17   0     8.3502     8.0000    -0.3502     1.9128     1.9128    -0.0000
 18   0     8.3543     8.0000    -0.3543     1.9078     1.9078     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.373295
                0             6               2            6                1.359554
                0             6               8            7                0.983023
                1             6               3            6                1.291717
                1             6               6            1                0.949416
                2             6               5            6                1.366163
                2             6               9            1                0.941452
                3             6               4            6                1.286287
                3             6               7            6                0.904172
                3             6              12            7                0.224019
                4             6               5            6                1.380448
                4             6              10            1                0.982438
                5             6              11            1                0.981915
                7             6              12            7                1.713368
                7             6              14            8                1.678695
                8             7              17            8                1.676595
                8             7              18            8                1.672290
               12             7              13            8                0.147588
               13             8              15           16                1.671048
               14             8              15           16                0.243937
               15            16              16            8                1.905063
               17             8              18            8                0.159598
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 2
   prop. index: 1
        Total number of Electrons                                                    116
        Number of correlated Electrons                                                78
        Number of alpha correlated Electrons                                          39
        Number of beta correlated Electrons                                           39
        Reference Energy:                                               -1148.1907710340
        Total Correlation Energy:                                          -3.1800211733
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1654919256
        Total MDCI Energy:                                              -1151.3707922074
# -----------------------------------------------------------
$ Energy_Extrapolation
   description: The Extrapolation of the energies
   geom. index: 2
   prop. index: 1
       Do EP1                            : 1 
       Do EP2                            : 0 
       Do EP3                            : 0 
       Number of energies                : 2 
       SCF energy with basis  cc-pVTZ :  -1148.1268968877 
       SCF energy with basis  cc-pVQZ :  -1148.1907710340 
       CBS SCF Energy                    :  -1148.2100165649 
       Correlation energy with basis  cc-pVTZ :  -1148.1268968877 
       Correlation energy with basis  cc-pVQZ :  -1148.1907710340 
       CBS Correlation Energy            :     -3.3140616988 
       CBS Total Energy                  :  -1151.5240782637 
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 2
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       19
     number of electrons:                   116
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             510
     number of aux C basis functions:       2226
     number of aux J basis functions:       0
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           0.000000
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 2
   prop. index: 1
       Filename                          : /scratch/obeletsan/orcajob__14537-ZzMV/ts2.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        3.1767715814
        Electronic Contribution:
                  0    
      0       3.335115
      1       6.763660
      2      -1.256917
        Nuclear Contribution:
                  0    
      0      -3.030840
      1      -7.846104
      2       1.802594
        Total Dipole moment:
                  0    
      0       0.304275
      1      -1.082444
      2       0.545677
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     1 
    Coordinates:
               0 C      0.049451000000    0.069704000000    0.090536000000
               1 C      0.963686000000    1.065809000000   -0.210588000000
               2 C      0.440377000000   -1.201997000000    0.495302000000
               3 C      2.312785000000    0.755980000000   -0.064682000000
               4 C      2.744927000000   -0.519622000000    0.305291000000
               5 C      1.798083000000   -1.491253000000    0.599406000000
               6 H      0.635476000000    2.043834000000   -0.542130000000
               7 C      3.363293000000    1.902786000000   -0.133393000000
               8 N     -1.393925000000    0.368444000000   -0.042724000000
               9 H     -0.316479000000   -1.947167000000    0.711236000000
              10 H      3.807889000000   -0.729986000000    0.366434000000
              11 H      2.116068000000   -2.483566000000    0.901676000000
              12 N      3.496550000000    1.723872000000   -1.373573000000
              13 O      4.807766000000    3.275309000000   -1.582976000000
              14 O      3.783578000000    2.574839000000    0.829107000000
              15 S      5.518659000000    3.552885000000   -0.275816000000
              16 O      6.583737000000    2.607009000000    0.081304000000
              17 O     -1.701550000000    1.495992000000   -0.401055000000
              18 O     -2.177601000000   -0.534523000000    0.215814000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     2 
    Coordinates:
               0 C      0.049451000000    0.069704000000    0.090536000000
               1 C      0.963686000000    1.065809000000   -0.210588000000
               2 C      0.440377000000   -1.201997000000    0.495302000000
               3 C      2.312785000000    0.755980000000   -0.064682000000
               4 C      2.744927000000   -0.519622000000    0.305291000000
               5 C      1.798083000000   -1.491253000000    0.599406000000
               6 H      0.635476000000    2.043834000000   -0.542130000000
               7 C      3.363293000000    1.902786000000   -0.133393000000
               8 N     -1.393925000000    0.368444000000   -0.042724000000
               9 H     -0.316479000000   -1.947167000000    0.711236000000
              10 H      3.807889000000   -0.729986000000    0.366434000000
              11 H      2.116068000000   -2.483566000000    0.901676000000
              12 N      3.496550000000    1.723872000000   -1.373573000000
              13 O      4.807766000000    3.275309000000   -1.582976000000
              14 O      3.783578000000    2.574839000000    0.829107000000
              15 S      5.518659000000    3.552885000000   -0.275816000000
              16 O      6.583737000000    2.607009000000    0.081304000000
              17 O     -1.701550000000    1.495992000000   -0.401055000000
              18 O     -2.177601000000   -0.534523000000    0.215814000000
