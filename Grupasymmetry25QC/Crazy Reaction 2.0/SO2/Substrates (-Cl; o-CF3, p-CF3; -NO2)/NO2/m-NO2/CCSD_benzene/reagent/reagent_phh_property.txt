-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -600.8728856397
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 1
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                940
     Surface Area:         693.2034845252
     Dielectric Energy:     -0.0121989400
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 16
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 18
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     5.9260     6.0000     0.0740     3.7466     3.7466     0.0000
  1   0     6.1427     6.0000    -0.1427     3.8793     3.8793     0.0000
  2   0     6.1340     6.0000    -0.1340     3.8669     3.8669     0.0000
  3   0     5.9412     6.0000     0.0588     3.7488     3.7488     0.0000
  4   0     6.1144     6.0000    -0.1144     3.8189     3.8189     0.0000
  5   0     6.1389     6.0000    -0.1389     3.7543     3.7543    -0.0000
  6   0     0.7852     1.0000     0.2148     0.9767     0.9767     0.0000
  7   0     6.0215     6.0000    -0.0215     3.9096     3.9096    -0.0000
  8   0     6.5019     7.0000     0.4981     4.2329     4.2329     0.0000
  9   0     0.7871     1.0000     0.2129     0.9752     0.9752     0.0000
 10   0     0.8101     1.0000     0.1899     0.9711     0.9711     0.0000
 11   0     0.8124     1.0000     0.1876     0.9664     0.9664    -0.0000
 12   0     6.6818     7.0000     0.3182     4.0541     4.0541    -0.0000
 13   0     8.4610     8.0000    -0.4610     1.7472     1.7472     0.0000
 14   0     8.3718     8.0000    -0.3718     1.8846     1.8846    -0.0000
 15   0     8.3700     8.0000    -0.3700     1.8878     1.8878    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.383196
                0             6               2            6                1.360547
                0             6               8            7                0.945209
                1             6               3            6                1.313832
                1             6               6            1                0.946375
                2             6               5            6                1.359824
                2             6               9            1                0.943258
                3             6               4            6                1.305821
                3             6               7            6                1.089548
                4             6               5            6                1.382582
                4             6              10            1                0.966613
                5             6              11            1                0.964894
                7             6              12            7                2.555125
                7             6              13            8                0.266388
                8             7              14            8                1.643607
                8             7              15            8                1.646535
               12             7              13            8                1.436467
               14             8              15            8                0.157092
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 1
   prop. index: 1
        Total number of Electrons                                                     84
        Number of correlated Electrons                                                60
        Number of alpha correlated Electrons                                          30
        Number of beta correlated Electrons                                           30
        Reference Energy:                                                -600.8728857950
        Total Correlation Energy:                                          -2.2984007752
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1149905950
        Total MDCI Energy:                                               -603.1712865703
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:     -600.9147136080
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 2
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                940
     Surface Area:         693.2034845252
     Dielectric Energy:     -0.0123561488
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 2
   prop. index: 1
     Number of atoms                     : 16
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 18
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     5.9976     6.0000     0.0024     3.7447     3.7447    -0.0000
  1   0     6.1175     6.0000    -0.1175     3.8501     3.8501    -0.0000
  2   0     6.1066     6.0000    -0.1066     3.8442     3.8442    -0.0000
  3   0     5.9846     6.0000     0.0154     3.7893     3.7893    -0.0000
  4   0     6.1294     6.0000    -0.1294     3.8238     3.8238    -0.0000
  5   0     6.1103     6.0000    -0.1103     3.7425     3.7425    -0.0000
  6   0     0.7668     1.0000     0.2332     0.9724     0.9724    -0.0000
  7   0     5.8923     6.0000     0.1077     3.8126     3.8126    -0.0000
  8   0     6.5277     7.0000     0.4723     4.2685     4.2685     0.0000
  9   0     0.7693     1.0000     0.2307     0.9730     0.9730    -0.0000
 10   0     0.8040     1.0000     0.1960     0.9747     0.9747    -0.0000
 11   0     0.8070     1.0000     0.1930     0.9679     0.9679     0.0000
 12   0     6.7496     7.0000     0.2504     3.9787     3.9787     0.0000
 13   0     8.4832     8.0000    -0.4832     1.7445     1.7445     0.0000
 14   0     8.3779     8.0000    -0.3779     1.8848     1.8848    -0.0000
 15   0     8.3761     8.0000    -0.3761     1.8855     1.8855     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.376541
                0             6               2            6                1.349439
                0             6               8            7                0.986666
                1             6               3            6                1.309062
                1             6               6            1                0.944704
                2             6               5            6                1.376681
                2             6               9            1                0.941731
                3             6               4            6                1.289353
                3             6               7            6                1.165225
                4             6               5            6                1.396653
                4             6              10            1                0.972799
                5             6              11            1                0.969473
                7             6              12            7                2.457793
                7             6              13            8                0.224126
                8             7              14            8                1.661181
                8             7              15            8                1.661212
               12             7              13            8                1.457275
               14             8              15            8                0.152299
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 2
   prop. index: 1
        Total number of Electrons                                                     84
        Number of correlated Electrons                                                60
        Number of alpha correlated Electrons                                          30
        Number of beta correlated Electrons                                           30
        Reference Energy:                                                -600.9147137618
        Total Correlation Energy:                                          -2.4352436162
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1248311187
        Total MDCI Energy:                                               -603.3499573779
# -----------------------------------------------------------
$ Energy_Extrapolation
   description: The Extrapolation of the energies
   geom. index: 2
   prop. index: 1
       Do EP1                            : 1 
       Do EP2                            : 0 
       Do EP3                            : 0 
       Number of energies                : 2 
       SCF energy with basis  cc-pVTZ :   -600.8728857950 
       SCF energy with basis  cc-pVQZ :   -600.9147137618 
       CBS SCF Energy                    :   -600.9273166923 
       Correlation energy with basis  cc-pVTZ :   -600.8728857950 
       Correlation energy with basis  cc-pVQZ :   -600.9147137618 
       CBS Correlation Energy            :     -2.5326605956 
       CBS Total Energy                  :   -603.4599772879 
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 2
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       16
     number of electrons:                   84
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             416
     number of aux C basis functions:       1804
     number of aux J basis functions:       0
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           0.000000
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 2
   prop. index: 1
       Filename                          : /scratch/obeletsan/orcajob__14542-41Lm/reagent_phh.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        6.1260893322
        Electronic Contribution:
                  0    
      0      -1.040783
      1       3.304783
      2      -0.119731
        Nuclear Contribution:
                  0    
      0       1.592395
      1      -5.649724
      2       0.195581
        Total Dipole moment:
                  0    
      0       0.551612
      1      -2.344941
      2       0.075850
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    16 
    Geometry Index:     1 
    Coordinates:
               0 C     -0.021039000000   -0.126389000000    0.357157000000
               1 C      0.815943000000    0.975315000000    0.418169000000
               2 C      0.447594000000   -1.433726000000    0.409625000000
               3 C      2.195299000000    0.754836000000    0.537688000000
               4 C      2.691003000000   -0.561088000000    0.592856000000
               5 C      1.818005000000   -1.637853000000    0.528481000000
               6 H      0.411080000000    1.978689000000    0.374545000000
               7 C      3.084245000000    1.853986000000    0.603479000000
               8 N     -1.476024000000    0.103845000000    0.232163000000
               9 H     -0.247854000000   -2.262627000000    0.358320000000
              10 H      3.759781000000   -0.722824000000    0.685921000000
              11 H      2.208689000000   -2.649324000000    0.571374000000
              12 N      3.831686000000    2.744158000000    0.651437000000
              13 O      4.603058000000    3.668180000000    0.703044000000
              14 O     -2.202599000000   -0.880048000000    0.177413000000
              15 O     -1.867660000000    1.263071000000    0.190998000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    16 
    Geometry Index:     2 
    Coordinates:
               0 C     -0.021039000000   -0.126389000000    0.357157000000
               1 C      0.815943000000    0.975315000000    0.418169000000
               2 C      0.447594000000   -1.433726000000    0.409625000000
               3 C      2.195299000000    0.754836000000    0.537688000000
               4 C      2.691003000000   -0.561088000000    0.592856000000
               5 C      1.818005000000   -1.637853000000    0.528481000000
               6 H      0.411080000000    1.978689000000    0.374545000000
               7 C      3.084245000000    1.853986000000    0.603479000000
               8 N     -1.476024000000    0.103845000000    0.232163000000
               9 H     -0.247854000000   -2.262627000000    0.358320000000
              10 H      3.759781000000   -0.722824000000    0.685921000000
              11 H      2.208689000000   -2.649324000000    0.571374000000
              12 N      3.831686000000    2.744158000000    0.651437000000
              13 O      4.603058000000    3.668180000000    0.703044000000
              14 O     -2.202599000000   -0.880048000000    0.177413000000
              15 O     -1.867660000000    1.263071000000    0.190998000000
