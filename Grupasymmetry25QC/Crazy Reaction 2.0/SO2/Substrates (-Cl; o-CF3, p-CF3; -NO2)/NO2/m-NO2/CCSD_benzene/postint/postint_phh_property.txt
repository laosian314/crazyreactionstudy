-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:    -1148.2734971850
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 1
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1165
     Surface Area:         886.3299554779
     Dielectric Energy:     -0.0115428158
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 19
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 20
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     5.9323     6.0000     0.0677     3.7423     3.7423    -0.0000
  1   0     6.1771     6.0000    -0.1771     3.8803     3.8803    -0.0000
  2   0     6.1658     6.0000    -0.1658     3.8682     3.8682    -0.0000
  3   0     5.7912     6.0000     0.2088     3.8278     3.8278     0.0000
  4   0     6.1780     6.0000    -0.1780     3.7979     3.7979     0.0000
  5   0     6.1422     6.0000    -0.1422     3.7650     3.7650     0.0000
  6   0     0.7922     1.0000     0.2078     0.9862     0.9862     0.0000
  7   0     5.5932     6.0000     0.4068     4.2674     4.2674    -0.0000
  8   0     6.5001     7.0000     0.4999     4.2356     4.2356    -0.0000
  9   0     0.7937     1.0000     0.2063     0.9779     0.9779    -0.0000
 10   0     0.8221     1.0000     0.1779     0.9933     0.9933     0.0000
 11   0     0.8172     1.0000     0.1828     0.9689     0.9689    -0.0000
 12   0     7.2046     7.0000    -0.2046     3.0925     3.0925     0.0000
 13   0     8.3535     8.0000    -0.3535     2.2396     2.2396    -0.0000
 14   0     8.4684     8.0000    -0.4684     2.0480     2.0480     0.0000
 15   0    15.0387    16.0000     0.9613     3.8408     3.8408    -0.0000
 16   0     8.4775     8.0000    -0.4775     2.0368     2.0368     0.0000
 17   0     8.3753     8.0000    -0.3753     1.8838     1.8838    -0.0000
 18   0     8.3770     8.0000    -0.3770     1.8813     1.8813     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.365622
                0             6               2            6                1.376330
                0             6               8            7                0.949506
                1             6               3            6                1.357758
                1             6               6            1                0.941798
                2             6               5            6                1.358351
                2             6               9            1                0.945556
                3             6               4            6                1.326747
                3             6              12            7                1.036248
                4             6               5            6                1.379670
                4             6              10            1                0.958523
                5             6              11            1                0.966195
                7             6              12            7                2.015391
                7             6              13            8                2.138807
                8             7              17            8                1.646833
                8             7              18            8                1.644581
               14             8              15           16                1.916449
               14             8              16            8                0.112518
               15            16              16            8                1.909745
               17             8              18            8                0.154895
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 1
   prop. index: 1
        Total number of Electrons                                                    116
        Number of correlated Electrons                                                78
        Number of alpha correlated Electrons                                          39
        Number of beta correlated Electrons                                           39
        Reference Energy:                                               -1148.2734972076
        Total Correlation Energy:                                          -2.9719845981
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1451244353
        Total MDCI Energy:                                              -1151.2454818057
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:    -1148.3374137354
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 2
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1165
     Surface Area:         886.3299554779
     Dielectric Energy:     -0.0114446839
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 2
   prop. index: 1
     Number of atoms                     : 19
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 19
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     5.9963     6.0000     0.0037     3.7426     3.7426    -0.0000
  1   0     6.1286     6.0000    -0.1286     3.8745     3.8745    -0.0000
  2   0     6.1249     6.0000    -0.1249     3.8516     3.8516     0.0000
  3   0     5.7553     6.0000     0.2447     3.7767     3.7767    -0.0000
  4   0     6.1538     6.0000    -0.1538     3.8107     3.8107    -0.0000
  5   0     6.1262     6.0000    -0.1262     3.7563     3.7563     0.0000
  6   0     0.7722     1.0000     0.2278     0.9837     0.9837    -0.0000
  7   0     5.3232     6.0000     0.6768     4.2723     4.2723    -0.0000
  8   0     6.5270     7.0000     0.4730     4.2703     4.2703     0.0000
  9   0     0.7800     1.0000     0.2200     0.9790     0.9790     0.0000
 10   0     0.8388     1.0000     0.1612     1.0046     1.0046     0.0000
 11   0     0.8085     1.0000     0.1915     0.9682     0.9682     0.0000
 12   0     7.4899     7.0000    -0.4899     3.1375     3.1375    -0.0000
 13   0     8.4192     8.0000    -0.4192     2.2107     2.2107    -0.0000
 14   0     8.5301     8.0000    -0.5301     1.9806     1.9806     0.0000
 15   0    14.9233    16.0000     1.0767     3.7727     3.7727     0.0000
 16   0     8.5375     8.0000    -0.5375     1.9727     1.9727    -0.0000
 17   0     8.3814     8.0000    -0.3814     1.8814     1.8814    -0.0000
 18   0     8.3840     8.0000    -0.3840     1.8802     1.8802     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.350562
                0             6               2            6                1.370775
                0             6               8            7                0.989695
                1             6               3            6                1.382059
                1             6               6            1                0.941245
                2             6               5            6                1.378439
                2             6               9            1                0.946227
                3             6               4            6                1.326358
                3             6              12            7                1.010552
                4             6               5            6                1.383581
                4             6              10            1                0.985625
                5             6              11            1                0.967645
                7             6              12            7                2.092381
                7             6              13            8                2.087822
                8             7              17            8                1.663810
                8             7              18            8                1.662921
               14             8              15           16                1.883457
               15            16              16            8                1.876895
               17             8              18            8                0.149446
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 2
   prop. index: 1
        Total number of Electrons                                                    116
        Number of correlated Electrons                                                78
        Number of alpha correlated Electrons                                          39
        Number of beta correlated Electrons                                           39
        Reference Energy:                                               -1148.3374137930
        Total Correlation Energy:                                          -3.1597378123
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1588305744
        Total MDCI Energy:                                              -1151.4971516053
# -----------------------------------------------------------
$ Energy_Extrapolation
   description: The Extrapolation of the energies
   geom. index: 2
   prop. index: 1
       Do EP1                            : 1 
       Do EP2                            : 0 
       Do EP3                            : 0 
       Number of energies                : 2 
       SCF energy with basis  cc-pVTZ :  -1148.2734972076 
       SCF energy with basis  cc-pVQZ :  -1148.3374137930 
       CBS SCF Energy                    :  -1148.3566721110 
       Correlation energy with basis  cc-pVTZ :  -1148.2734972076 
       Correlation energy with basis  cc-pVQZ :  -1148.3374137930 
       CBS Correlation Energy            :     -3.2933973510 
       CBS Total Energy                  :  -1151.6500694620 
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 2
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       19
     number of electrons:                   116
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             510
     number of aux C basis functions:       2226
     number of aux J basis functions:       0
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           0.000000
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 2
   prop. index: 1
       Filename                          : /scratch/obeletsan/orcajob__14539-wJGW/postint_phh.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        6.5698412279
        Electronic Contribution:
                  0    
      0       6.295390
      1       3.533823
      2      -2.588585
        Nuclear Contribution:
                  0    
      0      -4.963882
      1      -1.565261
      2       3.604772
        Total Dipole moment:
                  0    
      0       1.331509
      1       1.968562
      2       1.016186
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     1 
    Coordinates:
               0 C      0.935984000000   -1.358726000000    0.168584000000
               1 C      1.623788000000   -0.771928000000   -0.884004000000
               2 C      0.741185000000   -0.725003000000    1.388897000000
               3 C      2.137928000000    0.509049000000   -0.699091000000
               4 C      1.956417000000    1.175309000000    0.519752000000
               5 C      1.262321000000    0.554264000000    1.549656000000
               6 H      1.757316000000   -1.295153000000   -1.822630000000
               7 C      3.421259000000    2.124702000000   -1.955074000000
               8 N      0.389077000000   -2.715992000000   -0.024625000000
               9 H      0.197694000000   -1.222349000000    2.182516000000
              10 H      2.362527000000    2.173544000000    0.653362000000
              11 H      1.125593000000    1.075714000000    2.491681000000
              12 N      2.824801000000    1.093259000000   -1.762338000000
              13 O      4.018796000000    3.078897000000   -2.280057000000
              14 O      4.539701000000    3.928592000000    0.976394000000
              15 S      5.690278000000    3.099039000000    0.583751000000
              16 O      5.411711000000    1.691217000000    0.253885000000
              17 O      0.569830000000   -3.255781000000   -1.109512000000
              18 O     -0.214875000000   -3.224912000000    0.912103000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     2 
    Coordinates:
               0 C      0.935984000000   -1.358726000000    0.168584000000
               1 C      1.623788000000   -0.771928000000   -0.884004000000
               2 C      0.741185000000   -0.725003000000    1.388897000000
               3 C      2.137928000000    0.509049000000   -0.699091000000
               4 C      1.956417000000    1.175309000000    0.519752000000
               5 C      1.262321000000    0.554264000000    1.549656000000
               6 H      1.757316000000   -1.295153000000   -1.822630000000
               7 C      3.421259000000    2.124702000000   -1.955074000000
               8 N      0.389077000000   -2.715992000000   -0.024625000000
               9 H      0.197694000000   -1.222349000000    2.182516000000
              10 H      2.362527000000    2.173544000000    0.653362000000
              11 H      1.125593000000    1.075714000000    2.491681000000
              12 N      2.824801000000    1.093259000000   -1.762338000000
              13 O      4.018796000000    3.078897000000   -2.280057000000
              14 O      4.539701000000    3.928592000000    0.976394000000
              15 S      5.690278000000    3.099039000000    0.583751000000
              16 O      5.411711000000    1.691217000000    0.253885000000
              17 O      0.569830000000   -3.255781000000   -1.109512000000
              18 O     -0.214875000000   -3.224912000000    0.912103000000
