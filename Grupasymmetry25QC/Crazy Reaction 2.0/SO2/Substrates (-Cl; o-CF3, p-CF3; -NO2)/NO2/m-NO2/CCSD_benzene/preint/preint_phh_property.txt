-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:    -1148.1603073115
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 1
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1162
     Surface Area:         883.7455980232
     Dielectric Energy:     -0.0133132505
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 19
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 20
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     5.9266     6.0000     0.0734     3.7462     3.7462     0.0000
  1   0     6.1453     6.0000    -0.1453     3.8875     3.8875    -0.0000
  2   0     6.1297     6.0000    -0.1297     3.8639     3.8639     0.0000
  3   0     5.9444     6.0000     0.0556     3.7511     3.7511    -0.0000
  4   0     6.1140     6.0000    -0.1140     3.8121     3.8121    -0.0000
  5   0     6.1376     6.0000    -0.1376     3.7473     3.7473    -0.0000
  6   0     0.7852     1.0000     0.2148     0.9770     0.9770     0.0000
  7   0     5.9682     6.0000     0.0318     3.9354     3.9354    -0.0000
  8   0     6.5015     7.0000     0.4985     4.2326     4.2326     0.0000
  9   0     0.7867     1.0000     0.2133     0.9747     0.9747    -0.0000
 10   0     0.8051     1.0000     0.1949     0.9912     0.9912    -0.0000
 11   0     0.8111     1.0000     0.1889     0.9665     0.9665     0.0000
 12   0     6.6859     7.0000     0.3141     3.9933     3.9933     0.0000
 13   0     8.4891     8.0000    -0.4891     1.6843     1.6843     0.0000
 14   0     8.5121     8.0000    -0.5121     1.9992     1.9992     0.0000
 15   0    15.0320    16.0000     0.9680     3.8536     3.8536     0.0000
 16   0     8.4839     8.0000    -0.4839     2.0232     2.0232    -0.0000
 17   0     8.3706     8.0000    -0.3706     1.8871     1.8871    -0.0000
 18   0     8.3710     8.0000    -0.3710     1.8854     1.8854    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.383966
                0             6               2            6                1.360180
                0             6               8            7                0.945485
                1             6               3            6                1.313700
                1             6               6            1                0.946785
                2             6               5            6                1.357671
                2             6               9            1                0.943437
                3             6               4            6                1.317435
                3             6               7            6                1.081271
                4             6               5            6                1.376573
                4             6              10            1                0.951984
                5             6              11            1                0.965005
                7             6              12            7                2.587199
                7             6              13            8                0.241265
                8             7              17            8                1.645365
                8             7              18            8                1.643884
               12             7              13            8                1.341337
               14             8              15           16                1.869501
               15            16              16            8                1.915800
               17             8              18            8                0.157106
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 1
   prop. index: 1
        Total number of Electrons                                                    116
        Number of correlated Electrons                                                78
        Number of alpha correlated Electrons                                          39
        Number of beta correlated Electrons                                           39
        Reference Energy:                                               -1148.1603073308
        Total Correlation Energy:                                          -2.9898188956
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1478382653
        Total MDCI Energy:                                              -1151.1501262263
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:    -1148.2246569389
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 2
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1162
     Surface Area:         883.7455980232
     Dielectric Energy:     -0.0132929118
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 2
   prop. index: 1
     Number of atoms                     : 19
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 20
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     5.9975     6.0000     0.0025     3.7473     3.7473     0.0000
  1   0     6.1235     6.0000    -0.1235     3.8601     3.8601     0.0000
  2   0     6.1024     6.0000    -0.1024     3.8388     3.8388    -0.0000
  3   0     5.9636     6.0000     0.0364     3.7550     3.7550    -0.0000
  4   0     6.1435     6.0000    -0.1435     3.8220     3.8220     0.0000
  5   0     6.1064     6.0000    -0.1064     3.7321     3.7321    -0.0000
  6   0     0.7670     1.0000     0.2330     0.9725     0.9725     0.0000
  7   0     5.7983     6.0000     0.2017     3.8224     3.8224    -0.0000
  8   0     6.5268     7.0000     0.4732     4.2686     4.2686     0.0000
  9   0     0.7679     1.0000     0.2321     0.9718     0.9718     0.0000
 10   0     0.8009     1.0000     0.1991     0.9966     0.9966    -0.0000
 11   0     0.8059     1.0000     0.1941     0.9679     0.9679    -0.0000
 12   0     6.7982     7.0000     0.2018     3.9345     3.9345    -0.0000
 13   0     8.5069     8.0000    -0.5069     1.6902     1.6902    -0.0000
 14   0     8.5763     8.0000    -0.5763     1.9361     1.9361    -0.0000
 15   0    14.9216    16.0000     1.0784     3.7939     3.7939    -0.0000
 16   0     8.5392     8.0000    -0.5392     1.9575     1.9575     0.0000
 17   0     8.3769     8.0000    -0.3769     1.8845     1.8845     0.0000
 18   0     8.3771     8.0000    -0.3771     1.8858     1.8858    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.380033
                0             6               2            6                1.352501
                0             6               8            7                0.988042
                1             6               3            6                1.303222
                1             6               6            1                0.944366
                2             6               5            6                1.372184
                2             6               9            1                0.941273
                3             6               4            6                1.302535
                3             6               7            6                1.136546
                4             6               5            6                1.389686
                4             6              10            1                0.958903
                5             6              11            1                0.969175
                7             6              12            7                2.505002
                7             6              13            8                0.201053
                8             7              17            8                1.659579
                8             7              18            8                1.661786
               12             7              13            8                1.368269
               14             8              15           16                1.841910
               15            16              16            8                1.878527
               17             8              18            8                0.152386
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 2
   prop. index: 1
        Total number of Electrons                                                    116
        Number of correlated Electrons                                                78
        Number of alpha correlated Electrons                                          39
        Number of beta correlated Electrons                                           39
        Reference Energy:                                               -1148.2246569547
        Total Correlation Energy:                                          -3.1781274314
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1617998447
        Total MDCI Energy:                                              -1151.4027843861
# -----------------------------------------------------------
$ Energy_Extrapolation
   description: The Extrapolation of the energies
   geom. index: 2
   prop. index: 1
       Do EP1                            : 1 
       Do EP2                            : 0 
       Do EP3                            : 0 
       Number of energies                : 2 
       SCF energy with basis  cc-pVTZ :  -1148.1603073308 
       SCF energy with basis  cc-pVQZ :  -1148.2246569547 
       CBS SCF Energy                    :  -1148.2440457489 
       Correlation energy with basis  cc-pVTZ :  -1148.1603073308 
       Correlation energy with basis  cc-pVQZ :  -1148.2246569547 
       CBS Correlation Energy            :     -3.3121822977 
       CBS Total Energy                  :  -1151.5562280466 
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 2
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       19
     number of electrons:                   116
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             510
     number of aux C basis functions:       2226
     number of aux J basis functions:       0
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           0.000000
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 2
   prop. index: 1
       Filename                          : /scratch/obeletsan/orcajob__14540-fMOQ/preint_phh.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        4.4267402803
        Electronic Contribution:
                  0    
      0       4.225521
      1       5.810724
      2      -0.104089
        Nuclear Contribution:
                  0    
      0      -3.853176
      1      -6.972576
      2       1.346891
        Total Dipole moment:
                  0    
      0       0.372345
      1      -1.161851
      2       1.242801
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     1 
    Coordinates:
               0 C      0.010075000000   -0.007275000000   -0.125294000000
               1 C      0.735279000000    1.157930000000   -0.310995000000
               2 C      0.556140000000   -1.151156000000    0.444281000000
               3 C      2.074836000000    1.169968000000    0.096490000000
               4 C      2.651515000000    0.025338000000    0.676362000000
               5 C      1.887860000000   -1.121003000000    0.843907000000
               6 H      0.275800000000    2.030411000000   -0.758579000000
               7 C      2.860567000000    2.335550000000   -0.068255000000
               8 N     -1.405165000000   -0.028453000000   -0.552444000000
               9 H     -0.052391000000   -2.038738000000    0.568812000000
              10 H      3.690263000000    0.054768000000    0.988172000000
              11 H      2.334737000000   -2.002503000000    1.291797000000
              12 N      3.548634000000    3.262538000000   -0.181919000000
              13 O      4.302604000000    4.208242000000   -0.273883000000
              14 O      5.547202000000    2.021079000000    1.515986000000
              15 S      6.447272000000    3.054091000000    0.959444000000
              16 O      7.221070000000    2.669382000000   -0.233363000000
              17 O     -1.866318000000    0.994500000000   -1.041786000000
              18 O     -2.029971000000   -1.068309000000   -0.389914000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     2 
    Coordinates:
               0 C      0.010075000000   -0.007275000000   -0.125294000000
               1 C      0.735279000000    1.157930000000   -0.310995000000
               2 C      0.556140000000   -1.151156000000    0.444281000000
               3 C      2.074836000000    1.169968000000    0.096490000000
               4 C      2.651515000000    0.025338000000    0.676362000000
               5 C      1.887860000000   -1.121003000000    0.843907000000
               6 H      0.275800000000    2.030411000000   -0.758579000000
               7 C      2.860567000000    2.335550000000   -0.068255000000
               8 N     -1.405165000000   -0.028453000000   -0.552444000000
               9 H     -0.052391000000   -2.038738000000    0.568812000000
              10 H      3.690263000000    0.054768000000    0.988172000000
              11 H      2.334737000000   -2.002503000000    1.291797000000
              12 N      3.548634000000    3.262538000000   -0.181919000000
              13 O      4.302604000000    4.208242000000   -0.273883000000
              14 O      5.547202000000    2.021079000000    1.515986000000
              15 S      6.447272000000    3.054091000000    0.959444000000
              16 O      7.221070000000    2.669382000000   -0.233363000000
              17 O     -1.866318000000    0.994500000000   -1.041786000000
              18 O     -2.029971000000   -1.068309000000   -0.389914000000
