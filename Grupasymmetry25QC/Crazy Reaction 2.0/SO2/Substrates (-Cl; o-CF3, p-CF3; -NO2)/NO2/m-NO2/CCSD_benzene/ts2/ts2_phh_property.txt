-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:    -1148.1394249935
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 1
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1089
     Surface Area:         842.7719064693
     Dielectric Energy:     -0.0138542552
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 19
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 22
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     5.9333     6.0000     0.0667     3.7401     3.7401    -0.0000
  1   0     6.1088     6.0000    -0.1088     3.8918     3.8918     0.0000
  2   0     6.1100     6.0000    -0.1100     3.8597     3.8597     0.0000
  3   0     5.9919     6.0000     0.0081     3.7395     3.7395    -0.0000
  4   0     6.0880     6.0000    -0.0880     3.8352     3.8352    -0.0000
  5   0     6.1425     6.0000    -0.1425     3.7457     3.7457    -0.0000
  6   0     0.7806     1.0000     0.2194     0.9755     0.9755     0.0000
  7   0     5.6472     6.0000     0.3528     4.2801     4.2801    -0.0000
  8   0     6.5020     7.0000     0.4980     4.2370     4.2370     0.0000
  9   0     0.7854     1.0000     0.2146     0.9744     0.9744     0.0000
 10   0     0.8038     1.0000     0.1962     0.9710     0.9710    -0.0000
 11   0     0.8104     1.0000     0.1896     0.9665     0.9665     0.0000
 12   0     7.0001     7.0000    -0.0001     2.2018     2.2018    -0.0000
 13   0     8.5537     8.0000    -0.5537     1.9251     1.9251    -0.0000
 14   0     8.4868     8.0000    -0.4868     2.0173     2.0173    -0.0000
 15   0    15.0179    16.0000     0.9821     3.8492     3.8492     0.0000
 16   0     8.5008     8.0000    -0.5008     2.0088     2.0088    -0.0000
 17   0     8.3672     8.0000    -0.3672     1.8916     1.8916    -0.0000
 18   0     8.3698     8.0000    -0.3698     1.8870     1.8870     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.379828
                0             6               2            6                1.355620
                0             6               8            7                0.945595
                1             6               3            6                1.315787
                1             6               6            1                0.937324
                2             6               5            6                1.353693
                2             6               9            1                0.942211
                3             6               4            6                1.307455
                3             6               7            6                0.881499
                3             6              12            7                0.226464
                4             6               5            6                1.379929
                4             6              10            1                0.956320
                5             6              11            1                0.963805
                7             6              12            7                1.662734
                7             6              14            8                1.676475
                8             7              17            8                1.649907
                8             7              18            8                1.645256
               12             7              13            8                0.159537
               13             8              15           16                1.655653
               14             8              15           16                0.254260
               15            16              16            8                1.907550
               17             8              18            8                0.157824
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 1
   prop. index: 1
        Total number of Electrons                                                    116
        Number of correlated Electrons                                                78
        Number of alpha correlated Electrons                                          39
        Number of beta correlated Electrons                                           39
        Reference Energy:                                               -1148.1394250714
        Total Correlation Energy:                                          -2.9887161858
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1517686015
        Total MDCI Energy:                                              -1151.1281412572
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:    -1148.2031236797
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 2
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1089
     Surface Area:         842.7719064693
     Dielectric Energy:     -0.0137484332
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 2
   prop. index: 1
     Number of atoms                     : 19
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 22
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     5.9936     6.0000     0.0064     3.7271     3.7271     0.0000
  1   0     6.0638     6.0000    -0.0638     3.8762     3.8762     0.0000
  2   0     6.0829     6.0000    -0.0829     3.8266     3.8266     0.0000
  3   0     6.0082     6.0000    -0.0082     3.6539     3.6539     0.0000
  4   0     6.0747     6.0000    -0.0747     3.8428     3.8428     0.0000
  5   0     6.1085     6.0000    -0.1085     3.7294     3.7294     0.0000
  6   0     0.7713     1.0000     0.2287     0.9736     0.9736     0.0000
  7   0     5.5603     6.0000     0.4397     4.2799     4.2799     0.0000
  8   0     6.5294     7.0000     0.4706     4.2706     4.2706     0.0000
  9   0     0.7665     1.0000     0.2335     0.9710     0.9710     0.0000
 10   0     0.8094     1.0000     0.1906     0.9791     0.9791    -0.0000
 11   0     0.8075     1.0000     0.1925     0.9685     0.9685    -0.0000
 12   0     7.0651     7.0000    -0.0651     2.2561     2.2561     0.0000
 13   0     8.5923     8.0000    -0.5923     1.8743     1.8743     0.0000
 14   0     8.5139     8.0000    -0.5139     1.9843     1.9843    -0.0000
 15   0    14.9421    16.0000     1.0579     3.8299     3.8299    -0.0000
 16   0     8.5613     8.0000    -0.5613     1.9398     1.9398    -0.0000
 17   0     8.3735     8.0000    -0.3735     1.8897     1.8897    -0.0000
 18   0     8.3757     8.0000    -0.3757     1.8865     1.8865    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.368277
                0             6               2            6                1.342263
                0             6               8            7                0.982838
                1             6               3            6                1.294075
                1             6               6            1                0.946853
                2             6               5            6                1.370974
                2             6               9            1                0.940554
                3             6               4            6                1.277177
                3             6               7            6                0.910459
                3             6              12            7                0.214216
                4             6               5            6                1.385637
                4             6              10            1                0.973356
                5             6              11            1                0.969990
                7             6              12            7                1.718229
                7             6              14            8                1.627463
                8             7              17            8                1.664907
                8             7              18            8                1.662295
               12             7              13            8                0.151018
               13             8              15           16                1.645454
               14             8              15           16                0.276102
               15            16              16            8                1.871101
               17             8              18            8                0.153154
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 2
   prop. index: 1
        Total number of Electrons                                                    116
        Number of correlated Electrons                                                78
        Number of alpha correlated Electrons                                          39
        Number of beta correlated Electrons                                           39
        Reference Energy:                                               -1148.2031237940
        Total Correlation Energy:                                          -3.1773919991
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1658203797
        Total MDCI Energy:                                              -1151.3805157931
# -----------------------------------------------------------
$ Energy_Extrapolation
   description: The Extrapolation of the energies
   geom. index: 2
   prop. index: 1
       Do EP1                            : 1 
       Do EP2                            : 0 
       Do EP3                            : 0 
       Number of energies                : 2 
       SCF energy with basis  cc-pVTZ :  -1148.1394250714 
       SCF energy with basis  cc-pVQZ :  -1148.2031237940 
       CBS SCF Energy                    :  -1148.2223164690 
       Correlation energy with basis  cc-pVTZ :  -1148.1394250714 
       Correlation energy with basis  cc-pVQZ :  -1148.2031237940 
       CBS Correlation Energy            :     -3.3117083264 
       CBS Total Energy                  :  -1151.5340247954 
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 2
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       19
     number of electrons:                   116
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             510
     number of aux C basis functions:       2226
     number of aux J basis functions:       0
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           0.000000
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 2
   prop. index: 1
       Filename                          : /scratch/obeletsan/orcajob__14544-Lo6J/ts2_phh.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        3.7852569312
        Electronic Contribution:
                  0    
      0       3.255646
      1       6.561873
      2      -1.126191
        Nuclear Contribution:
                  0    
      0      -3.014558
      1      -7.868097
      2       1.799529
        Total Dipole moment:
                  0    
      0       0.241087
      1      -1.306224
      2       0.673338
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     1 
    Coordinates:
               0 C      0.051898000000    0.072447000000    0.091519000000
               1 C      0.967171000000    1.066572000000   -0.211691000000
               2 C      0.439020000000   -1.201537000000    0.494659000000
               3 C      2.315108000000    0.750298000000   -0.062769000000
               4 C      2.746088000000   -0.527174000000    0.305783000000
               5 C      1.795852000000   -1.495626000000    0.598896000000
               6 H      0.646275000000    2.047542000000   -0.542341000000
               7 C      3.363902000000    1.888769000000   -0.145975000000
               8 N     -1.389984000000    0.376941000000   -0.037768000000
               9 H     -0.315416000000   -1.949156000000    0.710570000000
              10 H      3.808181000000   -0.741806000000    0.369577000000
              11 H      2.110222000000   -2.489135000000    0.900614000000
              12 N      3.506408000000    1.711839000000   -1.384807000000
              13 O      4.811999000000    3.268005000000   -1.587521000000
              14 O      3.792018000000    2.577760000000    0.807055000000
              15 S      5.492315000000    3.559574000000   -0.263839000000
              16 O      6.570507000000    2.627376000000    0.098517000000
              17 O     -1.698532000000    1.506073000000   -0.391608000000
              18 O     -2.180261000000   -0.520413000000    0.220298000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     2 
    Coordinates:
               0 C      0.051898000000    0.072447000000    0.091519000000
               1 C      0.967171000000    1.066572000000   -0.211691000000
               2 C      0.439020000000   -1.201537000000    0.494659000000
               3 C      2.315108000000    0.750298000000   -0.062769000000
               4 C      2.746088000000   -0.527174000000    0.305783000000
               5 C      1.795852000000   -1.495626000000    0.598896000000
               6 H      0.646275000000    2.047542000000   -0.542341000000
               7 C      3.363902000000    1.888769000000   -0.145975000000
               8 N     -1.389984000000    0.376941000000   -0.037768000000
               9 H     -0.315416000000   -1.949156000000    0.710570000000
              10 H      3.808181000000   -0.741806000000    0.369577000000
              11 H      2.110222000000   -2.489135000000    0.900614000000
              12 N      3.506408000000    1.711839000000   -1.384807000000
              13 O      4.811999000000    3.268005000000   -1.587521000000
              14 O      3.792018000000    2.577760000000    0.807055000000
              15 S      5.492315000000    3.559574000000   -0.263839000000
              16 O      6.570507000000    2.627376000000    0.098517000000
              17 O     -1.698532000000    1.506073000000   -0.391608000000
              18 O     -2.180261000000   -0.520413000000    0.220298000000
