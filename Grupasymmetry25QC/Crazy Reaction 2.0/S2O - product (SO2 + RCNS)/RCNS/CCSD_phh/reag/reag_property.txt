-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -720.0439372043
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 1
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                851
     Surface Area:         633.0364942412
     Dielectric Energy:     -0.0113583937
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 14
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 15
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.1457     6.0000    -0.1457     3.7756     3.7756    -0.0000
  1   0     6.1448     6.0000    -0.1448     3.8327     3.8327    -0.0000
  2   0     6.1373     6.0000    -0.1373     3.8090     3.8090    -0.0000
  3   0     5.9322     6.0000     0.0678     3.7198     3.7198    -0.0000
  4   0     6.1449     6.0000    -0.1449     3.8328     3.8328     0.0000
  5   0     6.1456     6.0000    -0.1456     3.7758     3.7758     0.0000
  6   0     0.8158     1.0000     0.1842     0.9725     0.9725    -0.0000
  7   0     5.8743     6.0000     0.1257     3.8405     3.8405    -0.0000
  8   0     0.8199     1.0000     0.1801     0.9686     0.9686    -0.0000
  9   0     0.8174     1.0000     0.1826     0.9691     0.9691    -0.0000
 10   0     0.8160     1.0000     0.1840     0.9726     0.9726     0.0000
 11   0     0.8200     1.0000     0.1800     0.9687     0.9687     0.0000
 12   0     6.9920     7.0000     0.0080     3.5803     3.5803     0.0000
 13   0    16.3939    16.0000    -0.3939     1.2341     1.2341    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.394182
                0             6               2            6                1.358010
                0             6               8            1                0.968488
                1             6               3            6                1.303303
                1             6               6            1                0.967905
                2             6               5            6                1.358115
                2             6               9            1                0.968642
                3             6               4            6                1.303268
                3             6               7            6                1.080040
                4             6               5            6                1.394246
                4             6              10            1                0.967955
                5             6              11            1                0.968524
                7             6              12            7                2.530351
                7             6              13           16                0.215446
               12             7              13           16                0.976765
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 1
   prop. index: 1
        Total number of Electrons                                                     70
        Number of correlated Electrons                                                44
        Number of alpha correlated Electrons                                          22
        Number of beta correlated Electrons                                           22
        Reference Energy:                                                -720.0439372101
        Total Correlation Energy:                                          -1.5543678852
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.0795160834
        Total MDCI Energy:                                               -721.5983050953
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:     -720.0681936742
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 2
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                851
     Surface Area:         633.0364942412
     Dielectric Energy:     -0.0114250129
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 2
   prop. index: 1
     Number of atoms                     : 14
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 15
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.1205     6.0000    -0.1205     3.7758     3.7758    -0.0000
  1   0     6.1678     6.0000    -0.1678     3.8559     3.8559    -0.0000
  2   0     6.1488     6.0000    -0.1488     3.8368     3.8368     0.0000
  3   0     5.9192     6.0000     0.0808     3.6649     3.6649     0.0000
  4   0     6.1679     6.0000    -0.1679     3.8557     3.8557     0.0000
  5   0     6.1200     6.0000    -0.1200     3.7755     3.7755    -0.0000
  6   0     0.8134     1.0000     0.1866     0.9784     0.9784    -0.0000
  7   0     5.6989     6.0000     0.3011     3.8536     3.8536     0.0000
  8   0     0.8190     1.0000     0.1810     0.9747     0.9747    -0.0000
  9   0     0.8086     1.0000     0.1914     0.9719     0.9719     0.0000
 10   0     0.8139     1.0000     0.1861     0.9787     0.9787    -0.0000
 11   0     0.8193     1.0000     0.1807     0.9748     0.9748    -0.0000
 12   0     7.2671     7.0000    -0.2671     3.4114     3.4114     0.0000
 13   0    16.3156    16.0000    -0.3156     1.1080     1.1080    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.419281
                0             6               2            6                1.375244
                0             6               8            1                0.975740
                1             6               3            6                1.283744
                1             6               6            1                0.976881
                2             6               5            6                1.375255
                2             6               9            1                0.972784
                3             6               4            6                1.283642
                3             6               7            6                1.127966
                4             6               5            6                1.419122
                4             6              10            1                0.977138
                5             6              11            1                0.975897
                7             6              12            7                2.514500
                7             6              13           16                0.237649
               12             7              13           16                0.828592
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 2
   prop. index: 1
        Total number of Electrons                                                     70
        Number of correlated Electrons                                                44
        Number of alpha correlated Electrons                                          22
        Number of beta correlated Electrons                                           22
        Reference Energy:                                                -720.0681938737
        Total Correlation Energy:                                          -1.6399188600
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.0864067882
        Total MDCI Energy:                                               -721.7081127337
# -----------------------------------------------------------
$ Energy_Extrapolation
   description: The Extrapolation of the energies
   geom. index: 2
   prop. index: 1
       Do EP1                            : 1 
       Do EP2                            : 0 
       Do EP3                            : 0 
       Number of energies                : 2 
       SCF energy with basis  cc-pVTZ :   -720.0439372101 
       SCF energy with basis  cc-pVQZ :   -720.0681938737 
       CBS SCF Energy                    :   -720.0755025016 
       Correlation energy with basis  cc-pVTZ :   -720.0439372101 
       Correlation energy with basis  cc-pVQZ :   -720.0681938737 
       CBS Correlation Energy            :     -1.7008216993 
       CBS Total Energy                  :   -721.7763242009 
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 2
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       14
     number of electrons:                   70
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             344
     number of aux C basis functions:       1489
     number of aux J basis functions:       0
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           0.000000
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 2
   prop. index: 1
       Filename                          : /scratch/obeletsan/orcajob__14520-iOlG/reag.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        9.5764796027
        Electronic Contribution:
                  0    
      0       4.356574
      1       6.504755
      2      -0.766042
        Nuclear Contribution:
                  0    
      0      -6.440964
      1      -9.621527
      2       1.134640
        Total Dipole moment:
                  0    
      0      -2.084390
      1      -3.116772
      2       0.368598
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    14 
    Geometry Index:     1 
    Coordinates:
               0 C     -2.431731000000   -0.887464000000   -0.516877000000
               1 C     -1.672112000000    0.263819000000   -0.656822000000
               2 C     -1.904196000000   -2.007463000000    0.124713000000
               3 C     -0.360189000000    0.296146000000   -0.145779000000
               4 C      0.171276000000   -0.835706000000    0.502038000000
               5 C     -0.605426000000   -1.976802000000    0.631215000000
               6 H     -2.074773000000    1.139810000000   -1.154751000000
               7 C      0.419720000000    1.459519000000   -0.282684000000
               8 H     -3.443107000000   -0.911353000000   -0.911330000000
               9 H     -2.505919000000   -2.905167000000    0.230067000000
              10 H      1.182847000000   -0.803269000000    0.893346000000
              11 H     -0.194645000000   -2.848943000000    1.130876000000
              12 N      1.064398000000    2.426776000000   -0.398051000000
              13 S      1.957242000000    3.762113000000   -0.556022000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    14 
    Geometry Index:     2 
    Coordinates:
               0 C     -2.431731000000   -0.887464000000   -0.516877000000
               1 C     -1.672112000000    0.263819000000   -0.656822000000
               2 C     -1.904196000000   -2.007463000000    0.124713000000
               3 C     -0.360189000000    0.296146000000   -0.145779000000
               4 C      0.171276000000   -0.835706000000    0.502038000000
               5 C     -0.605426000000   -1.976802000000    0.631215000000
               6 H     -2.074773000000    1.139810000000   -1.154751000000
               7 C      0.419720000000    1.459519000000   -0.282684000000
               8 H     -3.443107000000   -0.911353000000   -0.911330000000
               9 H     -2.505919000000   -2.905167000000    0.230067000000
              10 H      1.182847000000   -0.803269000000    0.893346000000
              11 H     -0.194645000000   -2.848943000000    1.130876000000
              12 N      1.064398000000    2.426776000000   -0.398051000000
              13 S      1.957242000000    3.762113000000   -0.556022000000
