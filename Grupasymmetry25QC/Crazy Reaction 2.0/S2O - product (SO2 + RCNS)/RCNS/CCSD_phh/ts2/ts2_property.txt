-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:    -1267.2452338448
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 1
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                993
     Surface Area:         781.7990948351
     Dielectric Energy:     -0.0096798220
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 17
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 20
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.1491     6.0000    -0.1491     3.7637     3.7637     0.0000
  1   0     6.1281     6.0000    -0.1281     3.8412     3.8412     0.0000
  2   0     6.1221     6.0000    -0.1221     3.8025     3.8025    -0.0000
  3   0     5.9600     6.0000     0.0400     3.7482     3.7482    -0.0000
  4   0     6.1283     6.0000    -0.1283     3.8587     3.8587    -0.0000
  5   0     6.1508     6.0000    -0.1508     3.7685     3.7685    -0.0000
  6   0     0.8116     1.0000     0.1884     0.9735     0.9735     0.0000
  7   0     5.6031     6.0000     0.3969     4.2810     4.2810     0.0000
  8   0     0.8209     1.0000     0.1791     0.9689     0.9689     0.0000
  9   0     0.8184     1.0000     0.1816     0.9693     0.9693    -0.0000
 10   0     0.8141     1.0000     0.1859     0.9738     0.9738    -0.0000
 11   0     0.8214     1.0000     0.1786     0.9690     0.9690     0.0000
 12   0     7.1779     7.0000    -0.1779     2.4384     2.4384    -0.0000
 13   0    16.2412    16.0000    -0.2412     2.0195     2.0195    -0.0000
 14   0     8.4814     8.0000    -0.4814     2.0499     2.0499     0.0000
 15   0    15.2764    16.0000     0.7236     3.6137     3.6137     0.0000
 16   0     8.4953     8.0000    -0.4953     1.9665     1.9665     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.386204
                0             6               2            6                1.353717
                0             6               8            1                0.968276
                1             6               3            6                1.300385
                1             6               6            1                0.958787
                2             6               5            6                1.351433
                2             6               9            1                0.969283
                3             6               4            6                1.306469
                3             6               7            6                0.757789
                3             6              12            7                0.333973
                4             6               5            6                1.393031
                4             6              10            1                0.958857
                5             6              11            1                0.968304
                7             6              12            7                1.695610
                7             6              14            8                1.816074
               12             7              13           16                0.206197
               13            16              15           16                1.654578
               13            16              16            8                0.139140
               14             8              15           16                0.127367
               15            16              16            8                1.790073
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 1
   prop. index: 1
        Total number of Electrons                                                    102
        Number of correlated Electrons                                                62
        Number of alpha correlated Electrons                                          31
        Number of beta correlated Electrons                                           31
        Reference Energy:                                               -1267.2452338888
        Total Correlation Energy:                                          -2.2702355137
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1212071292
        Total MDCI Energy:                                              -1269.5154694025
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:    -1267.2893857106
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 2
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                993
     Surface Area:         781.7990948351
     Dielectric Energy:     -0.0096068964
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 2
   prop. index: 1
     Number of atoms                     : 17
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 20
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.1217     6.0000    -0.1217     3.7607     3.7607    -0.0000
  1   0     6.0941     6.0000    -0.0941     3.8403     3.8403    -0.0000
  2   0     6.1333     6.0000    -0.1333     3.8323     3.8323     0.0000
  3   0     6.0051     6.0000    -0.0051     3.6267     3.6267     0.0000
  4   0     6.0963     6.0000    -0.0963     3.8542     3.8542     0.0000
  5   0     6.1234     6.0000    -0.1234     3.7654     3.7654     0.0000
  6   0     0.8193     1.0000     0.1807     0.9816     0.9816     0.0000
  7   0     5.5233     6.0000     0.4767     4.3065     4.3065     0.0000
  8   0     0.8217     1.0000     0.1783     0.9745     0.9745     0.0000
  9   0     0.8098     1.0000     0.1902     0.9714     0.9714     0.0000
 10   0     0.8217     1.0000     0.1783     0.9815     0.9815    -0.0000
 11   0     0.8229     1.0000     0.1771     0.9747     0.9747     0.0000
 12   0     7.2438     7.0000    -0.2438     2.5212     2.5212     0.0000
 13   0    16.2360    16.0000    -0.2360     2.0437     2.0437     0.0000
 14   0     8.5284     8.0000    -0.5284     2.0136     2.0136     0.0000
 15   0    15.2407    16.0000     0.7593     3.6410     3.6410    -0.0000
 16   0     8.5586     8.0000    -0.5586     1.9061     1.9061     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.398591
                0             6               2            6                1.372959
                0             6               8            1                0.976312
                1             6               3            6                1.263404
                1             6               6            1                0.977320
                2             6               5            6                1.371189
                2             6               9            1                0.973447
                3             6               4            6                1.269032
                3             6               7            6                0.784364
                3             6              12            7                0.322373
                4             6               5            6                1.404565
                4             6              10            1                0.977714
                5             6              11            1                0.976546
                7             6              12            7                1.781062
                7             6              14            8                1.768081
               12             7              13           16                0.214111
               13            16              15           16                1.695106
               13            16              16            8                0.118734
               14             8              15           16                0.146624
               15            16              16            8                1.755635
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 2
   prop. index: 1
        Total number of Electrons                                                    102
        Number of correlated Electrons                                                62
        Number of alpha correlated Electrons                                          31
        Number of beta correlated Electrons                                           31
        Reference Energy:                                               -1267.2893857425
        Total Correlation Energy:                                          -2.4082396466
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1324241711
        Total MDCI Energy:                                              -1269.6976253891
# -----------------------------------------------------------
$ Energy_Extrapolation
   description: The Extrapolation of the energies
   geom. index: 2
   prop. index: 1
       Do EP1                            : 1 
       Do EP2                            : 0 
       Do EP3                            : 0 
       Number of energies                : 2 
       SCF energy with basis  cc-pVTZ :  -1267.2452338888 
       SCF energy with basis  cc-pVQZ :  -1267.2893857425 
       CBS SCF Energy                    :  -1267.3026888693 
       Correlation energy with basis  cc-pVTZ :  -1267.2452338888 
       Correlation energy with basis  cc-pVQZ :  -1267.2893857425 
       CBS Correlation Energy            :     -2.5064833375 
       CBS Total Energy                  :  -1269.8091722068 
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 2
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       17
     number of electrons:                   102
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             438
     number of aux C basis functions:       1911
     number of aux J basis functions:       0
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           0.000000
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 2
   prop. index: 1
       Filename                          : /scratch/obeletsan/orcajob__14530-hYJW/ts2.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        6.0528169472
        Electronic Contribution:
                  0    
      0      10.954207
      1       8.478695
      2      -0.570966
        Nuclear Contribution:
                  0    
      0     -12.411481
      1     -10.148225
      2       1.442557
        Total Dipole moment:
                  0    
      0      -1.457274
      1      -1.669531
      2       0.871591
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     1 
    Coordinates:
               0 C      0.282599000000   -0.100007000000   -0.576055000000
               1 C      1.335374000000    0.803220000000   -0.613045000000
               2 C      0.287656000000   -1.142428000000    0.350531000000
               3 C      2.400294000000    0.621912000000    0.270397000000
               4 C      2.405403000000   -0.394569000000    1.226429000000
               5 C      1.345110000000   -1.289214000000    1.248292000000
               6 H      1.350041000000    1.632056000000   -1.313287000000
               7 C      3.777376000000    1.381297000000   -0.039086000000
               8 H     -0.549917000000    0.015435000000   -1.263286000000
               9 H     -0.545810000000   -1.837793000000    0.382736000000
              10 H      3.232098000000   -0.476472000000    1.924540000000
              11 H      1.335661000000   -2.095307000000    1.975499000000
              12 N      3.303139000000    2.224232000000    0.768366000000
              13 S      5.166645000000    3.602792000000    1.034518000000
              14 O      4.693232000000    1.027105000000   -0.774300000000
              15 S      6.057474000000    3.098499000000   -0.650019000000
              16 O      5.499223000000    3.723774000000   -1.879526000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     2 
    Coordinates:
               0 C      0.282599000000   -0.100007000000   -0.576055000000
               1 C      1.335374000000    0.803220000000   -0.613045000000
               2 C      0.287656000000   -1.142428000000    0.350531000000
               3 C      2.400294000000    0.621912000000    0.270397000000
               4 C      2.405403000000   -0.394569000000    1.226429000000
               5 C      1.345110000000   -1.289214000000    1.248292000000
               6 H      1.350041000000    1.632056000000   -1.313287000000
               7 C      3.777376000000    1.381297000000   -0.039086000000
               8 H     -0.549917000000    0.015435000000   -1.263286000000
               9 H     -0.545810000000   -1.837793000000    0.382736000000
              10 H      3.232098000000   -0.476472000000    1.924540000000
              11 H      1.335661000000   -2.095307000000    1.975499000000
              12 N      3.303139000000    2.224232000000    0.768366000000
              13 S      5.166645000000    3.602792000000    1.034518000000
              14 O      4.693232000000    1.027105000000   -0.774300000000
              15 S      6.057474000000    3.098499000000   -0.650019000000
              16 O      5.499223000000    3.723774000000   -1.879526000000
