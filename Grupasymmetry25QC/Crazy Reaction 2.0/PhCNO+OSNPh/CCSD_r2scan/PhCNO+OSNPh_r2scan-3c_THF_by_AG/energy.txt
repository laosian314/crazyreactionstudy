                            a.u.                       D            kcal/mol
Reagents_a.xyz:      Energy -1159.265120316061  Dipole 9.20113  G(298) 99.80
Reagents_b.xyz:      Energy -1159.265041507023  Dipole 8.72914  G(298) 98.66
TS1_a.xyz:           Energy -1159.243042765094  Dipole 8.43084  G(298) 100.13
TS1_b.xyz:           Energy -1159.240080177230  Dipole 6.39756  G(298) 99.43
Intermediate_a.xyz:  Energy -1159.299926475702  Dipole 7.58951  G(298) 103.01
Intermediate_b.xyz:  Energy -1159.277510536907  Dipole 3.88385  G(298) 101.72
TS2_a.xyz:           Energy -1159.259105762817  Dipole 12.12462  G(298) 99.97
TS2_b.xyz:           Energy -1159.248994764423  Dipole 5.70004  G(298) 99.14
Products_a.xyz:      Energy -1159.336496363937  Dipole 3.15988  G(298) 98.32
Products_b.xyz:      Energy -1159.354751127218  Dipole 4.76788  G(298) 99.20
