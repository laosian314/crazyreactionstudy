## Short description from TM

1) Геометрии и гессианы на уровне r2SCAN-3c и энергии DLPNO/CCSD(T)/CBS(cc-pVTZ,cc-pVQZ) в этих геометриях в газе и CPCM(Benzene).
2) Геометрии и гессианы на уровне wB97X-D3BJ/def2-TZVPPD и энергии DLPNO/CCSD(T)/CBS(cc-pVTZ,cc-pVQZ) в этих геометриях в газе и CPCM(Benzene), CPCM(Toluene).
3) Сделан скан геометрии (вдоль вращения связи CC) интермедиата на уровне wB97X-D3BJ/def2-TZVPPD в газе и CPCM(Benzene).
4) Геометрии и гессианы на уровне MP2/(aug)-cc-pVTZ + энергии DLPNO/CCSD(T)/CBS(cc-pVTZ,cc-pVQZ) в этих геометриях в газе.
5) Энергии на на уровне DLPNO/CCSD(T)/CBS(cc-pVTZ,cc-pVQZ) в геометриях r2SCAN-3c (Pe4kaQC) в газе и CPCM(Benzene) для SeO2.

Актуальные результаты находяться по ссылке:
https://disk.yandex.ru/d/xOZP9SnIOKM_lQ
