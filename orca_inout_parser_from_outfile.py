def parse_input_tags(outfile):
    import re

    input_tags = []

    with open(outfile, "r") as of:
        for line in of.readlines():
            if re.search("> !", line):
                tags_on_the_line = line[6:].strip("!").strip().split(" ")
                for tag in tags_on_the_line:
                    input_tags.append(tag.strip().casefold())

    return input_tags


def parse_output_energies(outfile, input_tags=None):

    if input_tags is None:
        input_tags = parse_input_tags(outfile)

    import re

    global electronic_energy, zpe, total_thermal_energy, total_enthalpy, entropy_term, final_gibbs_free_energy, G_minus_electronic_energy

    electronic_energy = None
    zpe = None
    total_thermal_energy = None
    total_enthalpy = None
    entropy_term = None
    final_gibbs_free_energy = None
    G_minus_electronic_energy = None

    with open(outfile, "r") as of:
        for line in of.readlines():

            if "freq" in input_tags or "numfreq" in input_tags or "anfreq" in input_tags:

                if re.search("Electronic energy             ", line):
                    electronic_energy = line.split()[-2]

                if re.search("Zero point energy", line):
                    zpe = line.split()[-4]

                if re.search("Total thermal energy", line):
                    total_thermal_energy = line.split()[-2]

                if re.search("Total Enthalpy", line):
                    total_enthalpy = line.split()[-2]

                if re.search("Final entropy term", line):
                    entropy_term = line.split()[-4]

                if re.search("Final Gibbs free energy", line):
                    final_gibbs_free_energy = line.split()[-2]

                if re.search("G-E", line):
                    G_minus_electronic_energy = line.split()[-4]

            else:
                if re.search("FINAL SINGLE POINT ENERGY", line):
                    electronic_energy = line.split()[-1]

    output = {
        "Calculation setup": input_tags,
        "Electronic energy": float(electronic_energy) if electronic_energy is not None else 0.0,
        "Zero point energy": float(zpe) if zpe is not None else 0.0,
        "Total thermal energy": float(total_thermal_energy) if total_thermal_energy is not None else 0.0,
        "Total Enthalpy": float(total_enthalpy) if total_enthalpy is not None else 0.0,
        "Entropy term": float(entropy_term) if entropy_term is not None else 0.0,
        "Gibbs free energy": float(final_gibbs_free_energy) if final_gibbs_free_energy is not None else 0.0,
        "G-E(el)": float(G_minus_electronic_energy) if G_minus_electronic_energy is not None else 0.0,
    }

    return output
